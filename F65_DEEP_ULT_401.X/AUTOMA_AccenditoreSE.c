//#include <stdint.h>
#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_AccenditoreSE.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "MODULO_Norma.h"
#include "AUTOMA_DriverOptiCom.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/clc1.h"
#include "mcc_generated_files/mcc.h"
#include "AUTOMA_AccenditoreSA.h"


tFlagAccenditoreSE uFlagAccenditoreSE;
uint8_t cStatoSE;
uint16_t iDutyCompletoSE; //deposito del duty completo del CCP1 per gli aggiornamenti
uint8_t cTimerAccSE;
uint8_t cTimerCorrezioneSE;
uint16_t iSetPoint;
uint16_t iSensIled;
uint8_t cStripLedCorto;

uint16_t iDutyProtezioneCorto;

extern union tFlagAccenditoreSA uFlagAccenditoreSA;
extern tFlagLampada uFlagLampada;
extern tFlagFissiLampada uFlagFissiLampada;
extern tFlagTimer uFlagTimer;
extern tFiltrato uIngressiFilt;
extern misura mIled;
extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFlagOptiCom uFlagDriverOptiCom;
//extern tFlagOptiComTx uFlagDriverOptiComTx;
extern uint8_t cLuminosita;
extern tTimer sTimerDurataTest;
extern union tFlagNorma uFlagNorma;
extern void fInitAutomaAccenditoreSE(void);

//*******************************************************
// void fImponiPotenza(void)
//
// in Emergenza
//*******************************************************
void fImponiPotenza(void) {
    //800LM
    if (uFlagFissiLampada.iBit.Lumen800) {
        if (uFlagFissiLampada.iBit.Potenza8H) {
            iSetPoint = I_SP_800_SE_8H;
            iDutyCompletoSE = DUTY_800_8H;
            iSensIled = SENS_ILED_HIGH;
        } else {
            if (uFlagFissiLampada.iBit.Potenza3H) {
                iSetPoint = I_SP_800_SE_3H;
                iDutyCompletoSE = DUTY_800_3H;
                iSensIled = SENS_ILED_LOW;
            } else {
                if (uFlagFissiLampada.iBit.Potenza2H) {
                    iSetPoint = I_SP_800_SE_2H;
                    iDutyCompletoSE = DUTY_800_2H;
                    iSensIled = SENS_ILED_XLOW;
                } else {
                    if (uFlagFissiLampada.iBit.Potenza1H5) {
                        iSetPoint = I_SP_800_SE_1H5;
                        iDutyCompletoSE = DUTY_800_1H5;
                        iSensIled = SENS_ILED_XLOW;
                    } else {//altrimenti default ad 1h
                        iSetPoint = I_SP_800_SE_1H;
                        iDutyCompletoSE = DUTY_800_1H;
                        iSensIled = SENS_ILED_XXLOW;
                    }
                }
            }
        }
    } else {
        //500LM
        if (uFlagFissiLampada.iBit.Lumen500) {
            if (uFlagFissiLampada.iBit.Potenza8H) {
                iSetPoint = I_SP_500_SE_8H;
                iDutyCompletoSE = DUTY_500_8H;
                iSensIled = SENS_ILED_HIGH;
            } else {
                if (uFlagFissiLampada.iBit.Potenza3H) {
                    iSetPoint = I_SP_500_SE_3H;
                    iDutyCompletoSE = DUTY_500_3H;
                    iSensIled = SENS_ILED_LOW;
                } else {
                    if (uFlagFissiLampada.iBit.Potenza2H) {
                        iSetPoint = I_SP_500_SE_2H;
                        iDutyCompletoSE = DUTY_500_2H;
                        iSensIled = SENS_ILED_XLOW;
                    } else {
                        if (uFlagFissiLampada.iBit.Potenza1H5) {
                            iSetPoint = I_SP_500_SE_1H5;
                            iDutyCompletoSE = DUTY_500_1H5;
                            iSensIled = SENS_ILED_XLOW;
                        } else {//altrimenti default ad 1h
                            iSetPoint = I_SP_500_SE_1H;
                            iDutyCompletoSE = DUTY_500_1H;
                            iSensIled = SENS_ILED_XLOW;
                        }
                    }
                }
            }    
        } else { 
            //altrimenti 300LM
            if (uFlagFissiLampada.iBit.Potenza8H) {
                iSetPoint = I_SP_300_SE_8H;
                iDutyCompletoSE = DUTY_300_8H;
                iSensIled = SENS_ILED_HIGH;
            } else {
                if (uFlagFissiLampada.iBit.Potenza3H) {
                    iSetPoint = I_SP_300_SE_3H;
                    iDutyCompletoSE = DUTY_300_3H;
                    iSensIled = SENS_ILED_MEDIUM;
                } else {
                    if (uFlagFissiLampada.iBit.Potenza2H) {
                        iSetPoint = I_SP_800_SE_2H;
                        iDutyCompletoSE = DUTY_800_2H;
                        iSensIled = SENS_ILED_MEDIUM;
                    } else {
                        if (uFlagFissiLampada.iBit.Potenza1H5) {
                            iSetPoint = I_SP_800_SE_1H5;
                            iDutyCompletoSE = DUTY_800_1H5;
                            iSensIled = SENS_ILED_LOW;
                        } else {//altrimenti default ad 1h
                            iSetPoint = I_SP_800_SE_1H;
                            iDutyCompletoSE = DUTY_800_1H;
                            iSensIled = SENS_ILED_XLOW;
                        }
                    }
                }
            }
        }
    }
}

//*******************************************************
// void fImponiPotenzaTest(void)
//
// sempre 1h
//*******************************************************
void fImponiPotenzaTest(void) {

    if ((uFlagLampada.iBit.flagTestFunzInCorso) || (uFlagNorma.cBit.TestFunzionaleInCorso)) {
        //sempre 1h
        if (uFlagFissiLampada.iBit.Lumen300) {
            iSetPoint = I_SP_300_SE_1H;
            iDutyCompletoSE = DUTY_300_1H; 
            iSensIled = SENS_ILED_XLOW;
        } else {        
            if (uFlagFissiLampada.iBit.Lumen500) {
                iSetPoint = I_SP_500_SE_1H;
                iDutyCompletoSE = DUTY_500_1H; 
                iSensIled = SENS_ILED_XLOW;
            } else {
                //800lumen
                iSetPoint = I_SP_800_SE_1H;
                iDutyCompletoSE = DUTY_800_1H;
                iSensIled = SENS_ILED_XXLOW;
            }
        }
    } else {
        if (uFlagFissiLampada.iBit.SetDurataTest) {   
            fImponiPotenza();
        } else {
            if (uFlagFissiLampada.iBit.Lumen300) {
                iSetPoint = I_SP_300_SE_1H;
                iDutyCompletoSE = DUTY_300_1H; 
                iSensIled = SENS_ILED_XXLOW;
            } else {        
                if (uFlagFissiLampada.iBit.Lumen500) {
                    iSetPoint = I_SP_500_SE_1H;
                    iDutyCompletoSE = DUTY_500_1H;                        
                } else {            
                    //800lumen
                    iSetPoint = I_SP_800_SE_1H;
                    iDutyCompletoSE = DUTY_800_1H;
                }
                iSensIled = SENS_ILED_XLOW;
            }
        }
    }
}

//************************************************
//
// void fAggiornaDutySE(uint16_t dutySE)
//
// scopo: assegna il duty al CCP1
//
//************************************************
void fAggiornaDutySE(uint16_t iDutySE) {

    if (iDutySE > DUTY_SE_MAX) {
        iDutySE = DUTY_SE_MAX;
    }
    DC1B0 = iDutySE % 2;
    iDutySE = iDutySE / 2;
    DC1B1 = iDutySE % 2;
    iDutySE = iDutySE / 2;
    CCPR1L = (char) iDutySE;
}

//************************************************
// void fAggiornaDutySE(void)
//
// descrizione: assegna il duty al CCP1
//
//************************************************
void fAccendiPwmSE(uint16_t iDuty) {
    CLC1CONbits.LC1EN = 1;
    //se spento, eseguo i passi di accensione       
    PWM_LED_SetDigitalInput();
    PR2 = PERIOD_PWM;            
    CCP1CON = 0b00001100;
    fAggiornaDutySE(iDuty);
    PIR1bits.TMR2IF = 0;
    
    INTERRUPT_PeripheralInterruptDisable();
    T2CON = INIT_TMR2_SE;
    while (!PIR1bits.TMR2IF);
    INTERRUPT_PeripheralInterruptEnable();
    
    PWM_LED_SetDigitalOutput();
    
 }

//************************************************
// void fSpegniPwmSE(void)
//
// descrizione: spegne il CCP1
//
//************************************************
void fSpegniPwmSE(void) {
    iDutyCompletoSE = 0;
    CCP1CON = 0; //resetto il latch
    T2CON = 0;
    TMR2 = 0;
    CCPR1L = 0;
    PWM_LED_SetLow();
    PWM_LED_SetDigitalOutput();
    CLC1CONbits.LC1EN = 0;
}

//*******************************************************
// void fAccTransToIdle(void)
//
// descrizione: si spegne tutto e si torna in IDLE
//
//*******************************************************
void fAccTransToIdle(void) {
    cStatoSE = ST_ACC_SE_IDLE;
    uFlagAccenditoreSE.cBit.flagProtCorto = 0;
    fSpegniPwmSE();
}

//*******************************************************
// void fInitAutomaAccenditoreSE(void)
// 
// scopo: inizializzazione dell'Automa Accenditore
//
//*******************************************************
void fInitAutomaAccenditoreSE(void) {
//    CLC1_Initialize(); //� fatto all'inizio dall'MCC
//    cStripLedCorto = 0;
    cStatoSE = ST_ACC_SE_IDLE;
}

//*******************************************************
// void AUTOMA_AccenditoreSE(void)
// 
// scopo: realizza l'AUTOMA_AccenditoreSE
//
//*******************************************************
void AUTOMA_AccenditoreSE(void) {
    
    uFlagAccenditoreSE.cReg &= RESET_FLAG_ACCENDITORE;

    if (uFlagTimer.iBit.flag1msec)    
    {
        if (0 != cTimerCorrezioneSE) {
            cTimerCorrezioneSE--;
        }
        if (0 != cTimerAccSE) {
            cTimerAccSE--;
        }
    }

    if (uFlagAccenditoreSE.cBit.flagProtCorto) {
        uFlagAccenditoreSE.cBit.flagProtCorto = 0;
        cStatoSE = ST_ACC_SE_PROTEZIONE_CORTO;
    }
    
    if (uFlagDriverOptiCom.cBit.TxInCorso) {
        cStatoSE = ST_ACC_SE_DISABLE;
    }
    
    switch (cStatoSE) {
        
        //-------------------------------------------------------
        case ST_ACC_SE_PROTEZIONE_CORTO:
        {      
            uFlagAccenditoreSE.cBit.flagLedInCorto = 1;
            ACC_SE_SetLow();
            ACC_SE_SetDigitalOutput();
            fSpegniPwmSE();
            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                fAccTransToIdle();
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SE_DISABLE:
        {

            if (!uFlagDriverOptiCom.cBit.TxInCorso) {
                cStatoSE = ST_ACC_SE_IDLE;
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SE_IDLE:
        {
            ACC_SE_SetLow();
            ACC_SE_SetDigitalOutput();

            uFlagAccenditoreSE.cBit.flagIdleMode = 1;
            fSpegniPwmSE();
            PIE2bits.C1IE = 0; //disabilito COMP1
            cStripLedCorto = 0;

            if ((uFlagLampada.iBit.flagSEAccesa) || (uFlagNorma.cBit.TestInCorso) || (uFlagLampada.iBit.flagTestInCorso)) {//se s�,
                //carichiamo il duty e setpoint necessari
                if ((uFlagLampada.iBit.flagTestInCorso) || (uFlagNorma.cBit.TestInCorso)) {
                    fImponiPotenzaTest();
                } else {
                    fImponiPotenza();
                }
                
                PIR2bits.C1IF = 0;
                PIE2bits.C1IE = 1; //attiva comparatore 1
                
                cTimerAccSE = TIME_MASCHERATURA_SE;
                uFlagAccenditoreSE.cBit.flagMascheraturaOn = 1;
                
                cTimerCorrezioneSE = TIME_CORREZIONE_INIT;
                ACC_SE_SetHigh();
                ACC_SE_SetDigitalOutput();
                fAccendiPwmSE(DUTY_SOFT_START);
                                
                cStatoSE = ST_ACC_SE_SOFT_START_NO_PROT;
            }
            break;
        }
      
        //-------------------------------------------------------
        case ST_ACC_SE_SOFT_START_NO_PROT:
        {  
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagAccenditoreSE.cBit.flagLedAccesi = 1;
            T2CON = INIT_TMR2_SE; //ribadisco accendo il TMR2 con PRESCALER 1:4

            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                fAccTransToIdle();
            } else {
                if (0 == cTimerAccSE) {
                    if (1 == cStripLedCorto) {
                        //c'� un corto
                        ACC_SE_SetLow();
                        ACC_SE_SetDigitalOutput();
                        fSpegniPwmSE();
                        cStatoSE = ST_ACC_SE_PROTEZIONE_CORTO;
                    } else {
                        //termine mascheratura
                        uFlagAccenditoreSE.cBit.flagMascheraturaOn = 0;
                        CM1CON1 = 0x61; //interrupt NEG
                        cStatoSE = ST_ACC_SE_SOFT_START_INIT;
                    }                        
                }
            }
            break;
        }

        //-------------------------------------------------------
        case ST_ACC_SE_SOFT_START_INIT:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagAccenditoreSE.cBit.flagLedAccesi = 1;
            T2CON = INIT_TMR2_SE; //ribadisco accendo il TMR2 con PRESCALER 1:4

            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                fAccTransToIdle();
            } else {
                if (0 == cTimerCorrezioneSE) {
                    fAggiornaDutySE(iDutyCompletoSE);
                    cTimerCorrezioneSE = TIME_CORREZIONE_BREVE;
                    cStatoSE = ST_ACC_SE_SOFT_START;
                }  
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SE_SOFT_START:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();

            uFlagAccenditoreSE.cBit.flagLedAccesi = 1;
            T2CON = INIT_TMR2_SE; //ribadisco accendo il TMR2 con PRESCALER 1:4

            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                fAccTransToIdle();
            } else {
                if (0 == cTimerCorrezioneSE) {
                    if (mIled.iMedia < (iSetPoint - iSensIled)) {
                        cStatoSE = ST_ACC_SE_RICERCA_VELOCE_ALTO;
                    } else {
                        cStatoSE = ST_ACC_SE_ON;
                    }
                    fAggiornaDutySE(iDutyCompletoSE);
                    cTimerCorrezioneSE = TIME_CORREZIONE_BREVE;
                }
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SE_RICERCA_VELOCE_ALTO:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();

            uFlagAccenditoreSE.cBit.flagLedAccesi = 1;
            T2CON = INIT_TMR2_SE; //ribadisco accendo il TMR2 con PRESCALER 1:4            
            
            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                fAccTransToIdle();
            } else {
                if (mIled.iMedia > iSetPoint) {
                    //setpoint trovato
                    cTimerCorrezioneSE = TIME_CORREZIONE_LUNGO;
                    cStatoSE = ST_ACC_SE_ON;
                } else {
                    if (0 == cTimerCorrezioneSE) {
                        //uso variabili senza segno, ma ho verificato sopra che mIled.iMedia � maggiore di iSetPoint...
                        iDutyCompletoSE += ((iSetPoint - mIled.iMedia) >> 2) + 1; //Duty_correttivo = Errore/4 + 1
                        if (iDutyCompletoSE > DUTY_SE_MAX) {
                            iDutyCompletoSE = DUTY_SE_MAX;
                        }
                        fAggiornaDutySE(iDutyCompletoSE);
                        cTimerCorrezioneSE = TIME_CORREZIONE_BREVE;
                    }
                }
                //verifica ERRORI
                uFlagAccenditoreSE.cBit.flagLedAperti = uIngressiFilt.iBit.flagLedStaccatiFilt;       
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SE_ON:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();

            uFlagAccenditoreSE.cBit.flagLedAccesi = 1;
            T2CON = INIT_TMR2_SE; //ribadisco accendo il TMR2 con PRESCALER 1:4
            
            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                fAccTransToIdle();
            } else {
                if (0 == cTimerCorrezioneSE) {
                    if (mIled.iMedia < (iSetPoint - iSensIled)) {
                        cStatoSE = ST_ACC_SE_RICERCA_ALTO;
                    } else {
                        if (mIled.iMedia > (iSetPoint + iSensIled)) {
                            cStatoSE = ST_ACC_SE_RICERCA_BASSO;
                        }
                    }
                    fAggiornaDutySE(iDutyCompletoSE);
                    cTimerAccSE = TIME_MASCHERATURA_SE;
                    cTimerCorrezioneSE = TIME_CORREZIONE_LUNGO;
                }
                //verifica ERRORI
                uFlagAccenditoreSE.cBit.flagLedAperti = uIngressiFilt.iBit.flagLedStaccatiFilt;                  
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SE_RICERCA_ALTO:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();

            uFlagAccenditoreSE.cBit.flagLedAccesi = 1;
            T2CON = INIT_TMR2_SE; //ribadisco accendo il TMR2 con PRESCALER 1:4            
            
            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                fAccTransToIdle();
            } else {
                if (mIled.iMedia > iSetPoint) {
                    //setpoint ritrovato
                    cTimerCorrezioneSE = TIME_CORREZIONE_LUNGO;
                    cStatoSE = ST_ACC_SE_ON;
                } else {
                    if (0 == cTimerCorrezioneSE) {
                        if (iDutyCompletoSE < DUTY_SE_MAX) {
                            iDutyCompletoSE++;
                        }
                        fAggiornaDutySE(iDutyCompletoSE);
                        cTimerCorrezioneSE = TIME_CORREZIONE_LUNGO;
                    }
                }
                //verifica ERRORI
                uFlagAccenditoreSE.cBit.flagLedAperti = uIngressiFilt.iBit.flagLedStaccatiFilt;                    
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SE_RICERCA_BASSO:
        {            
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();

            uFlagAccenditoreSE.cBit.flagLedAccesi = 1;
            T2CON = INIT_TMR2_SE; //ribadisco accendo il TMR2 con PRESCALER 1:4            
            
            if ((!uFlagLampada.iBit.flagSEAccesa) && (!uFlagNorma.cBit.TestInCorso) && (!uFlagLampada.iBit.flagTestInCorso)) {
                //cStatoSE = ST_ACC_SE_IDLE;
                fAccTransToIdle();
            } else {
                if (mIled.iMedia < iSetPoint) {
                    //setpoint ritrovato
                    cTimerCorrezioneSE = TIME_CORREZIONE_LUNGO;
                    cStatoSE = ST_ACC_SE_ON;
                } else {
                    if (0 == cTimerCorrezioneSE) {
                        //per sicurezza non consentiamo valore = 0
                        if (iDutyCompletoSE > 1) {
                            iDutyCompletoSE--;
                        }
                        fAggiornaDutySE(iDutyCompletoSE);
                        cTimerCorrezioneSE = TIME_CORREZIONE_LUNGO;
                    }
                }
                //verifica ERRORI
                uFlagAccenditoreSE.cBit.flagLedAperti = uIngressiFilt.iBit.flagLedStaccatiFilt;                            
            }
            break;
        }
        //-------------------------------------------------------
        default:
        {
            //cStatoSE = ST_ACC_SE_IDLE;
            fAccTransToIdle();
        }
    }
}
