// AUTOMA_AccenditoreSE.h

#ifndef	__ACCENDITORE_H
#define	__ACCENDITORE_H


#define	ST_ACC_SE_IDLE                  0
#define	ST_ACC_SE_ON                    1
#define ST_ACC_SE_RICERCA_ALTO          2
#define ST_ACC_SE_RICERCA_BASSO         3
#define ST_ACC_SE_DISABLE               4
#define ST_ACC_SE_SOFT_START            5
#define ST_ACC_SE_RICERCA_VELOCE_ALTO   6
#define ST_ACC_SE_SOFT_START_NO_PROT    7
#define ST_ACC_SE_SOFT_START_INIT       8
#define ST_ACC_SE_PROTEZIONE_CORTO      9


//***********************************************************
// PWM:
//
//***********************************************************

////freq PWM --> 20KHz
//#define INIT_TMR2               0b00000101 //TMR2 ON, PRESCALER 1:4
//#define	PERIOD_PWM          99
//#define	DUTY_INIT           200
//#define	DUTY_MAX            400
////potenze SE
//#define DUTY_1H                 200
//#define I_SP_250_SE_1H        55
////potenze SA
//#define DUTY_PS                 4  //1%
//#define DUTY_SA                 401

// In ingresso c'� Fosc/4 = 8MHz
// TMR2 ON, PRESCALER 1:1 ->otteniamo 8MHz
#define INIT_TMR2_SE            0b00000100 
#define	PERIOD_PWM              199 // ->40KHz
#define	DUTY_INIT               480
//#define	DUTY_SE_MAX             800
#define	DUTY_SE_MAX             640 //80%

//// In ingresso c'� Fosc/4 = 8MHz
//// TMR2 ON, PRESCALER 1:64 ->otteniamo 125KHz, per ottenere 1KHz divido per 125, cio� periodo PR2 = 125
//#define INIT_TMR2_SE            0b00000111 
//#define	PERIOD_PWM              124 // ->1KHz
//#define	DUTY_INIT               280
//#define	DUTY_SE_MAX             500
//#define DUTY_TX_UNO             501
//#define DUTY_TX_ZERO            0

//// In ingresso c'� Fosc/4 = 8MHz
//// TMR2 ON, PRESCALER 1:64 ->otteniamo 125KHz
//#define INIT_TMR2_SE            0b00000111 
//#define	PERIOD_PWM              24 // ->5KHz
//#define	DUTY_INIT               280
//#define	DUTY_SE_MAX             100
//#define DUTY_TX_UNO             101
//#define DUTY_TX_ZERO            0


//    #define SET_POINT               100
#define SET_POINT               33

//10msec
#define	SOGLIA_MASCHERATURA     75	//se VreteB > 150mV devo introdurre un ritardo di 500msec
#define TIME_MASCHERATURA_SE    5   //x1msec -> 5msec

#define TIME_WAIT_OFF           20
#define TIME_TEST_LED           25 //250msec

//1msec
//#define	TIME_CORREZIONE         100	//x1msec = 100msec
#define	TIME_CORREZIONE_BREVE   50	//x1msec = 50msec
#define TIME_CORREZIONE_LUNGO   125
#define TIME_CORREZIONE_INIT    25


//DA DEFINIRE
#define SOGLIA_CORRENTE_1H      500


#define SENS_ILED_XXLOW         5
#define SENS_ILED_XLOW          4
#define SENS_ILED_LOW           3
#define SENS_ILED_MEDIUM        2
#define SENS_ILED_HIGH          1

//#define SENS_ILED               4

//#define SENS_ILED               3
//#define SENS_ILED               2
//--------------------------------------------
//  tFlagAccenditore
//--------------------------------------------

typedef union {
    uint8_t cReg;

    struct {
        uint8_t flagProtCorto : 1; //0
        uint8_t flagMascheraturaOn : 1; //1-mascheratura corto iniziale in corso
        uint8_t flagLedInCorto : 1; //2-led in corto
        uint8_t flagLedAperti : 1; //3-strip led aperta
        uint8_t flagIdleMode : 1; //4-in Idle Mode
        uint8_t flagReadyTx : 1; //5-pronto per la tx
        uint8_t flagLedAccesi : 1; //6-segnala che i led sono accesi
        
    } cBit;
} tFlagAccenditoreSE;

#define RESET_FLAG_ACCENDITORE      0b00000011 //esclusi flagProtCorto e MascheraturaOn

#endif
