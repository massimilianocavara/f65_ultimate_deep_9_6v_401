#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "Interrupt.h"

tFlagTimer uFlagTimer;

uint8_t cTimer10msec;
uint8_t cTimer250msec;
uint8_t cTimer1sec;
bit bTime20msec;
uint8_t cTimer50msec;
uint8_t cTimer1minuto;

uint8_t cDepoSecondiTmr1, cDepoSecondiTmr0;
bit bUnSecondoTmr0, bUnSecondoTmr1;
uint8_t cStatoTimer;
uint8_t cDelta;

//extern uint8_t TabellaOpti[32];

extern tTimer sTimerCadenzaFunzionale; //corrispondente al LOGICA TimerFunzionale
extern tTimer sTimerCadenzaAutonomia; //corrispondente al LOGICA TimerAutonomia
extern tTimer sTimerDurataTest;
//tTimer TimerRipetizioneTest; //@ nel caso manchi la rete durante un test di autonomia, ripetiamo il test dopo 7 giorni
extern tFlagFissiLampada uFlagFissiLampada;
extern tTimer sTimerDelayTest;
extern tFiltrato uIngressiFilt;
extern uint16_t iDurataUltimoTest;
extern tFiltrato uIngressiFilt;
//extern tTimer sTimerSleep;

extern void fMisuraOpticom(void);

//*******************************************************************
// void fInitTimer(void)
//
// inizializzazione di Timer
//*******************************************************************
void fInitTimer(void) {
    cTimer10msec = TIME_10MSEC;
    cTimer50msec = 5;
    cTimer250msec = TIME_250MSEC;
    cTimer1sec = TIME_1SEC;
    cTimer1minuto = TIME_1MIN;
    cDepoSecondiTmr1 = 0;
    bUnSecondoTmr1 = 0;
    cDepoSecondiTmr0 = 0;
    bUnSecondoTmr0 = 0;
    cStatoTimer = ST_TMR1_MASTER;
    TMR1 = 0;

    bTime20msec = 0;
    TMR0 = 0x17;
}

////*******************************************************************
////	void fAggiornaTimer(void)
////
////se ritorna 1->timer a ZERO
////se ritorna 0->decremento effettuato
////*******************************************************************
//void fAggiornaTimer(void) {
////    //GESTIONE TIMER SLEEP
////    if (fDecrementa(&sTimerSleep)) {
////        uFlagFissiLampada.iBit.Spegnimento = 1;
////    } else {
////        uFlagFissiLampada.iBit.Spegnimento = 0;
////    }
//    
//    //GESTIONE TIMER PERIOD E DUTY SANIFICATORE
//    if (fDecrementa(&sTimerPeriodSan)) {
//        uFlagFissiLampada.iBit.flagFinePeriodoProfilo = 1; //uso un flag fisso perch� 
//    }
//    if (fDecrementa(&sTimerDutySan)) {
//        uFlagFissiLampada.iBit.FineDutySan = 1; //deve persistere fino alla fine del periodo
//    }
//}

//*******************************************************************
//*******************************************************************
//*******************************************************************
//*******************************************************************
//	void Timer(void)
//*******************************************************************
//*******************************************************************
void Timer(void) {
    //    uint8_t cDelta;

    uFlagTimer.iReg &= RESET_FLAG; //reset dei flag escluso: 1msec_interrupt, 1secinterrupt
    if (uFlagTimer.iBit.flag1msec_interrupt) {
        uFlagTimer.iBit.flag1msec = 1;
        uFlagTimer.iBit.flag1msec_interrupt = 0;
        cTimer10msec--;

        if (0 == cTimer10msec) {
            cTimer10msec = TIME_10MSEC;
            uFlagTimer.iBit.flag10msec = 1;
            bTime20msec ^= 1; //commuto ogni 10msec
            if (1 == bTime20msec) {
                uFlagTimer.iBit.flag20msec = 1;
                if (!uIngressiFilt.iBit.flagRetePresenteFilt) {
                    fDisabilitaIntReteSync();
                    fMisuraOpticom();
                } else {
                    fAbilitaIntReteSync();                   
                }
            }
            cTimer50msec--;
            if (0 == cTimer50msec) {
              cTimer50msec = 5;  
              uFlagTimer.iBit.flag50msec = 1;
            }
            cTimer250msec--;
            if (0 == cTimer250msec) {
                cTimer250msec = TIME_250MSEC;
                uFlagTimer.iBit.flag250msec = 1;
                cTimer1sec--;
                if (0 == cTimer1sec) {
                    cTimer1sec = TIME_1SEC;
                    bUnSecondoTmr0 = 1;
                    cDepoSecondiTmr0++;
                    cTimer1minuto--;
                    if (0 == cTimer1minuto) {
                        cTimer1minuto = TIME_1MIN;
                        uFlagTimer.iBit.flag1minuto = 1;
                    }
                }
            }
        }
    }
    if (uFlagTimer.iBit.flag1sec_interrupt) {
        uFlagTimer.iBit.flag1sec_interrupt = 0;
        bUnSecondoTmr1 = 1;
        cDepoSecondiTmr1++;
    }

    switch (cStatoTimer)
    {
        //------------------------------------------------------------------------------------------------
        //parto dal presupposto che TMR0, essendo incrementato da un oscillatore interno, venga sempre incrementato
        //------------------------------------------------------------------------------------------------
        case ST_TMR1_MASTER:
        {
            if (100 == cDepoSecondiTmr0) {
                //verifico la consistenza del conteggio di TMR1
                if (cDepoSecondiTmr0 > cDepoSecondiTmr1) {
                    cDelta = cDepoSecondiTmr0 - cDepoSecondiTmr1;
                    if (cDelta > 10) {
                        //errore superiore al 10% -> ipotizziamo che tmr1 stia funzionando male
                        cStatoTimer = ST_TMR0_MASTER;
                    }
                } else {
                    cDelta = cDepoSecondiTmr1 - cDepoSecondiTmr0;
                    if (cDelta > 10) {
                        //errore superiore al 10% -> ipotizziamo che tmr1 stia funzionando male
                        cStatoTimer = ST_TMR0_MASTER;
                    }
                }
                //resetto per il prossimo controllo
                cDepoSecondiTmr0 = 0;
                cDepoSecondiTmr1 = 0;
            }
            if (bUnSecondoTmr1) {
                bUnSecondoTmr1 = 0;
                uFlagTimer.iBit.flag1sec = 1;
            }
            bUnSecondoTmr0 = 0;
        } break;

        //------------------------------------------------------------------------------------------------
        case ST_TMR0_MASTER:
        {
            if (100 == cDepoSecondiTmr0) {
                //verifico la consistenza del conteggio di TMR1
                if (cDepoSecondiTmr0 > cDepoSecondiTmr1) {
                    cDelta = cDepoSecondiTmr0 - cDepoSecondiTmr1;
                    if (cDelta < 10) {//errore inferiore al 10% -> ipotizziamo che tmr1 stia di nuovo funzionando bene
                        cStatoTimer = ST_TMR1_MASTER;
                    }
                } else {
                    cDelta = cDepoSecondiTmr1 - cDepoSecondiTmr0;
                    if (cDelta < 10) {
                        //errore inferiore al 10% -> ipotizziamo che tmr1 stia di nuovo funzionando bene
                        cStatoTimer = ST_TMR1_MASTER;
                    }
                }
                //resetto per il prossimo controllo
                cDepoSecondiTmr0 = 0;
                cDepoSecondiTmr1 = 0;
            }
            if (bUnSecondoTmr0) {
                bUnSecondoTmr0 = 0;
                uFlagTimer.iBit.flag1sec = 1;
//                fAggiornaTimer();
            }
            bUnSecondoTmr1 = 0;
        } break;

        //------------------------------------------------------------------------------------------------
        default:
        {
            cStatoTimer = ST_TMR0_MASTER;
        }
    }
}

