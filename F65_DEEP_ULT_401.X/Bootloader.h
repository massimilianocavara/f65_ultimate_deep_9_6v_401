// Project: NUCA CABLECOM
/* 
 * File:   Bootloader.h
 * Author: cavaram
 *
 * Created on 27 aprile 2020, 10.29
 */

#ifndef BOOTLOADER_H
#define	BOOTLOADER_H


// Inizio zona flash dove inizia il bootloader (default: 0x3B00)
#define FLASH_START_BOOT_H          0x3B
#define FLASH_START_BOOT_L          0x00


// Opcode possibili per il registro ADDR_FW_REGISTER della TableBoot
#define FIRST_START_MICRO           0x00        // Serve al primo avvio del micro per fare un salto al bootloader in modo che possa aggiornare la propria versione firmware nella Tabella dati
#define FIRST_START_AFTER_BOOT      0x01        // Serve al primo avvio del firmware, ogni volta che si rientra dal bootloader, per garantire l'aggiornamento dell'IDSW, Rel, Build, etc. nella Tabella dati
#define START_UPG_FW                0xA6
#define FW_NORMAL_MODE              0xFF

// Opcode possibili per il registro ADDR_FW_CHECK della TableBoot
#define FIRMWARE_OK                 0xBE
#define FIRMWARE_NOT_OK             0xFF

// Opcode che identificano il firmware che sta girando nel micro (servono nel comando GET_SW_INFO)
#define RUNNING_BOOTLOADER          0xB0        // Sta girando il bootloader
#define RUNNING_MAIN_FIRMWARE       0x5F        // Sta girando il firmware principale

// Mappa HEF PIC16F1716:
// 0x1F80 - 0x1F9F (8064 - 8095) 32 word a 14bit  <- Tabella Bootloader
// 0x1FA0 - 0x1FBF (8096 - 8127) 32 word a 14bit  <- Tabella DALI
// 0x1FC0 - 0x1FDF (8128 - 8159) 32 word a 14bit  <- Tabella Logica
// 0x1FE0 - 0x1FFF (8160 - 8191) 32 word a 14bit  <- Tabella Opticom

// Mappa HEF PIC16F1718:
// 0x3F80 - 0x3F9F (16256 - 16287) 32 word a 14bit  <- Tabella Bootloader
// 0x3FA0 - 0x3FBF (16288 - 16319) 32 word a 14bit  <- Tabella DALI
// 0x3FC0 - 0x3FDF (16320 - 16351) 32 word a 14bit  <- Tabella Logica
// 0x3FE0 - 0x3FFF (16352 - 16383) 32 word a 14bit  <- Tabella Opticom

//***********************************************************************
//***********************************************************************
// TABELLA BOOTLOADER
//***********************************************************************
//***********************************************************************

//Label del BOOTLOADER:
#define SIZE_BUFFER_FLASH           32

#define ADDR_BOOT_VERSION           (ADDR_START_TABLE_BOOT)
#define ADDR_FW_REGISTER            (ADDR_START_TABLE_BOOT + 1)
#define ADDR_FW_CHECK               (ADDR_START_TABLE_BOOT + 2)
#define ADDR_NUM_PACKETS_L          (ADDR_START_TABLE_BOOT + 3)
#define ADDR_NUM_PACKETS_H          (ADDR_START_TABLE_BOOT + 4)
#define ADDR_LENGTH_PACKET          (ADDR_START_TABLE_BOOT + 5)
#define ADDR_INIZIO_FW_L            (ADDR_START_TABLE_BOOT + 6)
#define ADDR_INIZIO_FW_H            (ADDR_START_TABLE_BOOT + 7)
#define ADDR_FINE_FW_L              (ADDR_START_TABLE_BOOT + 8)
#define ADDR_FINE_FW_H              (ADDR_START_TABLE_BOOT + 9)
#define ADDR_TOTAL_CHKSUM_L         (ADDR_START_TABLE_BOOT + 10)
#define ADDR_TOTAL_CHKSUM_ML        (ADDR_START_TABLE_BOOT + 11)
#define ADDR_TOTAL_CHKSUM_MH        (ADDR_START_TABLE_BOOT + 12)
#define ADDR_TOTAL_CHKSUM_H         (ADDR_START_TABLE_BOOT + 13)
#define ADDR_INIZIO_FW_L_TEMP	    (ADDR_START_TABLE_BOOT + 14)
#define ADDR_INIZIO_FW_H_TEMP	    (ADDR_START_TABLE_BOOT + 15)
#define ADDR_FINE_FW_L_TEMP         (ADDR_START_TABLE_BOOT + 16)
#define ADDR_FINE_FW_H_TEMP         (ADDR_START_TABLE_BOOT + 17)
#define ADDR_INIZIO_BOOT_L          (ADDR_START_TABLE_BOOT + 18)
#define ADDR_INIZIO_BOOT_H          (ADDR_START_TABLE_BOOT + 19)

// Indice buffer TableBoot
#define IBUFF_BOOT_VERSION          0
#define IBUFF_FW_REGISTER           1
#define IBUFF_FW_CHECK              2
#define IBUFF_NUM_PACKETS_L         3
#define IBUFF_NUM_PACKETS_H         4
#define IBUFF_LENGTH_PACKET         5
#define IBUFF_INIZIO_FW_L           6
#define IBUFF_INIZIO_FW_H           7
#define IBUFF_FINE_FW_L             8
#define IBUFF_FINE_FW_H             9
#define IBUFF_TOTAL_CHKSUM_L        10
#define IBUFF_TOTAL_CHKSUM_ML       11
#define IBUFF_TOTAL_CHKSUM_MH       12
#define IBUFF_TOTAL_CHKSUM_H        13
#define IBUFF_INIZIO_FW_L_TEMP	    14
#define IBUFF_INIZIO_FW_H_TEMP	    15
#define IBUFF_FINE_FW_L_TEMP        16
#define IBUFF_FINE_FW_H_TEMP        17
#define IBUFF_INIZIO_BOOT_L         18
#define IBUFF_INIZIO_BOOT_H         19

#endif	/* BOOTLOADER_H */

