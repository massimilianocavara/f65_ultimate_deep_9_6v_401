//**********************************************************************
//  MODULO_Norma.h
//
//  Descrizione:
//  header file di MODULO_Norma.h
//**********************************************************************

#ifndef MODULO_NORMA_H
#define	MODULO_NORMA_H


#define ST_NORMA_IDLE                       0
#define ST_NORMA_TEST_FUNZ                  1
#define ST_NORMA_TEST_AUTO                  2
#define ST_NORMA_TEST_DELAYED               3
#define ST_NORMA_TEST_INIBITO               4
#define ST_NORMA_ATTESA_RIPETIZIONE_AUTO    5
#define ST_NORMA_ATTESA_RIPETIZIONE_FUNZ    6
#define ST_NORMA_ATTESA_CARICA              7
#define ST_NORMA_DISABLE                    8


//30gg ->
#define TIME_MAX_CADENZA_FUNZ   0x278D00
//7gg ->
#define TIME_MAX_DELAYTEST      0x093A80
//24h ->
#define TIME_MAX_INIBIZIONE     0x015180


#ifdef TEMPO_TEST_DEMO
    #ifdef SOLO_AUTONOMIA_DEMO
    //28 gg
        #define CADENZA_FUNZ_H      0x24
        #define CADENZA_FUNZ_M      0xEA
        #define CADENZA_FUNZ_L      0x00
    //1 minuti
        #define CADENZA_AUTO_H      0x00
        #define CADENZA_AUTO_M      0x00
        #define CADENZA_AUTO_L      0x3C
    #else
    //1 minuti
        #define CADENZA_FUNZ_H      0x00
        #define CADENZA_FUNZ_M      0x00
        #define CADENZA_FUNZ_L      0x3C
    //5 minuti
        #define CADENZA_AUTO_H      0x00
        #define CADENZA_AUTO_M      0x01
        #define CADENZA_AUTO_L      0x2C
    #endif
#else
//28gg
    #define CADENZA_FUNZ_H      0x24
    #define CADENZA_FUNZ_M      0xEA
    #define CADENZA_FUNZ_L      0x00

    #ifdef VERIFICA_PARI_DISPARI
        #define CADENZA_AUTO_H      0x00
        #define CADENZA_AUTO_M      0x00
        #define CADENZA_AUTO_L      0x3C
    #else
    //182gg
        #define CADENZA_AUTO_H      0xEF
        #define CADENZA_AUTO_M      0xF1
        #define CADENZA_AUTO_L      0x00
    #endif

#endif

#ifdef DEBUG_RIPRISTINO
    #define TM_RIPRISTINO_H     0x00
    #define TM_RIPRISTINO_M     0x00
    #define TM_RIPRISTINO_L     0x3C
#else
    //90gg
    #define TM_RIPRISTINO_H     0x76
    #define TM_RIPRISTINO_M     0xA7
    #define TM_RIPRISTINO_L     0x00
#endif

//#define LUMINOSITA          LUM_SA
//#ifdef TIPO_SE
//    #define CONFIG_L            0b00010110 //SE, P/D da ID_SERIALE, 1H
//#else
//    #define CONFIG_L            0b00010010 //SA, P/D da ID_SERIALE, 1H
//#endif
//#define CONFIG_H            0b00000001 //Test AUTO sempre 1H
//#define V_ROTARY_L          0
//#define V_ROTARY_H          0



#ifdef TEMPO_TEST_DEMO
    //3 minuti
    #define DELAY_TEST_H        0x00
    #define DELAY_TEST_M        0x00
    #define DELAY_TEST_L        0xB4
#else
    #define DELAY_TEST_H        0x09
    #define DELAY_TEST_M        0x3A
    #define DELAY_TEST_L        0x80
#endif

#ifdef DEMO_APERTURA_OPTI
    #define DUE_ORE             60
#else
    #define DUE_ORE             7200 //secondi
#endif


//***********************************************
// tipi
//***********************************************

union tFlagNorma {
    uint8_t cReg;

    struct {
        uint8_t WaitRipetizioneTestAuto : 1; //0
        uint8_t WaitRipetizioneTestFunz : 1; //1
        uint8_t TestAutonomiaInCorso : 1; //2
        uint8_t TestFunzionaleInCorso : 1; //3
        uint8_t TestInCorso : 1; //4
        uint8_t DelayInCorso : 1; //5
        uint8_t WaitFinecarica : 1; //6
    } cBit;    
};

//init di tFlagNorma dei diversi Stati:
//                                        00000000
//                                        76543210
#define	FLAG_NORMA_TEST_FUNZ            0b00011000
#define	FLAG_NORMA_TEST_AUTO            0b00010100




union tFlagNormaFissi {
    uint8_t cReg;
    
    struct {
        uint8_t EseguiTestAuto : 1; //0
        uint8_t EseguiTestFunz : 1; //1
        uint8_t DelayTest : 1; //2
        uint8_t FineTest : 1; //3
        uint8_t RipetiTest : 1; //4
    } cBit;
};

#ifdef TEMPO_TEST_DEMO
//    #define TIME_24H        300 //5 minuti
    #define TIME_24H        30 //30sec
#else
    #define TIME_24H        86400
#endif

//prototipi funzioni
void fCancellaErrori(uint8_t mod);
void fInitAutomaNorma(void);


#endif

