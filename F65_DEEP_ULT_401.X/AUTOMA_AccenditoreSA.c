//#include <stdint.h>
#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_AccenditoreSA.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "MODULO_Norma.h"
#include "AUTOMA_DriverOptiCom.h"
#include "mcc_generated_files/clc1.h"
#include "mcc_generated_files/pin_manager.h"

union tFlagAccenditoreSA uFlagAccenditoreSA;
uint8_t cStatoSA;
uint8_t cTimerSA;

extern tFiltrato uIngressiFilt;
extern tFlagLampada uFlagLampada;
extern tFlagTimer uFlagTimer;
extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFlagOptiCom uFlagDriverOptiCom;


//*******************************************************
// void fSpegniSA(void)
// 
// descrizione: spengo l'SA
//
//*******************************************************
void fSpegniSA(void) {
    SA_ON_SetHigh();
    SA_ON_SetDigitalOutput();    
}

//*******************************************************
// void fAccendiSA(void)
// 
// descrizione: accendo l'SA
//
//*******************************************************
void fAccendiSA(void) {
    SA_ON_SetLow();
    SA_ON_SetDigitalOutput();
}

//*******************************************************
// void fTransToSAAccesa(void)
//
//*******************************************************
void fTransToSAAccesa(void) {
    cTimerSA = TIME_MASCHERATURA_SA;
    uFlagAccenditoreSA.cBit.LedSaAccesi = 1;
    fAccendiSA();   
//    CLC1_Initialize(); //viene fatto all'inizio dall'MCC
    CLC1CONbits.LC1EN = 1;
    cStatoSA = ST_ACC_SA_ON;
}

//*******************************************************
// void fInitAutomaAccenditoreSA(void)
//
//*******************************************************
void fInitAutomaAccenditoreSA(void) {
//    cStatoSA = ST_ACC_SA_IDLE;
//    fSpegniSA();
    cStatoSA = ST_ACC_SA_ON;
    fAccendiSA();
}

//*******************************************************
// void AUTOMA_AccenditoreSA(void)
//
//*******************************************************
void AUTOMA_AccenditoreSA(void) {
    
    uFlagAccenditoreSA.cReg = 0;
    
    if (uFlagTimer.iBit.flag10msec) {
        if (0 != cTimerSA) {
            cTimerSA--;
        }
    }

    //in caso di trasmissione dell'ACK, devo disabilitare quello che � in corso per dare il controllo al driver Opticom Lite
//    if ((uFlagProtOptiCom.iBit.TrasmettiDone) || (uFlagProtOptiCom.iBit.TrasmettiNotEnabled) || (uFlagProtOptiCom.iBit.TrasmettiNotImplemented)) {
    if (uFlagDriverOptiCom.cBit.TxInCorso) {
        cStatoSA = ST_ACC_SA_DISABLE;
    }
    
    switch(cStatoSA) {
        //-------------------------------------------------------
        case ST_ACC_SA_DISABLE:
        {
            if (!uFlagDriverOptiCom.cBit.TxInCorso) {
                cStatoSA = ST_ACC_SA_IDLE;
            }
            break;
        }
        
        //-------------------------------------------------------
        case ST_ACC_SA_SLEEP:
        {
            SA_ON_SetLow();
            SA_ON_SetDigitalOutput();
            if (uIngressiFilt.iBit.flagRetePresenteFilt) {
                cStatoSA = ST_ACC_SA_IDLE;
            }           
            break;
        }
        //-------------------------------------------------------
        case ST_ACC_SA_IDLE:
        {
            if (uFlagLampada.iBit.flagSAAccesa) {
                fAccendiSA();
                cTimerSA = TIME_MASCHERATURA_SA;
                cStatoSA = ST_ACC_SA_ON;
            } else {
                fSpegniSA();
            }
            break;
        }
        //-------------------------------------------------------
        case ST_ACC_SA_ON:
        {
            uFlagAccenditoreSA.cBit.LedSaAccesi = 1;
            fAccendiSA();
            if (uFlagLampada.iBit.flagSAPSspenta) {
                fSpegniSA();
                cStatoSA = ST_ACC_SA_IDLE;
            }  else {
                if (0 == cTimerSA) {
                    uFlagAccenditoreSA.cBit.StripLedAperta = uIngressiFilt.iBit.flagLedStaccatiFilt;
                }
            }
            break;
        }
    }
}


