// AUTOMA_ProtOptiCom.c

#include <htc.h>
#include "global.h"
#include "Hardware.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_DriverOptiCom.h"
#include "Timer.h"
#include "AUTOMA_Lampada.h"
#include "HEFlash.h"
#include "AUTOMA_Charger.h"
#include "MODULO_Norma.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "AUTOMA_Ingressi.h"
#include "AUTOMA_Eeprom.h"


uint8_t cStatoAutomaOpticom;
FlagProtOptiCom_t uFlagProtOptiCom;

//VARIABILI OPTICOM:
uint16_t iTimerConfigAttiva; //timer per gestire configurazione
//uint8_t cTimeScrittura;

//uint16_t iDurataUltimoTest;

extern uint8_t cDebug;
extern tFlagCharger uFlagCharger;

//variabili DRIVER OPTICOM:
extern uint8_t cBufferOptiLight;

extern tFlagOptiCom uFlagDriverOptiCom;
extern uint8_t cCheckSum;
extern tTimer sTimerDelayTest;

//variabili AT e LOGICA:
extern tFlagTimer uFlagTimer;
extern tFlagLampada uFlagLampada;
extern tByteErrori uByteErrori;
extern tTimer TimerCadenzaFunzionale;
extern tTimer TimerCadenzaAutonomia;
extern tFlagFissiLampada uFlagFissiLampada;
extern tFiltrato uIngressiFilt;
extern uint8_t cStatoLampada;
extern union tFlagNorma uFlagNorma;
extern uint8_t TabellaLogica[32];
extern union tCmdLogica uFlagLogica;
extern uint8_t cStatoDriverOptiCom;
extern misura mVrete;
extern filtro fRetePresente;

//extern void fAggiornaConfigDaTabellaOpti(void);
extern void fCancellaErrori(uint8_t mod);
extern void fInitTimerTest(void);
extern void fResetDiFabbrica(void);
extern void fAggiornaConfigDaTabellaLogica(void);
extern void fAzzeraMisura(misura * mMisDaAgg, unsigned int iMediaIniziale);
extern void fInitFiltri(filtro * FiltroDaInit, uint8_t valore);

//*******************************************************************
//  void fInitAutomaProtOptiComLite(void)
//
//  funzione: inizializza l'AUTOMA_ProtOptiCom 
//*******************************************************************
void fInitAutomaProtOptiComLite(void) {
    cStatoAutomaOpticom = ST_PROT_OPTICOM_COMPLETO;
    iTimerConfigAttiva = DUE_ORE;
}



//*******************************************************************
//  void AUTOMA_ProtOptiComLite(void)
//
//  funzione: Automa per la gestione della comunicazione LOGICA
//*******************************************************************
void AUTOMA_ProtOptiComLite(void) {
    
    uFlagProtOptiCom.iReg = 0;

    if (uFlagTimer.iBit.flag1sec) {
        if (0 != iTimerConfigAttiva) {
            iTimerConfigAttiva--;
        }
    }

    switch (cStatoAutomaOpticom) {

        //--------------------------------------------------------------------------------------
        // stato di disabilitazione
        //--------------------------------------------------------------------------------------
        case ST_PROT_OPTICOM_DISABLE:
        {
            uFlagProtOptiCom.iBit.OpticomDisabilitato = 1;
            
            if (!uFlagFissiLampada.iBit.OpticomDisabilitato) {
                //resettato solo da Logica...
                cStatoAutomaOpticom = ST_PROT_OPTICOM_LIMITATO;
            }
            break;
        }
        //--------------------------------------------------------------------------------------
        // Stato di decodifica limitata
        // solo comandi vecchio protocollo a 5 bit
        //--------------------------------------------------------------------------------------
        case ST_PROT_OPTICOM_LIMITATO:
        {           
            uFlagProtOptiCom.iBit.OpticomLimitato = 1;

//#ifdef  PRATICA_UVOXY_CBL           
            if (0 != iTimerConfigAttiva) {
                cStatoAutomaOpticom = ST_PROT_OPTICOM_COMPLETO;
            } else {
//#endif
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                //COMANDI SEMPRE APERTI, SIA VECCHIO PROTOCOLLO CHE NUOVO
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                if (uFlagDriverOptiCom.cBit.OldProtocol) {
                    switch (cBufferOptiLight) {
                        //+++++++++++++++++++++++++++++++++++++++++++++++++
                        case START_TEST_FUNZIONALE:
                        {
                            uFlagProtOptiCom.iBit.TestFunzionaleDaOpti = 1;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++++++
                        case STOP_TEST:
                        {
                            uFlagProtOptiCom.iBit.StopTest = 1;
                            break;
                        }
//                        //++++++++++++++++++++++++++++++++++++++++++++++++++
//                        // valida per tutti i tipi di lampada: solo i comandi START_TEST_FUNZIONALE e STOP_TEST
//                        // sono attivi, gli altri sono disabilitati a prescindere che siano implementati o meno
//                        // In ogni caso, la segnalazione � la medesima
//                        case START_TEST_AUTONOMIA:
//                        case REST_MODE_OFF:
//                        case DISABILITA_OPTICOM:                        
//                        case ATTIVA_EMERGENZA_LOCALE:
//                        case DISATTIVA_EMERGENZA_LOCALE:                        
//                        case SET_AUTONOMIA_8H:
//                        case SET_AUTONOMIA_2H:
//                        case SET_AUTONOMIA_1H5:
//                        case SINCRONIZZA_TEST_AUTO:
//                        case CANCELLA_ERRORI:
//                        case RESET_FABBRICA:
//                        case SET_FUNZIONALITA_SA:
//                        case SET_FUNZIONALITA_SE:                        
//                        case SET_TEST_1H:
//                        case SET_TEST_DURATA_AUTONOMIA:
//                        case SET_DISPARI:
//                        case SET_PARI:
//                        case SET_AUTONOMIA_3H:                        
//                        case SET_AUTONOMIA_1H:
//                        {
//                            uFlagProtOptiCom.iBit.TrasmettiNotEnabled = 1;
//                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_NACK;
//                            break;
//                        }                                
                        //++++++++++++++++++++++++++++++++++++++++++++++++++
                        default:
                        {
                            uFlagProtOptiCom.iBit.TrasmettiNotImplemented = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_NACK; 
                            break;
                        }
                    }
                } else {
                    if (uFlagDriverOptiCom.cBit.flagBufferDisponibile) {
                        //NUOVO PROTOCOLLO
                        switch (cBufferOptiLight) {

//no UVOXY
//                            case SANIFICATORE_OFF:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111110; //impongo lo spegnimento
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100000; //tipo UVOXY; contr da remoto
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case SANIFICATORE_ON:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111111; //non modifico la configurazione perch� questo cmd funge solo da interruttore
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100001; //tipo UVOXY; contr da remoto
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case VEL_VENTOLA_ALTA:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111111; //tipo UVOXY
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10111000; //vel ventola ALTA
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case VEL_VENTOLA_MEDIA:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10110111; //vel ventola MEDIA
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10110000; //tipo UVOXY; contr da remoto
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case VEL_VENTOLA_BASSA:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10101111; //vel ventola BASSA
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10101000; //tipo UVOXY; contr da remoto
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case SANIFICATORE_PROFILO_24H:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111111;
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100110;
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                                                        
//                                break;
//                            }
//                            case SANIFICATORE_PROFILO_12H:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111101;
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100100;
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case SANIFICATORE_PROFILO_8H:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111011;
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100010;
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }

                            default:
                            {
                                uFlagProtOptiCom.iBit.TrasmettiNotImplemented = 1;
                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_NACK; 
                                break;
                            }
                        }
                        fAggiornaConfigDaTabellaLogica();
                    }
//#ifdef  PRATICA_UVOXY_CBL
                }//*
//#endif
            }
            break;
        }
        
//#ifndef OPTI_SOLO_CMD_APERTI
        //--------------------------------------------------------------------------------------
        // stato di attesa per decodifica
        //--------------------------------------------------------------------------------------
        case ST_PROT_OPTICOM_COMPLETO:
        {
            union tParametriFunz uParametriLamp;
            
            uParametriLamp.cReg = TabellaLogica[IND_PARAMETRI_REG];
            
            if (0 == iTimerConfigAttiva) {
                cStatoAutomaOpticom = ST_PROT_OPTICOM_LIMITATO;
            } else {
                if (uFlagDriverOptiCom.cBit.OldProtocol) {
                    switch (cBufferOptiLight) {

                        //+++++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_FUNZIONALITA_SA:
                        {
                            if (uParametriLamp.cBit.SE) {
                                //impongo sempre il tipo SE                           
                                TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10101111;
                                TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10010000;
                                uFlagProtOptiCom.iBit.TrasmettiNotImplemented = 1;
                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_NACK; 
                            } else {
                                //Side-effect su Logica: impongo CentralDimming ed SA
                                TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10101111;
                                TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10100000;
                                fRichiediAggEE(MOD_DEFAULT);                            
                            
                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            }
                            
                            break;
                        }                            
                        //+++++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_FUNZIONALITA_SE:
                        {
                            //Side-effect su Logica: impongo CentralDimming e SE
                            TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10101111;
                            TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10010000;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
                            
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }
                        //+++++++++++++++++++++++++++++++++++++++++++++++++
                        case START_TEST_FUNZIONALE:
                        {
                            uFlagProtOptiCom.iBit.TestFunzionaleDaOpti = 1;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case STOP_TEST:
                        {
                            uFlagProtOptiCom.iBit.StopTest = 1;
                            break;
                        }                    
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_DISPARI:
                        {
                            TabellaLogica[IND_ACTION_REGISTER] &= 0b11100111;
                            TabellaLogica[IND_ACTION_REGISTER] |= 0b00001000;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                    
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_PARI:
                        {
                            TabellaLogica[IND_ACTION_REGISTER] &= 0b11100111;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);

                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                    
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_AUTONOMIA_3H:
                        {
                            //Side-effect su Logica: ActionRegister ed ActionRegister2
                            TabellaLogica[IND_ACTION_REGISTER] |= 0b01100000;
                            TabellaLogica[IND_ACTION_REGISTER_2] &= 0b11110001;
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b00001000;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                                
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                    
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_AUTONOMIA_1H:
                        {
                            //Side-effect su Logica: ActionRegister ed ActionRegister2
                            TabellaLogica[IND_ACTION_REGISTER] |= 0b01100000;
                            TabellaLogica[IND_ACTION_REGISTER_2] &= 0b11110001;
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b00000010;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                              
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                        
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case CANCELLA_ERRORI:
                        {
                            fCancellaErrori(TUTTI_GLI_ERRORI);
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case RESET_FABBRICA:
                        {
                            if (ST_PRE_SLEEP != cStatoLampada) {
                                fResetDiFabbrica();
                                //devo reimporre questi stati per poter rispondere
                                fAzzeraMisura(&mVrete, SOGLIA_PRESENZA_SE);
                                fInitFiltri(&fRetePresente, 1);
                                cStatoLampada = ST_EM_LOC_SA_ON;
                            } else {                            
                                fResetDiFabbrica();
                            }

                            cStatoDriverOptiCom = ST_OPTICOM_NORMALE; 
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_TEST_1H:
                        {
                            TabellaLogica[IND_ACTION_REGISTER_2] &= 0b11111110;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_TEST_DURATA_AUTONOMIA:
                        {
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b00000001;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                                                                        
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SINCRONIZZA_TEST_AUTO:
                        {
                            fInitTimerTest(); //reinizializza anche quello funzionale
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                        
                            break;
                        }
                        //+++++++++++++++++++++++++++++++++++++++++++++++++
                        case START_TEST_AUTONOMIA:
                        {
                            uFlagProtOptiCom.iBit.TestAutonomiaDaOpti = 1;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case REST_MODE_OFF:
                        {
                            uFlagProtOptiCom.iBit.RestModedaOpti = 1;
                            break;
                        }                                   
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case DISABILITA_OPTICOM:
                        {
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b10000000;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                        

                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case ATTIVA_EMERGENZA_LOCALE:
                        {
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b00100000;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case DISATTIVA_EMERGENZA_LOCALE:
                        {
                            TabellaLogica[IND_ACTION_REGISTER_2] &= 0b11011111;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_AUTONOMIA_1H5:
                        {
                            //Side-effect su Logica: ActionRegister ed ActionRegister2
                            TabellaLogica[IND_ACTION_REGISTER] |= 0b01100000;
                            TabellaLogica[IND_ACTION_REGISTER_2] &= 0b11110001;
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b00000100;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                            
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                    
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_AUTONOMIA_2H:
                        {
                            //Side-effect su Logica: ActionRegister ed ActionRegister2
                            TabellaLogica[IND_ACTION_REGISTER] |= 0b01100000;
                            TabellaLogica[IND_ACTION_REGISTER_2] &= 0b11110001;
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b00000110;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                                        
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }                    
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        case SET_AUTONOMIA_8H:
                        {
                            //Side-effect su Logica: ActionRegister ed ActionRegister2
                            TabellaLogica[IND_ACTION_REGISTER] |= 0b01100000;
                            TabellaLogica[IND_ACTION_REGISTER_2] &= 0b11110001;
                            TabellaLogica[IND_ACTION_REGISTER_2] |= 0b00001100;
                            //uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                            RegAggiornaParametri = 0x00;
//                            RegAggiornaTaratura = 0x00;
                            fRichiediAggEE(MOD_DEFAULT);
                             
                            uFlagProtOptiCom.iBit.TrasmettiDone = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;
                            break;
                        }
                        //++++++++++++++++++++++++++++++++++++++++++++++
                        default:
                        {
                            uFlagProtOptiCom.iBit.TrasmettiNotImplemented = 1;
                            cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_NACK; 
                            break;
                        }
                    }
                    fAggiornaConfigDaTabellaLogica();
                } else {
                    if (uFlagDriverOptiCom.cBit.flagBufferDisponibile) {
                        //NUOVO PROTOCOLLO
                        switch (cBufferOptiLight) {
//                            case SANIFICATORE_OFF:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111110; //impongo lo spegnimento
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100000; //tipo UVOXY; contr da remoto
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case SANIFICATORE_ON:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111111; //non modifico la configurazione perch� questo cmd funge solo da interruttore
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100001; //tipo UVOXY; contr da remoto                                
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case VEL_VENTOLA_ALTA:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111111; //tipo UVOXY
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10111000; //vel ventola ALTA
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case VEL_VENTOLA_MEDIA:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10110111; //vel ventola MEDIA
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10110000; //tipo UVOXY; contr da remoto
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case VEL_VENTOLA_BASSA:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10101111; //vel ventola BASSA
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10101000; //tipo UVOXY; contr da remoto
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case SANIFICATORE_PROFILO_24H:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111111;
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100110;
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                                                        
//                                break;
//                            }
//                            case SANIFICATORE_PROFILO_12H:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111101;
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100100;
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
//                            case SANIFICATORE_PROFILO_8H:
//                            {
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111011;
//                                TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100011;
//                                uFlagProtOptiCom.iBit.AggiornaTabLGInEeprom = 1;
//                                uFlagProtOptiCom.iBit.TrasmettiDone = 1;
//                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_ACK;                            
//                                break;
//                            }
                            default:
                            {
                                uFlagProtOptiCom.iBit.TrasmettiNotImplemented = 1;
                                cStatoAutomaOpticom = ST_PROT_OPTICOM_TX_NACK; 
                                break;
                            }
                        }
                        fAggiornaConfigDaTabellaLogica();
                    }
                }//*
            }
            break;       
        }
//#endif
        //--------------------------------------------------------------------------------------
        // ACK
        //--------------------------------------------------------------------------------------
        case ST_PROT_OPTICOM_TX_ACK:
        {
            if (uFlagDriverOptiCom.cBit.AckInviato) {
                cStatoAutomaOpticom = ST_PROT_OPTICOM_COMPLETO;
            }
            break;
        }
        
        //--------------------------------------------------------------------------------------
        // NACK
        //--------------------------------------------------------------------------------------
        case ST_PROT_OPTICOM_TX_NACK:
        {
            if (uFlagDriverOptiCom.cBit.NackInviato) {
                cStatoAutomaOpticom = ST_PROT_OPTICOM_COMPLETO;
            }
            break;
        }
        
        //--------------------------------------------------------------------------------------
        // default
        //--------------------------------------------------------------------------------------
        default:
        {
            cStatoAutomaOpticom = ST_PROT_OPTICOM_LIMITATO;
        }
    }//end switch
}

