//******************************************************************************
//
// Modulo:       ingressi.h
// Descrizione:  Modulo definizioni per modulo ingressi.c
//
//******************************************************************************

#ifndef	__INGRESSI_H
#define	__INGRESSI_H

//*****************************************************************************
// COSTANTI
//*****************************************************************************
//Presenza Rete SA
//Vref=2.048V
//dal picco si calcola il medio dividendo per pi-greco (raddrizzatore a singola semionda)
//160V (rms) -> 1.414V
#define SOGLIA_ASSENZA_SA           707
////180V (rms) -> 1.591V
//#define SOGLIA_PRESENZA_SA          795
//175V (rms) -> 1.546V
#define SOGLIA_PRESENZA_SA          773


//Vref=2.048V
//160V (rms) -> 1.145V
#define SOGLIA_ASSENZA_SE           573
//180V (rms) -> 1.288V
#define SOGLIA_PRESENZA_SE          644

////sulla scheda vedo che la tensione passa da 1.7V quando � spenta, a circa 0.725 quando si accende
////Vref=2.048V
////0.504V -> 252
//#define SOGLIA_ASSENZA_SE           252
////0.567V -> 283
//#define SOGLIA_PRESENZA_SE          283


//8V -> 154
#define SOGLIA_LOGICA_MIN           154
//14V -> 270
#define SOGLIA_LOGICA_MAX           270

//2.0V riferito a 3.3V (da verificare))
#define SOGLIA_1H                   621              

#define	NUM_CAMPIONI                10
#define NUM_CAMPIONI_LENTI          50  //500msec
#define NUM_CAMPIONI_MOLTO_LENTI    100 //1sec
#define	NUM_CAMPIONI_VBATT          20

// Soglie su Iled per individuare eventuai errori (DA VERIFICARE!!!)
#define	SOGLIA_STRIP_CORTO          400 //4 volte il Setpoint dell'1H
#define SOGLIA_STRIP_STACCATA       3

//1V -> 1/2.048*1024 = 500
#define SOGLIA_SA_STRIP_STACCATA    500

#ifdef F65_UVOXY_AT_TR
    #define SOGLIA_DUTY_STRIP_STACCATA   280 //35%
#else
    #define SOGLIA_DUTY_STRIP_STACCATA  210
#endif

//x10msec
#define	TIME_ACCECAMENTO            200 //2secondi

#define TRENTA_MINUTI               1800

#endif
