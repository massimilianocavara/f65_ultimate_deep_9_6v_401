//
// AUTOMA_Eeprom.c
//
// Gestione della Eeprom emulata dall'HEFlash
//

#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_Eeprom.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "AUTOMA_DriverOptiCom.h"
#include "Main.h"
#include "HEFlash.h"
#include "AUTOMA_Taratura.h"

uint8_t cStatoEeprom;
uint8_t cTimeoutAggiornamentoRam;
tFlagEeprom uFlagEeprom;

extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFlagTimer uFlagTimer;
extern uint8_t TabellaLogica[32];
extern uint8_t TabellaLogica2[32];
extern const uint8_t TabellaLogicaROM[12];
extern union tFlagLogica uFlagLogica;
extern tFlagOptiCom uFlagDriverOptiCom;
extern tFiltrato uIngressiFilt;
extern void fAccendiArancio(void);
extern const uint8_t TabellaLogicaDEFAULT[32];
extern const uint8_t TabellaLogica2DEFAULT[20];
extern tFlagTaratura uFlagTaratura;
//*******************************************************************
//  void fInitEeprom(void)
//
//*******************************************************************
void fInitEeprom(void) {
    uint8_t i;
    uint8_t j;

#ifdef NO_INIT_TABELLA_LOGICA
    uint8_t i;
    
    for (i = IND_BUILD_LOW; i < IND_CODICE_PRODOTTO_FINITO_L; i++) {
        TabellaLogica[i] = TabellaLogicaDEFAULT[i];
    }
#else
    if (0 != HEFLASH_readBlock((void*)TabellaLogica, TAB_LOGICA, sizeof(TabellaLogica))) {
        //se c'� un qualche problema, carichiamo il DEFAULT
        for (i = 0; i < 32; i++) {
            TabellaLogica[i] = TabellaLogicaDEFAULT[i];
        }        
    }
    if (0 != HEFLASH_readBlock((void*)TabellaLogica2, TAB_LOGICA_2, sizeof(TabellaLogica2))) {
        //i primi 20 record sono di TabellaLogica2
        for (i = 0; i < 20; i++) {
            TabellaLogica2[i] = TabellaLogica2DEFAULT[i];
        }
        //i restanti 12 record sono di TabellaLogicaROM
        j = 0;
        for (i = 20; i < 32; i++) {
            TabellaLogica2[i] = TabellaLogicaROM[j++];
        }
    }
#endif
    
    //compilo i dati da TabellaLogicaROM
    TabellaLogica[IND_SERIAL_L] = TabellaLogicaROM[IND_SERIAL_L_ROM];
    TabellaLogica[IND_SERIAL_H] = TabellaLogicaROM[IND_SERIAL_H_ROM];
//    TabellaLogica[IND_WEEK] = TabellaLogicaROM[IND_WEEK_ROM];
//    TabellaLogica[IND_YEAR] = TabellaLogicaROM[IND_YEAR_ROM];
    TabellaLogica[IND_TIPO_APPARECCHIO_LG] = TabellaLogicaROM[IND_TIPO_APP_ROM];
    TabellaLogica[IND_IDSW_VER_LOW] = TabellaLogicaROM[IND_IDSW_VER_LOW_ROM];
    TabellaLogica[IND_IDSW_REL_IDSW_VER_HI] = TabellaLogicaROM[IND_IDSW_REL_IDSW_VER_HI_ROM];
       
    cTimeoutAggiornamentoRam = TEMPO_AGG_RAM;
    cStatoEeprom = ST_EE_IDLE;
    uFlagEeprom.cReg = 0;
}

//*******************************************************************
//  void fRichiediAggEE(void)
//
//  funzione: richiede scrittura di TabellaLogica in EEPROM
//  parametri: 0 no parametro, no taratura
//             1 s� parametro, no taratura
//             2 no parametro, s� taratura
//*******************************************************************
void fRichiediAggEE(uint8_t mod)
{
    RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
    RegAggiornaParametri = 0x00; 
    RegAggiornaTaratura = 0x00;
    if (1 == mod) {
        RegAggiornaParametri = RICHIESTA_PARAMETRI;
    } else {
        if (2 == mod) {
            RegAggiornaTaratura = RICHIESTA_TARATURA;
        }
    }
}

//*******************************************************************
//  void AUTOMA_Eeprom(void)
//
//*******************************************************************
void AUTOMA_Eeprom(void) {

    uFlagEeprom.cReg = 0;
    
    if (uFlagTimer.iBit.flag1minuto) {
        if (0 != cTimeoutAggiornamentoRam) {
            cTimeoutAggiornamentoRam--;
        }
    }

    switch (cStatoEeprom) {
        //++++++++++++++++++++++++++++++++++++++++++++
        case ST_EE_IDLE:
        {
            if (RICHIESTA_AGGIORNAMENTO == RegAggiornaEeprom) {
                RegAggiornaEeprom = 0x00;
                cStatoEeprom = ST_EE_AGGIORNA;
            } else {
                if (0 == cTimeoutAggiornamentoRam) {
                    cStatoEeprom = ST_EE_AGGIORNA_RAM;
                }
            }
            break;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++
        case ST_EE_AGGIORNA:
        {
            uint8_t depoAction;
            uint8_t depoPar;
            uint8_t depoTarL;
            uint8_t depoTarH;
            
            if (!uFlagDriverOptiCom.cBit.flagRxInCorso) {    
                //i bit DisabilitaEmergenza e Sleep non devono mai essere salvati in EEPROM
                //prima salvo perch� non devo modificare il valore in RAM che pu� essere utilizzato
                depoAction = TabellaLogica[IND_ACTION_REGISTER];
                // --> uActionRegister.cBit.DisabilitaEmergenza = 0 && uActionRegister.cBit.Sleep = 0
                TabellaLogica[IND_ACTION_REGISTER] &= 0b11111001;                
                if (RICHIESTA_PARAMETRI != RegAggiornaParametri) {
                    //se non � stata una scrittura di parametri, non devo sovrascriverli
                    depoPar = HEFLASH_readByte(TAB_LOGICA, IND_PARAMETRI_REG);
                    TabellaLogica[IND_PARAMETRI_REG] = depoPar;
                }                                
                RegAggiornaParametri = 0x00; 
                
                if (RICHIESTA_TARATURA != RegAggiornaTaratura) {
                    //se non � una taratura, per sicurezza non devo sovrascriverli
                    depoTarL = HEFLASH_readByte(TAB_LOGICA_2, IND_MANTENIMENTO_L);
                    depoTarH = HEFLASH_readByte(TAB_LOGICA_2, IND_MANTENIMENTO_H);
                    TabellaLogica2[IND_MANTENIMENTO_L] = depoTarL;
                    TabellaLogica2[IND_MANTENIMENTO_H] = depoTarH;
                }
                RegAggiornaTaratura = 0x00;
                
                //la scrittura richiede 2.5msec quindi potrebbe compromettere una ricezione Logica
                HEFLASH_writeBlock(TAB_LOGICA, (void*)TabellaLogica, sizeof(TabellaLogica));
                HEFLASH_writeBlock(TAB_LOGICA_2, (void*)TabellaLogica2, sizeof(TabellaLogica2));
                //ripristino valore RAM
                TabellaLogica[IND_ACTION_REGISTER] = depoAction;
                cStatoEeprom = ST_EE_IDLE;
            }   
            break;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++
        case ST_EE_AGGIORNA_RAM:
        {
            HEFLASH_readBlock((void*)TabellaLogica, TAB_LOGICA, sizeof(TabellaLogica));
            HEFLASH_readBlock((void*)TabellaLogica2, TAB_LOGICA_2, sizeof(TabellaLogica2));
            cTimeoutAggiornamentoRam = TEMPO_AGG_RAM;
            uFlagEeprom.cBit.flagAggiornaConfig = 1;
            cStatoEeprom = ST_EE_IDLE;
            break;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++
        default:
        {
            cStatoEeprom = ST_EE_IDLE;
        }
    }
}

