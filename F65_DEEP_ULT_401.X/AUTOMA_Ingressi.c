//ingressi_new
#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_Ingressi.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_AccenditoreSE.h"
#include "AUTOMA_AccenditoreSA.h"
#include "AUTOMA_Charger.h"
#include "AUTOMA_DriverOptiCom.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "MODULO_Norma.h"
#include "mcc_generated_files/pin_manager.h"

filtro fMinimaBatt;
//filtro fLedCorto;
filtro fRetePresente;
filtro fLedStaccati;
filtro fLedStaccatiSA;
filtro fReteSAPresente;
filtro f1hn3h;
filtro fReteAssente;
filtro fBatteriaInCorto;
filtro fLogicaBusPresente;
//filtro fBatteriaUNOPresente;
//filtro fBatteriaDUEPresente;
//filtro fRestMode;
//filtro fMinimaAlta;
    
tFiltrato uIngressiFilt;

extern tFlagTimer uFlagTimer;
extern misura mVbatt;
extern misura mVrete;
extern misura mIled;
extern misura mVledSA;
extern misura msel_UV;
extern misura mJumper;
extern tFlagLampada uFlagLampada;
extern tFlagFissiLampada uFlagFissiLampada;
extern tFlagAccenditoreSE uFlagAccenditoreSE;
extern union tFlagAccenditoreSA uFlagAccenditoreSA;
extern bit bContaZeriSA;

extern uint8_t cInitTimer;
extern uint16_t iSetPoint;
extern tFlagOptiCom uFlagDriverOptiCom;
extern uint8_t TabellaLogica[32];
extern tFlagCharger uFlagCharger;
extern uint16_t iDutyCompletoSE;
extern union tFlagNorma uFlagNorma;

void fFiltra(filtro * FiltroDaAgg, unsigned char valore, unsigned char numCampioni) {
    if (valore == FiltroDaAgg->cValoreFilt) //valore gi� filtrato
    {
        FiltroDaAgg->cContaCampioni = 0; //azzero il contatore
    } else //valore non filtrato
    {
        FiltroDaAgg->cContaCampioni++;
        if (FiltroDaAgg->cContaCampioni > numCampioni) {//se abbiamo gi� campionato n valori uguali, allora lo rendiamo filtrato
            FiltroDaAgg->cValoreFilt = valore;
            FiltroDaAgg->cContaCampioni = 0;
        }
    }
}

void fInitFiltri(filtro * FiltroDaInit, uint8_t valore) {
    FiltroDaInit->cContaCampioni = 0;
    FiltroDaInit->cValoreFilt = valore;
}

void fInitFiltIngressi(void) {
    fInitFiltri(&fMinimaBatt, 0); // no minima
//    fInitFiltri(&fLedCorto, 0); // no corto
    fInitFiltri(&fLedStaccati, 0); // no led staccati in SE
    fInitFiltri(&fLedStaccatiSA, 0); // no led staccati in SA
    fInitFiltri(&fBatteriaInCorto, 0); //no corto
    fInitFiltri(&fRetePresente, 0); // rete non presente
    fInitFiltri(&fReteSAPresente, 1); // rete SA presente, per evitare un reset continuo nel caso mancasse la batteria e si dovesse accendere l'SA
    fInitFiltri(&fReteAssente, 1); //rete SE assente
    fInitFiltri(&fLogicaBusPresente, 0);
    fInitFiltri(&f1hn3h, 1); //jumper 1h presente
//    fInitFiltri(&fBatteriaUNOPresente, 1); //batteria 1 presente
//    fInitFiltri(&fBatteriaDUEPresente, 1); //batteria 2 presente
//    fInitFiltri(&fRestMode, 1); //no rest mode

    uIngressiFilt.iReg = INIT_INGRESSI;
}

void AUTOMA_Ingressi(void) {

    union tActionRegister uActionRegister;
    
    if (uFlagTimer.iBit.flag20msec) {

        //****************************************************************************
        // SA
        //****************************************************************************
        fFiltra(&fReteSAPresente, 1 == bContaZeriSA, NUM_CAMPIONI);
        bContaZeriSA = 0;

        //****************************************************************************
        // Rete Presente / Rete Presente
        //****************************************************************************
        fFiltra(&fRetePresente, mVrete.iMedia > SOGLIA_PRESENZA_SE, 3);
        fFiltra(&fReteAssente, mVrete.iMedia < SOGLIA_ASSENZA_SE, 3);
                        
        //****************************************************************************
        // Batteria in CORTO
        //****************************************************************************
        fFiltra(&fBatteriaInCorto, mVbatt.iMedia < SOGLIA_BATTERIA_CORTO, NUM_CAMPIONI);
        
        //****************************************************************************
        // Jumper
        //****************************************************************************
        fFiltra(&f1hn3h, mJumper.iMedia > SOGLIA_1H, NUM_CAMPIONI);
        
    }

    if (uFlagTimer.iBit.flag10msec) {
                
        //****************************************************************************
        // MINIMA
        //****************************************************************************
        fFiltra(&fMinimaBatt, mVbatt.iMedia < SOGLIA_MINIMA, NUM_CAMPIONI);

        //****************************************************************************
        // PROTEZIONE STRIP STACCATA SA
        //****************************************************************************
        if (uFlagAccenditoreSA.cBit.LedSaAccesi) {
//            fFiltra(&fLedStaccatiSA, mVledSA.iMedia > SOGLIA_SA_STRIP_STACCATA , NUM_CAMPIONI);
            fFiltra(&fLedStaccatiSA, mVledSA.iMedia > SOGLIA_SA_STRIP_STACCATA , NUM_CAMPIONI_MOLTO_LENTI);
        } else {
            fInitFiltri(&fLedStaccatiSA, 0); //no led staccati
        }        
            
            
        //****************************************************************************
        // PROTEZIONE STRIP STACCATA SE
        //****************************************************************************            
#ifdef BELGIO
        if ((uFlagBelgio.cBit.TestAutonomiaInCorso) || (uFlagNorma.cBit.TestInCorso) || (uFlagLampada.iBit.flagTestInCorso)) 
#else
        if ((uFlagNorma.cBit.TestInCorso) || (uFlagLampada.iBit.flagTestInCorso)) 
#endif
        {
            fFiltra(&fLedStaccati, iDutyCompletoSE < SOGLIA_DUTY_STRIP_STACCATA, NUM_CAMPIONI_MOLTO_LENTI);
        } else {
            fInitFiltri(&fLedStaccati, 0); //
        }

        //****************************************************************************
        // Bus LOGICA presente
        //****************************************************************************
        fFiltra(&fLogicaBusPresente, ((mVrete.iMedia > SOGLIA_LOGICA_MIN) && (mVrete.iMedia < SOGLIA_LOGICA_MAX)), NUM_CAMPIONI);
    
    }

    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************
    //
    //NON RESETTIAMO IL REGISTRO, QUINDI OCCORRE SETTARE E RESETTARE ESPLICITAMENTE I FLAG
    //	uIngressiFilt.iReg = 0;
    //************************************************************************************************************
    //************************************************************************************************************
    //************************************************************************************************************


#ifdef NO_MINIMA
    uIngressiFilt.iBit.flagBatteriaMinimaBassa = 0;
    uIngressiFilt.iBit.flagBatteriaMinimaAlta = 0;
#else
    if (1 == fMinimaBatt.cValoreFilt) {
        uIngressiFilt.iBit.flagBatteriaMinimaBassa = 1;
    } else {
        uIngressiFilt.iBit.flagBatteriaMinimaBassa = 0;
    }
//    //------------------------------------------------------------
//    //per l'Infinity Mode
//    if (1 == fMinimaAlta.cValoreFilt) {
//        uIngressiFilt.iBit.flagBatteriaMinimaAlta = 1;
//    } else {
//        uIngressiFilt.iBit.flagBatteriaMinimaAlta = 0;
//    }
#endif
    
//    //------------------------------------------------------------
//    if (1 == fLedCorto.cValoreFilt) {
//        uIngressiFilt.iBit.flagLedCortoFilt = 1;
//    } else {
//        uIngressiFilt.iBit.flagLedCortoFilt = 0;
//    }
    //------------------------------------------------------------
    if ((1 == fLedStaccati.cValoreFilt) || (1 == fLedStaccatiSA.cValoreFilt)) {
        uIngressiFilt.iBit.flagLedStaccatiFilt = 1;
    } else {
        uIngressiFilt.iBit.flagLedStaccatiFilt = 0;
    }
    //------------------------------------------------------------
    if (1 == fLogicaBusPresente.cValoreFilt) {
        uIngressiFilt.iBit.flagLogicaBusPresente = 1;
    } else {
        uIngressiFilt.iBit.flagLogicaBusPresente = 0;
    }
    //------------------------------------------------------------
    //
    //------------------------------------------------------------
#ifdef SEMPRE_ASSENZA_RETE
    uIngressiFilt.iBit.flagRetePresenteFilt = 0;
#else
    if (1 == fRetePresente.cValoreFilt) {
        //rete PRESENTE
        uIngressiFilt.iBit.flagRetePresenteFilt = 1;
#ifdef DEB_SENS_RETE
        DISC_OPTI_SetLow();
#endif
    } else {
        if (1 == fReteAssente.cValoreFilt) {
            //rete ASSENTE
            uIngressiFilt.iBit.flagRetePresenteFilt = 0;
#ifdef DEB_SENS_RETE
            DISC_OPTI_SetHigh();
#endif
        }
    }
#endif

    //------------------------------------------------------------
    if (cInitTimer > 0) {
        if (1 == f1hn3h.cValoreFilt) {
            uFlagFissiLampada.iBit.PonticelloPresente = 1;
        } else {
            uFlagFissiLampada.iBit.PonticelloPresente = 0;
        }
        uActionRegister.cReg = TabellaLogica[IND_ACTION_REGISTER];
        if ((0 == uActionRegister.cBit.nAutonomiaInBaseAiJumper) && (1 == cInitTimer)) {
            //jumper presente -> 1H
            //jumper assente  -> 3H
            uFlagFissiLampada.iReg &= RESET_AUTONOMIE; //resetto
            if (uFlagFissiLampada.iBit.PonticelloPresente) {
                uFlagFissiLampada.iBit.Potenza1H = 1;
            } else {
                uFlagFissiLampada.iBit.Potenza3H = 1;
            }
        }
    }   
        
    if (1 == fReteSAPresente.cValoreFilt) {
        uIngressiFilt.iBit.flagReteSAPresente = 1;
    } else {
        uIngressiFilt.iBit.flagReteSAPresente = 0;
    }    

    //------------------------------------------------------------
    if (1 == fBatteriaInCorto.cValoreFilt) {
        uIngressiFilt.iBit.flagBatteriaInCorto = 1;
    } else {
        uIngressiFilt.iBit.flagBatteriaInCorto = 0;
    }

    }
    
