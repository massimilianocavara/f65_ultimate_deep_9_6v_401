/* 
 * File:   AUTOMA_Comunicazione.h
 * Author: cavaram
 *
 * Created on 20 gennaio 2016, 10.18
 */

#ifndef AUTOMA_COMUNICAZIONE_H
#define	AUTOMA_COMUNICAZIONE_H

//STATI DRIVER RICEZIONE OPTICOM
#define ST_OPTICOM_MEDIA_INIZIALE               0
#define ST_OPTICOM_NORMALE                      1
#define ST_OPTICOM_RICERCA_IN_DECREMENTO        2
#define ST_OPTICOM_RICERCA_IN_INCREMENTO        3
#define ST_OPTICOM_ERRORE                       4
#define ST_OPTICOM_RX_STARTB_1HALF              5
#define ST_OPTICOM_RX_STARTB_2HALF              6
#define ST_OPTICOM_RX_BIT_1HALF_NO_FLASH        7
#define ST_OPTICOM_RX_BIT_1HALF_FLASH           8
#define ST_OPTICOM_RX_BIT_2HALF_NO_FLASH        9
#define ST_OPTICOM_RX_BIT_2HALF_FLASH           10
#define ST_OPTICOM_RX_PARITY_1HALF_FLASH        11
#define ST_OPTICOM_RX_PARITY_1HALF_NO_FLASH     12
#define ST_OPTICOM_RX_PARITY_2HALF_FLASH        13
#define ST_OPTICOM_RX_PARITY_2HALF_NO_FLASH     14
#define ST_OPTICOM_RX_STOP_BIT_1HALF_NO_FLASH   15
#define ST_OPTICOM_RX_STOP_BIT_2HALF_FLASH      16
#define ST_OPTICOM_INIT                         17
#define ST_OPTICOM_DISABLE                      18
#define ST_OPTICOM_WAIT_START_BIT               19
#define ST_OPTICOM_WAIT_TERMINE_CMD             20
#define ST_OPTICOM_TX_DONE_INIT                 21
#define ST_OPTICOM_TX_DONE_ON                   22
#define ST_OPTICOM_TX_DONE_END                  23
#define ST_OPTICOM_TX_NOT_EN_INIT               24
#define ST_OPTICOM_TX_NOT_EN_ON                 25
#define ST_OPTICOM_TX_NOT_EN_END                26
#define ST_OPTICOM_TX_NOT_IMPL_INIT             27
#define ST_OPTICOM_TX_NOT_IMPL_ON               28
#define ST_OPTICOM_TX_NOT_IMPL_END              29
#define ST_OPTICOM_TX_PROT_CORTO                30


    
//200MSEC di durata del semi-bit
#define SPEED_RX                200 //msec
#define NUM_MINIMO_CAMP         4 //>4 campioni


#define BIT_FRAME_CMD           8
#define BIT_FRAME_SHORT         7 //fallimento all'ottavo bit di comando, quindi 7 buoni, potrebbe essere un comando vecchio a 5 bit
#define FINE_FRAME_DATA0        10
#define FINE_FRAME_DATA1        15


//#define METABIT             3   // 40msec
//#define TIMEBIT             9  // 9+1 = 100msec

//flag 1sec
#define TIME_10SEC                  10 //10sec
#define TIME_ATTESA                 2 //2sec


//Label DAC1
//vdac = x% * 2.048V = xxxxV

#define LIVELLO_MIN_DAC             38 //per tenere conto della Vcesat massima
#define LIVELLO_MAX_DAC             255
#define DAC_7_PERCENTO              18 //143mV
#define DAC_15_PERCENTO             38 //307mV
#define DAC_30_PERCENTO             77 //
#define DAC_48_8_PERCENTO           125
#define DAC_50_PERCENTO             127
#define DAC_58_PERCENTO             150
#define DAC_75_PERCENTO             191
#define DAC_90_PERCENTO             231
#define DAC_80_PERCENTO             199

#define SENS_HI                     100 //1%
#define SENS_MEDIUM                 50  //2%
#define SENS_LO                     20  //5%


//#define SENS_ULTRAHI                67 //1.5%
//#define SENS_HI                     50 //2%
//#define SENS_LO                     50 //2%
//#define SENS_MEDIUM                 50 //2%

//#define SENS_HI                     40 //2.5%
//#define SENS_LO                     10 //10%
//#define SENS_MEDIUM                 20 //5%

#define CAMPIONI_SEMIBIT_MIN        5
#define CAMPIONI_SEMIBIT_MAX        16
#define CAMPIONI_SEMIBIT_MEDIUM     12
#define CAMPIONI_FRONTE             1
#define INIT_TMR1                   0b01110000 //prescal 1:8

//TMR6 conta 500nsec -> overflow ogni 127.5usec
//syncro 50Hz o NO_RETE -> periodo = 20msec -> conteggio max 40000
//syncro 60Hz -> periodo = 16.666msec -> conteggio max 33333
#define VAL_SETPOINT_LUM_60HZ       16500
#define VAL_MIN_LUM_60HZ            6150
#define VAL_MAX_LUM_60HZ            26800

//unit� di 0.5usec
#define VAL_SETPOINT_LUM            20000 //10msec
#define VAL_MIN_LUM                 7500
#define VAL_MAX_LUM                 32500

#define RANGE_INIZIALE              300 //1%
#define RANGE_INIT_MIN              19500
#define RANGE_INIT_MAX              20500

#define GAP_COMP                    2000

// BUFFER DI MEDIA da 32 CAMPIONI
#define PASSI_SHIFT                 5 // 32 CAMPIONI -> riempimento buffer in 32*20msec=640msec

#define TIME_RITARATURA_LUNGA       1000 //msec
#define TIME_RITARATURA_MEDIA       500 //msec
#define TIME_RITARATURA_CORTA       200 //msec

//#define TIME_WAIT                   500 //msec
#define TIME_WAIT                   1000 //msec

//#define NUM_MINIMO_CAMPIONI         50 //# min di campioni per fermare la modifica del DAC
//#define NUM_CAMP_PER_STAB           25
//velocizzo perch� ora � troppo lento
//    #define NUM_MINIMO_CAMPIONI         5 //# min di campioni per fermare la modifica del DAC
//    #define NUM_CAMP_PER_STAB           5
#define NUM_MINIMO_CAMPIONI         1 //# min di campioni per fermare la modifica del DAC
#define NUM_CAMP_PER_STAB           1

#define TIMEOUT_COM                 1005
#define TIMEOUT_BIT                 205

#define INIT_TMR6                   0b00000001 //no postscaler, tmr OFF, prescaler 1:4 => 32MHz/4/4 = 2MHz -> Tinc = 500nsec
//#define INIT_TMR6                   0b00000000 //no postscaler, tmr OFF, prescaler 1:1 => 32MHz/4 = 8MHz -> Tinc = 125nsec

//COMPENSAZIONE
#define INIT_TMR2_COMPENS           0b00000100 //TMR2 ON, PRESCALER 1:1  ->otteniamo 32KHz con 10bit di risoluzione
#define	PERIOD_PWM_COMPENS          249 // ->64KHz
#define DUTY_TOT_INIT_COMPENS       300 // 30%
#define DUTY_TOT_MAX_COMPENS        990

#define DELAY_COMPENS               3

//stati COMPENSATORE
#define ST_COMP_INIT            0
#define ST_COMP_RICERCAUP       1
#define ST_COMP_RICERCADOWN     2
#define ST_COMP_STABILE         3

////tempi ACK e NACK
//#define TM_LED_ON_DONE          200
//#define TM_LED_ON_NOT_EN        500
//#define TM_LED_ON_NOT_IMPL      2000
//#define TM_LED_OFF              1000
//#define TM_LED_OFF_INTRA        200
//
////Duty delle diverse risposte, PERIOD_PWM = 199 ->40KHz
////(0-800)
//#define DUTY_ACK                    400
//#define DUTY_NOT_EN                 300
//#define DUTY_NOT_IMPL               200

//tempi ACK e NACK
//#define TM_LED_ON_DONE          200
#define TM_LED_ON_DONE          3000
#define TM_LED_ON_NOT_EN        500
#define TM_LED_ON_NOT_IMPL      500
#define TM_LED_OFF_SA           250
#define TM_LED_OFF_SE           0
#define TM_LED_OFF_INTRA        500

//Duty delle diverse risposte, PERIOD_PWM = 199 ->40KHz
//(0-800)
//#define DUTY_ACK                    500
//#define DUTY_NOT_EN                 100
//#define DUTY_NOT_IMPL               100
#define DUTY_ACK                    400
#define DUTY_NOT_EN                 100
#define DUTY_NOT_IMPL               100

//------------------------------------------
//  Buffer di comunicazione
//------------------------------------------
union tBufferComunicazione {
    uint32_t lReg;

    struct {
        uint8_t byte4; // Tx Logica: comando a 8 bit per la lampada Logica
        uint8_t byte3; // Tx Logica: comando a 8 bit per la lampada Logica
        uint8_t byte2; // Tx Logica: indirizzo a 16 bit per la lampada Logica (byte low)
        uint8_t byte1; // Tx Logica: indirizzo a 16 bit per la lampada Logica (byte hi)
    } lTx;

    struct {
        uint8_t byte4; // Rx Logica: dato della lampada Logica (non utilizzato)
        uint8_t byte3; // Rx Logica: dato della lampada Logica
        uint8_t byte2; // Rx Logica: indirizzo a 16 bit della lampada Logica (byte low)
        uint8_t byte1; // Rx Logica: indirizzo a 16 bit della lampada Logica (byte hi)
    } lRx;
};

//------------------------------------------
//------------------------------------------
typedef union
{
    uint8_t cReg;
    struct
    {
        uint8_t flagFaseStabilizzazione: 1; //0
        uint8_t flagRxInCorso: 1; //1
        uint8_t flagBufferDisponibile: 1; //2
        uint8_t flagErroreCom : 1; //3 unifico qui tutti gli errori di comunicazione, anche parit� e stopbit
        uint8_t AckInviato: 1; //4
        uint8_t TxInCorso: 1; //5
        uint8_t OldProtocol: 1; //6
        uint8_t NackInviato: 1; //7
    } cBit;
} tFlagOptiCom;

//------------------------------------------
//------------------------------------------
typedef union
{
    uint8_t cReg;
    struct
    {
        uint8_t delay : 3; //0,1,2
        uint8_t FinecorsaAlto: 1; //3
        uint8_t FinecorsaBasso: 1; //4
    } cBit;
} tFlagCompensazione;

#endif	/* AUTOMA_COMUNICAZIONE_H */

