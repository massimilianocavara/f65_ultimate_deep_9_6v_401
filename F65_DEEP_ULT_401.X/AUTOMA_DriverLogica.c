#include <xc.h>
#include "global.h"
#include "AUTOMA_DriverLogica.h"
#include "Timer.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "AUTOMA_Ingressi.h"
#include "Bootloader.h"
#include "mcc_generated_files/pin_manager.h"


uint8_t cStatoDriverLogica;
union tFlagDriverLogica uFlagDriverLogica;

// Variabili driver seriale RS232
uint8_t cBufferRS232[SIZE_BUFFER_RS232];
uint8_t cTestaBufferRS232;
uint8_t cCodaBufferRS232;
union tRS232 uRS232; //usato dalle routine di interrupt che gestiscono la seriale
uint16_t iTimerResetRS232;
uint8_t cTimerEndTxRS232;
uint8_t cTimeoutRxRS232;
uint8_t cChecksum;
uint8_t cTimerLogica;
union tFrameRS232 uFrameRS232;

//union tFlagSeriale uFlagSeriale;
uint8_t cTimerSerialeOn; //fino a quando non scade, � possibile la programmazione da seriale, poi diventa REST-MODE

// Variabili OPTICOM/LOGICA
extern union tFlagLogica uFlagLogica;
extern tFlagTimer uFlagTimer;
extern uint8_t TabellaLogica[32];
extern tFiltrato uIngressiFilt;
extern uint8_t cBufferLogicaIn[SIZE_BUFFER_LOGICA_IN];
extern uint8_t cBufferLogicaOut[SIZE_BUFFER_LOGICA_OUT];
extern uint8_t cLengthBufferLogica;

extern union tStatusByte uStatusByte;
extern union tStatusByte2 uStatusByte2;
extern union tErrorRegister uErrorRegister;
extern union tSaStatusRegister uSaStatusRegister;
extern union tFlagLogica uFlagLogica;

#ifdef DEBUG_LED_SIGNAL
    extern tByteDebug uDebug;
#endif

extern void fAggiornaConfigDaTabellaLogica(void);
extern uint8_t fUtilizzaBufferLogica(uint8_t idAutoma);
extern void fRilasciaBufferLogica(uint8_t idAutoma);
extern void fInitFiltri(filtro * FiltroDaInit, uint8_t valore);

//******************************************************************************
// Configurazione hardware
// RC7->EUSART:RX
// RC6->EUSART:TX
//******************************************************************************

//******************************************************************************
// FUNZIONI DI INIZIALIZZAZIONE
//******************************************************************************
void UART_Disable(void) {
    SPBRG = 0;
    PIE1bits.TXIE = 0;
    PIE1bits.RCIE = 0;
    RC1STA = 0x00;
    TX1STA = 0x00;    
}
//------------------------------------------------------------------------------
void UART_Initialize(void) {
    //--------------------------------------------------------------------------------
    // Configurazione UART (Baud rate = 57600) per la comunicazione con il modulino
    //--------------------------------------------------------------------------------
    SPBRGH = 0x00; // Clock 32MHz -> Baud rate = 57550, errore = -0.08% (SYNC = 0, BRGH = 1, BRG16 = 1) -> SPBRGH2:SPBRG2 = 138 = 0x008A
    SPBRG = 0x8A;
//    SPBRGH = 0x00; // Clock 8MHz -> Baud rate = 57K14, errore = -0.79% (SYNC = 0, BRGH = 1, BRG16 = 1) -> SPBRGH2:SPBRG2 = 34 = 0x0022
//    SPBRG = 0x22;    

    //         76543210
    RC1STA = 0b10010000; // RC1STA:
    // <7> SPEN		: (1) Serial Port Enable bit
    // <6> RX9		: (0) 9-bit Receive Enable bit
    // <5> SREN		: (-) Single Receive Enable bit (don?t care)
    // <4> CREN		: (1) Continuous Receive Enable bit
    // <3> ADDEN	: (0) Address Detect Enable bit
    // <2> FERR		: (0) Framing Error bit
    // <1> OERR		: (0) Overrun Error bit
    // <0> RX9D		: (0) 9th bit of Received Data is the Parity bit

    //         76543210
    TX1STA = 0b00100110; // TX1STA:
    // <7> CSRC		: (-) Clock Source Select bit (don't care)
    // <6> TX9		: (0) 9-bit Transmit Enable bit
    // <5> TXEN		: (1) Transmit Enable bit
    // <4> SYNC		: (0) USART Mode Select bit
    // <3> SENDB	: (0) Sync Break transmission completed
    // <2> BRGH		: (1) High Baud Rate Select bit
    // <1> TRMT		: (0) Transmit Shift Register Status bit
    // <0> TX9D		: (1) 9th bit of Transmit Data, can be Parity bit

    //           76543210
    BAUD1CON = 0b01001000; // BAUD1CON (Baud rate control register):
    // <7> ABDOVF	: (0) Auto-Baud Detect Overflow bit
    // <6> RCIDL	: (1) Receive Idle Flag bit (idle)
    // <5> DTRXP	: (0) Data/Receive Polarity Select bit
    // <4> CKTXP	: (0) Clock/Transmit Polarity Select bit
    // <3> BRG16	: (1) 16-bit Baud Rate Generator bit
    // <2> -			: (0)
    // <1> WUE		: (0) Wake-up Enable bit
    // <0> ABDEN	: (0) Auto-Baud Detect Enable bit

    // Imposto gli interrupt della seriale RS232
    PIE1bits.RCIE = 1;
    PIE1bits.TXIE = 0;
}

//******************************************************************************
// VETTORI DI INTERRUPT
//******************************************************************************
void EUSART_Transmit_ISR(void) {

    if (NotBufferRS232Empty()) {
        TX1REG = GetChFromBufferRS232();
    } else {
        // Sono appena stati trasmessi tutti i pacchetti, per cui disabilito la seriale in Tx
        PIE1bits.TXIE = 0;

        // Aspetto 2 msec prima di girare la seriale in Rx, per essere sicuro di trasmettere correttamente l'EOF
        cTimerEndTxRS232 = PTIM_END_TX_RS232;
        uRS232.cBit.EnableEndTx = 1;

        ResetBufferRS232();
    }    
}

void EUSART_Receive_ISR(void) {
    
    uint8_t cRxChRS232;

    // Overrun Error bit
    if (RC1STAbits.OERR) {
        RC1STAbits.CREN = 0;
        RC1STAbits.CREN = 1;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    // Disabilito la routine di gestione della RS232 e segnalo che � in corso una ricezione di un frame
    // Precarico il timer cTimeoutRxRS232 che mi reinizializza il driver seriale RS232 in ricezione nel caso arrivi solo un frame parziale (sicurezza)
    // Precarico il timer iTimerResetRS232 che mi reinizializza il driver seriale RS232 in ricezione per inattivit�di 30 sec (sicurezza)
    //-----------------------------------------------------------------------------------------------------------------------------
    cTimeoutRxRS232 = PTIM_TIMEOUT_RX_RS232;
    uRS232.cBit.RxInCorso = 1;
    iTimerResetRS232 = PTIM_RESET_RS232; //30secondi
    //-----------------------------------------------------------------------------------------------------------------------------

    cRxChRS232 = RC1REG;

    PutChInBufferRS232(cRxChRS232);

    if (1 == cTestaBufferRS232) {
        if (cBufferRS232[0] < 6) {
            ResetBufferRS232();
            uRS232.cBit.RxInCorso = 0;
        }
    }   // #b# aggiunta
    else if (cTestaBufferRS232 >= cBufferRS232[0])
    {
        RC1STAbits.CREN = 0;
        PIE1bits.RCIE = 0;                                  // Disabilito l'interrupt in Rx
        uRS232.cBit.NewFrameArrived = 1;
    }       
}

//******************************************************************************
// void fEnableSeriale(void)
//
// configura la seriale
//******************************************************************************
void fInitSeriale(void) {
    
    UART_Initialize();
    
    RC6PPSbits.RC6PPS = 0b00010100;
    RXPPSbits.RXPPS = 0b00010111;
    
    RX_LG_SetDigitalInput();
    TX_LG_SetDigitalOutput();
    
    cTestaBufferRS232 = 0;
    cCodaBufferRS232 = 0;

    // Disabilito la trasmissione RS232
    uRS232.cBit.EnableEndTx = 0;

    // #b# aggiunta
    uRS232.cBit.RxInCorso = 0;
    uRS232.cBit.NewFrameArrived = 0;
    
    iTimerResetRS232 = PTIM_RESET_RS232;
}

//------------------------------------
void fLgTransToIdle(void) {
    fInitSeriale();   
    cStatoDriverLogica = ST_DRVLG_IDLE;
}

//------------------------------------
void fInitAutomaDriverLogica(void) {
    fLgTransToIdle();
}

//--------------------------------------------------------------
//--------------------------------------------------------------
void fPredisponiRestMode(void) {
    UART_Disable();
    //RC6 e RC7 pin digitali
    RC6PPSbits.RC6PPS = 0b00000000; // RC6 pin digitale
    RC7PPSbits.RC7PPS = 0b00000000; // RC7 pin digitale

    RX_LG_SetDigitalInput();
    RX_LG_SetPullup();

    TX_LG_SetHigh();
    TX_LG_SetDigitalOutput();
}



//******************************************************************************
//******************************************************************************
//******************************************************************************
//******************************************************************************
void AUTOMA_DriverLogica(void) {
    
    uFlagDriverLogica.cReg = 0;
    
    if (uFlagTimer.iBit.flag1minuto) {
        if (cTimerSerialeOn) {
            cTimerSerialeOn--;
        }
    }
    
    if (uFlagTimer.iBit.flag1msec) {

        //------------------------------------------------------------------------------
        // Gestione timer di sicurezza su SERIALE
        //------------------------------------------------------------------------------
        if (0 != cTimeoutRxRS232) {
            cTimeoutRxRS232--;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------
        // Reset driver RS232 per inattivit� della seriale di 30 secondi (sicurezza)
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------
        if (0 != iTimerResetRS232) {
            iTimerResetRS232--;
        }
        //------------------------------------------------------------------------------
        // Gestione timer TX
        //------------------------------------------------------------------------------
        if (0 != cTimerEndTxRS232) {
            cTimerEndTxRS232--;
        }
        
        if (0 != cTimerLogica) {
            cTimerLogica--;
        }
        
    }

    switch (cStatoDriverLogica) {        

        //**********************************************************************
        // 
        //**********************************************************************
        case ST_DVRLG_REST_MODE_IDLE:
        {
            fPredisponiRestMode();

            if (!uIngressiFilt.iBit.flagRetePresenteFilt) {
                if (1 == uIngressiFilt.iBit.flagRM) {
                    cStatoDriverLogica = ST_DVRLG_REST_MODE_ATTIVO;
                }
            }           
            break;
        }

        //**********************************************************************
        // 
        //**********************************************************************
        case ST_DVRLG_REST_MODE_ATTIVO:
        {
            if (uIngressiFilt.iBit.flagRetePresenteFilt) {
                cStatoDriverLogica = ST_DVRLG_REST_MODE_IDLE;
            } else {
                if (0 == uIngressiFilt.iBit.flagRM) {
                    uFlagDriverLogica.cBit.RestModeOFFDaPenna = 1;
                }
            }
            break;
        }

        //**********************************************************************
        // 
        //**********************************************************************
        case ST_DRVLG_IDLE:
        {
            fRilasciaBufferLogica(ID_DRV_LG); //libero il buffer di Logica
            
            if (0 == cTimerSerialeOn) {
                fPredisponiRestMode();
                cStatoDriverLogica = ST_DVRLG_REST_MODE_IDLE;
            } else {
                if (uRS232.cBit.RxInCorso) {
                    // Precarico il timer cTimeoutRxRS232 che mi reinizializza il driver seriale RS232 in ricezione nel caso arrivi solo un frame parziale (sicurezza)
                    // Precarico il timer iTimerResetRS232 che mi reinizializza il driver seriale RS232 in ricezione per inattivit� di 30 sec (sicurezza)
                    cTimeoutRxRS232 = PTIM_TIMEOUT_RX_RS232;
                    iTimerResetRS232 = PTIM_RESET_RS232;
                    cStatoDriverLogica = ST_DRVLG_RX;
                }
            }
            break;
        }
        
        //**********************************************************************
        // 
        //**********************************************************************
        case ST_DRVLG_RX:
        {
            uint8_t i;
 
            if (0 == cTimeoutRxRS232) { // S� -> ripristino il driver seriale RS232 in ricezione
                fLgTransToIdle();
            } else {
                if (!uRS232.cBit.RxInCorso) {
                    fLgTransToIdle();
                } else {
                    if (uRS232.cBit.NewFrameArrived) {
                        // E' arrivato un frame -> procedo con il parse
                        // Lunghezza frame Logica standard = 6 bytes
                        // Lunghezza frame Logica esteso > 6 bytes
                        
                        //resetto i flag della seriale
                        uRS232.cBit.NewFrameArrived = 0;
                        uRS232.cBit.RxInCorso = 0;
                        
                        // Verifico Checksum corretto:
                        // 1) calcolo del checksum del frame ricevuto
                        cChecksum = 0;
                        for (i = 1; i < (cTestaBufferRS232 - 1); i++) {
                            cChecksum ^= cBufferRS232[i];
                        }
                        // 2) recupero il checksum ricevuto per confronto
                        // La checksum � corretta ?
                        if (cChecksum == cBufferRS232[cTestaBufferRS232 - 1]) { // S�
                            //verifico se il Bufferlogica sia libero, altrimenti mi metto in attesa
                            if (fUtilizzaBufferLogica(ID_DRV_LG))
                            {
                                for (i = 0; i < (cTestaBufferRS232 - 2); i++)
                                {
                                    //tolgo gli orpelli della seriale, cio� byte iniziale con il #byte inviati, e checksum finale gi� controllata
                                    cBufferLogicaIn[i] = cBufferRS232[i+1];                                    
                                }
                                cLengthBufferLogica = (cTestaBufferRS232 - 2);                               
                                uFlagDriverLogica.cBit.ParseNewFrame = 1; //segnalo nuovo messaggio da decodificare
                                cTimerLogica = TIMEOUT_PARSE;
                                cStatoDriverLogica = ST_DRVLG_WAIT_PARSE;
                            } else {
                                cTimeoutRxRS232 = PTIM_ATTESA_BUFFER;
                                cStatoDriverLogica = ST_DRVLG_ATTESA_BUFFER;
                            }
                        }
                    }
                }
            }
            break;
        }
        //**********************************************************************
        // 
        //**********************************************************************
        case ST_DRVLG_ATTESA_BUFFER:
        {
            uint8_t i;
            
            if (0 == cTimeoutRxRS232) { // S� -> ripristino il driver seriale RS232 in ricezione
                fLgTransToIdle();
            } else {
                if (fUtilizzaBufferLogica(ID_DRV_LG))
                {                   
                    for (i = 0; i <= (cTestaBufferRS232 - 2); i++)
                    {
                        //tolgo gli orpelli della seriale, cio� byte iniziale con il #byte inviati, e checksum finale gi� controllata
                        cBufferLogicaIn[i] = cBufferRS232[i+1];
                    }
                    cLengthBufferLogica = (cTestaBufferRS232 - 2); 
                    uFlagDriverLogica.cBit.ParseNewFrame = 1; //segnalo nuovo messaggio da decodificare
                    cTimerLogica = TIMEOUT_PARSE;
                    cStatoDriverLogica = ST_DRVLG_WAIT_PARSE;
                }
            }
            break;
        }

        //**********************************************************************
        // 
        //**********************************************************************
        case ST_DRVLG_WAIT_PARSE:
        {
            uint8_t i;
            
            if (0 == cTimerLogica) {
                //scaduto il tempo di attesa, torno in Idle
                fLgTransToIdle();
            } else {
                if (uFlagLogica.iBit.TxRisposta) {
                    if (uFlagLogica.iBit.RispondiACK) {
                        //caso particolare di risposta al modulino
                        cBufferRS232[0] = 2;
                        cBufferRS232[1] = 0;
                        cTestaBufferRS232 = 2;
                    } else {
                        // 1) Calcolo il Checksum di BufferLogicaOut
                        cChecksum = 0;
                        for (i = 0; i < cLengthBufferLogica; i++) {
                            cChecksum ^= cBufferLogicaOut[i];
                        }
                        // 2) carico BufferLogicaOut nel buffer di tx RS232, tenendo conto che il byte 0 conterr� il #byte
                        for (i = 1; i < (cLengthBufferLogica + 1); i++) {
                            cBufferRS232[i] = cBufferLogicaOut[i-1];
                        }
                        // 3) aggiorno la testa del buffer 
                        cTestaBufferRS232 = (cLengthBufferLogica + 1);
                        // 4) inserisco la Checksum e il �byte
                        cBufferRS232[cTestaBufferRS232++] = cChecksum;
                        cBufferRS232[0] = cTestaBufferRS232;
                    }

                    //attivo periferica di trasmissione
                    PIE1bits.RCIE = 0;
                    TX1STAbits.SYNC = 0;
                    RC1STAbits.SPEN = 1;
                    TX1STAbits.TXEN = 1;
                    PIE1bits.TXIE = 1;  
                    
                    iTimerResetRS232 = PTIM_RESET_RS232;
                    cStatoDriverLogica = ST_DRVLG_TX;                          
                }
            }
            break;
        }
        //**********************************************************************
        // 
        //**********************************************************************
        case ST_DRVLG_TX:
        {
            if (0 == iTimerResetRS232) {
                fLgTransToIdle();
            } else {           
                if (uRS232.cBit.EnableEndTx) { // S� -> aspetto 2 msec prima di girare la seriale in Rx per essere sicuro di trasmettere correttamente l'ultimo byte
                    // Il tempo � scaduto ?
                    if (0 == cTimerEndTxRS232) { // S� -> reinizializzo il driver seriale in Rx
                        fLgTransToIdle();
                    }
                }
            }
            break;
        }
        
        //**********************************************************************
        // 
        //**********************************************************************
        default:
        {
            fLgTransToIdle();
        }
        //**********************************************************************
    }    
}






