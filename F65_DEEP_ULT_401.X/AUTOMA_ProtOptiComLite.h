/* 
 * File:   AUTOMA_Protocollo.h
 * Author: cavaram
 *
 * Created on 22 gennaio 2016, 17.41
 */

#ifndef AUTOMA_PROTOCOLLO_H
#define	AUTOMA_PROTOCOLLO_H

//STATI FOTONICI
#define ST_PROT_OPTICOM_COMPLETO        0
#define ST_PROT_OPTICOM_LIMITATO        1
#define ST_PROT_OPTICOM_DISABLE         2
#define ST_PROT_OPTICOM_TX_ACK          3
#define ST_PROT_OPTICOM_TX_NACK         4

//COMANDI OPTICOM
//1
#define SET_FUNZIONALITA_SA             0b00001000  //2
#define SET_FUNZIONALITA_PS             0b00001110  //3
#define SET_FUNZIONALITA_SE             0b00010000  //4
#define START_TEST_FUNZIONALE           0b00010110  //5
#define START_TEST_AUTONOMIA            0b00011010  //6
#define STOP_TEST                       0b00011100  //7
//8
#define DISABILITA_OPTICOM              0b00100110  //9
//10
#define CANCELLA_ERRORI                 0b00101100  //11
#define SINCRONIZZA_TEST_AUTO           0b00110010  //12
#define SET_TEST_1H                     0b00110100  //13
#define SET_TEST_DURATA_AUTONOMIA       0b00111000  //14
#define SET_DISPARI                     0b00111110  //15
#define SET_PARI                        0b01000000  //16
//17
//18
//19
//20
#define ATTIVA_EMERGENZA_LOCALE         0b01010100  //21
#define DISATTIVA_EMERGENZA_LOCALE      0b01011000  //22
#define RESET_FABBRICA                  0b01011110  //23
//24
#define REST_MODE_OFF                   0b01100100  //25
#define SET_AUTONOMIA_8H                0b01101000  //26
#define SET_AUTONOMIA_3H                0b01101110  //27
#define SET_AUTONOMIA_2H                0b01110000  //28
#define SET_AUTONOMIA_1H5               0b01110110  //29
#define SET_AUTONOMIA_1H                0b01111010  //30
//31
//
//COMANDI COVID ad 8bit
#define SANIFICATORE_OFF                0
#define SANIFICATORE_ON                 1
#define VEL_VENTOLA_ALTA                2
#define VEL_VENTOLA_MEDIA               3
#define VEL_VENTOLA_BASSA               4
#define SANIFICATORE_PROFILO_24H        5
#define SANIFICATORE_PROFILO_12H        6
#define SANIFICATORE_PROFILO_8H         7

//***********************************************************************
//***********************************************************************
// TABELLA OPTICOM
//***********************************************************************
//***********************************************************************

////Indici
//#define IND_VELOCITA_RX             0 //velocit� ricezione
//#define IND_VELOCITA_TX_UNO         1 //velocit� trasmissione
//#define IND_VELOCITA_TX_ZERO        2
//#define IND_SECURE_L                3 //codice di sicurezza L
//#define IND_SECURE_H                4 //codice di sicurezza H
//#define IND_CADENZA_FUNZ_L          5
//#define IND_CADENZA_FUNZ_M          6
//#define IND_CADENZA_FUNZ_H          7
//#define IND_CADENZA_AUTO_L          8
//#define IND_CADENZA_AUTO_M          9
//#define IND_CADENZA_AUTO_H          10
//#define IND_EMERG_PROLUNG_L         11
//#define IND_EMERG_PROLUNG_M         12
//#define IND_EMERG_PROLUNG_H         13
//#define IND_LUMINOSITA              14
//#define IND_CONFIG_L                15
//#define IND_CONFIG_H                16
//#define IND_V_ROTARY_L              17 //rotary L
//#define IND_V_ROTARY_H              18 //rotary H
//#define IND_DURATA_FUNZ_L           19
//#define IND_DURATA_FUNZ_M           20
//#define IND_DURATA_FUNZ_H           21
//#define IND_CODICE_PRODOTTO_FINITO_L             22
//#define IND_CODICE_PRODOTTO_FINITO_H             23
//#define IND_DELAY_TEST_L            24
//#define IND_DELAY_TEST_M            25
//#define IND_DELAY_TEST_H            26



#define SPEED_TX_UNO            33
#define SPEED_TX_ZERO           33
//#define SPEED_TX_UNO            50
//#define SPEED_TX_ZERO           50
//#define GAP_TX                  (SPEED_TX_ZERO - SPEED_TX_UNO)

#define SECURE_L                0x15
#define SECURE_H                0x15

#define LUM_SA                  14
#define LUM_PS                  1
#define LUM_OFF                 0


//----------------------------------------------------------------
//----------------------------------------------------------------
//dati del comando
#define NESSUN_PARAMETRO    0
#define UN_PARAMETRO        1
#define DUE_PARAMETRI       2

//valori default
#define INIT_STATO_AND      0xFF00
#define INIT_STATO_OR       0x0461
#define COD_SICUR_DEFAULT   365

//valori default dei test
#define GG_CADENZA_AUTONOMIA    182
#define GG_CADENZA_FUNZIONALE   28

//posizione tabelle in HE Flash
//#define TAB_BOOT            0
//#define TAB_DALI            1
//#define TAB_LOGICA          2
//#define TAB_LOGICA2         3

typedef union {
    uint16_t iReg;
    struct {
        uint16_t TestFunzionaleDaOpti: 1; //0
        uint16_t TestAutonomiaDaOpti: 1; //1
        uint16_t StopTest: 1; //2
        uint16_t AggiornaTabLGInEeprom: 1; //3
        uint16_t RestModedaOpti: 1; //4
        uint16_t OpticomDisabilitato: 1; //5
        uint16_t OpticomCompleto: 1; //6
        uint16_t OpticomLimitato: 1; //7
        uint16_t TrasmettiDone: 1; //8
        uint16_t AggiornaEeprom: 1; //9
        uint16_t AccendiSanificatore : 1; //10
        uint16_t SpegniSanificatore : 1; //11
        uint16_t TrasmettiNotEnabled: 1; //12
        uint16_t TrasmettiNotImplemented: 1; //13
    } iBit;
} FlagProtOptiCom_t;



typedef union {
    uint16_t iReg;
    struct {
        uint16_t TestInCorso: 1; //0
        uint16_t EmInCorso: 1; //1
        uint16_t RicRapida: 1; //2
        uint16_t StatoBatteria: 1; //3
        uint16_t PariDispari: 1; //4
        uint16_t AutonomiaL: 1; //5
        uint16_t AutonomiaM: 1; //6
        uint16_t AutonomiaH: 1; //7
        uint16_t SetDurataTest: 1; //8
        uint16_t TestInibito: 1; //9
        uint16_t TestRimandato: 1; //10
        uint16_t ModFunzL: 1; //11
        uint16_t ModFunzH: 1; //12
        uint16_t ErrBatt: 1; //13
        uint16_t ErrLed: 1; //14
        uint16_t ErrAuton: 1; //15
    } iBit;
} tStato;

#define RESET_A_PARTE_SA    0x1800      // escludo ModFunzH:ModFunzL

#endif	/* AUTOMA_PROTOCOLLO_H */

