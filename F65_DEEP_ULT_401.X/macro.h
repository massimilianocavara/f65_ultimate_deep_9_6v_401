/******************************************************************
*  Definizioni delle macro utilizzate in vari software.						
*******************************************************************
*																																	
*  Filename:	   macro.h	       	                                
*  Date:  			29/01/2004	(v. 1.0)                              
*						23/02/2007				                      							
*  File Version: 	1.6	                                            
*																																	
*  Author:			Giuseppe Modestino					                      
*																																	
*******************************************************************/

#ifndef __MACRO_H
#define __MACRO_H

#ifndef XTAL_FREQ
#define XTAL_FREQ			8000
#endif

//*********************
// Definizione macro
//*********************

#define hi(x)	(*(((char*)&x)+1))
#define low(x)	(*((char*)&x))

#define hi16(x)  (unsigned int)(x>>16)
#define low16(x) (unsigned int)(x & 0xFFFF)

#define lo_b0(x)	(unsigned char)(*(((unsigned char *)&x)+0))
#define hi_b0(x)	(unsigned char)(*(((unsigned char *)&x)+1))
#define lo_b1(x)	(unsigned char)(*(((bank1 unsigned char *)&x)+0))
#define hi_b1(x)	(unsigned char)(*(((bank1 unsigned char *)&x)+1))
#define lo_b2(x)	(unsigned char)(*(((bank2 unsigned char *)&x)+0))
#define hi_b2(x)	(unsigned char)(*(((bank2 unsigned char *)&x)+1))
#define lo_b3(x)	(unsigned char)(*(((bank3 unsigned char *)&x)+0))
#define hi_b3(x)	(unsigned char)(*(((bank3 unsigned char *)&x)+1))

#define hi16_b0(x)  (*(((unsigned int *)&x)+1))
#define low16_b0(x) (*(((unsigned int *)&x)+0))
#define hi16_b1(x)  (*(((bank1 unsigned int *)&x)+1))
#define low16_b1(x) (*(((bank1 unsigned int *)&x)+0))
#define hi16_b2(x)  (*(((bank2 unsigned int *)&x)+1))
#define low16_b2(x) (*(((bank2 unsigned int *)&x)+0))
#define hi16_b3(x)  (*(((bank3 unsigned int *)&x)+1))
#define low16_b3(x) (*(((bank3 unsigned int *)&x)+0))


#define MAX(a,b)	(((a)>(b))?(a):(b))
#define MIN(a,b)	(((a)<(b))?(a):(b))

#define Abs(x)		(((x)>0)?(x):(-x))

#define bit_set(var, bitnumber)	((var) |= 1 << (bitnumber))
#define bit_clr(var, bitnumber)	((var) &= ~(1 << (bitnumber)))

#define bits_on(var, mask)		(var |= mask)
#define bits_off(var, mask)	(var &= ~(mask))

#define restart_wdt()	{asm("clrwdt");}
#define asm_nop()			{asm("nop");}

// Macro utilizzate nel caso in cui il PWM venga realizzato utilizzando il Compare
#define Set_Duty_PWM1(duty) 	{ CCPR1H = (uint8_t)(duty >> 8); CCPR1L = (uint8_t)(duty & 0xFF); }
#define Set_Duty_PWM2(duty) 	{ CCPR2H = (uint8_t)(duty >> 8); CCPR2L = (uint8_t)(duty & 0xFF); }
#define Reset_Duty_PWM1() 		{ CCPR1H = 0; CCPR1L = 0; }
#define Reset_Duty_PWM2() 		{ CCPR2H = 0; CCPR2L = 0; }

#define Two8bit_To_One16bit(var, buffer, index)		{ var = buffer[index]; var |= (uint16)(buffer[index + 1]) << 8; }
#define One16bit_To_Two8bit(buffer, index, var)		{ buffer[index] = var; buffer[index + 1] = (var >> 8); }
#define One16bit_OR_Two8bit(buffer, index, var)		{ buffer[index] |= var; buffer[index + 1] |= (var >> 8); }
#define One16bit_AND_Two8bit(buffer, index, var)	{ buffer[index] &= var; buffer[index + 1] &= (var >> 8); }

#define Sel_Bit(buffer, index_obj)						( (buffer[index_obj / 8] >> (index_obj % 8)) & 0b00000001 )
#define Sel_Bit_From_Var(var, bitnumber)				( (var >> (bitnumber % 8)) & 0b00000001 )
#define Sel_Bit_From_Var_16bit(var, bitnumber)		( (var >> bitnumber) & 0x0001 )


#define	Delay3Us(x)	{	unsigned char _dcnt; \
			          	 	_dcnt = (x)/(12000/(XTAL_FREQ))|1; \
			          	 	while(--_dcnt != 0) \
			          	  	{\
				          	  	;\
					        	 	continue; \
					        	}\
				    		}

// Verica se un anno � bisestile.
#define Bisestile(year)										( ( (!(year % 4)) && (year % 100) ) || (!(year % 400)) )

#endif
