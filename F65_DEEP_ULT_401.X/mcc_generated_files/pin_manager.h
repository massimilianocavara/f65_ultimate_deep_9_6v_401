/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.5
        Device            :  PIC16F1718
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.20 and above
        MPLAB 	          :  MPLAB X 5.40	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>
#include "../global.h"

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set V_OPTI aliases
#define V_OPTI_TRIS                 TRISAbits.TRISA0
#define V_OPTI_LAT                  LATAbits.LATA0
#define V_OPTI_PORT                 PORTAbits.RA0
#define V_OPTI_WPU                  WPUAbits.WPUA0
#define V_OPTI_OD                   ODCONAbits.ODA0
#define V_OPTI_ANS                  ANSELAbits.ANSA0
#define V_OPTI_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define V_OPTI_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define V_OPTI_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define V_OPTI_GetValue()           PORTAbits.RA0
#define V_OPTI_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define V_OPTI_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define V_OPTI_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define V_OPTI_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define V_OPTI_SetPushPull()        do { ODCONAbits.ODA0 = 0; } while(0)
#define V_OPTI_SetOpenDrain()       do { ODCONAbits.ODA0 = 1; } while(0)
#define V_OPTI_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define V_OPTI_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set I_LED_ANALOG aliases
#define I_LED_ANALOG_TRIS                 TRISAbits.TRISA1
#define I_LED_ANALOG_LAT                  LATAbits.LATA1
#define I_LED_ANALOG_PORT                 PORTAbits.RA1
#define I_LED_ANALOG_WPU                  WPUAbits.WPUA1
#define I_LED_ANALOG_OD                   ODCONAbits.ODA1
#define I_LED_ANALOG_ANS                  ANSELAbits.ANSA1
#define I_LED_ANALOG_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define I_LED_ANALOG_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define I_LED_ANALOG_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define I_LED_ANALOG_GetValue()           PORTAbits.RA1
#define I_LED_ANALOG_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define I_LED_ANALOG_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define I_LED_ANALOG_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define I_LED_ANALOG_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define I_LED_ANALOG_SetPushPull()        do { ODCONAbits.ODA1 = 0; } while(0)
#define I_LED_ANALOG_SetOpenDrain()       do { ODCONAbits.ODA1 = 1; } while(0)
#define I_LED_ANALOG_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define I_LED_ANALOG_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set V_DRAIN_ANALOG aliases
#define V_DRAIN_ANALOG_TRIS                 TRISAbits.TRISA2
#define V_DRAIN_ANALOG_LAT                  LATAbits.LATA2
#define V_DRAIN_ANALOG_PORT                 PORTAbits.RA2
#define V_DRAIN_ANALOG_WPU                  WPUAbits.WPUA2
#define V_DRAIN_ANALOG_OD                   ODCONAbits.ODA2
#define V_DRAIN_ANALOG_ANS                  ANSELAbits.ANSA2
#define V_DRAIN_ANALOG_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define V_DRAIN_ANALOG_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define V_DRAIN_ANALOG_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define V_DRAIN_ANALOG_GetValue()           PORTAbits.RA2
#define V_DRAIN_ANALOG_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define V_DRAIN_ANALOG_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define V_DRAIN_ANALOG_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define V_DRAIN_ANALOG_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define V_DRAIN_ANALOG_SetPushPull()        do { ODCONAbits.ODA2 = 0; } while(0)
#define V_DRAIN_ANALOG_SetOpenDrain()       do { ODCONAbits.ODA2 = 1; } while(0)
#define V_DRAIN_ANALOG_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define V_DRAIN_ANALOG_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set V_BAT_ANALOG aliases
#define V_BAT_ANALOG_TRIS                 TRISAbits.TRISA3
#define V_BAT_ANALOG_LAT                  LATAbits.LATA3
#define V_BAT_ANALOG_PORT                 PORTAbits.RA3
#define V_BAT_ANALOG_WPU                  WPUAbits.WPUA3
#define V_BAT_ANALOG_OD                   ODCONAbits.ODA3
#define V_BAT_ANALOG_ANS                  ANSELAbits.ANSA3
#define V_BAT_ANALOG_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define V_BAT_ANALOG_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define V_BAT_ANALOG_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define V_BAT_ANALOG_GetValue()           PORTAbits.RA3
#define V_BAT_ANALOG_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define V_BAT_ANALOG_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define V_BAT_ANALOG_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define V_BAT_ANALOG_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)
#define V_BAT_ANALOG_SetPushPull()        do { ODCONAbits.ODA3 = 0; } while(0)
#define V_BAT_ANALOG_SetOpenDrain()       do { ODCONAbits.ODA3 = 1; } while(0)
#define V_BAT_ANALOG_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define V_BAT_ANALOG_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set nEN_CHG aliases
#define nEN_CHG_TRIS                 TRISAbits.TRISA4
#define nEN_CHG_LAT                  LATAbits.LATA4
#define nEN_CHG_PORT                 PORTAbits.RA4
#define nEN_CHG_WPU                  WPUAbits.WPUA4
#define nEN_CHG_OD                   ODCONAbits.ODA4
#define nEN_CHG_ANS                  ANSELAbits.ANSA4
#define nEN_CHG_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define nEN_CHG_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define nEN_CHG_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define nEN_CHG_GetValue()           PORTAbits.RA4
#define nEN_CHG_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define nEN_CHG_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define nEN_CHG_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define nEN_CHG_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define nEN_CHG_SetPushPull()        do { ODCONAbits.ODA4 = 0; } while(0)
#define nEN_CHG_SetOpenDrain()       do { ODCONAbits.ODA4 = 1; } while(0)
#define nEN_CHG_SetAnalogMode()      do { ANSELAbits.ANSA4 = 1; } while(0)
#define nEN_CHG_SetDigitalMode()     do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set ACC_SE aliases
#define ACC_SE_TRIS                 TRISAbits.TRISA5
#define ACC_SE_LAT                  LATAbits.LATA5
#define ACC_SE_PORT                 PORTAbits.RA5
#define ACC_SE_WPU                  WPUAbits.WPUA5
#define ACC_SE_OD                   ODCONAbits.ODA5
#define ACC_SE_ANS                  ANSELAbits.ANSA5
#define ACC_SE_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define ACC_SE_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define ACC_SE_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define ACC_SE_GetValue()           PORTAbits.RA5
#define ACC_SE_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define ACC_SE_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define ACC_SE_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define ACC_SE_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define ACC_SE_SetPushPull()        do { ODCONAbits.ODA5 = 0; } while(0)
#define ACC_SE_SetOpenDrain()       do { ODCONAbits.ODA5 = 1; } while(0)
#define ACC_SE_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define ACC_SE_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set EN_RX_CBL aliases
#define EN_RX_CBL_TRIS                 TRISAbits.TRISA6
#define EN_RX_CBL_LAT                  LATAbits.LATA6
#define EN_RX_CBL_PORT                 PORTAbits.RA6
#define EN_RX_CBL_WPU                  WPUAbits.WPUA6
#define EN_RX_CBL_OD                   ODCONAbits.ODA6
#define EN_RX_CBL_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define EN_RX_CBL_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define EN_RX_CBL_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define EN_RX_CBL_GetValue()           PORTAbits.RA6
#define EN_RX_CBL_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define EN_RX_CBL_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define EN_RX_CBL_SetPullup()          do { WPUAbits.WPUA6 = 1; } while(0)
#define EN_RX_CBL_ResetPullup()        do { WPUAbits.WPUA6 = 0; } while(0)
#define EN_RX_CBL_SetPushPull()        do { ODCONAbits.ODA6 = 0; } while(0)
#define EN_RX_CBL_SetOpenDrain()       do { ODCONAbits.ODA6 = 1; } while(0)

// get/set TX_CBL aliases
#define TX_CBL_TRIS                 TRISAbits.TRISA7
#define TX_CBL_LAT                  LATAbits.LATA7
#define TX_CBL_PORT                 PORTAbits.RA7
#define TX_CBL_WPU                  WPUAbits.WPUA7
#define TX_CBL_OD                   ODCONAbits.ODA7
#define TX_CBL_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define TX_CBL_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define TX_CBL_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define TX_CBL_GetValue()           PORTAbits.RA7
#define TX_CBL_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define TX_CBL_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)
#define TX_CBL_SetPullup()          do { WPUAbits.WPUA7 = 1; } while(0)
#define TX_CBL_ResetPullup()        do { WPUAbits.WPUA7 = 0; } while(0)
#define TX_CBL_SetPushPull()        do { ODCONAbits.ODA7 = 0; } while(0)
#define TX_CBL_SetOpenDrain()       do { ODCONAbits.ODA7 = 1; } while(0)

// get/set Vled_SA procedures
#define Vled_SA_TRIS                 TRISBbits.TRISB0
#define Vled_SA_LAT                  LATBbits.LATB0
#define Vled_SA_PORT                 PORTBbits.RB0
#define Vled_SA_WPU                  WPUBbits.WPUB0
#define Vled_SA_OD                   ODCONBbits.ODB0
#define Vled_SA_ANS                  ANSELBbits.ANSB0
#define Vled_SA_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define Vled_SA_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define Vled_SA_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define Vled_SA_GetValue()           PORTBbits.Vled_SA
#define Vled_SA_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define Vled_SA_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define Vled_SA_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define Vled_SA_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define Vled_SA_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define Vled_SA_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set SA_ON aliases
#define SA_ON_TRIS                 TRISBbits.TRISB1
#define SA_ON_LAT                  LATBbits.LATB1
#define SA_ON_PORT                 PORTBbits.RB1
#define SA_ON_WPU                  WPUBbits.WPUB1
#define SA_ON_OD                   ODCONBbits.ODB1
#define SA_ON_ANS                  ANSELBbits.ANSB1
#define SA_ON_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define SA_ON_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define SA_ON_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define SA_ON_GetValue()           PORTBbits.RB1
#define SA_ON_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define SA_ON_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define SA_ON_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define SA_ON_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define SA_ON_SetPushPull()        do { ODCONBbits.ODB1 = 0; } while(0)
#define SA_ON_SetOpenDrain()       do { ODCONBbits.ODB1 = 1; } while(0)
#define SA_ON_SetAnalogMode()      do { ANSELBbits.ANSB1 = 1; } while(0)
#define SA_ON_SetDigitalMode()     do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set V_RETE_sync aliases
#define V_RETE_sync_TRIS                 TRISBbits.TRISB2
#define V_RETE_sync_LAT                  LATBbits.LATB2
#define V_RETE_sync_PORT                 PORTBbits.RB2
#define V_RETE_sync_WPU                  WPUBbits.WPUB2
#define V_RETE_sync_OD                   ODCONBbits.ODB2
#define V_RETE_sync_ANS                  ANSELBbits.ANSB2
#define V_RETE_sync_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define V_RETE_sync_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define V_RETE_sync_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define V_RETE_sync_GetValue()           PORTBbits.RB2
#define V_RETE_sync_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define V_RETE_sync_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define V_RETE_sync_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define V_RETE_sync_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define V_RETE_sync_SetPushPull()        do { ODCONBbits.ODB2 = 0; } while(0)
#define V_RETE_sync_SetOpenDrain()       do { ODCONBbits.ODB2 = 1; } while(0)
#define V_RETE_sync_SetAnalogMode()      do { ANSELBbits.ANSB2 = 1; } while(0)
#define V_RETE_sync_SetDigitalMode()     do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set V_RETE_ANALOG aliases
#define V_RETE_ANALOG_TRIS                 TRISBbits.TRISB3
#define V_RETE_ANALOG_LAT                  LATBbits.LATB3
#define V_RETE_ANALOG_PORT                 PORTBbits.RB3
#define V_RETE_ANALOG_WPU                  WPUBbits.WPUB3
#define V_RETE_ANALOG_OD                   ODCONBbits.ODB3
#define V_RETE_ANALOG_ANS                  ANSELBbits.ANSB3
#define V_RETE_ANALOG_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define V_RETE_ANALOG_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define V_RETE_ANALOG_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define V_RETE_ANALOG_GetValue()           PORTBbits.RB3
#define V_RETE_ANALOG_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define V_RETE_ANALOG_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define V_RETE_ANALOG_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define V_RETE_ANALOG_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define V_RETE_ANALOG_SetPushPull()        do { ODCONBbits.ODB3 = 0; } while(0)
#define V_RETE_ANALOG_SetOpenDrain()       do { ODCONBbits.ODB3 = 1; } while(0)
#define V_RETE_ANALOG_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define V_RETE_ANALOG_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set LED_R aliases
#define LED_R_TRIS                 TRISBbits.TRISB4
#define LED_R_LAT                  LATBbits.LATB4
#define LED_R_PORT                 PORTBbits.RB4
#define LED_R_WPU                  WPUBbits.WPUB4
#define LED_R_OD                   ODCONBbits.ODB4
#define LED_R_ANS                  ANSELBbits.ANSB4
#define LED_R_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define LED_R_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define LED_R_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define LED_R_GetValue()           PORTBbits.RB4
#define LED_R_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define LED_R_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define LED_R_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define LED_R_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define LED_R_SetPushPull()        do { ODCONBbits.ODB4 = 0; } while(0)
#define LED_R_SetOpenDrain()       do { ODCONBbits.ODB4 = 1; } while(0)
#define LED_R_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define LED_R_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set J_1Hn3H aliases
#define J_1Hn3H_TRIS                 TRISBbits.TRISB4
#define J_1Hn3H_LAT                  LATBbits.LATB4
#define J_1Hn3H_PORT                 PORTBbits.RB4
#define J_1Hn3H_WPU                  WPUBbits.WPUB4
#define J_1Hn3H_OD                   ODCONBbits.ODB4
#define J_1Hn3H_ANS                  ANSELBbits.ANSB4
#define J_1Hn3H_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define J_1Hn3H_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define J_1Hn3H_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define J_1Hn3H_GetValue()           PORTBbits.RB4
#define J_1Hn3H_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define J_1Hn3H_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define J_1Hn3H_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define J_1Hn3H_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define J_1Hn3H_SetPushPull()        do { ODCONBbits.ODB4 = 0; } while(0)
#define J_1Hn3H_SetOpenDrain()       do { ODCONBbits.ODB4 = 1; } while(0)
#define J_1Hn3H_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define J_1Hn3H_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set LED_V aliases
#define LED_V_TRIS                 TRISBbits.TRISB5
#define LED_V_LAT                  LATBbits.LATB5
#define LED_V_PORT                 PORTBbits.RB5
#define LED_V_WPU                  WPUBbits.WPUB5
#define LED_V_OD                   ODCONBbits.ODB5
#define LED_V_ANS                  ANSELBbits.ANSB5
#define LED_V_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define LED_V_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define LED_V_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define LED_V_GetValue()           PORTBbits.RB5
#define LED_V_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define LED_V_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define LED_V_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define LED_V_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define LED_V_SetPushPull()        do { ODCONBbits.ODB5 = 0; } while(0)
#define LED_V_SetOpenDrain()       do { ODCONBbits.ODB5 = 1; } while(0)
#define LED_V_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define LED_V_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set nBatt1 aliases
#define nBatt1_TRIS                 TRISBbits.TRISB6
#define nBatt1_LAT                  LATBbits.LATB6
#define nBatt1_PORT                 PORTBbits.RB6
#define nBatt1_WPU                  WPUBbits.WPUB6
#define nBatt1_OD                   ODCONBbits.ODB6
#define nBatt1_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define nBatt1_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define nBatt1_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define nBatt1_GetValue()           PORTBbits.RB6
#define nBatt1_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define nBatt1_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define nBatt1_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define nBatt1_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)
#define nBatt1_SetPushPull()        do { ODCONBbits.ODB6 = 0; } while(0)
#define nBatt1_SetOpenDrain()       do { ODCONBbits.ODB6 = 1; } while(0)

// get/set nBatt2 aliases
#define nBatt2_TRIS                 TRISBbits.TRISB7
#define nBatt2_LAT                  LATBbits.LATB7
#define nBatt2_PORT                 PORTBbits.RB7
#define nBatt2_WPU                  WPUBbits.WPUB7
#define nBatt2_OD                   ODCONBbits.ODB7
#define nBatt2_ANS                  ANSELBbits.ANSB7
#define nBatt2_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define nBatt2_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define nBatt2_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define nBatt2_GetValue()           PORTBbits.RB7
#define nBatt2_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define nBatt2_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define nBatt2_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define nBatt2_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)
#define nBatt2_SetPushPull()        do { ODCONBbits.ODB7 = 0; } while(0)
#define nBatt2_SetOpenDrain()       do { ODCONBbits.ODB7 = 1; } while(0)
#define nBatt2_SetAnalogMode()      do { ANSELBbits.ANSB7 = 1; } while(0)
#define nBatt2_SetDigitalMode()     do { ANSELBbits.ANSB7 = 0; } while(0)

// get/set RC0 procedures
#define RC0_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define RC0_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define RC0_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define RC0_GetValue()           PORTCbits.RC0
#define RC0_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define RC0_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define RC0_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define RC0_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)

// get/set RC1 procedures
#define RC1_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define RC1_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define RC1_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define RC1_GetValue()           PORTCbits.RC1
#define RC1_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define RC1_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define RC1_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define RC1_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)


// get/set DISC_OPTI aliases
#define DISC_OPTI_TRIS                 TRISCbits.TRISC2
#define DISC_OPTI_LAT                  LATCbits.LATC2
#define DISC_OPTI_PORT                 PORTCbits.RC2
#define DISC_OPTI_WPU                  WPUCbits.WPUC2
#define DISC_OPTI_OD                   ODCONCbits.ODC2
#define DISC_OPTI_ANS                  ANSELCbits.ANSC2
#define DISC_OPTI_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define DISC_OPTI_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define DISC_OPTI_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define DISC_OPTI_GetValue()           PORTCbits.RC2
#define DISC_OPTI_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define DISC_OPTI_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define DISC_OPTI_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define DISC_OPTI_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define DISC_OPTI_SetPushPull()        do { ODCONCbits.ODC2 = 0; } while(0)
#define DISC_OPTI_SetOpenDrain()       do { ODCONCbits.ODC2 = 1; } while(0)
#define DISC_OPTI_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define DISC_OPTI_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)


// get/set RX_CBL aliases
#define RX_CBL_TRIS                 TRISCbits.TRISC3
#define RX_CBL_LAT                  LATCbits.LATC3
#define RX_CBL_PORT                 PORTCbits.RC3
#define RX_CBL_WPU                  WPUCbits.WPUC3
#define RX_CBL_OD                   ODCONCbits.ODC3
#define RX_CBL_ANS                  ANSELCbits.ANSC3
#define RX_CBL_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define RX_CBL_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define RX_CBL_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define RX_CBL_GetValue()           PORTCbits.RC3
#define RX_CBL_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define RX_CBL_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define RX_CBL_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define RX_CBL_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define RX_CBL_SetPushPull()        do { ODCONCbits.ODC3 = 0; } while(0)
#define RX_CBL_SetOpenDrain()       do { ODCONCbits.ODC3 = 1; } while(0)
#define RX_CBL_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define RX_CBL_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set nSleep aliases
#define nSleep_TRIS                 TRISCbits.TRISC4
#define nSleep_LAT                  LATCbits.LATC4
#define nSleep_PORT                 PORTCbits.RC4
#define nSleep_WPU                  WPUCbits.WPUC4
#define nSleep_OD                   ODCONCbits.ODC4
#define nSleep_ANS                  ANSELCbits.ANSC4
#define nSleep_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define nSleep_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define nSleep_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define nSleep_GetValue()           PORTCbits.RC4
#define nSleep_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define nSleep_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define nSleep_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define nSleep_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define nSleep_SetPushPull()        do { ODCONCbits.ODC4 = 0; } while(0)
#define nSleep_SetOpenDrain()       do { ODCONCbits.ODC4 = 1; } while(0)
#define nSleep_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define nSleep_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set PWM_LED aliases
#define PWM_LED_TRIS                 TRISCbits.TRISC5
#define PWM_LED_LAT                  LATCbits.LATC5
#define PWM_LED_PORT                 PORTCbits.RC5
#define PWM_LED_WPU                  WPUCbits.WPUC5
#define PWM_LED_OD                   ODCONCbits.ODC5
#define PWM_LED_ANS                  ANSELCbits.ANSC5
#define PWM_LED_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define PWM_LED_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define PWM_LED_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define PWM_LED_GetValue()           PORTCbits.RC5
#define PWM_LED_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define PWM_LED_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define PWM_LED_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define PWM_LED_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define PWM_LED_SetPushPull()        do { ODCONCbits.ODC5 = 0; } while(0)
#define PWM_LED_SetOpenDrain()       do { ODCONCbits.ODC5 = 1; } while(0)
#define PWM_LED_SetAnalogMode()      do { ANSELCbits.ANSC5 = 1; } while(0)
#define PWM_LED_SetDigitalMode()     do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set TX_LG aliases
#define TX_LG_TRIS                 TRISCbits.TRISC6
#define TX_LG_LAT                  LATCbits.LATC6
#define TX_LG_PORT                 PORTCbits.RC6
#define TX_LG_WPU                  WPUCbits.WPUC6
#define TX_LG_OD                   ODCONCbits.ODC6
#define TX_LG_ANS                  ANSELCbits.ANSC6
#define TX_LG_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define TX_LG_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define TX_LG_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define TX_LG_GetValue()           PORTCbits.RC6
#define TX_LG_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define TX_LG_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define TX_LG_SetPullup()          do { WPUCbits.WPUC6 = 1; } while(0)
#define TX_LG_ResetPullup()        do { WPUCbits.WPUC6 = 0; } while(0)
#define TX_LG_SetPushPull()        do { ODCONCbits.ODC6 = 0; } while(0)
#define TX_LG_SetOpenDrain()       do { ODCONCbits.ODC6 = 1; } while(0)
#define TX_LG_SetAnalogMode()      do { ANSELCbits.ANSC6 = 1; } while(0)
#define TX_LG_SetDigitalMode()     do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RX_LG aliases
#define RX_LG_TRIS                 TRISCbits.TRISC7
#define RX_LG_LAT                  LATCbits.LATC7
#define RX_LG_PORT                 PORTCbits.RC7
#define RX_LG_WPU                  WPUCbits.WPUC7
#define RX_LG_OD                   ODCONCbits.ODC7
#define RX_LG_ANS                  ANSELCbits.ANSC7
#define RX_LG_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define RX_LG_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define RX_LG_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RX_LG_GetValue()           PORTCbits.RC7
#define RX_LG_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define RX_LG_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define RX_LG_SetPullup()          do { WPUCbits.WPUC7 = 1; } while(0)
#define RX_LG_ResetPullup()        do { WPUCbits.WPUC7 = 0; } while(0)
#define RX_LG_SetPushPull()        do { ODCONCbits.ODC7 = 0; } while(0)
#define RX_LG_SetOpenDrain()       do { ODCONCbits.ODC7 = 1; } while(0)
#define RX_LG_SetAnalogMode()      do { ANSELCbits.ANSC7 = 1; } while(0)
#define RX_LG_SetDigitalMode()     do { ANSELCbits.ANSC7 = 0; } while(0)

// get/set SA_IN aliases
#define SA_IN_TRIS                 TRISEbits.TRISE3
#define SA_IN_PORT                 PORTEbits.RE3
#define SA_IN_WPU                  WPUEbits.WPUE3
#define SA_IN_GetValue()           PORTEbits.RE3
#define SA_IN_SetDigitalInput()    do { TRISEbits.TRISE3 = 1; } while(0)
#define SA_IN_SetDigitalOutput()   do { TRISEbits.TRISE3 = 0; } while(0)
#define SA_IN_SetPullup()          do { WPUEbits.WPUE3 = 1; } while(0)
#define SA_IN_ResetPullup()        do { WPUEbits.WPUE3 = 0; } while(0)


//label inizializzazione
#define LATA_INIT   0b00000000 // nEN_CHG=0 (abilita ricarica)
#define TRISA_INIT  0b00001111 // V_OPTI, V_BAT_MIS, I_LED, LAMP_ON in INPUT

#define LATB_INIT   0b00000000 //
// #define TRISB_INIT  0b00011100 // V_RETE_sync, V_RETE, 1h_n3h in INPUT
#define TRISB_INIT  0b11011100 // V_RETE_sync, V_RETE, 1h_n3h, nBatt1 e nBatt2 in INPUT

#define LATC_INIT   0b00010000 // nSleep=1
#define TRISC_INIT  0b11001110 // Vled_SA, RX_CBL, RXD in INPUT

#define TRISE_INIT  0b00001000 // SA_IN in INPUT

#define LATA_SLEEP  0b00000001 // V_OPTI resta alto
#define TRISA_SLEEP 0b01000000 // 

#define LATB_SLEEP  0b00000000 // 
#define TRISB_SLEEP 0b00001100 // V_RETE e V_RETE_sync in INPUT

#define LATC_SLEEP  0b01000000 // TXD in output ad 1, necessario per il RESTMODE
#define TRISC_SLEEP 0b10000000 // RXD in input

////DEBUG PIN: RB7
#define DEBUG_PIN             LATB7
#define DEBUG_TRIS            TRISB7
#define DEB_SIGNAL            LATB ^= 0b10000000; TRISB &= 0b01111111;
#define DEB_SET               LATB |= 0b10000000; TRISB &= 0b01111111;
#define DEB_RESET             LATB &= 0b01111111; TRISB &= 0b01111111;

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/