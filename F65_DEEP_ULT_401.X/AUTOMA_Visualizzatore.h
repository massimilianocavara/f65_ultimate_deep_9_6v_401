#ifndef _VISUALIZZATORE_H
#define _VISUALIZZATORE_H


#define	M_ACCENDI_VERDE		LATB |= 0b00100000; TRISB &= 0b11011111
#define	M_ACCENDI_ROSSO		LATB |= 0b00010000; TRISB &= 0b11101111
#define M_ACCENDI_ARANCIO   LATB |= 0b00110000; TRISB &= 0b11001111

#define	M_SPEGNI_LED		LATB &= 0b11001111; TRISB &= 0b11001111
#define	M_SPEGNI_VERDE		LATB &= 0b11011111; TRISB &= 0b11011111
#define	M_SPEGNI_ROSSO		LATB &= 0b11101111; TRISB &= 0b11101111

// Stati automa:
#define ST_LED_VERDE		0
#define ST_LED_ROSSO		1
#define ST_LED_ARANCIO      2


#define	PERIOD_ARANCIO		20		//20msec
#define	DUTY_ARANCIO_V		13		//65%

//------------------------------------------
//  tFlagInputVisualizzatore
//------------------------------------------
typedef union {
    uint16_t iReg;

    struct {
        uint16_t flagVisLedSpenti : 1; //1
        uint16_t flagVisRossoFisso : 1; //2
        uint16_t flagVisRosso1blink : 1; //3
        uint16_t flagVisRosso2blink : 1; //4
        uint16_t flagVisRosso3blink : 1; //5
        uint16_t flagVisRossoBlinkContinuo : 1; //6
        uint16_t flagVisArancioFisso : 1; //7
        uint16_t flagVisArancio1blink : 1; //8
        uint16_t flagVisArancio2blink : 1; //9
        uint16_t flagVisArancio3blink : 1; //10
        uint16_t flagVisArancioBlinkContinuo : 1; //11
        uint16_t flagVisVerdeFisso : 1; //12
        uint16_t flagVisVerde1blink : 1; //13
        uint16_t flagVisVerde2blink : 1; //14
        uint16_t flagVisVerde3blink : 1; //15
        uint16_t flagVisArancioSempre : 1; //16
    } iBit;
} tFlagInputVisualizzatore;


extern tFlagTimer uFlagTimer;
extern tFlagInputVisualizzatore uFlagInputVisualizzatore;

#endif
