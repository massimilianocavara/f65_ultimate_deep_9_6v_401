//**************************
//
//      AUTOMA_Logica
//
//**************************
#include <htc.h>
#include "global.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "AUTOMA_DriverLogica.h"
#include "Timer.h"
#include "AUTOMA_Ingressi.h"
#include "Flash.h"
#include "HEFlash.h"
#include "MODULO_Norma.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_Charger.h"
#include "Bootloader.h"
//#include "AUTOMA_DriverCablecom.h"
#include "AUTOMA_Filtri.h"
#include "AUTOMA_Eeprom.h"
#include "AUTOMA_Taratura.h"

union tErrorRegister uErrorRegister;
union tStatusByte uStatusByte;
union tStatusByte2 uStatusByte2;
union tSaStatusRegister uSaStatusRegister;

// Variabili per upgrade firmware
uint8_t cTimerResetDisp;

union tFlagLogica uFlagLogica;
uint8_t cStatoLogica;
uint8_t cTipoWrComene;
uint8_t cAddressComene;
uint8_t cBufferInterfacciaLogica;
uint16_t iTimerDelayAggiornamento; //@@@@
uint8_t cTimeoutAggiornamento;
tTimer sTimerRitardoAttuazione;

uint8_t cCmdDaAttuare;

uint8_t cDepoProtocollo; //memorizza il protocollo per la gestione differente, pe, del REST-MODE

uint8_t cBufferLogicaIn[SIZE_BUFFER_LOGICA_IN];
uint8_t cBufferLogicaOut[SIZE_BUFFER_LOGICA_OUT];
uint8_t cLengthBufferLogica;                // contiene la dimensione del buffer, pe, se ultimo elemento � B[3], il valore sar� 4
uint8_t	cbufferLogicaMutex;                 // semaforo per l'utilizzo del BufferLogica

//Tabella in RAM
uint8_t TabellaLogica[32];
uint8_t TabellaLogica2[32];

//Tabella in EEPROM
const uint8_t TabellaLogicaINIT[32] @ ADDR_START_TABLE_LOGICA = {ID_SERIAL_L, ID_SERIAL_H, 0x00, 0x00, TIPO_APPARECCHIO_LG,
    IDSW_VER_LOW, IDSW_REL_IDSW_VER_HI, PARAMETRI_REG, 0x00, SW_BUILD_BYTE_LOW, SW_BUILD_BYTE_HI, GRUPPI_7_0, GRUPPI_15_8, SCENE_1_0, SCENE_3_2, SCENE_5_4, SCENE_7_6, SCENE_9_8,
    SCENE_11_10, SCENE_13_12, SCENE_15_14, UVOXY_CONFIG_REG, ACTION_REGISTER, ACTION_REGISTER_2, SA_MODE_REGISTER, TEMPO_RESIDUO_LAST_TEST,
    CODICE_PRODOTTO_FINITO_LOW, CODICE_PRODOTTO_FINITO_HI, 0x00, 0x00, 0x00, 0x00};
//Tabella in ROM (default)
//N.B. le prime 7 locazioni vengono modificate in fase di collaudo, quindi non sono veri default
const uint8_t TabellaLogicaDEFAULT[32] = {ID_SERIAL_L, ID_SERIAL_H, 0x00, 0x00, TIPO_APPARECCHIO_LG,
    IDSW_VER_LOW, IDSW_REL_IDSW_VER_HI, TIPO_LAMP_DEFAULT, 0x00, SW_BUILD_BYTE_LOW, SW_BUILD_BYTE_HI, GRUPPI_7_0, GRUPPI_15_8, SCENE_1_0, SCENE_3_2, SCENE_5_4, SCENE_7_6, SCENE_9_8,
    SCENE_11_10, SCENE_13_12, SCENE_15_14, UVOXY_CONFIG_REG, ACTION_REGISTER, ACTION_REGISTER_2, SA_MODE_REGISTER, TEMPO_RESIDUO_LAST_TEST,
    CODICE_PRODOTTO_FINITO_LOW, CODICE_PRODOTTO_FINITO_HI, 0x00, 0x00, 0x00, 0x00};

//non posso definirla [32] perch� andrei a sovrappormi a TabellaLogicaROM e il compilatore d� errore
const uint8_t TabellaLogica2INIT[20] @ ADDR_START_TABLE_LOGICA2 = {
    INIT_VITA_TUBO_L, INIT_VITA_TUBO_M, INIT_VITA_TUBO_H, V_MANTENIMENTO_L, V_MANTENIMENTO_H, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00};

const uint8_t TabellaLogica2DEFAULT[20] = {
    INIT_VITA_TUBO_L, INIT_VITA_TUBO_M, INIT_VITA_TUBO_H, V_MANTENIMENTO_L, V_MANTENIMENTO_H, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00};


//const uint8_t TabellaLogica2DEFAULT[32] = {
//    INIT_VITA_TUBO_H, INIT_VITA_TUBO_M, INIT_VITA_TUBO_L, 0x00, 0x00, 0x00, 0x00, 0x00,
//    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//    0x00, 0x00, 0x00, 0x00, TIPO_APPARECCHIO_LG, IDSW_VER_LOW, IDSW_REL_IDSW_VER_HI, 0xFF, 
//    0xFF, ID_SERIAL_L, ID_SERIAL_H, TIPO_BATTERIA, TIPO_TUBO, 0xFF, WEEK_PRODUCTION, YEAR_PRODUCTION};

//--------------------------------------------------------------------------------------------------------------------------------------------------
// TABELLA ROM:
//--------------------------------------------------------------------------------------------------------------------------------------------------
// 0x1FF4 -> 0 - Tipo apparecchio
// 0x1FF5 -> 1 - IDSW_VER_LOW
// 0x1FF6 -> 2 - IDSW_REL - IDSW_VER_HI
// 0x1FF7 -> 3 - Kref (not used)
// 0x1FF8 -> 4 - Kbat (not used)
// 0x1FF9 -> 5 - ID_LOW (viene scritto in fase di programmazione)
// 0x1FFA -> 6 - ID_HI (viene scritto in fase di programmazione)
// 0x1FFB -> 7 - Tipo batteria
// 0x1FFC -> 8 - Tipo tubo
// 0x1FFD -> 9 - not used
// 0x1FFE -> 10- Numero settimana produzione (viene scritto in fase di programmazione)
// 0x1FFF -> 11- Numero anno produzione (viene scritto in fase di programmazione)
//--------------------------------------------------------------------------------------------------------------------------------------------------
const uint8_t TabellaLogicaROM[12] @ ADDR_START_TABLE_LOGICA_ROM = {TIPO_APPARECCHIO_LG, IDSW_VER_LOW, IDSW_REL_IDSW_VER_HI, 0xFF, 0xFF, ID_SERIAL_L, ID_SERIAL_H, 
                                                                    TIPO_BATTERIA, TIPO_TUBO, 0xFF, 0xFF, 0xFF};


//Tabella in RAM
uint8_t TabellaBootloader[SIZE_BUFFER_FLASH];
//TABELLA del BOOTLOADER
const uint8_t TabellaBootloaderINIT[SIZE_BUFFER_FLASH] @ ADDR_START_TABLE_BOOT = {
    0x01, FIRST_START_MICRO, FIRMWARE_OK, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00,
    0x00, 0x00, FLASH_START_BOOT_L, FLASH_START_BOOT_H, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};


extern tFiltrato uIngressiFilt;
extern union tRS232 uRS232;
extern tFlagTimer uFlagTimer;
extern union tFlagDriverLogica uFlagDriverLogica;
//extern uint8_t cLengthBufferLogica;

extern uint8_t TabellaBootloader[32];

//extern union tLampStatus uLampStatus;
extern tFlagFissiLampada uFlagFissiLampada;
//extern tFlagAutomaLed uFlagAutomaLed;
extern uint16_t iDuty;
extern uint8_t IndiceStep;
extern tByteErrori uByteErrori;
extern union tFlagNorma uFlagNorma;
extern tFlagLampada uFlagLampada;
extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFlagCharger uFlagCharger;

extern tTimer sTimerCadenzaFunzionale; //timer che conta la cadenza dei test funzionali
extern tTimer sTimerCadenzaAutonomia; //timer che conta la cedenza dei test di autonomia
extern tTimer sTimerDelayTest; //timer che conta un eventuale delay
extern union tFlagNormaFissi uFlagNormaFissi;
extern uint8_t cStatoSan;
//extern tFlagDriverCbl uFlagDriverCbl;

extern misura mVbatt;
extern misura mIled;
extern misura mVbattRaw;

extern void fResetDiFabbrica(void);
extern void fInitCompleto(void);
extern void fRicaricaTimerRipristino(void);
extern void fInitTimerTest(void);
extern unsigned char fDecrementa(tTimer * timer);
extern uint8_t fZerotimer(tTimer * timer);

extern tFlagEeprom uFlagEeprom;

//prototipi, definizioni sotto
void fAggiornaConfigDaTabellaLogica(void);


//----------------------------------------------------------------------------------------
//  void fResetDiFabbrica(void)
//
//
//----------------------------------------------------------------------------------------
void fResetDiFabbrica(void) {
    
    uint8_t i;
   
//    for (i = IND_BUILD_LOW; i < IND_CODICE_PRODOTTO_FINITO_L; i++) {
    for (i = IND_GRUPPI_7_0; i < IND_CODICE_PRODOTTO_FINITO_L; i++) {
        TabellaLogica[i] = TabellaLogicaDEFAULT[i];
    }
   //scrivo in EEPROM i valori aggiornati che sono in RAM
    HEFLASH_writeBlock(TAB_LOGICA, (void*) TabellaLogica, sizeof (TabellaLogica));
    fAggiornaConfigDaTabellaLogica();
    fInitCompleto();
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//
//  void AUTOMA_ProtocolloLogica(void)
//
//----------------------------------------------------------------------------------------
void AUTOMA_ProtocolloLogica(void) {

    uFlagLogica.iReg &= MASK_RESET_FLAG_LOGICA; //azzero i flag eclusi AbilitaScrittura, ComenePendente e AbilitaResetDisp
    
    if (uFlagTimer.iBit.flag1msec) {
        //------------------------------------------------------------------------------
        // Gestione reset generale del prodotto in caso di comando ricevuto da seriale
        //------------------------------------------------------------------------------
        if (uFlagLogica.iBit.AbilitaResetDisp) {
            if (!cTimerResetDisp) {
                asm("RESET");
            } else {
                cTimerResetDisp--;
            }
        } else {
            cTimerResetDisp = PTIM_DELAY_RESET_DISP;
        }        
        //------------------------------------------------------------------------------
        // Timer di ritardo dell'aggiornamento Reg LOGICA per consentire operazioni multiple
        //------------------------------------------------------------------------------
        if (0 != iTimerDelayAggiornamento) {
            iTimerDelayAggiornamento--;
            if (0 == iTimerDelayAggiornamento) {
                uFlagLogica.iBit.ModConfigDaLogica = 1; //viene settato solo una volta ad ogni azzeramento del timer
            }
        }
    }
    if (uFlagTimer.iBit.flag1sec) {
        if (0 != cTimeoutAggiornamento) {
            cTimeoutAggiornamento--;
            if ((0 == cTimeoutAggiornamento) && (0 == iTimerDelayAggiornamento)) {
                uFlagLogica.iBit.ModConfigDaLogica = 1;
                cTimeoutAggiornamento = TM_AGGIORNAMENTO;
            }
        }
        
        //L'attuazione va eseguita UNA SOLA VOLTA, quindi:
        //se il Timer � diverso da zero devo entrare per decrementare, e poi, quando sar� ZERO, attuare
        //altrimenti non faccio nulla, perch� ci ha gi� pensato la decodifica del comando
        if (!fZerotimer(&sTimerRitardoAttuazione)) {
            if (fDecrementa(&sTimerRitardoAttuazione)) {
                switch (cCmdDaAttuare) {
                    case SUBCMD_SINCROTEST:
                    {
                        fInitTimerTest();
                        break;
    }
                    case SUBCMD_SPEGNISAN:
                    {
                        uFlagLogica.iBit.SpegniSanificaLg = 1;
                        break;
                    }
                    case SUBCMD_ACCENDISAN:
                    {
                        uFlagLogica.iBit.AccendiSanificaLg = 1;
                        break;
                    }
                    default:
                    {
                        //non far nulla
                    }
                }                        
            }
        }
    }

    
    switch(cStatoLogica) {
        //**********************************************************************
        //
        //**********************************************************************
        case ST_LOGICA_IDLE:
        {
            if (uFlagDriverLogica.cBit.ParseNewFrame) {
                cDepoProtocollo = PROT_LOGICA;
                uFlagDriverLogica.cBit.ParseNewFrame = 0;
                //nuovo frame da decodificare
                //1) verifico se � per questa lampada (indirizzamento Broadcast o di Gruppo o per Serial ID)
                if (CmdBroadcastOrGruppoOrId(cBufferLogicaIn)) { // S� -> analizzo il frame ricevuto
                    // Verifico se � un frame di tipo Logica esteso
                    if (COMEXT == cBufferLogicaIn[2]) // >>>>> FRAME LOGICA ESTESO
                    { // S�
                        cStatoLogica = ST_LOGICA_FRAME_EXT;
                    } else {
                        cStatoLogica = ST_LOGICA_FRAME_STD;
                    }
                }
            }
            break;
        }       
        //**********************************************************************
        //
        //**********************************************************************
        case ST_LOGICA_FRAME_STD:
        {
            fRicaricaTimerRipristino(); //blocchiamo i timer di cadenza test per 90gg dall'ultimo msg logica per noi
            
            if (COMRDE == (SEL_BIT_COMRDE & cBufferLogicaIn[2])) // COMRDE*
            {
                uFlagLogica.iBit.AbilitaScrittura = 0;
                uFlagLogica.iBit.ComenePendente = 0;
                
                // Devo leggere la Tabella Protetta ?
                if (TABELLA_PROTETTA == (SEL_BIT_TIPO_TABELLA & cBufferLogicaIn[2])) { // S� -> preparo il byte di risposta della tabella protetta (solo lettura)
                    cBufferLogicaOut[2] = LeggiTabellaProtetta(cBufferLogicaIn[3]);
                } else { // No -> preparo il byte di risposta della tabella non protetta (lettura / scrittura)
                    cBufferLogicaOut[2] = LeggiTabella(cBufferLogicaIn[2]);
                }
                //abilito risposta
                uFlagLogica.iBit.TxRisposta = 1;
                cLengthBufferLogica = 3;
            }
            //-----------------------------------------------------------

            //-----------------------------------------------------------
            else if (COMENE == (SEL_BIT_COMENE & cBufferLogicaIn[2])) // COMENE*
            {
                uFlagLogica.iBit.AbilitaScrittura = 0;
                uFlagLogica.iBit.ComenePendente = 0;
                
                if (CmdPerMe(cBufferLogicaIn[3])) {                    
                    cAddressComene = SEL_BIT_ADDRESS & cBufferLogicaIn[2];
                    cTipoWrComene = SEL_BIT_TIPO_WRITE & cBufferLogicaIn[3];

                    uFlagLogica.iBit.AbilitaScrittura = 1;

                    // Cmd solo per lampade Non Programmate ?
                    if (SEL_BIT_LAMP_NOT_PROG & cBufferLogicaIn[3]) {
                        // S� -> setto il flag "uLogica.cBit.ComenePendente" per l'eventuale validazione del COMENE. 
                        //       E' un flag che ho aggiunto in quanto serve durante in fase di configurazione. 
                        //       Infatti, inviando la centrale un comando di COMENE broadcast a tutte le lampade non programmate, 
                        //       si avr� che tutti gli apparecchi risponderanno contemporaneamente, ma soltanto quello che riuscir� a terminare la risposta (vincitore di tutte
                        //       le contese sul bus) potr� validare il COMENE per la successiva scrittura
                        uFlagLogica.iBit.AbilitaScrittura = 0;
                        uFlagLogica.iBit.ComenePendente = 1;
                    }
                    //preparo buffer di risposta
                    cBufferLogicaOut[2] = uStatusByte.cReg;

                    //abilito risposta
                    uFlagLogica.iBit.TxRisposta = 1;
                    cLengthBufferLogica = 3;
                } else {
                    //nessuna risposta
                    uFlagLogica.iBit.TxRisposta = 0;
                    cLengthBufferLogica = 0;
                }
            }
            //-----------------------------------------------------------

            //-----------------------------------------------------------
            else if (COMWRE == (SEL_BIT_COMWRE & cBufferLogicaIn[2])) // COMWRE*
            {
                uint8_t res;
                
                uFlagLogica.iBit.ComenePendente = 0;
                res = 0;
                
                if (uFlagLogica.iBit.AbilitaScrittura) {
                    uFlagLogica.iBit.AbilitaScrittura = 0;

                    // L'indirizzo del registro passato al COMWRE � lo stesso del COMENE ?
                    if ((SEL_BIT_ADDRESS & cBufferLogicaIn[2]) == cAddressComene) { // S� -> Procedo con la scrittura del registro
                        res = ScriviTabella(cAddressComene, cBufferLogicaIn[3], cDepoProtocollo);
                    }
                }
                uFlagLogica.iBit.TxRisposta = 1;
                
                //poich� sar� presente un modulino, devo aggiungere la risposta con ACK
                uFlagLogica.iBit.RispondiACK = 1;
            }
            //-----------------------------------------------------------

            //-----------------------------------------------------------
            // Serve per la contesa nel caso ci sia il modulino: infatti con la seriale,
            // la lampada perde l'informazione di contesa, che per� ha il modulino, che allora
            // invia un ACK nel caso la risposta al COMENE sia passata oppure un NACK nel caso
            // non sia passata
            //-----------------------------------------------------------
            else if (COMACK == cBufferLogicaIn[2]) // COMACK*
            {
                if (ACK == cBufferLogicaIn[3]) {
                    if (uFlagLogica.iBit.ComenePendente) {
                        uFlagLogica.iBit.ComenePendente = 0;
                        uFlagLogica.iBit.AbilitaScrittura = 1;
                    }
                } else {
                    // NACK
                    uFlagLogica.iBit.ComenePendente = 0;
                }
                //poich� sar� presente un modulino, devo aggiungere la risposta con ACK
                uFlagLogica.iBit.TxRisposta = 1;
                uFlagLogica.iBit.RispondiACK = 1;
            }
            //-----------------------------------------------------------
            //++++++++++++++++++++++++++++++++++
            // COMACT*
            //++++++++++++++++++++++++++++++++++
            else if (COMACT == (SEL_BIT_COMACT & cBufferLogicaIn[2])) {
                uFlagLogica.iBit.AbilitaScrittura = 0;
                uFlagLogica.iBit.ComenePendente = 0;
                
                switch (cBufferLogicaIn[2] & SEL_SUBCMD_COMACT) {
                    case SUBCMD_CONFIG_CBL: //Cablecom*
                    {
                        switch(cBufferLogicaIn[3]) {
                            //----------------------------------------------------------
                            case 0:
                            {
                                uFlagFissiLampada.iBit.ErroreConfigurazione = 0;
                                cBufferLogicaOut[2] = 1; //YES, cmd eseguito                          
                                break;
                            }
                            //----------------------------------------------------------
                            case 1:
                            {
                                uFlagFissiLampada.iBit.ErroreConfigurazione = 1;
                                cBufferLogicaOut[2] = 1; //YES, cmd eseguito
                                break;
                            }
                            //----------------------------------------------------------
                            case 2:
                            {
                                uFlagFissiLampada.iBit.LampeggiaLed = 0;
                                cBufferLogicaOut[2] = 1; //YES, cmd eseguito
                                break;
                            }
                            //----------------------------------------------------------
                            case 3:
                            {
                                uFlagFissiLampada.iBit.LampeggiaLed = 1;
                                cBufferLogicaOut[2] = 1; //YES, cmd eseguito
                                break;
                            }                    
                            //----------------------------------------------------------
                            default:
                            {
                                cBufferLogicaOut[2] = 0; //NO, cmd non eseguito
                            }
                        }
                        break;
                    }
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    case SUBCMD_SINCROTEST: //sincronizza test*
                    {                        
                        if (0 == cBufferLogicaIn[3]) {
                            fInitTimerTest();
                        } else {
#ifdef DEB_COMACT
                            sTimerRitardoAttuazione.slTimer = cBufferLogicaIn[3];
#else
                            sTimerRitardoAttuazione.slTimer = cBufferLogicaIn[3] * 3600;
#endif
                            cCmdDaAttuare = SUBCMD_SINCROTEST;
                        }                       
                        cBufferLogicaOut[2] = 1; //YES, cmd eseguito
                        break;
                    }
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    case SUBCMD_SPEGNISAN: //spegnimento sanificatore*
                    {
                        if (0 == cBufferLogicaIn[3]) {
                            TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111110; //non modifico la configurazione perch� questo cmd funge solo da interruttore
                            TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100000; //tipo UVOXY; contr da remoto
                            uFlagLogica.iBit.AggiornaTabInEeprom = 1;
                        } else {
                            sTimerRitardoAttuazione.slTimer = cBufferLogicaIn[3] * 3600;
                            cCmdDaAttuare = SUBCMD_SPEGNISAN;
                        }
                        cBufferLogicaOut[2] = 1; //YES, cmd eseguito
                        break;
                    }
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    case SUBCMD_ACCENDISAN: //accensione sanificatore*
                    {
                        if (0 == cBufferLogicaIn[3]) {
                            TabellaLogica[IND_UVOXY_CONFIG_REG] &= 0b10111111; //non modifico la configurazione perch� questo cmd funge solo da interruttore
                            TabellaLogica[IND_UVOXY_CONFIG_REG] |= 0b10100001; //tipo UVOXY; contr da remoto
                            uFlagLogica.iBit.AggiornaTabInEeprom = 1;
                        } else {
                            sTimerRitardoAttuazione.slTimer = cBufferLogicaIn[3] * 3600;
                            cCmdDaAttuare = SUBCMD_ACCENDISAN;
                        }
                        cBufferLogicaOut[2] = 1; //YES, cmd eseguito
                        break;
                    }
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    default:
                    {
                        cBufferLogicaOut[2] = 0; //NO, cmd non eseguito
                    }
                }
                cLengthBufferLogica = 3;
                uFlagLogica.iBit.TxRisposta = 1;  
            }
            //++++++++++++++++++++++++++++++++++
            else {
                uFlagLogica.iBit.AbilitaScrittura = 0;
                uFlagLogica.iBit.ComenePendente = 0;

                switch (cBufferLogicaIn[2]) {
                    //---------------------------------------------------
                    case COMPOL: // COMPOL*
                    {
                        if (CmdPerMe(cBufferLogicaIn[3])) {
                            cBufferLogicaOut[2] = uStatusByte.cReg;
                            //abilito risposta
                            uFlagLogica.iBit.TxRisposta = 1;                                                        
                            cLengthBufferLogica = 3;
                        }
                        break;
                    }
                    
                    //---------------------------------------------------
                    // COMPOL2
                    //---------------------------------------------------
                    case COMPOL2: // COMPOL2*
                    {
                        if (CmdPerMe(cBufferLogicaIn[3])) {
                            cBufferLogicaOut[2] = uStatusByte2.cReg;
                            //abilito risposta
                            uFlagLogica.iBit.TxRisposta = 1;                                                        
                            cLengthBufferLogica = 3;
                        }
                        break;
                    }
                    
//                    //---------------------------------------------------
//                    // COMPOL3
//                    //---------------------------------------------------
//                    case COMPOL3: // COMPOL3*
//                    {
//                        if (CmdPerMe(cBufferLogicaIn[3])) {
//                            cBufferLogicaOut[2] = uStatusByte3.cReg;
//                            //abilito risposta
//                            uFlagLogica.iBit.TxRisposta = 1;                                                        
//                            cLengthBufferLogica = 3;
//                        }
//                        break;
//                    }
                    //---------------------------------------------------

                    //---------------------------------------------------
                    case COMIT: // COMIT*
                    {
                        if (!cBufferLogicaIn[3]) {
                            uFlagLogica.iBit.TestFunzionaleDaLogica = 1;
                        } else if (0xFF == cBufferLogicaIn[3]) {
                            uFlagLogica.iBit.AccIncondizionataDaLogica = 1;
                        } else {
                            uFlagLogica.iBit.TestAutonomiaDaLogica = 1;
                            cBufferInterfacciaLogica = cBufferLogicaIn[3]; //contiene la durata in unit� di 4min16sec
                        }
                        cBufferLogicaOut[2] = 1;
                        cLengthBufferLogica = 3;
                        uFlagLogica.iBit.TxRisposta = 1;
                        
                        uFlagLogica.iBit.RispondiACK = 1;
                        break;
                    }
                    
                    //---------------------------------------------------

                    //---------------------------------------------------
                    case COMFT: // COMFT*
                    {
                        uFlagLogica.iBit.StopTestDaLogica = 1;
                        
                        cBufferLogicaOut[2] = 1;
                        cLengthBufferLogica = 3;
                        uFlagLogica.iBit.TxRisposta = 1;
                        
                        //poich� sar� presente un modulino, devo aggiungere la risposta con ACK
                        uFlagLogica.iBit.RispondiACK = 1;
                        break;
                    }
                    
                    //---------------------------------------------------

                    //---------------------------------------------------
                    case COMRST: // COMRST*
                    {
                        uFlagLogica.iBit.AbilitaResetDisp = 1;

                        // E' un reset con ripristino parametri di fabbrica ?
                        if (cBufferLogicaIn[3]) { // S�
                            // Ripristino parametri di fabbrica, ma solo quelli non scritti in fase di programmazione:
                            // da IND_BUILD_LOW a IND_CODICE_PRODOTTO_FINITO_H (escluso)
                            // prima agisco sulla tabella in RAM
                            fResetDiFabbrica();
                            uFlagLogica.iBit.ModConfigDaLogica = 1;
                            uFlagLogica.iBit.AggiornaTabInEeprom = 1;
                        } else {
                            fInitCompleto();
                        }

                        cBufferLogicaOut[2] = 1;
                        cLengthBufferLogica = 3;
                        uFlagLogica.iBit.TxRisposta = 1;
                        
                        //poich� sar� presente un modulino, devo aggiungere la risposta con ACK
                        uFlagLogica.iBit.RispondiACK = 1;
                        
                        break;
                    }
                    
                    //---------------------------------------------------

                    //---------------------------------------------------
                    case COMLI: // COMLI*
                    {
                        tUnion8bit cAppoggioDatoRx;
                        uint8_t cAppoggioDimmerSA;
                        union tParametriFunz uParametriLamp;

                        cAppoggioDatoRx.cReg = cBufferLogicaIn[3];

                        if (0x0F == cAppoggioDatoRx.cNibble.Low) {
                            // S�
                            switch (cAppoggioDatoRx.cNibble.High) {
                                case 0:
                                { //scena_0
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_1_0] & 0x0F);
                                    break;
                                }
                                
                                case 1:
                                { //scena_1
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_1_0] >> 4) & 0x0F);
                                    break;
                                }
                                
                                case 2:
                                { //scena_2
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_3_2] & 0x0F);
                                }
                                break;

                                case 3:
                                { //scena_3
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_3_2] >> 4) & 0x0F);
                                    break;
                                }
                                
                                case 4:
                                { //scena_4
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_5_4] & 0x0F);
                                    break;
                                }
                                
                                case 5:
                                { //scena_5
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_5_4] >> 4) & 0x0F);
                                    break;
                                }
                                
                                case 6:
                                { //scena_6
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_7_6] & 0x0F);
                                    break;
                                }
                                
                                case 7:
                                { //scena_7
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_7_6] >> 4) & 0x0F);
                                    break;
                                }
                                
                                case 8:
                                { //scena_8
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_9_8] & 0x0F);
                                    break;
                                }
                                
                                case 9:
                                { //scena_9
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_9_8] >> 4) & 0x0F);
                                    break;
                                }
                                
                                case 10:
                                { //scena_10
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_11_10] & 0x0F);
                                    break;
                                }
                                
                                case 11:
                                { //scena_11
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_11_10] >> 4) & 0x0F);
                                    break;
                                }
                                
                                case 12:
                                { //scena_12
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_13_12] & 0x0F);
                                    break;
                                }
                                
                                case 13:
                                { //scena_13
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_13_12] >> 4) & 0x0F);
                                    break;
                                }
                                
                                case 14:
                                { //scena_14
                                    cAppoggioDimmerSA = (TabellaLogica[IND_SCENE_15_14] & 0x0F);
                                    break;
                                }
                                
                                case 15:
                                { //scena_15
                                    cAppoggioDimmerSA = ((TabellaLogica[IND_SCENE_15_14] >> 4) & 0x0F);
                                    break;
                                }                                
                            }
                        } else { // No -> � un comando diretto di dimming
                            cAppoggioDimmerSA = cAppoggioDatoRx.cNibble.Low;
                        }
                        uParametriLamp.cReg = TabellaLogica[IND_PARAMETRI_REG];
                        if (uParametriLamp.cBit.SE) {
                            //impongo sempre il tipo SE
                            uFlagFissiLampada.iBit.FunzionePS = 0;
                            uFlagFissiLampada.iBit.FunzioneSA = 0;
                            uFlagFissiLampada.iBit.FunzioneSE = 1;   
                            TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10101111;
                            TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10010000;
                        } else {
                            //**********Gestione SA***************
                            if (0 == cAppoggioDimmerSA) {
                                uFlagFissiLampada.iBit.FunzionePS = 0;
                                uFlagFissiLampada.iBit.FunzioneSA = 0;
                                uFlagFissiLampada.iBit.FunzioneSE = 1;
                                //impongo CentralDimming e SE
                                TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10101111;
                                TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10010000;                            
                            } else {
                                if (14 == cAppoggioDimmerSA) {
                                    uFlagFissiLampada.iBit.FunzionePS = 0;
                                    uFlagFissiLampada.iBit.FunzioneSA = 1;
                                    uFlagFissiLampada.iBit.FunzioneSE = 0; 
                                    //impongo CentralDimming e SA
                                    TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10001111;
                                    TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10100000;
                                } else {
                                    uFlagFissiLampada.iBit.FunzionePS = 1;
                                    uFlagFissiLampada.iBit.FunzioneSA = 0;
                                    uFlagFissiLampada.iBit.FunzioneSE = 0;
                                    //impongo CentralDimming e PS
                                    TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10001111;  
                                    TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10000000;                                           
                                }
                            }
                        }
                        
                        //************************************
                        uFlagLogica.iBit.AggiornaTabInEeprom = 1;

                        cBufferLogicaOut[2] = 1;
                        cLengthBufferLogica = 3;
                        uFlagLogica.iBit.TxRisposta = 1;                       
                        
                        //poich� sar� presente un modulino, devo aggiungere la risposta con ACK
                        uFlagLogica.iBit.RispondiACK = 1;
                        break;
                    }
                    
                    //---------------------------------------------------

                    //---------------------------------------------------
                    default: // UNKNOWN
                    {
                        //nessuna risposta
                        cLengthBufferLogica = 0;
                    }
                    //---------------------------------------------------
                }
            }

             //valida quasi tutte le risposte
            cBufferLogicaOut[0] = TabellaLogica[IND_SERIAL_L];
            cBufferLogicaOut[1] = TabellaLogica[IND_SERIAL_H];
                       
            cStatoLogica = ST_LOGICA_IDLE; //torniamo in IDLE
            break;
        }
        
        //**********************************************************************
//            
        //**********************************************************************
        case ST_LOGICA_FRAME_EXT:
        {
            uFlagLogica.iBit.AbilitaScrittura = 0;
            uFlagLogica.iBit.ComenePendente = 0;
            
            switch (cBufferLogicaIn[4]) // cOpCodeFrameExt
            {
                //---------------------------------------------------
                // Serve solo per non rispondere con errore quando si riceve questo comando
                //---------------------------------------------------
                case OPCOD_RESET_SWAP_FW: //*
                {
                    cBufferLogicaOut[3] = 2; // Scrivi codice di controllo frame esteso
                    cBufferLogicaOut[4] = OPCOD_RESET_SWAP_FW;
                    cBufferLogicaOut[5] = cBufferLogicaIn[5];
                    cLengthBufferLogica = 6;
                    uFlagLogica.iBit.TxRisposta = 1;
                    break;
                }
                
                //---------------------------------------------------
                // Serve solo per non rispondere con errore quando si riceve questo comando
                //---------------------------------------------------
                case OPCOD_UPG_FLASH_END: //*
                {
                    cBufferLogicaOut[3] = 1; // Scrivi codice di controllo frame esteso
                    cBufferLogicaOut[4] = OPCOD_UPG_FLASH_END;
                    cLengthBufferLogica = 5;
                    uFlagLogica.iBit.TxRisposta = 1;
                    break;
                }
                              
                //---------------------------------------------------
                // Comando di inizio aggiornamento firmware: a seguito di questo comando, il controllo passa al bootloader
                //---------------------------------------------------
                case OPCOD_GET_SW_INFO: //*
                {
                    tUnion16bit uLetturaHEF;

                    cBufferLogicaOut[4] = OPCOD_GET_SW_INFO;
                    cBufferLogicaOut[5] = (TabellaLogica[IND_IDSW_REL_IDSW_VER_HI] & 0x0F);
                    cBufferLogicaOut[6] = TabellaLogica[IND_IDSW_VER_LOW];
                    cBufferLogicaOut[7] = TabellaLogica[IND_BUILD_HIGH];
                    cBufferLogicaOut[8] = TabellaLogica[IND_BUILD_LOW];
                    cBufferLogicaOut[9] = TabellaLogica[IND_TIPO_APPARECCHIO_LG];
                    cBufferLogicaOut[10] = 0;

//                    uLetturaHEF.iReg = FLASH_read(ADDR_BOOT_VERSION);
                    uLetturaHEF.iReg = TabellaBootloader[IBUFF_BOOT_VERSION];
                    cBufferLogicaOut[11] = uLetturaHEF.iByte.Low; // Versione bootloader

                    cBufferLogicaOut[12] = RUNNING_MAIN_FIRMWARE; // Firmware attuale che sta girando: firmware principale

                    cBufferLogicaOut[3] = 9; // Scrivi codice di controllo frame esteso
                    
                    uFlagLogica.iBit.TxRisposta = 1;
                    cLengthBufferLogica = 13;
                    break;
                }
                
                
                //---------------------------------------------------
                // Comando di inizio aggiornamento firmware: a seguito di questo comando, il controllo passa al bootloader
                //---------------------------------------------------
                case OPCOD_UPG_FLASH_START: //*
                {
                    tUnion16bit uCalcoloEndFw;
                    tUnion16bit uAddrStartBootloader;
                    uint8_t cFlashLengthPacketWord;

                    uCalcoloEndFw.iByte.High = cBufferLogicaIn[7];
                    uCalcoloEndFw.iByte.Low = cBufferLogicaIn[8];

                    cFlashLengthPacketWord = cBufferLogicaIn[9];
                    cFlashLengthPacketWord >>= 1;

                    uCalcoloEndFw.iReg *= cFlashLengthPacketWord;
                    uCalcoloEndFw.iReg -= 1;

                    //ridondandnte
//                    HEFLASH_readBlock((void*) TabellaBootloader, TAB_BOOT, sizeof (TabellaBootloader));

                    uAddrStartBootloader.iByte.High = TabellaBootloader[IBUFF_INIZIO_BOOT_H];
                    uAddrStartBootloader.iByte.Low = TabellaBootloader[IBUFF_INIZIO_BOOT_L];

                    // La dimensione del nuovo firwmare � compatibile con lo spazio della program memory a disposizione ?
                    if (uCalcoloEndFw.iReg < uAddrStartBootloader.iReg) { // S�
                        TabellaBootloader[IBUFF_NUM_PACKETS_H] = cBufferLogicaIn[7];
                        TabellaBootloader[IBUFF_NUM_PACKETS_L] = cBufferLogicaIn[8];
                        TabellaBootloader[IBUFF_LENGTH_PACKET] = cBufferLogicaIn[9];
                        TabellaBootloader[IBUFF_FW_REGISTER] = START_UPG_FW;
                        TabellaBootloader[IBUFF_FINE_FW_H_TEMP] = uCalcoloEndFw.iByte.High;
                        TabellaBootloader[IBUFF_FINE_FW_L_TEMP] = uCalcoloEndFw.iByte.Low;

                        HEFLASH_writeBlock(TAB_BOOT, (void*) TabellaBootloader, sizeof (TabellaBootloader));

                        uFlagLogica.iBit.AbilitaResetDisp = 1; //impongo il reset per dare il controllo al Bootloader
                        cBufferLogicaOut[4] = OPCOD_UPG_FLASH_START;
                    } else { // No -> tx errore
                        cBufferLogicaOut[4] = (0x80 | OPCOD_UPG_FLASH_START);
                    }
                    
                    //PREDISPONGO LA RISPOSTA
                    cBufferLogicaOut[3] = 6; // Codice di controllo frame esteso
                    cBufferLogicaOut[5] = cBufferLogicaIn[5];
                    cBufferLogicaOut[6] = cBufferLogicaIn[6];
                    cBufferLogicaOut[7] = cBufferLogicaIn[7];
                    cBufferLogicaOut[8] = cBufferLogicaIn[8];
                    cBufferLogicaOut[9] = cBufferLogicaIn[9];
                    
                    uFlagLogica.iBit.TxRisposta = 1;
                    cLengthBufferLogica = 10;
                    break;
                }
                
                //---------------------------------------------------
                // Comando di scrittura del Serial ID
                //---------------------------------------------------
                case OPCOD_WRITE_SERIAL_ID: //*
                {
                    if ((cBufferLogicaIn[5] == cBufferLogicaIn[7]) && (cBufferLogicaIn[6] == cBufferLogicaIn[8]) && (cBufferLogicaIn[5] != 0xFF)) {
                        TabellaLogica[IND_SERIAL_H] = cBufferLogicaIn[5];
                        TabellaLogica[IND_SERIAL_L] = cBufferLogicaIn[6];
                        cBufferLogicaOut[4] =  OPCOD_WRITE_SERIAL_ID;
                    } else {
                        cBufferLogicaOut[4] = (0x80 | OPCOD_WRITE_SERIAL_ID);
                    }
                    uFlagLogica.iBit.AggiornaTabInEeprom = 1;
                    
                    //PREDISPONGO LA RISPOSTA
                    cBufferLogicaOut[3] = 3; // Scrivi codice di controllo frame esteso
                    cBufferLogicaOut[5] = cBufferLogicaIn[5];
                    cBufferLogicaOut[6] = cBufferLogicaIn[6];
                    
                    uFlagLogica.iBit.TxRisposta = 1;
                    cLengthBufferLogica = 7;
                    break;
                }
                
                //---------------------------------------------------
                // Comando di scrittura della data di produzione
                //---------------------------------------------------
                case OPCOD_WRITE_DATE_PRODUCTION: //*
                {
                    if ((cBufferLogicaIn[5] == cBufferLogicaIn[7]) && (cBufferLogicaIn[6] == cBufferLogicaIn[8]) && (cBufferLogicaIn[6] < 53)) {
                        TabellaLogica[IND_YEAR] = cBufferLogicaIn[5];
                        TabellaLogica[IND_WEEK] = cBufferLogicaIn[6];
                        cBufferLogicaOut[4] =  OPCOD_WRITE_DATE_PRODUCTION;
                    } else {
                        cBufferLogicaOut[4] = (0x80 | OPCOD_WRITE_DATE_PRODUCTION);
                    }
                    uFlagLogica.iBit.AggiornaTabInEeprom = 1;

                    //PREDISPONGO LA RISPOSTA
                    cBufferLogicaOut[3] = 3; // Scrivi codice di controllo frame esteso
                    cBufferLogicaOut[5] = cBufferLogicaIn[5];
                    cBufferLogicaOut[6] = cBufferLogicaIn[6];
                    
                    uFlagLogica.iBit.TxRisposta = 1;
                    cLengthBufferLogica = 7;
                    break;
                }

                //---------------------------------------------------
//                
                //---------------------------------------------------
                default:
                {
                    cBufferLogicaOut[4] = (0x80 | cBufferLogicaIn[4]);
                    cBufferLogicaOut[3] = 1; // Scrivi codice di controllo frame esteso                    
                    uFlagLogica.iBit.TxRisposta = 1;
                    cLengthBufferLogica = 5;                    
                }
            }
            
            //valida per tutte le risposte
            cBufferLogicaOut[0] = TabellaLogica[IND_SERIAL_L];
            cBufferLogicaOut[1] = TabellaLogica[IND_SERIAL_H];
            cBufferLogicaOut[2] = COMEXT;
            
            // Il comando ricevuto � di tipo ESTESO BROADCAST?
            // S� -> reinizializzo la seriale in ricezione in quanto un comando broadcast non prevede risposta
            if (uFlagLogica.iBit.RxAddrBroadcastOrGruppo) {
                uFlagLogica.iBit.TxRisposta = 0;
                cLengthBufferLogica = 0;
            }
            cStatoLogica = ST_LOGICA_IDLE; //torniamo in IDLE
            break;
        }
        
        //**********************************************************************
        //**********************************************************************
        default:
        {
            cStatoLogica = ST_LOGICA_IDLE;
        }
    }
    //aggiornamento dei flag di Configurazione
    if ((uFlagLogica.iBit.ModConfigDaLogica) || (uFlagEeprom.cBit.flagAggiornaConfig)) {
        fAggiornaConfigDaTabellaLogica();
    }
    //aggiornamento delle variabili Logica
    fAggiornaVariabiliLogica();    
}
//**********************************************************************************
//**********************************************************************************
//**********************************************************************************
//**********************************************************************************
//**********************************************************************************
//**********************************************************************************


//-------------------------------------------------------------------------------------------------------
// Funzioni per la realizzazione della mutua esclusione nell'accesso alla risorsa condivisa BufferLogica:
//
// fUtilizzaBufferLogica(idAutoma), assegna, se libera, la risorsa all'automa idAutoma
// fRilasciaBufferLogica(idAutoma), rilascia la risorsa precedentemente occupata
//-------------------------------------------------------------------------------------------------------
uint8_t fUtilizzaBufferLogica(uint8_t idAutoma)
{
	if (0 == cbufferLogicaMutex)
	{
		cbufferLogicaMutex = idAutoma;
		return 1;
	}
	else
	{
		if (idAutoma == cbufferLogicaMutex)
		{
			return 1;
		}
		else
		{
			return 0;
		 }
	}
}
//-----------------------------------------------------------------------------------------------------
void fRilasciaBufferLogica(uint8_t idAutoma)
{
	if (idAutoma == cbufferLogicaMutex)
	{
		cbufferLogicaMutex = 0;
	}
}
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
//  void fInitAutomaProtocolloLG(void)
//
//
//----------------------------------------------------------------------------------------
void fInitAutomaProtocolloLG(void) {
    fAggiornaVariabiliLogica();
    iTimerDelayAggiornamento = 0;
    uFlagLogica.iReg = FLAG_LOGICA_INIT;
    cTimeoutAggiornamento = TM_AGGIORNAMENTO;
    cStatoLogica = ST_LOGICA_IDLE;
}

//****************************************************************
// void fAggiornaConfigDaTabellaLogica(void)
//
// modifiche al registro di funzionamento interno dell'AUTOMA_Lampada secondo il valore dei registri
// di configurazione Logica
//
//****************************************************************
void fAggiornaConfigDaTabellaLogica(void) {
    
    uint8_t cDeposito;
    union tActionRegister uActionRegister;
    union tActionRegister2 uActionRegister2;
    union tSaModeRegister uSaModeRegister;
    union tParametriFunz uParametriLamp;

    uActionRegister.cReg = TabellaLogica[IND_ACTION_REGISTER];
    uActionRegister2.cReg = TabellaLogica[IND_ACTION_REGISTER_2];
    uParametriLamp.cReg = TabellaLogica[IND_PARAMETRI_REG];
    
    //-----------------------------------------------------------------------------------------------------
    // Valutazione Pari o Dispari in base all'ID o in base al bus Logica
    //-----------------------------------------------------------------------------------------------------
    if (uActionRegister.cBit.PariDispariID) {
        // In base all'ID_SERIALE
        if (1 == (TabellaLogica[IND_SERIAL_L] % 2)) {
//        if (1 == (TabellaLogica[LG_ID_LOW] % 2)) {
            //DISPARI
            uFlagFissiLampada.iBit.DisparinPari = 1;
        } else {
            //PARI
            uFlagFissiLampada.iBit.DisparinPari = 0;
        }
    } else { // No -> override da bus Logica
        uFlagFissiLampada.iBit.DisparinPari = uActionRegister.cBit.PariDispariUserDefined;
    }
    
    uFlagFissiLampada.iReg &= RESET_AUTONOMIE; //resetto
    if (0 == uActionRegister.cBit.nAutonomiaInBaseAiJumper) {
        //jumper presente -> 1H
        //jumper assente  -> 3H
        //if (uIngressiFilt.iBit.flag1Hn3H) {
        if (uFlagFissiLampada.iBit.PonticelloPresente) {
            uFlagFissiLampada.iBit.Potenza1H = 1;
        } else {
            uFlagFissiLampada.iBit.Potenza3H = 1;
        }
    } else {
        //-----------------------------------------------------------------------------------------------------
        // Durata Autonomia in base ad ActionRegister2 e DIP-SWITCH prioritario
        // (ActionRegister prevede i bit 6 e 5 sempre ad 1...)
        //-----------------------------------------------------------------------------------------------------
        cDeposito = uActionRegister2.cReg >> 1;
        cDeposito = cDeposito & 0b00000111;

        switch (cDeposito) {
            case 1:
            {
                uFlagFissiLampada.iBit.Potenza1H = 1;
            }
            break;

            case 2:
            {
                uFlagFissiLampada.iBit.Potenza1H5 = 1;
            }
            break;

            case 3:
            {
                uFlagFissiLampada.iBit.Potenza2H = 1;
            }
            break;

            case 4:
            {
                uFlagFissiLampada.iBit.Potenza3H = 1;
            }
            break;

            case 6:
            {
                uFlagFissiLampada.iBit.Potenza8H = 1;
            }
            break;

            default:
            {
                uFlagFissiLampada.iBit.Potenza1H = 1;
            }
        }  
    }
    
    //*******************Gestione Parametri*******************
    // L'imposizione di SE di PARAMETRI_REG � prevalente su SaModeRegister, quindi devo sempre ribadirlo.
    // Nel caso di lampada SA, invece, devo lasciare la possibilit� di controllo tramite SaModeRegister, quindi non ribadisco
    if (uParametriLamp.cBit.SE) {
        //impongo CentralDimming e SE
        TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10101111;
        TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10010000;                 
    }
    //poi uFlagFissiLampada viene gestito da uSAModeRegister
    
    switch (uParametriLamp.cBit.Brightness) {
        //----------------------------------------------------------------------
        case LUMEN300:
        {
            uFlagFissiLampada.iBit.Lumen300 = 1;
            uFlagFissiLampada.iBit.Lumen500 = 0;            
            uFlagFissiLampada.iBit.Lumen800 = 0;
            break;
        }
        //----------------------------------------------------------------------
        case LUMEN500:
        {
            uFlagFissiLampada.iBit.Lumen300 = 0;
            uFlagFissiLampada.iBit.Lumen500 = 1;            
            uFlagFissiLampada.iBit.Lumen800 = 0;
            break;
        }
        //----------------------------------------------------------------------
        case LUMEN800:
        {
            uFlagFissiLampada.iBit.Lumen300 = 0;
            uFlagFissiLampada.iBit.Lumen500 = 0;            
            uFlagFissiLampada.iBit.Lumen800 = 1;
            break;
        }
        //----------------------------------------------------------------------
        default:
        {
            uFlagFissiLampada.iBit.Lumen300 = 1;
            uFlagFissiLampada.iBit.Lumen500 = 0;            
            uFlagFissiLampada.iBit.Lumen800 = 0;
            break;
        }
    }
    
//    if (uParametriLamp.cBit.TR) {
//        uFlagFissiLampada.iBit.FunzioneTR = 1;
//    } else {
//        uFlagFissiLampada.iBit.FunzioneTR = 0;
//    }

    //*******************Gestione SA********************
    uSaModeRegister.cReg = TabellaLogica[IND_SA_MODE_REGISTER];
    if (uSaModeRegister.cBit.GestSAPSDaBus) {
        //SAPS decisi da Centrale Logica
        if (!uSaModeRegister.cBit.AutoCentralDimming) {
            //0 -> deciso da Centrale
            if (uSaModeRegister.cBit.FunzioneSE) {
                uFlagFissiLampada.iBit.FunzionePS = 0;
                uFlagFissiLampada.iBit.FunzioneSA = 0;
                uFlagFissiLampada.iBit.FunzioneSE = 1;                        
            } else {
                if (uSaModeRegister.cBit.FunzioneSAnPS) {
                    //1 -> SA
                    uFlagFissiLampada.iBit.FunzionePS = 0;
                    uFlagFissiLampada.iBit.FunzioneSA = 1;
                    uFlagFissiLampada.iBit.FunzioneSE = 0;                            
                } else {
                    //0 -> PS
                    uFlagFissiLampada.iBit.FunzionePS = 1;
                    uFlagFissiLampada.iBit.FunzioneSA = 0;
                    uFlagFissiLampada.iBit.FunzioneSE = 0;                            
                }
            }
        }
    }

    //**************************************************      
    //-----------------------------------------------------------------------------------------------------
    // DisabilitaEmergenza e Sleep e TestPendente sono gestiti direttamente al momento della
    // scrittura di ACTION_REGISTER
    //-----------------------------------------------------------------------------------------------------
    
    //**************************************************       
    //-----------------------------------------------------------------------------------------------------
    // Set Durata Test
    //-----------------------------------------------------------------------------------------------------
    uFlagFissiLampada.iBit.SetDurataTest = uActionRegister2.cBit.Set_Durata_Test;

    //-----------------------------------------------------------------------------------------------------
    // Opticom disabilitato
    //-----------------------------------------------------------------------------------------------------
    uFlagFissiLampada.iBit.OpticomDisabilitato = uActionRegister2.cBit.DisabilitaOpticom;
    
//    //-----------------------------------------------------------------------------------------------------
//    // Emergenza Locale
//    //-----------------------------------------------------------------------------------------------------
//    uFlagFissiLampada.iBit.EmergenzaLocale = uActionRegister2.cBit.EmergenzaLocale;
//      
}

//----------------------------------------------------------------------------------------
//  void fAggiornaVariabiliLogica(void)
//
//
//----------------------------------------------------------------------------------------
void fAggiornaVariabiliLogica(void) {

    union tActionRegister uActionRegister;
//    union tActionRegister2 uActionRegister2;
    union tParametriFunz uParametriLamp;
    union tSaModeRegister uSaModeRegister;

    uActionRegister.cReg = TabellaLogica[IND_ACTION_REGISTER];
//    uActionRegister2.cReg = TabellaLogica[IND_ACTION_REGISTER_2];
    uParametriLamp.cReg = TabellaLogica[IND_PARAMETRI_REG];
    uSaModeRegister.cReg = TabellaLogica[IND_SA_MODE_REGISTER];

    if (uIngressiFilt.iBit.flagRetePresenteFilt) {
        //MAIN presente
        // --> uActionRegister.cBit.DisabilitaEmergenza = 0 && uActionRegister.cBit.Sleep = 0
        TabellaLogica[IND_ACTION_REGISTER] &= 0b11111001;
    }
    
    //----------------------------------------------------------------------------------------
    // ErrorRegister (in RAM)
    //----------------------------------------------------------------------------------------
    uErrorRegister.cReg = 0;

    uErrorRegister.cBit.Led = (uByteErrori.cBit.flagELed || uByteErrori.cBit.flagELedSA); //OR logico tra i 2 possibili errori
    uErrorRegister.cBit.BatteriaAutonomia = uByteErrori.cBit.flagEAuto;
    uErrorRegister.cBit.BatteriaFunzionale = uByteErrori.cBit.flagEFunz;
    uErrorRegister.cBit.BatteriaRicarica = (uIngressiFilt.iBit.flagBatteriaInCorto || uFlagCharger.cBit.flagBatteriaStaccata);
    uErrorRegister.cBit.RicaricaIncompleta = uByteErrori.cBit.flagRicaricaIncompleta;
    //errore hardware non gestito, sempre a zero
    uErrorRegister.cBit.TuboUV = uByteErrori.cBit.flagEtuboUV;
    //----------------------------------------------------------------------------------------
    // StatusByte (in RAM)
    //----------------------------------------------------------------------------------------
    uStatusByte.cReg = 0;
    if (0 != uErrorRegister.cReg) {
        uStatusByte.cBit.ErroriPresenti = 1;
    }
    uStatusByte.cBit.NonProgrammata = uActionRegister.cBit.NonProgrammata;
    uStatusByte.cBit.TestPendente = uFlagNorma.cBit.DelayInCorso;
    uStatusByte.cBit.TestInCorso = ((uFlagLampada.iBit.flagTestInCorso) || (uFlagNorma.cBit.TestInCorso));
    uStatusByte.cBit.EmergenzaDisabilitata = uActionRegister.cBit.DisabilitaEmergenza;
    uStatusByte.cBit.nPariDispari = uFlagFissiLampada.iBit.DisparinPari;
    uStatusByte.cBit.AssenzaRete = !uIngressiFilt.iBit.flagRetePresenteFilt; //logica invertita
    uStatusByte.cBit.Autonomia3h = !uFlagFissiLampada.iBit.Potenza1H; //a zero solo se � 1h

    //----------------------------------------------------------------------------------------
    // ActionRegister (in EEPROM)
    // il bit di test pendente � poco usato e non ha un valore di configurazione, quindi non impongo un
    // aggiornamento dell'Eeprom
    //----------------------------------------------------------------------------------------
    uActionRegister.cBit.TestPendente = uFlagNorma.cBit.DelayInCorso;
    TabellaLogica[IND_ACTION_REGISTER] = uActionRegister.cReg; //aggiorno la tabella RAM

    //----------------------------------------------------------------------------------------
    // StatusByte2 (in RAM)
    //----------------------------------------------------------------------------------------
    uStatusByte2.cReg = 0;

//    uStatusByte2.cBit.EmergenzaLocale = uFlagFissiLampada.iBit.EmergenzaLocale;
    uStatusByte2.cBit.StatoOpticom = uFlagProtOptiCom.iBit.OpticomDisabilitato; //1 -> disabilitato
    uStatusByte2.cBit.StatoCaricaBatt = uFlagCharger.cBit.flagFineCarica; //1 -> finecarica 0 -> ricarica
    uStatusByte2.cBit.ReteSA = uIngressiFilt.iBit.flagReteSAPresente;
    uStatusByte2.cBit.SetDurataTest = uFlagFissiLampada.iBit.SetDurataTest; //0->test auto sempre di 1h  1->test deciso dal setting
    if (uFlagFissiLampada.iBit.Potenza1H) {
        uStatusByte2.cBit.DurataAutonomia = 1;
    } else {
        if (uFlagFissiLampada.iBit.Potenza1H5) {
            uStatusByte2.cBit.DurataAutonomia = 2;
        } else {
            if (uFlagFissiLampada.iBit.Potenza2H) {
                uStatusByte2.cBit.DurataAutonomia = 3;
            } else {
                if (uFlagFissiLampada.iBit.Potenza3H) {
                    uStatusByte2.cBit.DurataAutonomia = 4;
                } else {//8h
                    if (uFlagFissiLampada.iBit.Potenza8H) {
                        uStatusByte2.cBit.DurataAutonomia = 6;
                    } else {
                        uStatusByte2.cBit.DurataAutonomia = 1; //default 1h
                    }
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------
    // SaStatusRegister (in RAM)
    //----------------------------------------------------------------------------------------
    uSaStatusRegister.cReg = 0;
    
    uSaStatusRegister.cBit.TipoSAnSE = !uParametriLamp.cBit.SE;
    
    if (uSaModeRegister.cBit.GestSAPSDaBus) {
        //SAPS decisi da Centrale Logica
        if (!uSaModeRegister.cBit.AutoCentralDimming) {
            //0 -> deciso da Centrale
            uSaStatusRegister.cBit.AutoCentralDimming = uSaModeRegister.cBit.AutoCentralDimming; //??
            uSaStatusRegister.cBit.FunzioneSAnPS = uSaModeRegister.cBit.FunzioneSAnPS;
        } else {
            //1 -> deciso da Lampada
            uSaStatusRegister.cBit.AutoCentralDimming = 1; //gestito da Lampada autonomamente
            if (uFlagFissiLampada.iBit.FunzioneSA) {
                uSaStatusRegister.cBit.FunzioneSAnPS = 1; //0->PS 1->SA
            }
        }
    } else {
        //SAPS decisi da Lampada
        uSaStatusRegister.cBit.AutoCentralDimming = 1; //gestito da Lampada autonomamente
        if (uFlagFissiLampada.iBit.FunzioneSA) {
            uSaStatusRegister.cBit.FunzioneSAnPS = 1; //0->PS 1->SA
        }
    }
    if (uFlagFissiLampada.iBit.FunzionePS) {
        uSaStatusRegister.cBit.DimmerSA = 1;
    } else {
        if (uFlagFissiLampada.iBit.FunzioneSA) {
            uSaStatusRegister.cBit.DimmerSA = 14;
        } else {
            if (uFlagFissiLampada.iBit.FunzioneSE) {
                uSaStatusRegister.cBit.DimmerSA = 0;
            } //in caso di dimming, non modifichiamo
        }
    }
}


//******************************************************************************
//
// uint8_t CmdBroadcastOrGruppoOrId(uint8_t *buffer)
//
// parametri: buffer che contenga  buffer[0]=ID_HI; buffer[1]=ID_LOW
// Ritorna 1 se il comando � per questa Lampada, 0 in caso contrario
//******************************************************************************
uint8_t CmdBroadcastOrGruppoOrId(uint8_t *buffer) {

    uint16_t iMaskGruppi;
    tUnion8bit uAddrByte2;
    uint8_t cId_High;
    uint8_t cId_Low;
    
    cId_High = buffer[0];
    cId_Low = buffer[1];
    
    uFlagLogica.iBit.RxAddrBroadcastOrGruppo = 0;

    if (0xFF == cId_High) {
        if (0xFF == cId_Low) {
            uFlagLogica.iBit.RxAddrBroadcastOrGruppo = 1;            
            return 1; // Cmd per me (indirizzamento broadcast)
        }

        uAddrByte2.cReg = cId_Low;

        if (0xE == uAddrByte2.cNibble.High) {
            iMaskGruppi = (uint16_t) (1 << (uAddrByte2.cNibble.Low));

            if (((TabellaLogica[IND_GRUPPI_15_8] << 8) + TabellaLogica[IND_GRUPPI_7_0]) & iMaskGruppi) {
                uFlagLogica.iBit.RxAddrBroadcastOrGruppo = 1;
                return 1; // Cmd per me (indirizzamento per gruppo)
            }
        }
    }

    if ((TabellaLogica[IND_SERIAL_H] == cId_High) && (TabellaLogica[IND_SERIAL_L] == cId_Low)) {  
        return 1; // Cmd per me (indirizzamento per Serial ID)
    }

    return 0; // Cmd non per me
}

//******************************************************************************
//
// uint8_t CmdPerMe(uint8_t selelection)
//
// paramentri: sel = il byte di selezione dei comandi, il 4 byte per intenderci
//******************************************************************************
uint8_t CmdPerMe(uint8_t selection) {

    union tLampadaAddressBit uLamp;

    uLamp.cReg = selection;

    //--------------------------------------------------------------------------------------------------
    if (uLamp.cBit.NonProgrammata) {
        // La lampada � Non Programmata ?
        if (!uStatusByte.cBit.NonProgrammata) { // No -> la lampada � Programmata
            return 0; // Il comando non � per me
        }
    }
    //--------------------------------------------------------------------------------------------------
    if (uLamp.cBit.InErrore) // Cmd solo per lampade in Errore
    {
        // Errori presenti ?
        if (!uStatusByte.cBit.ErroriPresenti) { // No -> nessun errore presente
            return 0; // Il comando non � per me
        }
    }
    //--------------------------------------------------------------------------------------------------
    if (uLamp.cBit.Autonomia3h) // Cmd solo per lampade con Autonomia 3h
    {
        // Autonomia 3h ?
        if (!uStatusByte.cBit.Autonomia3h) { // No -> Autonomia 1h
            return 0; // Il comando non � per me
        }
    }
    //--------------------------------------------------------------------------------------------------
    if (uLamp.cBit.Autonomia1h) // Cmd solo per lampade con Autonomia 1h
    {
        // Autonomia 1h ?
        if (uStatusByte.cBit.Autonomia3h) { // No -> Autonomia 3h
            return 0; // Il comando non � per me
        }
    }
    //--------------------------------------------------------------------------------------------------
    if (uLamp.cBit.Dispari) // Cmd solo per lampade Dispari
    {
        // Lampada Dispari ?
        if (!uStatusByte.cBit.nPariDispari) { // No -> Lampada Pari
            return 0; // Il comando non � per me
        }
    }
    //--------------------------------------------------------------------------------------------------
    if (uLamp.cBit.Pari) // Cmd solo per lampade Pari
    {
        // Lampada Pari ?
        if (uStatusByte.cBit.nPariDispari) { // No -> Lampada Dispari
            return 0; // Il comando non � per me
        }
    }
    //--------------------------------------------------------------------------------------------------

    return 1; // Il comando � per me
}


//******************************************************************************
//
// void EseguiScrittura(uint8_t *cRegister, uint8_t value)
//
//******************************************************************************
void EseguiScrittura(uint8_t *cRegister, uint8_t value) {

    switch (cTipoWrComene) {
        
        case WR_SET:
        {
            *cRegister = value;
            break;
        }
        
        case WR_OR:
        {
            *cRegister |= value;
            break;
        }
        
        case WR_AND:
        {
            *cRegister &= value;
            break;
        }
        
        case WR_XOR:
        {
            *cRegister ^= value;
            break;
        }
        
    }
}

//******************************************************************************
//
// uint8_t LeggiTabellaProtetta(uint8_t index)
//
// parametri: index = indice di COMRDE, che nel caso di tabella protetta � il 4� byte
//
//-----------------------------
// TABELLA DI SOLA LETTURA
//-----------------------------
// 0 -> TIPO_APPARECCHIO_LG
// 1 -> IDSW_VER_LOW
// 2 -> IDSW_REL_IDSW_VER_HI
// 3 -> not used
// 4 -> not used
// 5 -> ID_LAMPADA_LOW
// 6 -> ID_LAMPADA_HI
// 7 -> TIPO_BATTERIA
// 8 -> TIPO_TUBO
// 9 -> not used
// 10 -> NUM_SETTIMANA_PRODUZIONE
// 11 -> NUM_ANNO_PRODUZIONE
//-----------------------------
//******************************************************************************
uint8_t LeggiTabellaProtetta(uint8_t index) {

    switch (index) {
        case 0:
        {
            return TabellaLogica[IND_TIPO_APPARECCHIO_LG];
        }

        case 1:
        {
            return TabellaLogica[IND_IDSW_VER_LOW];
        }

        case 2:
        {
            return TabellaLogica[IND_IDSW_REL_IDSW_VER_HI];
        }

        case 5:
        {
            return TabellaLogica[IND_SERIAL_L];
        }

        case 6:
        {
            return TabellaLogica[IND_SERIAL_H];
        }

        case 7:
        {
            return TabellaLogicaROM[IND_TIPO_BATTERIA];
        }

        case 8:
        {
            return TabellaLogicaROM[IND_TIPO_TUBO];
        }

        case 10:
        {
            return TabellaLogica[IND_WEEK];
        }

        case 11:
        {
            return TabellaLogica[IND_YEAR];
        }

        default:
        {
            return 0xFF;
        }
    }
}


//******************************************************************************
//
// uint8_t LeggiTabella(uint8_t indice)
//
// parametri: indice = indice di COMRDE, che nel caso di tabella protetta � il 3� byte
//
//------------------------------------------------
// 0 -> ACTION REGISTER
// 1 -> ERROR REGISTER
// 2 -> TIMER FUNZIONALE byteH
// 3 -> TIMER FUNZIONALE byteM
// 4 -> TIMER FUNZIONALE byteL
// 5 -> TIMER AUTONOMIA byteH
// 6 -> TIMER AUTONOMIA byteM
// 7 -> TIMER AUTONOMIA byteL
// 8 -> GRUPPI 7-0
// 9 -> GRUPPI 15-8
// 10 -> TEMPO RESIDUO ULTIMO TEST DI AUTONOMIA (4min e 16sec)
// 11 -> not used
// 12 -> not used
// 13 -> not used
// 14 -> not used
// 15 -> not used
// 16 -> SA MODE REGISTER
// 17 -> SA STATUS REGISTER
// 18 -> not used
// 19 -> not used
// 20 -> not used
// 21 -> SCENA 1 e SCENA 0
// 22 -> SCENA 3 e SCENA 2
// 23 -> SCENA 5 e SCENA 4
// 24 -> SCENA 7 e SCENA 6
// 25 -> SCENA 9 e SCENA 8
// 26 -> SCENA 11 e SCENA 10
// 27 -> SCENA 13 e SCENA 12
// 28 -> SCENA 15 e SCENA 14
// 29..47 -> not used
// 48 -> PARAMETRI REG
// 49 -> TIMER VITA TUBO UV byteH
// 50 -> TIMER VITA TUBO UV byteM
// 51 -> TIMER VITA TUBO UV byteL
// 52 -> POS_BUILD_L
// 53 -> POS_BUILD_H
// 54 -> POS_UVOXY_CONFIG_REG
// 55 -> POS_GIORNO
// 56 -> POS_MESE
// 57 -> POS_ANNO
// 58 -> POS_ID_PLANT
// 59 -> DEVICE_TYPE byteL
// 60 -> DEVICE_TYPE byteH
// 61 -> SERIAL ID byteL
// 62 -> SERIAL ID byteH
// 63 -> RESERVED (indirizzo non disponibile)
//------------------------------------------------
//******************************************************************************
uint8_t LeggiTabella(uint8_t indice) {

    switch (SEL_BIT_ADDRESS & indice) {

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ACTION_REG:
        {
            return TabellaLogica[IND_ACTION_REGISTER];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ERROR_REG:
        {
            return uErrorRegister.cReg;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_FUNZ_H:
        {
            return sTimerCadenzaFunzionale.cByte.High;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_FUNZ_M:
        {
            return sTimerCadenzaFunzionale.cByte.Medium;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_FUNZ_L:
        {
            return sTimerCadenzaFunzionale.cByte.Low;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_AUTO_H:
        {
            return sTimerCadenzaAutonomia.cByte.High;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_AUTO_M:
        {
            return sTimerCadenzaAutonomia.cByte.Medium;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_AUTO_L:
        {
            return sTimerCadenzaAutonomia.cByte.Low;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_GR_7_0:
        {
            return TabellaLogica[IND_GRUPPI_7_0];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_GR_15_8:
        {
            return TabellaLogica[IND_GRUPPI_15_8];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TEMPO_RESIDUO_TEST:
        {
            return TabellaLogica[IND_TEMPO_RESIDUO_LAST_TEST];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SA_MODE_REG:
        {
            return TabellaLogica[IND_SA_MODE_REGISTER];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SA_STATUS_REG:
        {
            return uSaStatusRegister.cReg;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ACTION_REG_2:
        {
            return TabellaLogica[IND_ACTION_REGISTER_2];
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_1_0:
        {
            return TabellaLogica[IND_SCENE_1_0];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_3_2:
        {
            return TabellaLogica[IND_SCENE_3_2];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_5_4:
        {
            return TabellaLogica[IND_SCENE_5_4];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_7_6:
        {
            return TabellaLogica[IND_SCENE_7_6];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_9_8:
        {
            return TabellaLogica[IND_SCENE_9_8];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_11_10:
        {
            return TabellaLogica[IND_SCENE_11_10];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_13_12:
        {
            return TabellaLogica[IND_SCENE_13_12];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_15_14:
        {
            return TabellaLogica[IND_SCENE_15_14];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_PARAMETRI_REG:
        {
            return TabellaLogica[IND_PARAMETRI_REG];
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_MANTENIMENTO_L:
        {
            return TabellaLogica2[IND_MANTENIMENTO_L];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_MANTENIMENTO_H:
        {
            return TabellaLogica2[IND_MANTENIMENTO_H];
        }
        
#ifdef DEBUG_TAR
        case POS_VBATT_L:
        {
            return (uint8_t) mVbatt.iMedia;
        }

        case POS_VBATT_H:
        {
            return (mVbatt.iMedia >> 8);
        }
#endif
        
#ifdef DEB_SU_TAB        
        case POS_DEB_HI:
        {
            return (uIngressiFilt.iReg >> 8);
        }
#endif
        
//        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        case POS_TMR_VITA_H:
//        {
//            return TabellaLogica2[IND_VITA_TUBO_H];
//        }
//
//        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        case POS_TMR_VITA_M:
//        {
//            return TabellaLogica2[IND_VITA_TUBO_M];
//        }
//        
//        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        case POS_TMR_VITA_L:
//        {
//            return TabellaLogica2[IND_VITA_TUBO_L];
//        }        
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_BUILD_L:
        {
            return TabellaLogica[IND_BUILD_LOW];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_BUILD_H:
        {
            return TabellaLogica[IND_BUILD_HIGH];
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_UVOXY_CONFIG_REG:
        {
            return TabellaLogica[IND_UVOXY_CONFIG_REG];
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_GIORNO:
        {
            return TabellaLogica[IND_GIORNO_PROD];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_MESE:
        {
            return TabellaLogica[IND_MESE_PROD];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ANNO:
        {
            return TabellaLogica[IND_ANNO_PROD];
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ID_PLANT:
        {
            return TabellaLogica[IND_ID_PLANT];
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_CODICE_PRODOTTO_FINITO_L:
        {
            return TabellaLogica[IND_CODICE_PRODOTTO_FINITO_L];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_CODICE_PRODOTTO_FINITO_H:
        {
            return TabellaLogica[IND_CODICE_PRODOTTO_FINITO_H];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ID_LAMP_L:
        {
            return TabellaLogica[IND_SERIAL_L];
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ID_LAMP_H:
        {
            return TabellaLogica[IND_SERIAL_H];
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        default:
        {
            return 0xFF;
        }
    }
}

//************************************************************************************
//	void fAllineauByteErrori(void)
//
// descrizione: a fronte di una modifica da remoto del registro LOGICA ErrorRegister, dobbiamo 
//	uniformare anche il registro AUTORIPARA uByteErrori
//
//************************************************************************************
void fAllineauByteErrori(void) {
    if (0 == uErrorRegister.cBit.BatteriaAutonomia) {
        uByteErrori.cBit.flagEAuto = 0;
    }
    if (0 == uErrorRegister.cBit.BatteriaFunzionale) {
        uByteErrori.cBit.flagEFunz = 0;
    }
    if (0 == uErrorRegister.cBit.Led) {
        uByteErrori.cBit.flagELed = 0;
        uByteErrori.cBit.flagELedSA = 0;
    }
    if (0 == uErrorRegister.cBit.BatteriaRicarica) {
        uIngressiFilt.iBit.flagBatteriaInCorto = 0;
        uFlagCharger.cBit.flagBatteriaStaccata = 0;
    }
    if (0 == uErrorRegister.cBit.RicaricaIncompleta) {
        uByteErrori.cBit.flagRicaricaIncompleta = 0;
    }
//    if (0 == uErrorRegister.cBit.TuboUV) {
//        uByteErrori.cBit.flagEtuboUV = 0;
//    }
}

//******************************************************************************
//
// uint8_t ScriviTabella(uint8_t index, uint8_t value, uint8_t chiamante)
//
// parametri: index = indice cella di TabellaLOGICA (=cAddressComene)
//            value = valore da scrivere (4� byte)
//
//******************************************************************************
uint8_t ScriviTabella(uint8_t index, uint8_t value, uint8_t chiamante) {
    
    union tActionRegister uActionRegister;
    union tParametriFunz uParametriLamp;

    switch (index) {
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ACTION_REG:
        {
            EseguiScrittura(&TabellaLogica[IND_ACTION_REGISTER], value);
            
            //1 bit deve essere sempre ad 1:'Autonomia User Defined'
            TabellaLogica[IND_ACTION_REGISTER] |= 0b00100000;

            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            
            iTimerDelayAggiornamento = TIME_WRITE_250MSEC; //ritardiamo l'aggiornamento per consentire operazioni multiple
            
            uActionRegister.cReg = TabellaLogica[IND_ACTION_REGISTER];
//            // 1) considero DisabilitaEmergenza e Sleep solo in assenza rete per sicurezza
            if (uActionRegister.cBit.DisabilitaEmergenza) {
                uFlagLogica.iBit.DisabilitaEmergenza = 1;
            } else {
                uFlagLogica.iBit.AbilitaEmergenza = 1;
            }
            if (uActionRegister.cBit.Sleep) {
                uFlagLogica.iBit.SleepDaLogica = 1;
            }
            TabellaLogica[IND_ACTION_REGISTER] &= 0b11111101; //SICUREZZA: resetto eventuale Sleep che sono one-shot...
//            }
        
            // 2) se c'� un test rimandato e il bit di ActionReg viene resettato, allora devo eseguire il test
            //if ((uFlagFissiLampada.iBit.TestRimandato) && (!uActionRegister.cBit.TestPendente)) {
            if ((uFlagNormaFissi.cBit.DelayTest) && (!uActionRegister.cBit.TestPendente)) {
                uFlagLogica.iBit.TestAutonomiaDaLogica = 1; //impongo un test
                cBufferInterfacciaLogica = 0xFE; //richiede che venga caricata l'autonomia prevista dalla configurazione
                //resetto le variabili e i flag di test pendente, mentre non tocco la parte di INHIBIT
                sTimerDelayTest.slTimer = 0;
                uFlagNormaFissi.cBit.DelayTest = 0; //fine rimando
            }
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ERROR_REG:
        {
            EseguiScrittura(&uErrorRegister.cReg, value);
            fAllineauByteErrori();
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_FUNZ_H:
        {
            EseguiScrittura(&sTimerCadenzaFunzionale.cByte.High, value);
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_FUNZ_M:
        {
            EseguiScrittura(&sTimerCadenzaFunzionale.cByte.Medium, value);
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_FUNZ_L:
        {
            EseguiScrittura(&sTimerCadenzaFunzionale.cByte.Low, value);
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_AUTO_H:
        {
            EseguiScrittura(&sTimerCadenzaAutonomia.cByte.High, value);
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_AUTO_M:
        {
            EseguiScrittura(&sTimerCadenzaAutonomia.cByte.Medium, value);
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_TMR_CAD_AUTO_L:
        {
            EseguiScrittura(&sTimerCadenzaAutonomia.cByte.Low, value);
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_GR_7_0:
        {
            EseguiScrittura(&TabellaLogica[IND_GRUPPI_7_0], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_GR_15_8:
        {
            EseguiScrittura(&TabellaLogica[IND_GRUPPI_15_8], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SA_MODE_REG:
        {
            EseguiScrittura(&TabellaLogica[IND_SA_MODE_REGISTER], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            iTimerDelayAggiornamento = TIME_WRITE_250MSEC; //ritardiamo l'aggiornamento per consentire operazioni multiple
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ACTION_REG_2:
        {
            EseguiScrittura(&TabellaLogica[IND_ACTION_REGISTER_2], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            iTimerDelayAggiornamento = TIME_WRITE_250MSEC; //ritardiamo l'aggiornamento per consentire operazioni multiple
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_1_0:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_1_0], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_3_2:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_3_2], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_5_4:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_5_4], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_7_6:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_7_6], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_9_8:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_9_8], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_11_10:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_11_10], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_13_12:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_13_12], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_SCENA_15_14:
        {
            EseguiScrittura(&TabellaLogica[IND_SCENE_15_14], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_PARAMETRI_REG:
        {
            value &= MASK_PARAMETRI;
            //procedo solo se diverso da ZERO
            if (0 != value) {
                EseguiScrittura(&TabellaLogica[IND_PARAMETRI_REG], value);
//                RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                RegAggiornaParametri = RICHIESTA_PARAMETRI;
//                RegAggiornaTaratura = 0x00;
                fRichiediAggEE(MOD_PARAMETRO);
                iTimerDelayAggiornamento = TIME_WRITE_250MSEC; //ritardiamo l'aggiornamento per consentire operazioni multiple
            
                //*******************Gestione Parametri*******************
                //qui gestiamo le imposizioni su ParametriAT che valgono in teoria solo all'inizio, poi
                //il funzionamento normale prosegue con il registro Sa_Mode_Register
                uParametriLamp.cReg = TabellaLogica[IND_PARAMETRI_REG];
                if (uParametriLamp.cBit.SE) {
                    //impongo CentralDimming e SE
                    TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10101111;
                    TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10010000;                 
                    //impongo il TIPO_DISPOSITIVO_LG 
                    TabellaLogica[IND_TIPO_APPARECCHIO_LG] = TIPO_LG_SE;
                } else {
                    if (uParametriLamp.cBit.SAnPS) {
                        //impongo CentralDimming e SA
                        TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10001111;
                        TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10100000;            
                    } else {
                        //impongo CentralDimming e PS
                        TabellaLogica[IND_SA_MODE_REGISTER] &= 0b10001111;  
                        TabellaLogica[IND_SA_MODE_REGISTER] |= 0b10000000;
                    }
                    //impongo il TIPO_DISPOSITIVO_LG 
                    TabellaLogica[IND_TIPO_APPARECCHIO_LG] = TIPO_LG_SA;
                }
            }            
            return 1;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_MANTENIMENTO_H:
        {
            EseguiScrittura(&TabellaLogica2[IND_MANTENIMENTO_H], value);
            return 1;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_MANTENIMENTO_L:
        {
            EseguiScrittura(&TabellaLogica2[IND_MANTENIMENTO_L], value);
            return 1;
        }
        

//        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        case POS_TMR_VITA_H:
//        {
//            EseguiScrittura(&TabellaLogica2[IND_VITA_TUBO_H], value);
//            return 1;
//        }
//
//        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        case POS_TMR_VITA_M:
//        {
//            EseguiScrittura(&TabellaLogica2[IND_VITA_TUBO_M], value);
//            return 1;
//        }
//        
//        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        case POS_TMR_VITA_L:
//        {
//            EseguiScrittura(&TabellaLogica2[IND_VITA_TUBO_L], value);
//            return 1;
//        }
//            
//        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        case POS_UVOXY_CONFIG_REG:
//        {
//            value &= 0b00111111;
//            value |= 0b10000000; //mantengo l'indicazione che si tratta di lampada COVID
//            EseguiScrittura(&TabellaLogica[IND_UVOXY_CONFIG_REG], value); 
//            uFlagLogica.iBit.AggiornaTabInEeprom = 1;
//            iTimerDelayAggiornamento = TIME_WRITE_250MSEC; //ritardiamo l'aggiornamento per consentire operazioni multiple
//            return 1;
//        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_GIORNO:
        {
            EseguiScrittura(&TabellaLogica[IND_GIORNO_PROD], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_MESE:
        {
            EseguiScrittura(&TabellaLogica[IND_MESE_PROD], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;      
            return 1;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ANNO:
        {
            EseguiScrittura(&TabellaLogica[IND_ANNO_PROD], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ID_PLANT:
        {
            EseguiScrittura(&TabellaLogica[IND_ID_PLANT], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_CODICE_PRODOTTO_FINITO_L:
        {
            EseguiScrittura(&TabellaLogica[IND_CODICE_PRODOTTO_FINITO_L], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_CODICE_PRODOTTO_FINITO_H:
        {
            EseguiScrittura(&TabellaLogica[IND_CODICE_PRODOTTO_FINITO_H], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ID_LAMP_L: // Serial ID (byte low)
        {
            EseguiScrittura(&TabellaLogica[IND_SERIAL_L], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        case POS_ID_LAMP_H: // Serial ID (byte high)
        {
            EseguiScrittura(&TabellaLogica[IND_SERIAL_H], value);
            //uFlagLogica.iBit.AggiornaTabInEeprom = 1;
            RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
            RegAggiornaParametri = 0x00;
            return 1;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        default:
        {
            return 0;
        }
    }
}
