//
// AUTOMA_Comunicazione.c
//
// Convenzioni:
// FLASH ON (=> valore luminosit� maggiore => tempo di carica del condensatore minore)
//           => UNO => (iCampione < (mLuminositaMedia.iMedia - VALORE_UNO))
//
// FLASH OFF (=> valore luminosit� minore => tempo di carica del condensatore maggiore) 
//           => ZERO => (!(iCampione < (mLuminositaMedia.iMedia - VALORE_UNO)))
//
// START_BIT � UNO
// STOP_BIT � ZERO
// BIT_UNO � UNO
// BIT_ZERO � ZERO
//
#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_DriverOptiCom.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_AccenditoreSE.h"
#include "AUTOMA_AccenditoreSA.h"
#include "mcc_generated_files/pin_manager.h"

//tFlagCompensazione uFlagCompensazione;
//uint16_t iDutyCompensTotale;
//uint8_t cStatoCompensatore;

uint8_t cStatoDriverOptiCom;
uint16_t iTimerComunicazione;
uint16_t iDepoTimer;
tFlagOptiCom uFlagDriverOptiCom;
uint8_t cTimeoutDriver;
uint16_t iTempoRitaratura;

uint8_t cParitaRx;

uint8_t cContaCampioni;
uint8_t cNumeroCampioniOpti;
uint8_t cCampioniCorretti;
uint8_t cContaCampioniMin;
uint8_t cContaCampioniMag;
uint16_t iGapFlash;

uint8_t cBufferOptiLight;   

uint8_t cGapCampioniFirstHalf;
uint8_t cGapCampioniSecondHalf;
    
uint8_t cBitReceivedOPT; // Indice dei bit ricevuti

volatile uint8_t cDivisore;

misuralong mLuminositaMedia6;
uint8_t cMultipliTmr;
uint16_t iCampione6;

uint8_t cCampioniCorrezioneLenta;

uint8_t cCicliAck;

extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFlagTimer uFlagTimer;
extern tFlagLampada uFlagLampada;
extern tFlagFissiLampada uFlagFissiLampada;
extern tFlagAccenditoreSE uFlagAccenditoreSE;
extern tFiltrato uIngressiFilt;
extern union tFlagAccenditoreSA uFlagAccenditoreSA;

//extern void fImponiDutyAck(void);
extern void fAccendiPwmSE(uint16_t d);
extern void fSpegniPwmSE(void);
extern void fAggiornaDutySE(uint16_t iDutySE);
extern void fSpegniSA(void);

//----------------------------------------------------------------------------------------------
//Le misure di luminosit� sono misure di tempo di carica di un condensatore da parte
//del fototransistor: maggiore � la luce presente e minore sar� il tempo di carica.
//Il valore di riferimento � fornito dal DAC.
//L'unit� di tempo di misurazione � di 20msec: in questo tempo, il comparatore deve scattare,
//fornire un valore temporale che rappresenter� la luce vista, e poi il condensatore 
//e il timer devono essere azzerati per ricominciare il conteggio nell'unit� di tempo successiva.
//Se nei 10msec non avviene questo scatto, occorre modificare la soglia del DAC, abbassandola
//perch� la luminosit� risulta molto bassa.
//Il buffer che effettua la media � di 1024 campioni, che presi ogni 10msec, determinano
//una base temporale di 20sec e 240msec: questo tempo deve essere sempre atteso prima di
//effettuare una verifica di comunicazione perch� � necessario avere una media precisa e
//stabile. In questa fase faremo in modo che la lampada segnali di non fare comunicazioni...
//La profondit� del buffer della luminosit� media, dovrebbe garantire che una eventuale 
//comunicazioni non vada a perturbare in modo significativo la media stessa, in modo che il
//confronto tra media e valore istantaneo consenta sempre di individuare la presenza di una
//comunicazione luminosa.
//-----------------------------------------------------------------------------------------------


//*****************************************************************************
// void fAggiornaLuminosita(misuralong * mMisDaAgg, unsigned int iNuovoCamp)
//
// aggiorna le strutture di tipo "misuralong", facendo la nuova media
//*****************************************************************************
void fAggiornaLuminosita(misuralong * mMisDaAgg, unsigned int iNuovoCamp, unsigned char numcampioni) {
    mMisDaAgg->iSomma = mMisDaAgg->iSomma + iNuovoCamp - mMisDaAgg->iMedia;
    mMisDaAgg->iMedia = mMisDaAgg->iSomma >> numcampioni;
}

//*****************************************************************************
// void fAzzeraLuminosita(misuralong * mMisDaAgg, unsigned int iMediaIniziale)
//
// azzera la struttura di tipo "misuralong"
//*****************************************************************************
void fInizializzaLuminosita(misuralong * mMisDaAgg, unsigned int iMediaIniziale, unsigned char numcampioni) {
    mMisDaAgg->iSomma = (uint32_t) iMediaIniziale << numcampioni;
    mMisDaAgg->iMedia = iMediaIniziale;
}

//*****************************************************************************
// bool_t fDecrementaDac(uint8_t step)
//
//*****************************************************************************
bool_t fDecrementaDac(uint8_t step) {
    while (step > 0) {
        if (DAC1CON1 > LIVELLO_MIN_DAC) {
            DAC1CON1--;
            step--;
        } else {
            return FALSE; //usciamo perch� in overflow
        }
    }
    return TRUE;
}

//*****************************************************************************
// bool_t fIncrementaDac(uint8_t step)
//
//*****************************************************************************
bool_t fIncrementaDac(uint8_t step) {
    while (step > 0) {
        if (DAC1CON1 < LIVELLO_MAX_DAC) {
            DAC1CON1++;
            step--;
        } else {
            return FALSE; //usciamo perch� in overflow
        }
    }
    return TRUE;
}

//*****************************************************************************
// void fTransToOptInit(uint16_t time)
//
//*****************************************************************************
void fTransToOptInit(uint16_t time) {
    cContaCampioniMin = 0; //azzero
    cContaCampioniMag = 0;
    cCampioniCorretti = 0;
    iTempoRitaratura = time;
    cStatoDriverOptiCom = ST_OPTICOM_INIT;
}

//*****************************************************************************
// void fInitAutomaDriverOptiComRx(void)
//
//*****************************************************************************

void fInitAutomaDriverOptiComRx(void) {
    //INIT delle PERIFERICHE:
    //inizializzazione della Fixed Voltage Reference
    FVRCON = FVR_BASE; //FVR ON; COMP FVR = 2.048V; ADC FVR = 2.048V

    //DAC1
    DAC1CON0 = 0b00001000; //disable,disconnected_from_pin,fvr,vss
    DAC1CON1 = DAC_50_PERCENTO; //vdac = x% * 2.048V = xxxxV

    //    //COMPARATOR1
    //    //l'ingresso positivo riceve il DAC, quello negativo il condensatore caricato dal fotosensore, l'uscita deve essere negata
    //    // -   | C1IN3- (RC3)
    //    // +   | DAC_output
    //    // out | C1POL=1 (polarit� invertita)
    //    CM1CON0 = 0b00010111; //disable,inverted,normalspeed,hysteresis, tmr1_sync
    //    CM1CON1 = 0b10101011; //int pos,dac in+,c1in3- in-

    //COMPARATOR2
    //l'ingresso positivo riceve il DAC, quello negativo il condensatore caricato dal fotosensore, l'uscita deve essere negata
    // -   | CxIN0- (RA0)
    // +   | DAC_output
    // out | C2POL=1 (polarit� invertita)
    CM2CON0 = 0b00010110; //disable,inverted,normalspeed,hysteresis, no tmr1_sync
    CM2CON1 = 0b10101000; //int pos,dac in+,c2in0- in-
    //teniamo spento il sistema non facendo mai salire il condensatore
    DISC_OPTI_SetHigh();
    DISC_OPTI_SetDigitalOutput();

    TMR6 = 0;
    cMultipliTmr = 0;
    T6CON = INIT_TMR6;
    PR6 = 255;
    PIE2bits.TMR6IE = 0; //interrupt disabilitato
    //fInizializzaLuminosita(&mLuminositaMedia6, VAL_SETPOINT);

    V_OPTI_SetDigitalInput();

    DAC1CON0bits.DAC1EN = 1;
    //    CM1CON0bits.C1ON = 1; //abilito il COMPARATOR_1
    //    PIR2bits.C1IF = 0; //resetto flag
    //    PIE2bits.C1IE = 1; //abilito interrupt COMPARATOR1
    CM2CON0bits.C2ON = 1; //abilito il COMPARATOR_2
    PIR2bits.C2IF = 0; //resetto flag
    PIE2bits.C2IE = 1; //abilito interrupt COMPARATOR2

    fTransToOptInit(TIME_RITARATURA_LUNGA);
}

////*****************************************************************************
//// void fSpegniOptiCom(void)
////
////*****************************************************************************
//void fSpegniOptiCom(void) {
//    DAC1EN = 0; //spengo DAC1
//    C2ON = 0;   //spengo COMP2
//    C2IF = 0;
//    C2IE = 0;   //disabilito interrupt COMP2
//    TMR6IE = 0; //disabilito interrupt TMR6
//    TMR6ON = 0; //sepgno TMR6
//    DISC_OPTI_SetLow();
//    DISC_OPTI_SetDigitalOutput();
//}

//*****************************************************************************
// void fTransToRicercaInDecremento(void)
//
//*****************************************************************************
void fTransToRicercaInDecremento(void) {
    cCampioniCorretti = 0; //azzero
    cContaCampioni = 0;
    cStatoDriverOptiCom = ST_OPTICOM_RICERCA_IN_DECREMENTO;
}

//*****************************************************************************
// void fTransToRicercaInIncremento(void)
//
//*****************************************************************************
void fTransToRicercaInIncremento(void) {
    cCampioniCorretti = 0; //azzero
    cContaCampioni = 0;
    cStatoDriverOptiCom = ST_OPTICOM_RICERCA_IN_INCREMENTO;
}

//*****************************************************************************
// void fTransToMediaIniziale(uint16_t time)
//
//*****************************************************************************
void fTransToMediaIniziale(uint16_t time) { 
    iTimerComunicazione = time;
    cStatoDriverOptiCom = ST_OPTICOM_MEDIA_INIZIALE;
}

//*****************************************************************************
// void fTransToOptNormale(void)
//
//*****************************************************************************
void fTransToOptNormale(void) {
    cContaCampioni = 0;
    cCampioniCorrezioneLenta = 50;
    cStatoDriverOptiCom = ST_OPTICOM_NORMALE;   
}

//*****************************************************************************
// void fRicominciaCamp(void)
//
//*****************************************************************************
void fRicominciaCamp(void) {
    uFlagFissiLampada.iBit.CampioneOpti = 0; //resettiamo
}

//*****************************************************************************
// void fInitContatoriFirstHalf(void)
//
//*****************************************************************************
void fInitContatoriFirstHalf(void) {
    cGapCampioniFirstHalf = 3;
    cContaCampioni = cNumeroCampioniOpti + cGapCampioniFirstHalf; //7
//    cContaCampioni = cNumeroCampioniOpti + 3;
    cContaCampioniMin = 0;
    cContaCampioniMag = 0;
    iTimerComunicazione = TIMEOUT_BIT;
}

//*****************************************************************************
// void fInitContatoriSecondHalf(void)
//
//*****************************************************************************
void fInitContatoriSecondHalf(void) {
    cGapCampioniSecondHalf = 2;
    cContaCampioni = cNumeroCampioniOpti + cGapCampioniSecondHalf; //7
//    cContaCampioni = cNumeroCampioniOpti + 3;
    cContaCampioniMin = 0;
    cContaCampioniMag = 0;
    iTimerComunicazione = TIMEOUT_BIT;
}

//*****************************************************************************
// void fRxUNO(void)
//
// inserisce nel buffer di ricezione un UNO
//*****************************************************************************
void fRxUNO(void) {
    //10 -> 1
    //rx del bit UNO
    cBufferOptiLight <<= 1;
    cBufferOptiLight |= 0x01;
    cParitaRx ^= 1;
}

//*****************************************************************************
// void fRxZERO(void)
//
// inserisce nel buffer di ricezione uno ZERO
//*****************************************************************************
void fRxZERO(void) {
    //01 -> 0
    //rx del bit ZERO
    cBufferOptiLight <<= 1;
    cBufferOptiLight &= 0xFE;    
}


//*****************************************************************************
// void AUTOMA_DriverOptiComRx(void)
//
//*****************************************************************************
void AUTOMA_DriverOptiComRx(void) {

    uint16_t iDepo;
    
    uFlagDriverOptiCom.cReg = 0;

    if (uFlagTimer.iBit.flag1msec) {
        if (0 != iTimerComunicazione) {
            iTimerComunicazione--;
        }
    }

    if (uFlagTimer.iBit.flag20msec) {
        if (0 != cTimeoutDriver) {
            cTimeoutDriver--;
        }
    }

    //protezione corto
    if (uFlagAccenditoreSE.cBit.flagProtCorto) {
        uFlagAccenditoreSE.cBit.flagProtCorto = 0;
        cStatoDriverOptiCom = ST_OPTICOM_TX_PROT_CORTO;
    }

    switch (cStatoDriverOptiCom) {
               
        //----------------------------------------------------------------------------------------------
        // Questo stato decide all'inizio la direzione della taratura del DAC, poi essa verr� portata
        // a termine da RICERCA IN ALTO oppure da RICERCA IN BASSO
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_INIT:
        {
            uFlagDriverOptiCom.cBit.flagFaseStabilizzazione = 1; //impone una segnalazione led

#ifdef DAC_FISSO
            DAC1CON1 = DAC_75_PERCENTO;
            fTransToMediaIniziale(iTempoRitaratura);
            cStatoDriverOptiCom = ST_OPTICOM_MEDIA_INIZIALE;
#else
            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if (iCampione6 < RANGE_INIT_MIN) {
                    //campione fuori in basso
                    cContaCampioniMag = 0;
                    cContaCampioniMin++;
                    if (cContaCampioniMin > NUM_MINIMO_CAMPIONI) {
                        if (fIncrementaDac(1)) {
                            fTransToRicercaInIncremento();
                        } else {
                            //siamo gi� al massimo, meglio non possiam fare
                            //inizializzo la media con il valore di campione trovato...
                            fInizializzaLuminosita(&mLuminositaMedia6, iCampione6, PASSI_SHIFT);
                            fTransToMediaIniziale(iTempoRitaratura);
                        }
                    }
                } else {
                    cContaCampioniMin = 0;
                    if (iCampione6 > RANGE_INIT_MAX) {
                        //campione fuori in alto
                        cContaCampioniMag++;
                        if (cContaCampioniMag > NUM_MINIMO_CAMPIONI) {
                            if (fDecrementaDac(1)) {
                                fTransToRicercaInDecremento();
                            } else {
                                //siamo gi� al minimo, meglio non possiamo fare:
                                //inizializzo la media con il valore di campione trovato...
                                fInizializzaLuminosita(&mLuminositaMedia6, iCampione6, PASSI_SHIFT);
                                fTransToMediaIniziale(iTempoRitaratura);
                            }
                        }
                    } else {
                        //campione in range
                        cContaCampioniMag = 0;
                        cContaCampioniMin = 0;
                        cCampioniCorretti++;
                        if (cCampioniCorretti > NUM_MINIMO_CAMPIONI) {                            
                            fInizializzaLuminosita(&mLuminositaMedia6, VAL_SETPOINT_LUM, PASSI_SHIFT);
                            fTransToMediaIniziale(iTempoRitaratura);
                        }
                    }
                }
            }
#endif
            break;
        }

        //----------------------------------------------------------------------------------------------
        // Stato di ricerca del corretto valore del DAC, verso il basso: si giunge qui quando non interviene
        // l'interrupt del Comparatore, quindi quando il tempo di carica del condensatore � superiore a
        // VAL_RANGEALTO.
        //
        // Algoritmo: si decrementa il DAC, si attendono la stabilit� delle misure, e si procede fino a quando
        // non si ottengono almeno NUM_CAMP_PER_STAB campioni sequenziali con un valore inferiore a VAL_SETPOINT.
        //
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_RICERCA_IN_INCREMENTO:
        {//dobbiamo scendere subito sotto VAL_SETPOINT
            uFlagDriverOptiCom.cBit.flagFaseStabilizzazione = 1; //impone una segnalazione led

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                //anche se ci fosse un rimpallo sulla soglia tra campioni corretti e non, al limite l'effetto sar�
                //di scendere di un altro punto di DAC, e a questo punto si avrebbero tutti campioni corretti (da verificare)
                if (iCampione6 > VAL_SETPOINT_LUM) {
                    cCampioniCorretti++;
                    if (cCampioniCorretti > NUM_MINIMO_CAMPIONI) {
                        //siamo a regime, ripartiamo con la media iniziale
                        fInizializzaLuminosita(&mLuminositaMedia6, VAL_SETPOINT_LUM, PASSI_SHIFT);
                        fTransToMediaIniziale(iTempoRitaratura);
                    }//altrimenti continuo
                } else {
                    cContaCampioni++;
                    if (cContaCampioni > NUM_CAMP_PER_STAB) {
                        if (fIncrementaDac(1)) { //non dovrebbe creare problemi alla misura in corso perch� la soglia cambia leggermente
                            //decremento e ricomincio
                            cCampioniCorretti = 0;
                            cContaCampioni = 0;
                        } else {
                            //siamo al massimo di DACCON1, meglio non si pu� fare...
                            fInizializzaLuminosita(&mLuminositaMedia6, iCampione6, PASSI_SHIFT);
                            fTransToMediaIniziale(iTempoRitaratura);
                        }
                    }
                }
            }
            break;
        }
        
        //----------------------------------------------------------------------------------------------
        // Stato di ricerca del corretto valore del DAC, verso l'alto: si giunge qui quando il tempo di carica
        // del condensatore � inferiore VAL_RANGEBASSO.
        //
        // Algoritmo: si incrementa il DAC, per ogni passo si attendono la stabilit� delle misure, e si procede fino
        // a quando non si ottengono almeno NUM_CAMP_PER_STAB campioni sequenziali con un valore superiore a VAL_SETPOINT
        //
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_RICERCA_IN_DECREMENTO:
        {//dobbiamo risalire subito sopra a VAL_SETPOINT
            uFlagDriverOptiCom.cBit.flagFaseStabilizzazione = 1; //impone una segnalazione led

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if (iCampione6 < VAL_SETPOINT_LUM) {
                    cCampioniCorretti++;
                    if (cCampioniCorretti > NUM_MINIMO_CAMPIONI) {
                        //siamo a regime, ripartiamo con la media iniziale
                        fInizializzaLuminosita(&mLuminositaMedia6, VAL_SETPOINT_LUM, PASSI_SHIFT);
                        fTransToMediaIniziale(iTempoRitaratura);
                    }//altrimenti continuo
                } else {
                    cContaCampioni++;
                    if (cContaCampioni > NUM_CAMP_PER_STAB) {
                        //cio� se ho aspetto almeno 30msec e son sicuro che il DAC sia stabile
                        if (fDecrementaDac(1)) {
                            //incremento e ricomincio
                            cCampioniCorretti = 0;
                            cContaCampioni = 0;
                        } else {
                            //sono arrivato a finecorsa, meglio non si pu� fare....
                            fInizializzaLuminosita(&mLuminositaMedia6, iCampione6, PASSI_SHIFT);
                            fTransToMediaIniziale(iTempoRitaratura);
                        }
                    } //altrimenti continuo
                }
            }
            break;
        }
        //----------------------------------------------------------------------------------------------
        // Data la variabilit� della luminosit� ambientale, prevediamo un aggiustamento tramite il DAC per
        // porci sempre ad un livello di luminosit� media pari a circa il 60% del valore massimo in modo
        // da evitare saturazioni che renderebbero inefficace la ricezione.
        // Il livello luminosit� MEDIO � dato dal tempo di carica MEDIO del condensatore in serie al
        // fotodiodo (maggior luce => maggior corrente di carica => minor tempo di carica del condensatore)
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_MEDIA_INIZIALE:
        {
            uFlagDriverOptiCom.cBit.flagFaseStabilizzazione = 1; //impone una segnalazione led

            if (uFlagFissiLampada.iBit.CampioneOpti) {

                fRicominciaCamp();
                if (0 == iTimerComunicazione) {
                    fTransToOptNormale();
                } else {
                    fAggiornaLuminosita(&mLuminositaMedia6, iCampione6, PASSI_SHIFT);
                }
            }
            break;
        }
              
        //----------------------------------------------------------------------------------------------
        // Stato di funzionamento ordinario: la luminosit� media � stata calcolata, e si continua comunque
        // a calcolare, mentre si resta in attesa di uno START_BIT nella forma di CAMPIONE con valore
        // significativamente inferiore alla media.
        // In caso avvenga ci�, si passa nello stato di ricezione dello START_BIT.
        // Se la luminosit� MEDIA dovesse salire troppo o scendere sotto il livello VAL_RANGEBASSO, allora
        // si passa nuovamente in regolazione.
        //
        // VARIABILI IMPORTANTI
        // cContaCampioniMin = conta i campioni minori della MEDIA
        // cContaCampioniMag = conta i campioni maggiori della MEDIA
        // cNumeroCampioniOpti = attualmente � 10 (10x20=200msec)
        // cContaCampioni = conta il numero di campioni a 20msec ricevuti
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_NORMALE:
        {
            if (uFlagProtOptiCom.iBit.TrasmettiDone) {
                if (uFlagAccenditoreSA.cBit.LedSaAccesi) {
                    iDepoTimer = TM_LED_OFF_SA; //1000msec di led OFF
                } else {
                    iDepoTimer = TM_LED_OFF_SE; //100msec di led OFF
                }
                iTimerComunicazione = iDepoTimer; //memorizzo anche per l'ultima fase
                cCicliAck = 1;
                cStatoDriverOptiCom = ST_OPTICOM_TX_DONE_INIT;
            } else {
                if (uFlagProtOptiCom.iBit.TrasmettiNotEnabled) {
                    if (uFlagAccenditoreSA.cBit.LedSaAccesi) {
                        iDepoTimer = TM_LED_OFF_SA; //1000msec di led OFF
                    } else {
                        iDepoTimer = TM_LED_OFF_SE; //100msec di led OFF
                    }
                    iTimerComunicazione = iDepoTimer; //memorizzo anche per l'ultima fase
                    cCicliAck = 5;
                    cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_EN_INIT;
                } else {
                    if (uFlagProtOptiCom.iBit.TrasmettiNotImplemented) {
                        if (uFlagAccenditoreSA.cBit.LedSaAccesi) {
                            iDepoTimer = TM_LED_OFF_SA; //1000msec di led OFF
                        } else {
                            iDepoTimer = TM_LED_OFF_SE; //100msec di led OFF
                        }
                        iTimerComunicazione = iDepoTimer; //memorizzo anche per l'ultima fase                    
                        cCicliAck = 5;
                        cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_IMPL_INIT;
                    } else {
                        if (uFlagFissiLampada.iBit.CampioneOpti) {
                            fRicominciaCamp();
                            //terminato il periodo di stabilizzazione del sensore di luminosit� (buffer di xy secondi circa)
                            //stabilisco la sensibilit�

        #ifdef GAP_FISSO
                            //ogni 10msec, occorre verificare se � intervenuto il comparatore_1, in caso affermativo,
                            //calcolare la nuova media ed azzerare il condensatore e il TMR1
                            fAggiornaLuminosita(&mLuminositaMedia6, iCampione6, PASSI_SHIFT);
                            //controllo di ricezione START BIT, verificando la differenza tra valore medio ed istantaneo

                            iGapFlash = mLuminositaMedia6.iMedia / SENS_HI;
        #else                    
                            if (DAC1CON1 > 235) {
                                cDivisore = SENS_HI; //%
                            } else {
                            if (DAC1CON1 > 128) {
                                    //molta luce, quindi soglia DAC alta, rendiamo pi� sensibile
                                    cDivisore = SENS_MEDIUM; //%
                                } else {
                                    //poca luce, quindi soglia DAC bassa, rendiamo meno sensibile
                                    cDivisore = SENS_LO; //%
                                }
                            }

                            //ogni 10msec, occorre verificare se � intervenuto il comparatore_1, in caso affermativo,
                            //calcolare la nuova media ed azzerare il condensatore e il TMR1
                            fAggiornaLuminosita(&mLuminositaMedia6, iCampione6, PASSI_SHIFT);
                            //controllo di ricezione START BIT, verificando la differenza tra valore medio ed istantaneo

                            iGapFlash = mLuminositaMedia6.iMedia / cDivisore;
        #endif
                            if (iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash)) {
        //DEB_SIGNAL                        
                                cBufferOptiLight = 0;
                                cBitReceivedOPT = 0; //serve per individuare il comando al 5 bit ricevuto
                                cParitaRx = 1; //init con parit� dispari (se cParitaRx == 1 ci aspettiamo che il bit di parit� sia 1)                   
                                cNumeroCampioniOpti = (SPEED_RX / 20); //200msec/20msec
                                fInitContatoriFirstHalf();
                                cContaCampioniMin++; //1 campione a zero ricevuto
                                cContaCampioni--; //gi� ricevuto il primo campione
                                cStatoDriverOptiCom = ST_OPTICOM_RX_STARTB_1HALF;
                                
                                if (cCampioniCorrezioneLenta) {
                                    cCampioniCorrezioneLenta--;
                                }
                                
                            } else {

        #ifndef DAC_FISSO
                                //leggera correzione sul DAC per inseguire leggeri cambi di luminosit� solo non in alta luminosit�
                                //potrebbe aver senso solo la correzione nel caso sia superiore al valor medio, perch� se correggiamo quando � inferiore
                                //potremmo contrastare la ricezione
                                //Vincolo ad 1 secondo (50x20msec) perch� questa � circa la velocit� di regime della media, a fronte di un cambiamento
                                if ((DAC1CON1 < 235) && (!cCampioniCorrezioneLenta)) {
                                    if (iCampione6 < (mLuminositaMedia6.iMedia - (iGapFlash >> 1))) {
                                        fIncrementaDac(1);
                                        cCampioniCorrezioneLenta = 50;
                                    } else {
                                        if (iCampione6 > (mLuminositaMedia6.iMedia + iGapFlash)) {
                                            fDecrementaDac(1);
                                            cCampioniCorrezioneLenta = 50;
                                        }
                                    }
                                }
                                //se la MEDIA � inferiore VAL_MIN_LUM, effettuiamo una nuova taratura a meno di non essere gi� a finecorsa
                                iDepo = VAL_MIN_LUM;
                                if (mLuminositaMedia6.iMedia < iDepo) {
                                    //c'� molta luce e il compensatore � al max previsto
                                    if (DAC1CON1 < LIVELLO_MAX_DAC) {
                                        if (fIncrementaDac(DAC1CON1)) {
                                            fTransToOptInit(TIME_RITARATURA_MEDIA);
                                        }                                
                                    }
                                    //altrimenti non si pu� far nulla...
                                } else {
                                    iDepo = VAL_MAX_LUM;
                                    if (mLuminositaMedia6.iMedia > iDepo) {
                                        //c'� poca luce e il compensatore � spento
                                        if (DAC1CON1 > LIVELLO_MIN_DAC) {
                                            if (fDecrementaDac(DAC1CON1/2)) {
                                                fTransToOptInit(TIME_RITARATURA_MEDIA);
                                            }
                                        }
                                    }
                                }
        #endif
                            }
                        }
                    }
                }
            }
            break;
        }

        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_PROT_CORTO:
        {
            //anche se falliti, ma dobbiamo sbloccare AUTOMA_ProtOptiComLite, per cui settiamo sia ACK che NACK
            uFlagDriverOptiCom.cBit.AckInviato = 1;
            uFlagDriverOptiCom.cBit.NackInviato = 1;
            cStatoDriverOptiCom = ST_OPTICOM_NORMALE;

            break;
        }
        
        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_NOT_IMPL_INIT:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();

            uFlagDriverOptiCom.cBit.TxInCorso = 1;
            if (0 == iTimerComunicazione) {
                fSpegniSA();
                if (uFlagAccenditoreSE.cBit.flagLedAccesi) {
                    fAggiornaDutySE(DUTY_NOT_IMPL);
                } else {
                    fAccendiPwmSE(DUTY_NOT_IMPL);
                }
                iTimerComunicazione = TM_LED_ON_NOT_IMPL;
                cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_IMPL_ON;
            } else {
                fSpegniPwmSE();
                fSpegniSA();
            }
            break;
        }
        
        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_NOT_IMPL_ON:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagDriverOptiCom.cBit.TxInCorso = 1;
            if (0 == iTimerComunicazione) {
                fSpegniPwmSE();
                cCicliAck--;
                if (0 == cCicliAck) {
                    iTimerComunicazione = iDepoTimer;
                    cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_IMPL_END;
                } else {
                    iTimerComunicazione = TM_LED_OFF_INTRA;
                    cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_IMPL_INIT;
                }
            }            
            break;
        }
        
        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_NOT_IMPL_END:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            if (0 == iTimerComunicazione) {
                uFlagDriverOptiCom.cBit.NackInviato = 1;
                cStatoDriverOptiCom = ST_OPTICOM_NORMALE;
            } else {
                uFlagDriverOptiCom.cBit.TxInCorso = 1;
                fSpegniPwmSE();
            }            
            break;
        }
        
        //**********************************************************************************************
        //**********************************************************************************************
        //**********************************************************************************************
        //**********************************************************************************************
        case ST_OPTICOM_TX_NOT_EN_INIT:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagDriverOptiCom.cBit.TxInCorso = 1;
            if (0 == iTimerComunicazione) {
                fSpegniSA();
                if (uFlagAccenditoreSE.cBit.flagLedAccesi) {
                    fAggiornaDutySE(DUTY_NOT_EN);
                } else {
                fAccendiPwmSE(DUTY_NOT_EN);
                }
                iTimerComunicazione = TM_LED_ON_NOT_EN;
                cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_EN_ON;
            } else {
                fSpegniPwmSE();
                fSpegniSA();
            }
            break;
        }
        
        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_NOT_EN_ON:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagDriverOptiCom.cBit.TxInCorso = 1;
            if (0 == iTimerComunicazione) {
                cCicliAck--;                
                if (0 == cCicliAck) {
                    iTimerComunicazione = iDepoTimer;
                    cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_EN_END;
                } else {
                    iTimerComunicazione = TM_LED_OFF_INTRA;
                    cStatoDriverOptiCom = ST_OPTICOM_TX_NOT_EN_INIT;
                }
            }
            break;
        }
        
        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_NOT_EN_END:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            if (0 == iTimerComunicazione) {
                uFlagDriverOptiCom.cBit.NackInviato = 1;
                cStatoDriverOptiCom = ST_OPTICOM_NORMALE;
            } else {
                uFlagDriverOptiCom.cBit.TxInCorso = 1;
                fSpegniPwmSE();
            }            
            break;
        }
        
        //**********************************************************************************************
        //**********************************************************************************************
        //**********************************************************************************************
        //**********************************************************************************************
        case ST_OPTICOM_TX_DONE_INIT:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagDriverOptiCom.cBit.TxInCorso = 1;
            if (0 == iTimerComunicazione) {
                fSpegniSA();
                if (uFlagAccenditoreSE.cBit.flagLedAccesi) {
                    fAggiornaDutySE(DUTY_ACK);
                } else {
                fAccendiPwmSE(DUTY_ACK);
                }                
                iTimerComunicazione = TM_LED_ON_DONE;
                cStatoDriverOptiCom = ST_OPTICOM_TX_DONE_ON;
            } else {
                fSpegniPwmSE();
                fSpegniSA();
            }
            break;
        }
        
        //----------------------------------------------------------------------------------------------
        // Stato init di ACK DONE, led accesi
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_DONE_ON:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagDriverOptiCom.cBit.TxInCorso = 1;
            if (0 == iTimerComunicazione) {
                cCicliAck--;                
                if (0 == cCicliAck) {
                    iTimerComunicazione = iDepoTimer;
                    cStatoDriverOptiCom = ST_OPTICOM_TX_DONE_END;
                } else {
                    iTimerComunicazione = TM_LED_OFF_INTRA;
                    cStatoDriverOptiCom = ST_OPTICOM_TX_DONE_INIT;
                }
            }
            break;
        }
        
        //----------------------------------------------------------------------------------------------
        // Stato init di ACK DONE, led spenti
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_TX_DONE_END:
        {
            ACC_SE_SetHigh();
            ACC_SE_SetDigitalOutput();
            
            uFlagDriverOptiCom.cBit.TxInCorso = 1;
            if (0 == iTimerComunicazione) {
                uFlagDriverOptiCom.cBit.AckInviato = 1;
                cStatoDriverOptiCom = ST_OPTICOM_NORMALE;
            } else {
                uFlagDriverOptiCom.cBit.TxInCorso = 1;
                fSpegniPwmSE();
            }            
            break;
        }
        //**********************************************************************************************
        //**********************************************************************************************
        //**********************************************************************************************
        //**********************************************************************************************
        
        //----------------------------------------------------------------------------------------------
        // Stato di ricezione del primo semibit dello START_BIT (FLASH))
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_STARTB_1HALF:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
                    cContaCampioniMin++;
                    cContaCampioni--;
                    if (0 == cContaCampioni) {
                        // 6 campioni con presunto Flash, in realt�, probabilmente, � cambiata la luce ...
                        fTransToOptInit(TIME_RITARATURA_CORTA);
                    }                    
                } else {
                    //1) � arrivato uno zero (no flash) quindi � il fronte del mezzo bit
                    //2) se corretto, cio� abbiamo campionato almeno 3 periodi di flash, passiamo oltre risincronizzandoci
                    if (cContaCampioniMin > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        //almeno 3 campioni corretti...
                        fInitContatoriSecondHalf();
                        cContaCampioniMag++;
                        cContaCampioni--; //gi� ricevuto il primo campione
                        cStatoDriverOptiCom = ST_OPTICOM_RX_STARTB_2HALF;
                    } else {
                        //altrimenti errore
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                    }
                }

            }
            break;
        }

        //----------------------------------------------------------------------------------------------
        // Stato di ricezione del secondo semibit dello START_BIT (NO FLASH))
        //----------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_STARTB_2HALF:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
                    //campione con flash
                    if (cContaCampioniMag > NUM_MINIMO_CAMP) {
                        //almeno 3 campioni corretti
                        fInitContatoriFirstHalf();
                        cContaCampioniMin++; //ricevuto il primo flash
                        cContaCampioni--; //gi� ricevuto il primo campione
                        cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_1HALF_FLASH;
                    } else {
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                    }
                } else {
//DEB_NO_FLASH                    
                    cContaCampioniMag++;
                    
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
//DEB_SIGNAL                        
                        // x campioni > media (->ZERO)
                        //ok ricevuto START-BIT, mi preparo al prox bit
                        fInitContatoriFirstHalf();
                        cContaCampioniMag = cGapCampioniSecondHalf;
                        //cContaCampioni = cContaCampioni - cGapCampioniSecondHalf;
                        cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_1HALF_NO_FLASH;
                    }
                }
            }
            break;
        }
 
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del primo semibit (0-1) (NO FLASH))
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_BIT_1HALF_NO_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //arrivato campione con flash...
                    if (cContaCampioniMag > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        fInitContatoriSecondHalf();
                        cContaCampioniMin++;
                        cContaCampioni--;
                        cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_2HALF_FLASH;
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                    }                        
                } else {
//DEB_NO_FLASH                    
                    cContaCampioniMag++;
                    
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
                        //sono arrivati 7 campioni senza flash, � mancata la transizione e forse � cambiata la luce
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        //ma se si tratta del 7� bit allora potrebbe essere un comando a 5 bit
                        if (BIT_FRAME_SHORT == cBitReceivedOPT) {
                            uFlagDriverOptiCom.cBit.OldProtocol = 1;
                            fTransToOptNormale();
                        } else {
                            uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                            fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                        }
                    }                    
                }
            }
            break;
        }

        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del 2o semibit (0-1) (FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_BIT_2HALF_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //campione con flash
                    cContaCampioniMin++; //ricevuto il primo flash
                    
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
//DEB_SIGNAL                        
                        // x campioni < media (->FLASH)
                        fRxZERO(); //ricevuto ZERO
                        fInitContatoriFirstHalf();
                        cContaCampioniMin = cGapCampioniSecondHalf;
                        cBitReceivedOPT++; //incremento il contatore dei bit ricevuti
                        if (BIT_FRAME_CMD == cBitReceivedOPT) {
                            if (cParitaRx) {
                                //aspetto un 1 quindi ok perch� sta arrivando un 1 e procedo
                                cStatoDriverOptiCom = ST_OPTICOM_RX_PARITY_1HALF_FLASH;
                            } else {
                                uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                                fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                            }
                        } else {
                            cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_1HALF_FLASH;
                        }
                    }                    
                } else {
//DEB_NO_FLASH                    
                    //arrivato campione senza flash
                    if (cContaCampioniMin > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        //almeno 3 campioni corretti
                        fRxZERO(); //ricevuto ZERO
                        fInitContatoriFirstHalf();
                        cContaCampioniMag++; //ricevuto il primo no flash
                        cContaCampioni--; //gi� ricevuto il primo campione
                        cBitReceivedOPT++; //incremento il contatore dei bit ricevuti
                        if (BIT_FRAME_CMD == cBitReceivedOPT) {
                            if (cParitaRx) {
                                //errore perch� mi aspetto 1 ed ora � in arrivo uno 0
                                uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                                fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                            } else {
                                //ok, � in arrivo uno 0
                                cStatoDriverOptiCom = ST_OPTICOM_RX_PARITY_1HALF_NO_FLASH;
                            }
                        } else {
                            cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_1HALF_NO_FLASH;
                        }
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                    }
                }

            }
            break;
        }
        
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del primo semibit (1-0) (FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_BIT_1HALF_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    cContaCampioniMin++;
                    cContaCampioni--;
                    if (0 == cContaCampioni) {
                        //sono arrivati 7 campioni con flash, � mancata la transizione e forse � cambiata la luce
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                    }                    
                } else {
//DEB_NO_FLASH                    
                    //arrivato campione senza flash...
                    if (cContaCampioniMin > NUM_MINIMO_CAMP) {
//DEB_SIGNAL
                        fInitContatoriSecondHalf();
                        cContaCampioniMag++;
                        cContaCampioni--;
                        cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_2HALF_NO_FLASH;
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                    }                        
                }
            }
            break;
        }

        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del 2o semibit (1-0) (NO FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_BIT_2HALF_NO_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //campione con flash
                    if (cContaCampioniMag > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        //almeno 3 campioni corretti
                        fRxUNO(); //ricevuto UNO
                        fInitContatoriFirstHalf();
                        cContaCampioniMin++; //ricevuto il primo no flash
                        cContaCampioni--; //gi� ricevuto il primo campione
                        cBitReceivedOPT++; //incremento il contatore dei bit ricevuti
                        if (BIT_FRAME_CMD == cBitReceivedOPT) {
                            if (cParitaRx) {
                                //aspetto un 1 e quindi procedo perch� sta arrivando un 1
                                cStatoDriverOptiCom = ST_OPTICOM_RX_PARITY_1HALF_FLASH;
                            } else {
                                uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                                fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                                fTransToOptNormale();
                            }
                        } else {
                            cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_1HALF_FLASH;
                        }
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }                    

                } else {
//DEB_NO_FLASH                    
                    //campione senza flash
                    cContaCampioniMag++;
                    
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
//DEB_SIGNAL                        
                        // x campioni > media (->NO FLASH)
                        fRxUNO(); //ricevuto UNO
                        fInitContatoriFirstHalf();
                        cContaCampioniMag = cGapCampioniSecondHalf;
//                        cContaCampioni = cContaCampioni - cGapCampioni;
                        cBitReceivedOPT++; //incremento il contatore dei bit ricevuti
                        if (BIT_FRAME_CMD == cBitReceivedOPT) {
                            if (cParitaRx) {
                                //errore parit� perch� mi aspetto un 1 mentre ora c'� uno 0
                                uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                                fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                                fTransToOptNormale();
                            } else {
                                cStatoDriverOptiCom = ST_OPTICOM_RX_PARITY_1HALF_NO_FLASH;
                            }
                        } else {
                            cStatoDriverOptiCom = ST_OPTICOM_RX_BIT_1HALF_NO_FLASH;
                        }
                    }                    
                }
            }
            break;
        }
       
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del primo semibit (1-0) (FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_PARITY_1HALF_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    cContaCampioniMin++;
                    cContaCampioni--;
                    if (0 == cContaCampioni) {
                        //sono arrivati 6 campioni con flash, � mancata la transizione e forse � cambiata la luce
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }                    
                } else {
//DEB_NO_FLASH                    
                    //arrivato campione senza flash...
                    if (cContaCampioniMin > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        fInitContatoriSecondHalf();
                        cContaCampioniMag++;
                        cContaCampioni--;
                        cStatoDriverOptiCom = ST_OPTICOM_RX_PARITY_2HALF_NO_FLASH;
                    }
                }
            }
            break;
        }
        
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del primo semibit (0-1) (NO FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_PARITY_1HALF_NO_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //arrivato campione con flash...
                    if (cContaCampioniMag > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        fInitContatoriSecondHalf();
                        cContaCampioniMin++;
                        cContaCampioni--;
                        cStatoDriverOptiCom = ST_OPTICOM_RX_PARITY_2HALF_FLASH;
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }
                } else {
//DEB_NO_FLASH                    
                    cContaCampioniMag++;                    
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
                        //sono arrivati 6 campioni senza flash, � mancata la transizione e forse � cambiata la luce
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }                    
                }
            }
            break;
        }
        
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del secondo semibit (0-1) (FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_PARITY_2HALF_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //campione con flash
                    cContaCampioniMin++;                   
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
                        // x campioni < media (->FLASH)
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }                    
                } else {
//DEB_NO_FLASH                    
                    //arrivato campione senza flash
                    if (cContaCampioniMin > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        fInitContatoriFirstHalf();
                        cContaCampioniMag++;
                        cContaCampioni--;                        
                        cStatoDriverOptiCom = ST_OPTICOM_RX_STOP_BIT_1HALF_NO_FLASH;
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }
                }
            }
            break;
        }
        
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del secondo semibit (1-0) (NO FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_PARITY_2HALF_NO_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {                
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //errore
                    uFlagDriverOptiCom.cBit.flagErroreCom = 1; //mi aspetto uno 0
                    fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                    fTransToOptNormale();
                } else {
//DEB_NO_FLASH                    
                    //campione senza flash
                    cContaCampioniMag++; //ricevuto il primo flash                    
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
                        // x campioni > media (->NO FLASH)
//DEB_SIGNAL                        
                        fInitContatoriFirstHalf();
                        cContaCampioniMag = cGapCampioniSecondHalf;
//                        cContaCampioni = cContaCampioni - cGapCampioni;                                                
                        cStatoDriverOptiCom = ST_OPTICOM_RX_STOP_BIT_1HALF_NO_FLASH;
                    }                    
                }
            }
            break;
        }
 
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del 1o semibit stop (0-1) (NO FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_STOP_BIT_1HALF_NO_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //arrivato campione con flash...
                    if (cContaCampioniMag > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        fInitContatoriFirstHalf();
                        cContaCampioniMin++;
                        cContaCampioni--;
                        cStatoDriverOptiCom = ST_OPTICOM_RX_STOP_BIT_2HALF_FLASH;
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }
                } else {
//DEB_NO_FLASH                    
                    cContaCampioniMag++;                    
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
                        //sono arrivati 6 campioni senza flash, � mancata la transizione e forse � cambiata la luce
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }                    
                }
            }
            break;
        }
        
        //------------------------------------------------------------------------------------------------------
        // Stato di ricezione del secondo semibit stop (0-1) (FLASH)
        //------------------------------------------------------------------------------------------------------
        case ST_OPTICOM_RX_STOP_BIT_2HALF_FLASH:
        {
            uFlagDriverOptiCom.cBit.flagRxInCorso = 1;

            if (uFlagFissiLampada.iBit.CampioneOpti) {
                fRicominciaCamp();
                if ((iCampione6 < (mLuminositaMedia6.iMedia - iGapFlash))) {
//DEB_FLASH                    
                    //campione con flash
                    cContaCampioniMin++;                   
                    cContaCampioni--;

                    if (0 == cContaCampioni) {
                        // 7 campioni < media (->FLASH)
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
//                        fTransToOptNormale();
                    }                    
                } else {
//DEB_NO_FLASH                    
                    //arrivato campione senza flash
                    if (cContaCampioniMin > NUM_MINIMO_CAMP) {
//DEB_SIGNAL                        
                        uFlagDriverOptiCom.cBit.flagBufferDisponibile = 1;
                        fTransToOptNormale(); //esco comunque
                    } else {
                        uFlagDriverOptiCom.cBit.flagErroreCom = 1;
                        fTransToMediaIniziale(TIME_RITARATURA_CORTA);
                    }                    
                }
            }
            break;
        }       
    }
}