/******************************************************************************

Modulo:       includes.h
Descrizione:  Modulo file header inclusi

******************************************************************************/

#ifndef _INCLUDES_H
#define _INCLUDES_H

/*****************************************************************************/

#include <stdint.h>
#include <htc.h>
#include "global.h"
#include "macro.h"
#include "Timer.h"
#include "Flash.h"
#include "HEFlash.h"

#endif


