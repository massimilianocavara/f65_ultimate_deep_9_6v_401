/* 
 * File:   AUTOMA_AccenditoreSA.h
 * Author: cavaram
 *
 * Created on 13 ottobre 2020, 12.02
 */

#ifndef AUTOMA_ACCENDITORESA_H
#define	AUTOMA_ACCENDITORESA_H

#define ST_ACC_SA_IDLE              0
#define	ST_ACC_SA_ON                1
#define ST_ACC_SA_SLEEP             2
#define ST_ACC_SA_DISABLE           3

//#define	TIME_MASCHERATURA_SA    50	//x10msec -> 500msec
#define	TIME_MASCHERATURA_SA    100	//x10msec -> 1sec

//POTENZE SA
#define DUTY_PS                 5  //1%
#define DUTY_SA                 501


union tFlagAccenditoreSA {
    uint8_t cReg;
    
    struct {
        uint8_t LedSaAccesi : 1; //0
        uint8_t StripLedAperta : 1; //1
    } cBit;  
};


#endif	/* AUTOMA_ACCENDITORESA_H */

