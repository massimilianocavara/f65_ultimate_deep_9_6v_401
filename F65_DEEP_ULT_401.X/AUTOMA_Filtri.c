// AUTOMA_Filtri.c

#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_Filtri.h"
#include "AUTOMA_Charger.h"
#include "AUTOMA_AccenditoreSE.h"
#include "AUTOMA_AccenditoreSA.h"
#include "mcc_generated_files/adc.h"
#include "mcc_generated_files/pin_manager.h"
#include "AUTOMA_ProtocolloLogica.h"

uint8_t cStatoFiltri;

misura mIled;
misura mVbatt;
misura mVrete;
misura mJumper;
misura mVledSA;
misura mVbattRaw;
bit bContaZeriSA;


extern tFlagTimer uFlagTimer;
extern uint8_t cInitTimer;
extern tFlagAccenditoreSE uFlagAccenditoreSE;
extern union tFlagAccenditoreSA uFlagAccenditoreSA;
extern uint8_t TabellaLogica2[32];
extern tFiltrato uIngressiFilt;

////*****************************************************************************
//// unsigned int Campiona(unsigned char cChannel)
////
//// Campionamento e conversione AD
////*****************************************************************************
//
//unsigned int Campiona(unsigned char cChannel) {
//    unsigned int depo;
//
//    ADCON0 = ((cChannel << 2) | 0b10000000); //solo #channel
//    ADON = 1;
//    __delay_us(12);
//    GO_nDONE = 1;
//    while (GO_nDONE);
//    depo = ((ADRESH << 8) | (0x00FF & ADRESL));
//    return depo;
//}

//*****************************************************************************
// void fAggiornaMisura(misura * mMisDaAgg, unsigned int iNuovoCamp, unsigned char cExp2PesoCamp)
//
// aggiorna le strutture di tipo "misura", facendo la nuova media
//*****************************************************************************

void fAggiornaMisura(misura * mMisDaAgg, unsigned int iNuovoCamp, unsigned char cExp2PesoCamp) {
    mMisDaAgg->iSomma = mMisDaAgg->iSomma + iNuovoCamp - mMisDaAgg->iMedia;
    mMisDaAgg->iMedia = mMisDaAgg->iSomma >> cExp2PesoCamp;
}

//*****************************************************************************
// void fAzzeraMisura(misura * mMisDaAgg, unsigned int iMediaIniziale)
//
// azzera la struttura di tipo "misura"
//*****************************************************************************

void fAzzeraMisura(misura * mMisDaAgg, unsigned int iMediaIniziale) {
    mMisDaAgg->iSomma = iMediaIniziale << DIV2ALLAX;
    mMisDaAgg->iMedia = iMediaIniziale;
}

//*****************************************************************************
// void fInitAutomaFiltri(void)
//
// inizializzazione dell'AUTOMA_Filtri
//*****************************************************************************

void fInitAutomaFiltri(void) {
    
    fAzzeraMisura(&mIled, 0);
    fAzzeraMisura(&mVbatt, TENSIONE_NOMINALE);
    fAzzeraMisura(&mVrete, 0);
    fAzzeraMisura(&mJumper, 0);
    fAzzeraMisura(&mVledSA, 0);
    fAzzeraMisura(&mVbattRaw, 0);

    FVRCON = FVR_ILED;
    cStatoFiltri = ST_I_LED;
    
}

//*****************************************************************************
// void AUTOMA_Filtri(void)
//
//
//*****************************************************************************

void AUTOMA_Filtri(void) {
    
    uint16_t iNewValue;
//    uint8_t cCoeffTar;
//    uint24_t lRes;

    if (uFlagTimer.iBit.flag1msec)
    {
        ANSELA = 0b00001110; //RA1 AN1 I_LED; RA2 AN2 V_DRAIN; RA3 AN3 V_BAT
        TRISA |= 0b00001110; //analogici in INPUT
        ANSELB = 0b00001000; //RB3 AN9 V_RETE, RB4 solo al momento
        TRISB |= 0b00001000;
//        if (uFlagAccenditoreSA.cBit.LedSaAccesi) {
        if (uIngressiFilt.iBit.flagRetePresenteFilt) {
            //solo in SA facciamo la misura di Vled_SA, mentre in SE usiamo il CLC e il pin non pu� essere analogico
            Vled_SA_SetAnalogMode();
        } else {
            Vled_SA_SetDigitalMode();
            Vled_SA_SetDigitalInput();
        }
        SA_IN_SetDigitalInput();
        //SA_IN deve essere sempre campionato
        if (0 == SA_IN_GetValue())
        {
#ifdef NO_RETE_SA
            bContaZeriSA = 0;
#else            
            bContaZeriSA = 1;
#endif
        }        
  
//#endif
        
        switch (cStatoFiltri)
        {
            //******************************************************************
            // Misura della Iled
            //  !!!!la Vref � a 1.024V!!!!
            //******************************************************************
            case ST_I_LED:
            {  
                FVRCON = FVR_ILED; //1.024V
                ADCON1 = 0b10100011; //Right just, Fosc/32, Vref = FVR
                iNewValue = ADC_GetConversion(I_LED_ANALOG);
                fAggiornaMisura(&mIled, iNewValue, 3);
                cStatoFiltri = ST_V_BATT;                
                break;
            }
           
            //******************************************************************
            // Misura della Vbat
            //******************************************************************
            case ST_V_BATT:
            {
                FVRCON = FVR_BASE; //2.048V
                ADCON1 = 0b10100011; //Right just, Fosc/32, Vref = FVR
                iNewValue = ADC_GetConversion(V_BAT_ANALOG);
                fAggiornaMisura(&mVbatt, iNewValue, 3);
                cStatoFiltri = ST_V_RETE;
                break;
            }
            
            //******************************************************************
            // Vrete
            //******************************************************************
            case ST_V_RETE:
            {
                FVRCON = FVR_BASE; //2.048V
                ADCON1 = 0b10100011; //Right just, Fosc/32, Vref = FVR
                iNewValue = ADC_GetConversion(V_RETE_ANALOG);
                fAggiornaMisura(&mVrete, iNewValue, 3);
                cStatoFiltri = ST_V_LED_SA;
                break;
            }

            //******************************************************************
            // Vled_SA
            //******************************************************************
            case ST_V_LED_SA:
            {
                FVRCON = FVR_BASE; //2.048V
                ADCON1 = 0b10100011; //Right just, Fosc/32, Vref = FVR
                iNewValue = ADC_GetConversion(V_LED_SA_ANALOG);
                fAggiornaMisura(&mVledSA, iNewValue, 3);                
                cStatoFiltri = ST_JUMPER;
                break;
            }

            //******************************************************************
            // verifica Jumper 1H/n3H
            //******************************************************************
            case ST_JUMPER:
            {
                uint8_t cDepoV;
                uint8_t cDepoR;

                ANSELB |= 0b00010000; //RB4 AN11
                ADCON1 = 0b10100000; //Right just, Fosc/32, Vref = Vdd
                cDepoR = LED_R_GetValue();
                LED_R_SetDigitalInput();
                J_1Hn3H_SetAnalogMode();
                cDepoV = LED_V_GetValue();
                LED_V_SetHigh();
                LED_V_SetDigitalOutput();
                iNewValue = ADC_GetConversion(JUMPER_ANALOG);
                fAggiornaMisura(&mJumper, iNewValue, 3);
                //ripristino
                if (0 == cDepoV) {
                    LED_V_SetLow();
                    LED_V_SetDigitalOutput();
                }
                if (0 == cDepoR) {
                    LED_R_SetLow();
                } else {
                    LED_R_SetHigh();
                }
                J_1Hn3H_SetDigitalMode();
                LED_R_SetDigitalOutput();
                J_1Hn3H_SetDigitalOutput();
//                ANSELB &= 0b11101111; //RB4 di nuovo output
                cStatoFiltri = ST_I_LED;
                break;
            }            
            //******************************************************************
            default:
            {
                cStatoFiltri = ST_I_LED;
            }
        }//switch
    }//if
}
