/* 
 * File:   Interrupt.h
 * Author: cavaram
 *
 * Created on 25 novembre 2014, 17.25
 */

#ifndef INTERRUPT_H
#define	INTERRUPT_H

//***********************
// Definizione costanti
//***********************
#define	M_TX_CBL_0		TX_CBL_PORT = 0; TX_CBL_TRIS = 0;
#define	M_TX_CBL_1		TX_CBL_PORT = 1; TX_CBL_TRIS = 0;
#define	M_RX_CBL        RX_CBL_PORT


// SCELTA OTTIMA (...data l'asimmetria del driver, devo differenziare)
#define TEMPO_MEZZO_BIT_UNO         230 //x8usec (256-26))

#define	TEMPO_SEMIBIT_UNO           203 //x8usec = 416usec (255-52)
#define	TEMPO_SEMIBIT_ZERO          203 //x8usec = 416usec (255-52)
#define TEMPO_SEMIBIT               203
#define TEMPO_BIT                   151 //x8usec = 832usec (255-104))
////SCELTA OTTIMA
#define	TEMPO_QUARTO_DI_BIT			235		//x8usec = 208usec (255-xx)
#define	TEMPO_MEZZO_BIT				207		//x8usec = 416usec (255-yy)

//union tBufferCablecom {
//    uint32_t lReg;
//
//    struct {
//        uint8_t byte4; // Tx Logica: comando a 8 bit per la lampada Logica
//        uint8_t byte3; // Tx Logica: comando a 8 bit per la lampada Logica
//        uint8_t byte2; // Tx Logica: indirizzo a 16 bit per la lampada Logica (byte low)
//        uint8_t byte1; // Tx Logica: indirizzo a 16 bit per la lampada Logica (byte hi)
//    } lTx;
//
//    struct {
//        uint8_t byte4; // Rx Logica: dato della lampada Logica (non utilizzato)
//        uint8_t byte3; // Rx Logica: dato della lampada Logica
//        uint8_t byte2; // Rx Logica: indirizzo a 16 bit della lampada Logica (byte low)
//        uint8_t byte1; // Rx Logica: indirizzo a 16 bit della lampada Logica (byte hi)
//    } lRx;
//};

void fAbilitaIntReteSync(void);
void fDisabilitaIntReteSync(void);

#endif	/* INTERRUPT_H */

