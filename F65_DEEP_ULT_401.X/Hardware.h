// PorteReg.h

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT               1
#define OUTPUT              0

#define HIGH                1
#define LOW                 0

#define ANALOG              1
#define DIGITAL             0

#define PULL_UP_ENABLED     1
#define PULL_UP_DISABLED    0

#ifdef CON_PULSANTE
    // get/set PULSANTE (*RA0)
    #define PULSANTE_TRIS                       TRISAbits.TRISA0
    #define PULSANTE_LAT                        LATAbits.LATA0
    #define PULSANTE_PORT                       PORTAbits.RA0
    #define PULSANTE_WPU                        WPUAbits.WPUA0
    #define PULSANTE_OD                         ODCONAbits.ODA0
    #define PULSANTE_ANS                        ANSELAbits.ANSA0
    #define PULSANTE_SetHigh()                  do { LATAbits.LATA0 = 1; } while(0)
    #define PULSANTE_SetLow()                   do { LATAbits.LATA0 = 0; } while(0)
    #define PULSANTE_Toggle()                   do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
    #define PULSANTE_GetValue()                 PORTAbits.RA0
    #define PULSANTE_SetDigitalInput()          do { TRISAbits.TRISA0 = 1; } while(0)
    #define PULSANTE_SetDigitalOutput()         do { TRISAbits.TRISA0 = 0; } while(0)
    #define PULSANTE_SetPullup()                do { WPUAbits.WPUA0 = 1; } while(0)
    #define PULSANTE_ResetPullup()              do { WPUAbits.WPUA0 = 0; } while(0)
    #define PULSANTE_SetPushPull()              do { ODCONAbits.ODA0 = 1; } while(0)
    #define PULSANTE_SetOpenDrain()             do { ODCONAbits.ODA0 = 0; } while(0)
    #define PULSANTE_SetAnalogMode()            do { ANSELAbits.ANSA0 = 1; } while(0)
    #define PULSANTE_SetDigitalMode()           do { ANSELAbits.ANSA0 = 0; } while(0)
#else
    // get/set V_OPTI (*RA0)
    #define V_OPTI_TRIS                         TRISAbits.TRISA0
    #define V_OPTI_LAT                          LATAbits.LATA0
    #define V_OPTI_PORT                         PORTAbits.RA0
    #define V_OPTI_WPU                          WPUAbits.WPUA0
    #define V_OPTI_OD                           ODCONAbits.ODA0
    #define V_OPTI_ANS                          ANSELAbits.ANSA0
    #define V_OPTI_SetHigh()                    do { LATAbits.LATA0 = 1; } while(0)
    #define V_OPTI_SetLow()                     do { LATAbits.LATA0 = 0; } while(0)
    #define V_OPTI_Toggle()                     do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
    #define V_OPTI_GetValue()                   PORTAbits.RA0
    #define V_OPTI_SetDigitalInput()            do { TRISAbits.TRISA0 = 1; } while(0)
    #define V_OPTI_SetDigitalOutput()           do { TRISAbits.TRISA0 = 0; } while(0)
    #define V_OPTI_SetPullup()                  do { WPUAbits.WPUA0 = 1; } while(0)
    #define V_OPTI_ResetPullup()                do { WPUAbits.WPUA0 = 0; } while(0)
    #define V_OPTI_SetPushPull()                do { ODCONAbits.ODA0 = 1; } while(0)
    #define V_OPTI_SetOpenDrain()               do { ODCONAbits.ODA0 = 0; } while(0)
    #define V_OPTI_SetAnalogMode()              do { ANSELAbits.ANSA0 = 1; } while(0)
    #define V_OPTI_SetDigitalMode()             do { ANSELAbits.ANSA0 = 0; } while(0)
#endif

// get/set LAMP_ON (*RA1) verifica che il tubo sia effettivamente acceso
#define LAMP_ON_TRIS                        TRISAbits.TRISA1
#define LAMP_ON_LAT                         LATAbits.LATA1
#define LAMP_ON_PORT                        PORTAbits.RA1
#define LAMP_ON_WPU                         WPUAbits.WPUA1
#define LAMP_ON_OD                          ODCONAbits.ODA1
#define LAMP_ON_ANS                         ANSELAbits.ANSA1
#define LAMP_ON_SetHigh()                   do { LATAbits.LATA1 = 1; } while(0)
#define LAMP_ON_SetLow()                    do { LATAbits.LATA1 = 0; } while(0)
#define LAMP_ON_Toggle()                    do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define LAMP_ON_GetValue()                  PORTAbits.RA1
#define LAMP_ON_SetDigitalInput()           do { TRISAbits.TRISA1 = 1; } while(0)
#define LAMP_ON_SetDigitalOutput()          do { TRISAbits.TRISA1 = 0; } while(0)
#define LAMP_ON_SetPullup()                 do { WPUAbits.WPUA1 = 1; } while(0)
#define LAMP_ON_ResetPullup()               do { WPUAbits.WPUA1 = 0; } while(0)
#define LAMP_ON_SetPushPull()               do { ODCONAbits.ODA1 = 1; } while(0)
#define LAMP_ON_SetOpenDrain()              do { ODCONAbits.ODA1 = 0; } while(0)
#define LAMP_ON_SetAnalogMode()             do { ANSELAbits.ANSA1 = 1; } while(0)
#define LAMP_ON_SetDigitalMode()            do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set I_LED (*RA2)
#define I_LED_TRIS                         TRISAbits.TRISA2
#define I_LED_LAT                          LATAbits.LATA2
#define I_LED_PORT                         PORTAbits.RA2
#define I_LED_WPU                          WPUAbits.WPUA2
#define I_LED_OD                           ODCONAbits.ODA2
#define I_LED_ANS                          ANSELAbits.ANSA2
#define I_LED_SetHigh()                    do { LATAbits.LATA2 = 1; } while(0)
#define I_LED_SetLow()                     do { LATAbits.LATA2 = 0; } while(0)
#define I_LED_Toggle()                     do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define I_LED_GetValue()                   PORTAbits.RA2
#define I_LED_SetDigitalInput()            do { TRISAbits.TRISA2 = 1; } while(0)
#define I_LED_SetDigitalOutput()           do { TRISAbits.TRISA2 = 0; } while(0)
#define I_LED_SetPullup()                  do { WPUAbits.WPUA2 = 1; } while(0)
#define I_LED_ResetPullup()                do { WPUAbits.WPUA2 = 0; } while(0)
#define I_LED_SetPushPull()                do { ODCONAbits.ODA2 = 1; } while(0)
#define I_LED_SetOpenDrain()               do { ODCONAbits.ODA2 = 0; } while(0)
#define I_LED_SetAnalogMode()              do { ANSELAbits.ANSA2 = 1; } while(0)
#define I_LED_SetDigitalMode()             do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set VBAT_MIS (*RA3)
#define VBAT_MIS_TRIS                         TRISAbits.TRISA3
#define VBAT_MIS_LAT                          LATAbits.LATA3
#define VBAT_MIS_PORT                         PORTAbits.RA3
#define VBAT_MIS_WPU                          WPUAbits.WPUA3
#define VBAT_MIS_OD                           ODCONAbits.ODA3
#define VBAT_MIS_ANS                          ANSELAbits.ANSA3
#define VBAT_MIS_SetHigh()                    do { LATAbits.LATA3 = 1; } while(0)
#define VBAT_MIS_SetLow()                     do { LATAbits.LATA3 = 0; } while(0)
#define VBAT_MIS_Toggle()                     do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define VBAT_MIS_GetValue()                   PORTAbits.RA3
#define VBAT_MIS_SetDigitalInput()            do { TRISAbits.TRISA3 = 1; } while(0)
#define VBAT_MIS_SetDigitalOutput()           do { TRISAbits.TRISA3 = 0; } while(0)
#define VBAT_MIS_SetPullup()                  do { WPUAbits.WPUA3 = 1; } while(0)
#define VBAT_MIS_ResetPullup()                do { WPUAbits.WPUA3 = 0; } while(0)
#define VBAT_MIS_SetPushPull()                do { ODCONAbits.ODA3 = 1; } while(0)
#define VBAT_MIS_SetOpenDrain()               do { ODCONAbits.ODA3 = 0; } while(0)
#define VBAT_MIS_SetAnalogMode()              do { ANSELAbits.ANSA3 = 1; } while(0)
#define VBAT_MIS_SetDigitalMode()             do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set nEN_CHG (*RA4)
#define nEN_CHG_TRIS                         TRISAbits.TRISA4
#define nEN_CHG_LAT                          LATAbits.LATA4
#define nEN_CHG_PORT                         PORTAbits.RA4
#define nEN_CHG_WPU                          WPUAbits.WPUA4
#define nEN_CHG_OD                           ODCONAbits.ODA4
#define nEN_CHG_ANS                          ANSELAbits.ANSA4
#define nEN_CHG_SetHigh()                    do { LATAbits.LATA4 = 1; } while(0)
#define nEN_CHG_SetLow()                     do { LATAbits.LATA4 = 0; } while(0)
#define nEN_CHG_Toggle()                     do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define nEN_CHG_GetValue()                   PORTAbits.RA4
#define nEN_CHG_SetDigitalInput()            do { TRISAbits.TRISA4 = 1; } while(0)
#define nEN_CHG_SetDigitalOutput()           do { TRISAbits.TRISA4 = 0; } while(0)
#define nEN_CHG_SetPullup()                  do { WPUAbits.WPUA4 = 1; } while(0)
#define nEN_CHG_ResetPullup()                do { WPUAbits.WPUA4 = 0; } while(0)
#define nEN_CHG_SetPushPull()                do { ODCONAbits.ODA4 = 1; } while(0)
#define nEN_CHG_SetOpenDrain()               do { ODCONAbits.ODA4 = 0; } while(0)
#define nEN_CHG_SetAnalogMode()              do { ANSELAbits.ANSA4 = 1; } while(0)
#define nEN_CHG_SetDigitalMode()             do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set sel_UV (*RA5)
#define sel_UV_TRIS                          TRISAbits.TRISA5
#define sel_UV_LAT                           LATAbits.LATA5
#define sel_UV_PORT                          PORTAbits.RA5
#define sel_UV_WPU                           WPUAbits.WPUA5
#define sel_UV_OD                            ODCONAbits.ODA5
#define sel_UV_ANS                           ANSELAbits.ANSA5
#define sel_UV_SetHigh()                     do { LATAbits.LATA5 = 1; } while(0)
#define sel_UV_SetLow()                      do { LATAbits.LATA5 = 0; } while(0)
#define sel_UV_Toggle()                      do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define sel_UV_GetValue()                    PORTAbits.RA5
#define sel_UV_SetDigitalInput()             do { TRISAbits.TRISA5 = 1; } while(0)
#define sel_UV_SetDigitalOutput()            do { TRISAbits.TRISA5 = 0; } while(0)
#define sel_UV_SetPullup()                   do { WPUAbits.WPUA5 = 1; } while(0)
#define sel_UV_ResetPullup()                 do { WPUAbits.WPUA5 = 0; } while(0)
#define sel_UV_SetPushPull()                 do { ODCONAbits.ODA5 = 1; } while(0)
#define sel_UV_SetOpenDrain()                do { ODCONAbits.ODA5 = 0; } while(0)
#define sel_UV_SetAnalogMode()               do { ANSELAbits.ANSA5 = 1; } while(0)
#define sel_UV_SetDigitalMode()              do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set UVLAMP_ON (*RA6)
#define UVLAMP_ON_TRIS                    TRISAbits.TRISA6
#define UVLAMP_ON_LAT                     LATAbits.LATA6
#define UVLAMP_ON_PORT                    PORTAbits.RA6
#define UVLAMP_ON_WPU                     WPUAbits.WPUA6
#define UVLAMP_ON_OD                      ODCONAbits.ODA6
#define UVLAMP_ON_ANS                     ANSELAbits.ANSA6
#define UVLAMP_ON_SetHigh()               do { LATAbits.LATA6 = 1; } while(0)
#define UVLAMP_ON_SetLow()                do { LATAbits.LATA6 = 0; } while(0)
#define UVLAMP_ON_Toggle()                do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define UVLAMP_ON_GetValue()              PORTAbits.RA6
#define UVLAMP_ON_SetDigitalInput()       do { TRISAbits.TRISA6 = 1; } while(0)
#define UVLAMP_ON_SetDigitalOutput()      do { TRISAbits.TRISA6 = 0; } while(0)
#define UVLAMP_ON_SetPullup()             do { WPUAbits.WPUA6 = 1; } while(0)
#define UVLAMP_ON_ResetPullup()           do { WPUAbits.WPUA6 = 0; } while(0)
#define UVLAMP_ON_SetPushPull()           do { ODCONAbits.ODA6 = 1; } while(0)
#define UVLAMP_ON_SetOpenDrain()          do { ODCONAbits.ODA6 = 0; } while(0)
#define UVLAMP_ON_SetAnalogMode()         do { ANSELAbits.ANSA6 = 1; } while(0)
#define UVLAMP_ON_SetDigitalMode()        do { ANSELAbits.ANSA6 = 0; } while(0)

// get/set TX_CBL (*RA7)
#define TX_CBL_TRIS                         TRISAbits.TRISA7
#define TX_CBL_LAT                          LATAbits.LATA7
#define TX_CBL_PORT                         PORTAbits.RA7
#define TX_CBL_WPU                          WPUAbits.WPUA7
#define TX_CBL_OD                           ODCONAbits.ODA7
#define TX_CBL_ANS                          ANSELAbits.ANSA7
#define TX_CBL_SetHigh()                    do { LATAbits.LATA7 = 1; } while(0)
#define TX_CBL_SetLow()                     do { LATAbits.LATA7 = 0; } while(0)
#define TX_CBL_Toggle()                     do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define TX_CBL_GetValue()                   PORTAbits.RA7
#define TX_CBL_SetDigitalInput()            do { TRISAbits.TRISA7 = 1; } while(0)
#define TX_CBL_SetDigitalOutput()           do { TRISAbits.TRISA7 = 0; } while(0)
#define TX_CBL_SetPullup()                  do { WPUAbits.WPUA7 = 1; } while(0)
#define TX_CBL_ResetPullup()                do { WPUAbits.WPUA7 = 0; } while(0)
#define TX_CBL_SetPushPull()                do { ODCONAbits.ODA7 = 1; } while(0)
#define TX_CBL_SetOpenDrain()               do { ODCONAbits.ODA7 = 0; } while(0)
#define TX_CBL_SetAnalogMode()              do { ANSELAbits.ANSA7 = 1; } while(0)
#define TX_CBL_SetDigitalMode()             do { ANSELAbits.ANSA7 = 0; } while(0)


// get/set DISC_OPTI (*RB0)
#define DISC_OPTI_TRIS                      TRISBbits.TRISB0
#define DISC_OPTI_LAT                       LATBbits.LATB0
#define DISC_OPTI_PORT                      PORTBbits.RB0
#define DISC_OPTI_WPU                       WPUBbits.WPUB0
#define DISC_OPTI_OD                        ODCONBbits.ODB0
#define DISC_OPTI_ANS                       ANSELBbits.ANSB0
#define DISC_OPTI_SetHigh()                 do { LATBbits.LATB0 = 1; } while(0)
#define DISC_OPTI_SetLow()                  do { LATBbits.LATB0 = 0; } while(0)
#define DISC_OPTI_Toggle()                  do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define DISC_OPTI_GetValue()                PORTBbits.RB0
#define DISC_OPTI_SetDigitalInput()         do { TRISBbits.TRISB0 = 1; } while(0)
#define DISC_OPTI_SetDigitalOutput()        do { TRISBbits.TRISB0 = 0; } while(0)
#define DISC_OPTI_SetPullup()               do { WPUBbits.WPUB0 = 1; } while(0)
#define DISC_OPTI_ResetPullup()             do { WPUBbits.WPUB0 = 0; } while(0)
#define DISC_OPTI_SetPushPull()             do { ODCONBbits.ODB0 = 1; } while(0)
#define DISC_OPTI_SetOpenDrain()            do { ODCONBbits.ODB0 = 0; } while(0)
#define DISC_OPTI_SetAnalogMode()           do { ANSELBbits.ANSB0 = 1; } while(0)
#define DISC_OPTI_SetDigitalMode()          do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set SA_ON (*RB1)
#define SA_ON_TRIS                          TRISBbits.TRISB1
#define SA_ON_LAT                           LATBbits.LATB1
#define SA_ON_PORT                          PORTBbits.RB1
#define SA_ON_WPU                           WPUBbits.WPUB1
#define SA_ON_OD                            ODCONBbits.ODB1
#define SA_ON_ANS                           ANSELBbits.ANSB1
#define SA_ON_SetHigh()                     do { LATBbits.LATB1 = 1; } while(0)
#define SA_ON_SetLow()                      do { LATBbits.LATB1 = 0; } while(0)
#define SA_ON_Toggle()                      do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define SA_ON_GetValue()                    PORTBbits.RB1
#define SA_ON_SetDigitalInput()             do { TRISBbits.TRISB1 = 1; } while(0)
#define SA_ON_SetDigitalOutput()            do { TRISBbits.TRISB1 = 0; } while(0)
#define SA_ON_SetPullup()                   do { WPUBbits.WPUB1 = 1; } while(0)
#define SA_ON_ResetPullup()                 do { WPUBbits.WPUB1 = 0; } while(0)
#define SA_ON_SetPushPull()                 do { ODCONBbits.ODB1 = 1; } while(0)
#define SA_ON_SetOpenDrain()                do { ODCONBbits.ODB1 = 0; } while(0)
#define SA_ON_SetAnalogMode()               do { ANSELBbits.ANSB1 = 1; } while(0)
#define SA_ON_SetDigitalMode()              do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set V_RETE_sync (*RB2)
#define V_RETE_sync_TRIS                    TRISBbits.TRISB2
#define V_RETE_sync_LAT                     LATBbits.LATB2
#define V_RETE_sync_PORT                    PORTBbits.RB2
#define V_RETE_sync_WPU                     WPUBbits.WPUB2
#define V_RETE_sync_OD                      ODCONBbits.ODB2
#define V_RETE_sync_ANS                     ANSELBbits.ANSB2
#define V_RETE_sync_SetHigh()               do { LATBbits.LATB2 = 1; } while(0)
#define V_RETE_sync_SetLow()                do { LATBbits.LATB2 = 0; } while(0)
#define V_RETE_sync_Toggle()                do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define V_RETE_sync_GetValue()              PORTBbits.RB2
#define V_RETE_sync_SetDigitalInput()       do { TRISBbits.TRISB2 = 1; } while(0)
#define V_RETE_sync_SetDigitalOutput()      do { TRISBbits.TRISB2 = 0; } while(0)
#define V_RETE_sync_SetPullup()             do { WPUBbits.WPUB2 = 1; } while(0)
#define V_RETE_sync_ResetPullup()           do { WPUBbits.WPUB2 = 0; } while(0)
#define V_RETE_sync_SetPushPull()           do { ODCONBbits.ODB2 = 1; } while(0)
#define V_RETE_sync_SetOpenDrain()          do { ODCONBbits.ODB2 = 0; } while(0)
#define V_RETE_sync_SetAnalogMode()         do { ANSELBbits.ANSB2 = 1; } while(0)
#define V_RETE_sync_SetDigitalMode()        do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set V_RETE (*RB3)
#define V_RETE_TRIS                         TRISBbits.TRISB3
#define V_RETE_LAT                          LATBbits.LATB3
#define V_RETE_PORT                         PORTBbits.RB3
#define V_RETE_WPU                          WPUBbits.WPUB3
#define V_RETE_OD                           ODCONBbits.ODB3
#define V_RETE_ANS                          ANSELBbits.ANSB3
#define V_RETE_SetHigh()                    do { LATBbits.LATB3 = 1; } while(0)
#define V_RETE_SetLow()                     do { LATBbits.LATB3 = 0; } while(0)
#define V_RETE_Toggle()                     do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define V_RETE_GetValue()                   PORTBbits.RB3
#define V_RETE_SetDigitalInput()            do { TRISBbits.TRISB3 = 1; } while(0)
#define V_RETE_SetDigitalOutput()           do { TRISBbits.TRISB3 = 0; } while(0)
#define V_RETE_SetPullup()                  do { WPUBbits.WPUB3 = 1; } while(0)
#define V_RETE_ResetPullup()                do { WPUBbits.WPUB3 = 0; } while(0)
#define V_RETE_SetPushPull()                do { ODCONBbits.ODB3 = 1; } while(0)
#define V_RETE_SetOpenDrain()               do { ODCONBbits.ODB3 = 0; } while(0)
#define V_RETE_SetAnalogMode()              do { ANSELBbits.ANSB3 = 1; } while(0)
#define V_RETE_SetDigitalMode()             do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set J_1Hn3H (*RB4)
#define J_1Hn3H_TRIS                        TRISBbits.TRISB4
#define J_1Hn3H_LAT                         LATBbits.LATB4
#define J_1Hn3H_PORT                        PORTBbits.RB4
#define J_1Hn3H_WPU                         WPUBbits.WPUB4
#define J_1Hn3H_OD                          ODCONBbits.ODB4
#define J_1Hn3H_ANS                         ANSELBbits.ANSB4
#define J_1Hn3H_SetHigh()                   do { LATBbits.LATB4 = 1; } while(0)
#define J_1Hn3H_SetLow()                    do { LATBbits.LATB4 = 0; } while(0)
#define J_1Hn3H_Toggle()                    do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define J_1Hn3H_GetValue()                  PORTBbits.RB4
#define J_1Hn3H_SetDigitalInput()           do { TRISBbits.TRISB4 = 1; } while(0)
#define J_1Hn3H_SetDigitalOutput()          do { TRISBbits.TRISB4 = 0; } while(0)
#define J_1Hn3H_SetPullup()                 do { WPUBbits.WPUB4 = 1; } while(0)
#define J_1Hn3H_ResetPullup()               do { WPUBbits.WPUB4 = 0; } while(0)
#define J_1Hn3H_SetPushPull()               do { ODCONBbits.ODB4 = 1; } while(0)
#define J_1Hn3H_SetOpenDrain()              do { ODCONBbits.ODB4 = 0; } while(0)
#define J_1Hn3H_SetAnalogMode()             do { ANSELBbits.ANSB4 = 1; } while(0)
#define J_1Hn3H_SetDigitalMode()            do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set LED_R aliases
#define LED_R_TRIS                          TRISBbits.TRISB4
#define LED_R_LAT                           LATBbits.LATB4
#define LED_R_PORT                          PORTBbits.RB4
#define LED_R_WPU                           WPUBbits.WPUB4
#define LED_R_OD                            ODCONBbits.ODB4
#define LED_R_ANS                           ANSELBbits.ANSB4
#define LED_R_SetHigh()                     do { LATBbits.LATB4 = 1; } while(0)
#define LED_R_SetLow()                      do { LATBbits.LATB4 = 0; } while(0)
#define LED_R_Toggle()                      do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define LED_R_GetValue()                    PORTBbits.RB4
#define LED_R_SetDigitalInput()             do { TRISBbits.TRISB4 = 1; } while(0)
#define LED_R_SetDigitalOutput()            do { TRISBbits.TRISB4 = 0; } while(0)
#define LED_R_SetPullup()                   do { WPUBbits.WPUB4 = 1; } while(0)
#define LED_R_ResetPullup()                 do { WPUBbits.WPUB4 = 0; } while(0)
#define LED_R_SetPushPull()                 do { ODCONBbits.ODB4 = 1; } while(0)
#define LED_R_SetOpenDrain()                do { ODCONBbits.ODB4 = 0; } while(0)
#define LED_R_SetAnalogMode()               do { ANSELBbits.ANSB4 = 1; } while(0)
#define LED_R_SetDigitalMode()              do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set LED_V (*RB5)
#define LED_V_TRIS                          TRISBbits.TRISB5
#define LED_V_LAT                           LATBbits.LATB5
#define LED_V_PORT                          PORTBbits.RB5
#define LED_V_WPU                           WPUBbits.WPUB5
#define LED_V_OD                            ODCONBbits.ODB5
#define LED_V_ANS                           ANSELBbits.ANSB5
#define LED_V_SetHigh()                     do { LATBbits.LATB5 = 1; } while(0)
#define LED_V_SetLow()                      do { LATBbits.LATB5 = 0; } while(0)
#define LED_V_Toggle()                      do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define LED_V_GetValue()                    PORTBbits.RB5
#define LED_V_SetDigitalInput()             do { TRISBbits.TRISB5 = 1; } while(0)
#define LED_V_SetDigitalOutput()            do { TRISBbits.TRISB5 = 0; } while(0)
#define LED_V_SetPullup()                   do { WPUBbits.WPUB5 = 1; } while(0)
#define LED_V_ResetPullup()                 do { WPUBbits.WPUB5 = 0; } while(0)
#define LED_V_SetPushPull()                 do { ODCONBbits.ODB5 = 1; } while(0)
#define LED_V_SetOpenDrain()                do { ODCONBbits.ODB5 = 0; } while(0)
#define LED_V_SetAnalogMode()               do { ANSELBbits.ANSB5 = 1; } while(0)
#define LED_V_SetDigitalMode()              do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set EN_RX_CBL (*RB6)
#define EN_RX_CBL_TRIS                          TRISBbits.TRISB6
#define EN_RX_CBL_LAT                           LATBbits.LATB6
#define EN_RX_CBL_PORT                          PORTBbits.RB6
#define EN_RX_CBL_WPU                           WPUBbits.WPUB6
#define EN_RX_CBL_OD                            ODCONBbits.ODB6
#define EN_RX_CBL_ANS                           ANSELBbits.ANSB6
#define EN_RX_CBL_SetHigh()                     do { LATBbits.LATB6 = 1; } while(0)
#define EN_RX_CBL_SetLow()                      do { LATBbits.LATB6 = 0; } while(0)
#define EN_RX_CBL_Toggle()                      do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define EN_RX_CBL_GetValue()                    PORTBbits.RB6
#define EN_RX_CBL_SetDigitalInput()             do { TRISBbits.TRISB6 = 1; } while(0)
#define EN_RX_CBL_SetDigitalOutput()            do { TRISBbits.TRISB6 = 0; } while(0)
#define EN_RX_CBL_SetPullup()                   do { WPUBbits.WPUB6 = 1; } while(0)
#define EN_RX_CBL_ResetPullup()                 do { WPUBbits.WPUB6 = 0; } while(0)
#define EN_RX_CBL_SetPushPull()                 do { ODCONBbits.ODB6 = 1; } while(0)
#define EN_RX_CBL_SetOpenDrain()                do { ODCONBbits.ODB6 = 0; } while(0)
#define EN_RX_CBL_SetAnalogMode()               do { ANSELBbits.ANSB6 = 1; } while(0)
#define EN_RX_CBL_SetDigitalMode()              do { ANSELBbits.ANSB6 = 0; } while(0)

// get/set nSleep (*RB7)
#define nSleep_TRIS                         TRISBbits.TRISB7
#define nSleep_LAT                          LATBbits.LATB7
#define nSleep_PORT                         PORTBbits.RB7
#define nSleep_WPU                          WPUBbits.WPUB7
#define nSleep_OD                           ODCONBbits.ODB7
#define nSleep_ANS                          ANSELBbits.ANSB7
#define nSleep_SetHigh()                    do { LATBbits.LATB7 = 1; } while(0)
#define nSleep_SetLow()                     do { LATBbits.LATB7 = 0; } while(0)
#define nSleep_Toggle()                     do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define nSleep_GetValue()                   PORTBbits.RB7
#define nSleep_SetDigitalInput()            do { TRISBbits.TRISB7 = 1; } while(0)
#define nSleep_SetDigitalOutput()           do { TRISBbits.TRISB7 = 0; } while(0)
#define nSleep_SetPullup()                  do { WPUBbits.WPUB7 = 1; } while(0)
#define nSleep_ResetPullup()                do { WPUBbits.WPUB7 = 0; } while(0)
#define nSleep_SetPushPull()                do { ODCONBbits.ODB7 = 1; } while(0)
#define nSleep_SetOpenDrain()               do { ODCONBbits.ODB7 = 0; } while(0)
#define nSleep_SetAnalogMode()              do { ANSELBbits.ANSB7 = 1; } while(0)
#define nSleep_SetDigitalMode()             do { ANSELBbits.ANSB7 = 0; } while(0)

// RC0 -> T1OSO

// RC1 -> T1OSI

// get/set Vled_SA (*RC2)
#define Vled_SA_TRIS                        TRISCbits.TRISC2
#define Vled_SA_LAT                         LATCbits.LATC2
#define Vled_SA_PORT                        PORTCbits.RC2
#define Vled_SA_WPU                         WPUCbits.WPUC2
#define Vled_SA_OD                          ODCONCbits.ODC2
#define Vled_SA_ANS                         ANSELCbits.ANSC2
#define Vled_SA_SetHigh()                   do { LATCbits.LATC2 = 1; } while(0)
#define Vled_SA_SetLow()                    do { LATCbits.LATC2 = 0; } while(0)
#define Vled_SA_Toggle()                    do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define Vled_SA_GetValue()                  PORTCbits.RC2
#define Vled_SA_SetDigitalInput()           do { TRISCbits.TRISC2 = 1; } while(0)
#define Vled_SA_SetDigitalOutput()          do { TRISCbits.TRISC2 = 0; } while(0)
#define Vled_SA_SetPullup()                 do { WPUCbits.WPUC2 = 1; } while(0)
#define Vled_SA_ResetPullup()               do { WPUCbits.WPUC2 = 0; } while(0)
#define Vled_SA_SetPushPull()               do { ODCONCbits.ODC2 = 1; } while(0)
#define Vled_SA_SetOpenDrain()              do { ODCONCbits.ODC2 = 0; } while(0)
#define Vled_SA_SetAnalogMode()             do { ANSELCbits.ANSC2 = 1; } while(0)
#define Vled_SA_SetDigitalMode()            do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set RX_CBL (*RC3)
#define RX_CBL_TRIS                        TRISCbits.TRISC3
#define RX_CBL_LAT                         LATCbits.LATC3
#define RX_CBL_PORT                        PORTCbits.RC3
#define RX_CBL_WPU                         WPUCbits.WPUC3
#define RX_CBL_OD                          ODCONCbits.ODC3
#define RX_CBL_ANS                         ANSELCbits.ANSC3
#define RX_CBL_SetHigh()                   do { LATCbits.LATC3 = 1; } while(0)
#define RX_CBL_SetLow()                    do { LATCbits.LATC3 = 0; } while(0)
#define RX_CBL_Toggle()                    do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define RX_CBL_GetValue()                  PORTCbits.RC3
#define RX_CBL_SetDigitalInput()           do { TRISCbits.TRISC3 = 1; } while(0)
#define RX_CBL_SetDigitalOutput()          do { TRISCbits.TRISC3 = 0; } while(0)
#define RX_CBL_SetPullup()                 do { WPUCbits.WPUC3 = 1; } while(0)
#define RX_CBL_ResetPullup()               do { WPUCbits.WPUC3 = 0; } while(0)
#define RX_CBL_SetPushPull()               do { ODCONCbits.ODC3 = 1; } while(0)
#define RX_CBL_SetOpenDrain()              do { ODCONCbits.ODC3 = 0; } while(0)
#define RX_CBL_SetAnalogMode()             do { ANSELCbits.ANSC3 = 1; } while(0)
#define RX_CBL_SetDigitalMode()            do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set FAN_DIM (*RC4)
#define FAN_DIM_TRIS                     TRISCbits.TRISC4
#define FAN_DIM_LAT                      LATCbits.LATC4
#define FAN_DIM_PORT                     PORTCbits.RC4
#define FAN_DIM_WPU                      WPUCbits.WPUC4
#define FAN_DIM_OD                       ODCONCbits.ODC4
#define FAN_DIM_SetHigh()                do { LATCbits.LATC4 = 1; } while(0)
#define FAN_DIM_SetLow()                 do { LATCbits.LATC4 = 0; } while(0)
#define FAN_DIM_Toggle()                 do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define FAN_DIM_GetValue()               PORTCbits.RC4
#define FAN_DIM_SetDigitalInput()        do { TRISCbits.TRISC4 = 1; } while(0)
#define FAN_DIM_SetDigitalOutput()       do { TRISCbits.TRISC4 = 0; } while(0)
#define FAN_DIM_SetPullup()              do { WPUCbits.WPUC4 = 1; } while(0)
#define FAN_DIM_ResetPullup()            do { WPUCbits.WPUC4 = 0; } while(0)
#define FAN_DIM_SetPushPull()            do { ODCONCbits.ODC4 = 1; } while(0)
#define FAN_DIM_SetOpenDrain()           do { ODCONCbits.ODC4 = 0; } while(0)

// PWM_LED (*RC5)
#define PWM_LED_TRIS                      TRISCbits.TRISC5
#define PWM_LED_LAT                       LATCbits.LATC5
#define PWM_LED_PORT                      PORTCbits.RC5
#define PWM_LED_WPU                       WPUCbits.WPUC5
#define PWM_LED_OD                        ODCONCbits.ODC5
#define PWM_LED_SetHigh()                 do { LATCbits.LATC5 = 1; } while(0)
#define PWM_LED_SetLow()                  do { LATCbits.LATC5 = 0; } while(0)
#define PWM_LED_Toggle()                  do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define PWM_LED_GetValue()                PORTCbits.RC5
#define PWM_LED_SetDigitalInput()         do { TRISCbits.TRISC5 = 1; } while(0)
#define PWM_LED_SetDigitalOutput()        do { TRISCbits.TRISC5 = 0; } while(0)
#define PWM_LED_SetPullup()               do { WPUCbits.WPUC5 = 1; } while(0)
#define PWM_LED_ResetPullup()             do { WPUCbits.WPUC5 = 0; } while(0)
#define PWM_LED_SetPushPull()             do { ODCONCbits.ODC5 = 1; } while(0)
#define PWM_LED_SetOpenDrain()            do { ODCONCbits.ODC5 = 0; } while(0)

// get/set TXD (*RC6)
#define TXD_TRIS                        TRISCbits.TRISC6
#define TXD_LAT                         LATCbits.LATC6
#define TXD_PORT                        PORTCbits.RC6
#define TXD_WPU                         WPUCbits.WPUC6
#define TXD_OD                          ODCONCbits.ODC6
#define TXD_ANS                         ANSELCbits.ANSC6
#define TXD_SetHigh()                   do { LATCbits.LATC6 = 1; } while(0)
#define TXD_SetLow()                    do { LATCbits.LATC6 = 0; } while(0)
#define TXD_Toggle()                    do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define TXD_GetValue()                  PORTCbits.RC6
#define TXD_SetDigitalInput()           do { TRISCbits.TRISC6 = 1; } while(0)
#define TXD_SetDigitalOutput()          do { TRISCbits.TRISC6 = 0; } while(0)
#define TXD_SetPullup()                 do { WPUCbits.WPUC6 = 1; } while(0)
#define TXD_ResetPullup()               do { WPUCbits.WPUC6 = 0; } while(0)
#define TXD_SetPushPull()               do { ODCONCbits.ODC6 = 1; } while(0)
#define TXD_SetOpenDrain()              do { ODCONCbits.ODC6 = 0; } while(0)
#define TXD_SetAnalogMode()             do { ANSELCbits.ANSC6 = 1; } while(0)
#define TXD_SetDigitalMode()            do { ANSELCbits.ANSC6 = 0; } while(0)


// get/set RXD (*RC7)
#define RXD_TRIS                        TRISCbits.TRISC7
#define RXD_LAT                         LATCbits.LATC7
#define RXD_PORT                        PORTCbits.RC7
#define RXD_WPU                         WPUCbits.WPUC7
#define RXD_OD                          ODCONCbits.ODC7
#define RXD_ANS                         ANSELCbits.ANSC7
#define RXD_SetHigh()                   do { LATCbits.LATC7 = 1; } while(0)
#define RXD_SetLow()                    do { LATCbits.LATC7 = 0; } while(0)
#define RXD_Toggle()                    do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RXD_GetValue()                  PORTCbits.RC7
#define RXD_SetDigitalInput()           do { TRISCbits.TRISC7 = 1; } while(0)
#define RXD_SetDigitalOutput()          do { TRISCbits.TRISC7 = 0; } while(0)
#define RXD_SetPullup()                 do { WPUCbits.WPUC7 = 1; } while(0)
#define RXD_ResetPullup()               do { WPUCbits.WPUC7 = 0; } while(0)
#define RXD_SetPushPull()               do { ODCONCbits.ODC7 = 1; } while(0)
#define RXD_SetOpenDrain()              do { ODCONCbits.ODC7 = 0; } while(0)
#define RXD_SetAnalogMode()             do { ANSELCbits.ANSC7 = 1; } while(0)
#define RXD_SetDigitalMode()            do { ANSELCbits.ANSC7 = 0; } while(0)


// get/set SA_IN (*RE3)
#define SA_IN_TRIS                          TRISEbits.TRISE3
#define SA_IN_LAT                           LATEbits.LATE3
#define SA_IN_PORT                          PORTEbits.RE3
#define SA_IN_WPU                           WPUEbits.WPUE3
#define SA_IN_OD                            ODCONEbits.ODE3
#define SA_IN_ANS                           ANSELEbits.ANSE3
#define SA_IN_SetHigh()                     do { LATEbits.LATE3 = 1; } while(0)
#define SA_IN_SetLow()                      do { LATEbits.LATE3 = 0; } while(0)
#define SA_IN_Toggle()                      do { LATEbits.LATE3 = ~LATEbits.LATE3; } while(0)
#define SA_IN_GetValue()                    PORTEbits.RE3
#define SA_IN_SetDigitalInput()             do { TRISEbits.TRISE3 = 1; } while(0)
#define SA_IN_SetDigitalOutput()            do { TRISEbits.TRISE3 = 0; } while(0)
#define SA_IN_SetPullup()                   do { WPUEbits.WPUE3 = 1; } while(0)
#define SA_IN_ResetPullup()                 do { WPUEbits.WPUE3 = 0; } while(0)
#define SA_IN_SetPushPull()                 do { ODCONEbits.ODE3 = 1; } while(0)
#define SA_IN_SetOpenDrain()                do { ODCONEbits.ODE3 = 0; } while(0)
#define SA_IN_SetAnalogMode()               do { ANSELEbits.ANSE3 = 1; } while(0)
#define SA_IN_SetDigitalMode()              do { ANSELEbits.ANSE3 = 0; } while(0)


#ifdef CON_PULSANTE
    //label inizializzazione

    #define LATA_INIT   0b00000000 // nEN_CHG=0 (abilita ricarica)
    #define TRISA_INIT  0b00101111 // nEN_CHG, TX_CBL e UVLAMP_ON in OUTPUT; V_OPTI, sel_UV, V_BAT_MIS, I_LED, LAMP_ON in INPUT

    #define LATB_INIT   0b10000000 // n_sleep=1, SA ON
    #define TRISB_INIT  0b00011100 // V_RETE_sync, V_RETE, 1h_n3h in INPUT

    #define LATC_INIT   0b00000000 //
    #ifdef CON_STRIP_LED
        #define TRISC_INIT  0b10000101 // Vled_SA, RXD in INPUT; RX_CBL in OUTPUT
    #else
//        #define TRISC_INIT  0b10000001 // RXD in INPUT; RX_CBL, Vled_SA in OUTPUT
        #define TRISC_INIT  0b10000101 // PROVA!!!!! Vled_SA in INPUT
    #endif
    
    #define TRISE_INIT  0b00001000 // SA_IN in INPUT

    #define LATA_SLEEP  0b00000000 //
    #define TRISA_SLEEP 0b01000001 // V_opti in INPUT

    #define LATB_SLEEP  0b00000000 //
    #define TRISB_SLEEP 0b00001100 // V_RETE e V_RETE_sync in INPUT

    #define LATC_SLEEP  0b00000000 //
    #define TRISC_SLEEP 0b00000010 // RC1 (T1OSI) in INPUT
    
#else
    //label inizializzazione
    #define LATA_INIT   0b00000000 // nEN_CHG=0 (abilita ricarica)
    #define TRISA_INIT  0b00101111 // V_OPTI, V_BAT_MIS, I_LED, LAMP_ON in INPUT

    #define LATB_INIT   0b10000000 // n_sleep=1, SA ON
    #define TRISB_INIT  0b00011100 // V_RETE_sync, V_RETE, 1h_n3h in INPUT

    #define LATC_INIT   0b00000000 //
    #ifdef CON_STRIP_LED
        #define TRISC_INIT  0b10001101 // Vled_SA, RX_CBL, RXD in INPUT
    #else
        #define TRISC_INIT  0b10001001 // RX_CBL, RXD in INPUT; Vled_SA in OUTPUT
    #endif

    #define TRISE_INIT  0b00001000 // SA_IN in INPUT

    #define LATA_SLEEP  0b00000001 // V_OPTI resta alto
    #define TRISA_SLEEP 0b01000001 // 

    #define LATB_SLEEP  0b00000000 // 
    #define TRISB_SLEEP 0b00001100 // V_RETE e V_RETE_sync in INPUT

    #define LATC_SLEEP  0b00000000 //
    #define TRISC_SLEEP 0b00000000 //
    
#endif

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);

//#ifndef CON_STRIP_LED
//    #ifdef DEBUG_CON_SA_IN
//        //DEBUG PIN: RB5
//        #define DEBUG_PIN				LATB5
//        #define DEBUG_TRIS              TRISB5
//        #define DEB_SIGNAL              LATB ^= 0b00100000; TRISB &= 0b11011111;
//        #define DEB_SET                 LATB |= 0b00100000; TRISB &= 0b11011111;
//        #define DEB_RESET               LATB &= 0b11011111; TRISB &= 0b11011111;
//    #else
//    //DEBUG PIN: RC7
//        #define DEBUG_PIN				LATC7
//        #define DEBUG_TRIS              TRISC7
//        #define DEB_SIGNAL              LATC ^= 0b10000000; TRISC &= 0b01111111;
//        #define DEB_SET                 LATC |= 0b10000000; TRISC &= 0b01111111;
//        #define DEB_RESET               LATC &= 0b01111111; TRISC &= 0b01111111;
//    #endif
//#endif

////DEBUG PIN: RC2
//    #define DEBUG_PIN				LATC2
//    #define DEBUG_TRIS              TRISC2
//    #define DEB_SIGNAL              LATC ^= 0b00000100; TRISC &= 0b11111011;
//    #define DEB_SET                 LATC |= 0b00000100; TRISC &= 0b11111011;
//    #define DEB_RESET               LATC &= 0b11111011; TRISC &= 0b11111011;

#endif // PIN_MANAGER_H


