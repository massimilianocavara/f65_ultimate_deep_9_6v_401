#include <htc.h>
#include "global.h"
#include "Interrupt.h"
#include "Timer.h"
//#include "AUTOMA_DriverCablecom.h"
#include "AUTOMA_DriverLogica.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/tmr1.h"
#include "mcc_generated_files/cmp1.h"


extern uint8_t cMultipliTmr;
extern uint16_t iCampione6;
extern uint8_t cTimer1msec;
extern uint8_t cContaUniReteSE;
extern tFlagTimer uFlagTimer;
extern tFlagFissiLampada uFlagFissiLampada;
//extern tFlagDriverCbl uFlagDriverCbl;
//extern uint8_t cBitSendingCB;					// Indice bit inviati
//extern uint8_t cBitReceivedCB;                  // Indiced bit ricevuti
//extern uint8_t cHalfBitCB;                      // Mezzo bit che verr� campionato in Rx (0 = primo mezzo bit, 1 = secondo mezzo bit)
//extern uint8_t cRxByteCB;
//extern uint8_t cTxByteCB;
//extern uint8_t data;
//extern uint8_t cStatoDriverCBL; 
//extern tBufferCablecom uBufferCablTx;
//extern tBufferCablecom uBufferCablRx;

//extern uint8_t cBufferCablecom[SIZE_BUFFER_CABLECOM];
//extern uint8_t cTestaBufferCbl;
//extern uint8_t cCodaBufferCabl;

extern union tFlagLogica uFlagLogica;

extern void fAccendiLedRosso(void);

void fAbilitaIntReteSync(void) {
    IOCBPbits.IOCBP2 = 1;
    IOCBNbits.IOCBN2 = 0;
    IOCBFbits.IOCBF2 = 0;
    INTCONbits.IOCIE = 1;
}

void fDisabilitaIntReteSync(void) {
    IOCBPbits.IOCBP2 = 0;
    IOCBNbits.IOCBN2 = 0;
    IOCBFbits.IOCBF2 = 0;
}



////*******************************************************************
//// void fAbilitaIntDiscesa(void)
////
//// funzione: abilita l'interrupt sul pin in discesa (INT o PORT-CHANGE)
////*******************************************************************
//void fAbilitaIntDiscesa(void) {
//    IOCCPbits.IOCCP3 = 0;
//    IOCCNbits.IOCCN3 = 1;
//    IOCCFbits.IOCCF3 = 0; //reset del flag
//    INTCONbits.IOCIE = 1;
//}
//
////*******************************************************************
//// void fAbilitaIntSalita(void)
////
//// funzione:
////*******************************************************************
//void fAbilitaIntSalita(void) {
//    IOCCPbits.IOCCP3 = 1;
//    IOCCNbits.IOCCN3 = 0;
//    IOCCFbits.IOCCF3 = 0; //reset del bit
//    INTCONbits.IOCIE = 1;
//}
//
////*******************************************************************
//// void fDisabilitaInt(void)
////
//// funzione: disabilita l'interrupt sul pin (INT o PORT-CHANGE)
////*******************************************************************
//void fDisabilitaInt(void) {
//    IOCCPbits.IOCCP3 = 0;
//    IOCCNbits.IOCCN3 = 0;
//}
//
////*******************************************************************
////*******************************************************************
//void fAbilitaContesa(void) {
//    uFlagDriverCbl.iBit.CheckContesa = 1;
////    fAbilitaIntSalita();
//    fAbilitaIntDiscesa();
//}
//
////*******************************************************************
////*******************************************************************
//void fDisabilitaContesa(void) {
//    uFlagDriverCbl.iBit.CheckContesa = 0;
//    fDisabilitaInt();
//}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  void fInitInterrupt(void)
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void fInitInterrupt(void) {
    IOCAP = 0; //resetto tutti i flag
    //per sicurezza resetto tutti i registri
    IOCAN = 0;
    IOCBP = 0;
    IOCBN = 0;
    IOCCP = 0;
    IOCCN = 0;  
#ifdef SOLO_AT
    INTCON = 0b11100000; //abilito solo TMR0
#else
    IOCBPbits.IOCBP2 = 1; //attivo l'IOC positivo su RB2 (V_RETE_sync)
    INTCON = 0b11101000; //abilitato TMR0 e IOC
#endif
    
    PIE1bits.TMR1IE = 1; //abilito anche TMR1
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  void fRoutineIntComp2Opti(void)
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void fRoutineIntComp2Opti(void) {
//DEB_SIGNAL    
    T6CONbits.TMR6ON = 0; //spengo il conteggio di TMR6
    DISC_OPTI_SetHigh();
    DISC_OPTI_SetDigitalOutput();
    PIE2bits.C2IE = 0; //disabilito
    PIR2bits.C2IF = 0;
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  void fMisuraOpticom(void)
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void fMisuraOpticom(void) {    
#ifndef SOLO_AT      
    T6CONbits.TMR6ON = 0; //spengo il TMR6
    DISC_OPTI_SetHigh();
    DISC_OPTI_SetDigitalOutput();
    iCampione6 = (uint16_t) (TMR6 & 0x00FF) + (uint16_t) (cMultipliTmr << 8); //ricostruisco il valore con il numero di overflow del timer
    TMR6 = 0;
    cMultipliTmr = 0;
    PR6 = 255;
    T6CONbits.TMR6ON = 1;
    PIE2bits.TMR6IE = 1;
    PIR2bits.C2IF = 0; //resetto flag
    PIE2bits.C2IE = 1; //abilito interrupt COMPARATOR2
    uFlagFissiLampada.iBit.CampioneOpti = 1;
    //e scarico il condensatore    
    __delay_us(10);
    DISC_OPTI_SetLow();
    DISC_OPTI_SetDigitalOutput();      
#endif    
}



////******************************************************************************
////******************************************************************************
//// ROUTINE CABLECOM
////******************************************************************************
////******************************************************************************
//
////*******************************************************************
//// void fAbilitaIntTmr(uint8_t value)
////
//// funzione: abilita l'interrupt legato al timer utilizzato per
//// la ricezione e trasmissione LOGICA
////*******************************************************************
//void fAbilitaIntTmr(uint8_t value) {
//    // Configuro il TMR4:
//    // Fosc = 32MHz -> Tosc = 31.25nsec
//    // Tbase_logica = 8usec
//    // prescaler 1:64 significa che il timer viene incrementato ogni 4Tosc*64 =
//    // = 125nsec * 64 = 8usec
//    T4CON = 0b00000011; // Tmr4 OFF; prescaler = 1:64
//    TMR4 = value;
//    PIR2bits.TMR4IF = 0;
//    PIE2bits.TMR4IE = 1;
//    // Riaccendo il TMR4
//    T4CONbits.TMR4ON = 1;
//}
//
////*******************************************************************
//// void fDisabilitaIntTmr(void)
////
//// funzione: disabilita l'interrupt legato al timer utilizzato per 
//// la ricezione e trasmissione LOGICA
////*******************************************************************
//void fDisabilitaIntTmr(void) {
//    // Spengo il TMR4
//    T4CONbits.TMR4ON = 0;
//    // Disabilito l'interrupt del TMR4
//    PIR2bits.TMR4IF = 0;
//    PIE2bits.TMR4IE = 0;
//    TMR4 = 0;
//}
//
////*******************************************************************
//// void fErroreRxCabl(void)
////
//// funzione: invocata in caso di errore di ricezione, segnala l'errore
//// e disabilita gli interrupt
////*******************************************************************
//void fErroreRxCabl(void) {
////DEB_RESET    
//    // Setto il flag di errore decodifica
//    uFlagDriverCbl.iBit.ErroreRxLogica = 1;
//    // Disabilito l'interrupt sul fronte per il riallineamento del segnale in Rx (ridondanza)
//    fDisabilitaInt();
//    fDisabilitaIntTmr();
//}
//
////*******************************************************************
//// void fTerminaComunicazione(void)
////
//// funzione: termina una trasmissione, disabilitando gli interrupt
//// e rilasciando il bus, che torna nello stato di idle
////*******************************************************************
//void fTerminaComunicazione(void) {
//    fDisabilitaIntTmr();
//    fDisabilitaInt();
//    // Rilascio il bus LOGICA che si riporter� nello stato di idle (alto)
//    TX_CBL_SetHigh();
//    TX_CBL_SetDigitalOutput();
//}
//
////+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////  void fRoutineIntIocCabl(void)
////+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//void fRoutineIntIocCabl(void) {
//        
//    // Rx del fronte di riallineamento del bus DALI-LOGICA
//    INTCONbits.IOCIF = 0;
//    IOCCFbits.IOCCF3 = 0; //resetto il registro con i flag di Port-Change
//    
//    // Ho ricevuto il fronte del primo mezzo bit dello Start bit oppure il fronte di uno dei tanti secondi mezzi bit del frame
//    if (uFlagDriverCbl.iBit.CheckContesa) {
////DEB_SIGNAL 
//        //serve solo in TX
//        if (0 == RX_CBL_PORT) {
//            //ho ricevuto uno zero mentre stavo inviando un valore alto -> un altro dispositivo sta trasmettendo
//            fTerminaComunicazione();
//            uFlagDriverCbl.iBit.ErroreTxContesa = 1;
//            uFlagDriverCbl.iBit.TxInCorso = 0;
//            //Resetto eventuali COMENE PENDENTI
//            if (uFlagLogica.iBit.ComenePendente) {
//                uFlagLogica.iBit.ComenePendente = 0;
//            }
//        }
//    } else { 
//        //Nel caso di START_BIT, attivo entrambi i driver;
//        //nel caso di 1� byte, si attiva sempre il campionamento del 2� mezzo bit Manchester
//        //al termine del 1� byte, solo uno dei 2 driver prosegue il campionamento
//        if ((BUS_RX == uFlagDriverCbl.iBit.ModalitaTxRx)) {
////DEB_SIGNAL               
//            // Comuni a tutti gli stati, ecluso ST_DVR_RX_WAIT_SALITA_FINERX che chiude la RX
//            fDisabilitaInt();
//            fAbilitaIntTmr(TEMPO_QUARTO_DI_BIT);
//            
//            switch (cStatoDriverCBL) {
//                //--------------------------------------------------------------
//                case ST_DVR_RX_IDLE: //fronte di discesa dello StartBit
//                {
//                    uFlagDriverCbl.iBit.InitRx = 1;
//                    ResetBufferCBL();
//                    cBitReceivedCB = 0;
//                    cStatoDriverCBL = ST_DVR_RX_SB_LOW_1HALF;
//                    break;
//                }
//                //--------------------------------------------------------------
//                case ST_DVR_RX_SB_WAIT_SALITA: //fronte di salita dello StartBit
//                {
//                    cStatoDriverCBL = ST_DVR_RX_SB_HIGH_2HALF;
//                    break;
//                }
//                //------------------------------------------------------------------            
//                case ST_DVR_RX_WAIT_SALITA:
//                {
//                    cStatoDriverCBL = ST_DVR_RX_HIGH_2HALF;
//                    break;
//                }
//                //------------------------------------------------------------------            
//                case ST_DVR_RX_WAIT_DISCESA:
//                {
//                    cStatoDriverCBL = ST_DVR_RX_LOW_2HALF;
//                    break;
//                }
//                //------------------------------------------------------------------                            
//                case ST_DVR_RX_WAIT_SALITA_FINERX:
//                {
//                    // Ricevuto fronte di salita dopo 2 stop bit consecutivi -> fine frame
//                    fTerminaComunicazione();
//                    // Segnalo che � stato ricevuto un frame Logica corretto
//                    uFlagDriverCbl.iBit.FrameCBLReady = 1;
//                    break;
//                }
//                //------------------------------------------------------------------            
//                default:
//                {
//                    fErroreRxCabl();
//                    break;
//                }
//            }
//   
//        }
//    }
//    
////DEB_RESET       
//}
////+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////  void fRoutineIntTmr4Cabl(void)
////+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//void fRoutineIntTmr4Cabl(void) {
//    
//    PIR2bits.TMR4IF = 0;
//    
//    //**************************************************
//    //
//    // CABLECOM
//    //
//    //**************************************************
//    if (BUS_RX == uFlagDriverCbl.iBit.ModalitaTxRx) { //RX
////DEB_SIGNAL                
//        //Comuni a tutti gli stati
//        TX_CBL_SetHigh();
//        TX_CBL_SetDigitalOutput(); // -> bus alto        
//        fAbilitaIntTmr(TEMPO_MEZZO_BIT);
//        
//        switch (cStatoDriverCBL) {
//            //------------------------------------------------------------------
//            case ST_DVR_RX_SB_LOW_1HALF:
//            {
//                if (0 == RX_CBL_PORT) {
//                    fAbilitaIntSalita();
//                    cStatoDriverCBL = ST_DVR_RX_SB_WAIT_SALITA;
//                } else {
//                    fErroreRxCabl();
//                }
//                break;
//            }
//            //------------------------------------------------------------------
//            case ST_DVR_RX_SB_HIGH_2HALF:
//            {
//                if (1 == RX_CBL_PORT) {                    
//                    cStatoDriverCBL = ST_DVR_RX_1HALF;
//                } else {
//                    fErroreRxCabl();
//                }
//                break;
//            }
//            //------------------------------------------------------------------            
//            //------------------------------------------------------------------            
//            //------------------------------------------------------------------            
//            case ST_DVR_RX_1HALF:
//            {
//                if (1 == RX_CBL_PORT) {
//                    fAbilitaIntDiscesa(); 
//                    cStatoDriverCBL = ST_DVR_RX_WAIT_DISCESA;
//                } else {
//                    fAbilitaIntSalita();
//                    cStatoDriverCBL = ST_DVR_RX_WAIT_SALITA;
//                }
//                break;
//            }
//            //------------------------------------------------------------------
//            case ST_DVR_RX_HIGH_2HALF:
//            {
//                if (1 == RX_CBL_PORT) {
//                    cBitReceivedCB++;
//                    cRxByteCB <<= 1;
//                    cRxByteCB |= 0b00000001;
//                    cStatoDriverCBL = ST_DVR_RX_1HALF;
//                    if (8 == cBitReceivedCB)
//                    {
//                        fAbilitaIntSalita(); //se arrivasse un fronte, ci sarebbe errore
//                        cStatoDriverCBL = ST_DVR_RX_STOP_1HALF;
//                    }
//                } else {
//                    fErroreRxCabl();
//                }
//                break;
//            }
//            //------------------------------------------------------------------
//            case ST_DVR_RX_LOW_2HALF:
//            {
//                if (0 == RX_CBL_PORT) {
//                    cBitReceivedCB++;
//                    cRxByteCB <<= 1;
//                    cRxByteCB &= 0b11111110;
//                    cStatoDriverCBL = ST_DVR_RX_1HALF;
//                    if (8 == cBitReceivedCB)
//                    {
//                        fAbilitaIntSalita(); //se arrivasse un fronte, ci sarebbe errore
//                        cStatoDriverCBL = ST_DVR_RX_STOP_1HALF;
//                    }
//                } else {
//                    fErroreRxCabl();
//                }
//                break;
//            }
//            //------------------------------------------------------------------
//            case ST_DVR_RX_STOP_1HALF:
//            {
//                if (0 == RX_CBL_PORT) {
//                    cStatoDriverCBL = ST_DVR_RX_STOP_2HALF;
//                } else {
//                    fErroreRxCabl();
//                }
//                break;
//            }
//            //------------------------------------------------------------------
//            //------------------------------------------------------------------
//            //------------------------------------------------------------------
//            case ST_DVR_RX_STOP_2HALF:
//            {
//                if (0 == RX_CBL_PORT) {
//                    //fine ricezione byte, salvo nel buffer
//                    PutByteInBufferCBL(cRxByteCB);
//                    cBitReceivedCB = 0;
//                    fDisabilitaInt();
//                    cStatoDriverCBL = ST_DVR_RX_1HALF;
//                } else {
//                    fErroreRxCabl();
//                }
//                break;
//            }
//            //------------------------------------------------------------------
//            case ST_DVR_RX_WAIT_SALITA:
//            {
//                //casi:
//                //1) 2o stop bit del fine frame
//                //2) errore
//                if ((0 == cBitReceivedCB) && IsBufferCBLValid()) {
//                    //attendo fine ricezione frame
//                    fAbilitaIntSalita();
//                    cStatoDriverCBL = ST_DVR_RX_WAIT_SALITA_FINERX;
//                } else {
//                    fErroreRxCabl();
//                }                
//                break;
//            }
//            //------------------------------------------------------------------
//            default:
//            {
//                //� intervenuto questo interrupt prima di quello sul fronte
//                fErroreRxCabl();
//                break;
//            }
//        }
//    } else {
////DEB_SIGNAL
//        //---------------------------------------------------------------------------------------------------------------------------
//        // Gestione contesa
//        // Controllo lo stato del bus: quando inizia la trasmissione del frame, oppure quando ho appena trasmesso uno mezzo bit basso
//        // mi aspetto che il bus sia ALTO (idle), per cui verifico che lo sia veramente.
//        // Se non dovesse essere ALTO interrompo la trasmissione del frame.
//        //---------------------------------------------------------------------------------------------------------------------------
//        uFlagDriverCbl.iBit.TxInCorso = 1;
//        switch (cStatoDriverCBL) {
//            //-----------------------------------------------------------------
//            case ST_DVR_TX_SB_LOW_1HALF:
//            {
//                if (uFlagDriverCbl.iBit.InitTx) {
//                    uFlagDriverCbl.iBit.InitTx = 0;
//                    cBitSendingCB = 0;                
//                    // -> bus basso
//                    TxZERO();
//                    fDisabilitaContesa();
//                    TMR4 = TEMPO_SEMIBIT;
//                    cStatoDriverCBL = ST_DVR_TX_SB_HIGH_2HALF;
//                }
//                break;
//            }
//            //-----------------------------------------------------------------
//            case ST_DVR_TX_SB_HIGH_2HALF:
//            {
//                TxUNO();
//                fAbilitaContesa();
//                TMR4 = TEMPO_SEMIBIT;
//                cStatoDriverCBL = ST_DVR_TX_CARICA_BIT;
//                break;
//            }
//            //-----------------------------------------------------------------
//            case ST_DVR_TX_CARICA_BIT:
//            {
//                if (0 == cBitSendingCB) {
//                    //devo caricare il byte
//                    if (!IsBufferCBLEmpty()) {
//                        cTxByteCB = GetByteFromBufferCBL();
//                        if (cTxByteCB & 0b10000000) {
//                            //UNO ('01')
//                            TxZERO();
//                            fDisabilitaContesa();
//                            cStatoDriverCBL = ST_DVR_TX_LOW_1HALF;
//                        } else {
//                            //ZERO ('10')
//                            TxUNO();
//                            fAbilitaContesa();
//                            cStatoDriverCBL = ST_DVR_TX_HIGH_1HALF;
//                        }
//                        cTxByteCB <<= 1; //consumo il bit inviato
//                        cBitSendingCB++;
//                        TMR4 = TEMPO_SEMIBIT;
//                    } else {
//                        //fine frame, trasmetto il 2o ZERO
//                        TxZERO();
//                        fDisabilitaContesa();
//                        TMR4 = TEMPO_BIT;
//                        cStatoDriverCBL = ST_DVR_TX_FINE_FRAME;
//                    }
//                } else {
//                    if (cBitSendingCB < 8) {
//                        if (cTxByteCB & 0b10000000) {
//                            //UNO ('01')
//                            TxZERO();
//                            fDisabilitaContesa();
//                            cStatoDriverCBL = ST_DVR_TX_LOW_1HALF;
//                        } else {
//                            //ZERO ('10')
//                            TxUNO();
//                            fAbilitaContesa();
//                            cStatoDriverCBL = ST_DVR_TX_HIGH_1HALF;
//                        }
//                        cTxByteCB <<= 1; //consumo il bit inviato
//                        cBitSendingCB++;
//                        TMR4 = TEMPO_SEMIBIT;
//                    } else {
//                        //stop bit
//                        cBitSendingCB = 0;
//                        TxZERO();
//                        fDisabilitaContesa();
//                        TMR4 = TEMPO_BIT;                       
//                    }
//                }
//                break;
//            }
//            //-----------------------------------------------------------------
//            case ST_DVR_TX_HIGH_1HALF:
//            {
//                TxZERO();
//                fDisabilitaContesa();
//                TMR4 = TEMPO_SEMIBIT;                
//                cStatoDriverCBL = ST_DVR_TX_CARICA_BIT;
//                break;
//            }
//            //-----------------------------------------------------------------
//            case ST_DVR_TX_LOW_1HALF:
//            {
//                TxUNO();
//                fAbilitaContesa();
//                TMR4 = TEMPO_SEMIBIT;
//                cStatoDriverCBL = ST_DVR_TX_CARICA_BIT;
//                break;
//            }
//            //-----------------------------------------------------------------
//            case ST_DVR_TX_FINE_FRAME:
//            {
//                uFlagDriverCbl.iBit.TxInCorso = 0; //fine tx
//                fTerminaComunicazione();
//                if (uFlagLogica.iBit.ComenePendente) {
//                    uFlagLogica.iBit.ComenePendente = 0;
//                    uFlagLogica.iBit.AbilitaScrittura = 1;
//                }
//                break;
//            }
//            //-----------------------------------------------------------------
//            default:
//            {
//                uFlagDriverCbl.iBit.TxInCorso = 0; //fine tx
//                break;
//            }
//        }
//    }//switch
//}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void interrupt IntVector(void) {

    //*********************************************************************************
    //	COMPARATOR_2 ==> DEVE ESSERE IL PIU' PRIORITARIO!!!!
    //  interrupt quando C2OUT diventa positivo, cio� quando la tensione del condensatore
    //  supera quella del DAC (con polarit� invertita)
    //*********************************************************************************
    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) {
        fRoutineIntComp2Opti();
    }
    
    //*********************************************************************************
    //	COMPARATOR_1
    //  interrupt quando c'� protezione corto
    //*********************************************************************************
    if(PIE2bits.C1IE == 1 && PIR2bits.C1IF == 1)
    {
        CMP1_ISR();
    } 
//    //--------------------------------------------------------------------------------------------------------------------------------
//    // Gestione interrupt per driver DALI-LOGICA
//    //
//    // La trasmissione DALI-LOGICA viene realizzata mediante il TMR4
//    // La ricezione DALI-LOGICA viene realizzata mediante il PORT-CHANGE e il TMR4
//    // Lo stato di bus idle � alto
//    // Un bit dura 833 us e quindi un mezzo bit dura 417 us
//    // La campionatura viene effettuata a 209 us dopo ogni fronte (RABI), cio� alla met� del mezzo bit
//    //
//    // TRASMISSIONE
//    // Viene inizializzato il TMR4 (407us) ad ogni trasmissione di un mezzo bit
//    //
//    // RICEZIONE
//    // Quando il bus si abbassa scatta l'interrupt (PORT-CHANGE) e inizia la ricezione
//    // Viene inzializzato il TMR4 (209 us) per la campionatura del primo mezzo bit dello start bit, abilitato l'interrupt sul TMR4 e disabilitato
//    // il PORT-CHANGE.
//    // Effettuata la campionatura (TMR4) viene abilitato nuovamente il PORT-CHANGE per il riallineamento sul fronte tra il primo e il secondo mezzo bit
//    // Arrivato il fronte (PORT_CHANGE) viene inizializzato il TMR4 (209 us) per la campionatura del secondo mezzo bit
//    // Effettuata la campionatura viene reinizializzato il TMR4 (407us) per la campionatura del primo mezzo bit
//    // Se non dovesse arrivare il fronte tra il primo e il secondo mezzo bit verrebbe segnalato l'errore per poi uscire dalla ricezione
//    //--------------------------------------------------------------------------------------------------------------------------------
//
//    //*********************************************************************************
//    //	IOC su RC3, RX_CBL
//    //*********************************************************************************
//    if (IOCCFbits.IOCCF3 && INTCONbits.IOCIE) {
//        fRoutineIntIocCabl();
//    }
//    
//    //*********************************************************************************
//    //	ritesto per ridurre l'overhead dell'Interrupt
//    //*********************************************************************************
//    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) { //@
//        fRoutineIntComp2Opti();
//    }
//    
//    //*********************************************************************************
//    //  TMR4
//    //*********************************************************************************
//    //  Cablecom
//    //*********************************************************************************
//    if (PIR2bits.TMR4IF && PIE2bits.TMR4IE) {
////DEB_SIGNAL
//        fRoutineIntTmr4Cabl();
//    }
        
    //*********************************************************************************
    //	ritesto per ridurre l'overhead dell'Interrupt
    //*********************************************************************************
    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) { //@
        fRoutineIntComp2Opti();
    }
    
    //*********************************************************************************
    //	TMR6
    // riceve in ingresso un onda quadra di freq=20MHz con periodo=500nsec, che
    // consente di contare 40000 in un intervallo di 20msec.
    // Ogni 255*500nsec=127.5usec c'� un overflow.
    //*********************************************************************************
    if ((PIR2bits.TMR6IF) && (PIE2bits.TMR6IE)) {
        cMultipliTmr++;
        PIR2bits.TMR6IF = 0;
    }

    //*********************************************************************************
    //	ritesto per ridurre l'overhead dell'interrupt
    //*********************************************************************************
    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) {
        fRoutineIntComp2Opti();
    }

    //*********************************************************************************
    //	IOCBP - SYNCRO
    //*********************************************************************************
    if ((IOCBFbits.IOCBF2) && (INTCONbits.IOCIE)) {//SYNCRO con la rete
        fMisuraOpticom();
        IOCBFbits.IOCBF2 = 0;
    }
  
    //*********************************************************************************
    //	ritesto per ridurre l'overhead dell'interrupt
    //*********************************************************************************
    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) {
        fRoutineIntComp2Opti();
    }
    
    //*********************************************************************************
    //	TMR0
    //  conteggio dei tempi veloci, escluso il secondo
    //*********************************************************************************
    if ((INTCONbits.TMR0IF) && (INTCONbits.TMR0IE)) {
        //CLK=32MHz
        //Tclk=31.25nsec
        //Tistruzione (Fosc/4)=31.25nsec*4=125nsec
        //TMR0 prescaler 1:32 => Tincrtmr0=125nsec*32=4usec
        //Tinterrupt=250*4usec=1msec
        TMR0 = 6;
        uFlagTimer.iBit.flag1msec_interrupt = 1;
        INTCONbits.TMR0IF = 0;
    }

    //*********************************************************************************
    //	ritesto per ridurre l'overhead dell'interrupt
    //*********************************************************************************
    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) { //@
        fRoutineIntComp2Opti();
    }
    
    //*********************************************************************************
    //	TMR1
    //*********************************************************************************
    if ((PIR1bits.TMR1IF) && (PIE1bits.TMR1IE)) {
        TMR1_ISR();
    }

    //*********************************************************************************
    //	ritesto per ridurre l'overhead dell'interrupt
    //*********************************************************************************
    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) {
        fRoutineIntComp2Opti();
    }
    
    //*********************************************************************************
    // TX SERIALE
    //*********************************************************************************
    if(INTCONbits.PEIE == 1 && PIE1bits.TXIE == 1 && PIR1bits.TXIF == 1) {
        EUSART_Transmit_ISR();
    }
    
    //*********************************************************************************
    //	ritesto per ridurre l'overhead dell'interrupt
    //*********************************************************************************
    if ((PIR2bits.C2IF) && (PIE2bits.C2IE)) {
        fRoutineIntComp2Opti();
    }

    //*********************************************************************************
    // RX SERIALE
    //*********************************************************************************    
    if(INTCONbits.PEIE == 1 && PIE1bits.RCIE == 1 && PIR1bits.RCIF == 1) {
        EUSART_Receive_ISR();
    }
}

