
#ifndef GLOBAL_H
#define	GLOBAL_H

//******************************************************************************
//
//Descrizione:  Modulo definizioni flag e variabili globali
//
//******************************************************************************
#include <stdint.h>


#define _XTAL_FREQ          32000000

#define INIT_OPTION         0b01000100  //weak pullup enabled; TMR0 con prescaler 1:32
#define INIT_OSC_32MHZ      0b11110000  //8MHz, pll
#define INIT_OSC_8MHZ       0b01110000  //8MHz, no pll


//#define AMERICA_60HZ

//!!!!label di debug

//#define DEBUG_LABEL

//#define NO_INIT_TABELLA_LOGICA

//#define VERSIONE_EMULATORE
//#define DEBUG_CON_SA_IN         //il default � RB5
//#define PARTENZA_IN_EMERGENZA
//#define SEMPRE_ASSENZA_RETE
//#define NO_RETE_SA

#ifdef VERSIONE_EMULATORE
//    #define DIS_PIN_STRIP_PROG
//    #define DIS_OPTICOM
    #define NO_RETE_SA
    #define PARTENZA_IN_EMERGENZA
    #define NO_MINIMA
//    #define NO_ERRORE_TUBOUV
#endif

//#define CON_PS                   //per ora non prevediamo l'accensione PS
//#define TIMEOUT_RIPRISTINO_AT_LG  //blocco del decremento dei timer dei test automatici per 90gg se c'� comunizazione CBL
//#define GAP_FISSO

//#define NO_CP

//#define DEBUG_AUTONOMIA_F65
//#define DEMO_APERTURA_OPTI
//#define DIMEZZA_SCENE

//#define MINIMA_BASSA
//#define DAC_FISSO

//#define DEBUG_RITARATURA_OPTI
//#define PARTENZA_IN_EMERGENZA

//#define DEBUG_NEW_CMD

//#define DEBUG_COMUNICAZIONE
//#define NO_PROTEZIONI
//#define DEBUG_SA
//#define NO_SLEEP

//#define TEMPO_TEST_DEMO
//#define SOLO_AUTONOMIA_DEMO     //per avere solo il test di autonomia demo

//#define TEMPO_SLEEP_DEMO
//#define DEBUG_WRITE
//#define DEB_LOCALE
//#define DEBUG_RISVEGLIO

//#define VERIFICA_PARI_DISPARI
//#define DEMO_SANIFICATORE
//#define DEMO_SANIFICATORE_12H
//#define NO_RETE_SA
//#define DEB_SENS_RETE
//#define DEB_LEDROSSO

//#define TEMPO_RIC_DEMO
#define DEBUG_TAR
//
//#define DEB_ZCD //disabilita il pin RB0, DISC_OPTI, che � l'ingresso di ZCD
//
//#define DEB_NO_PRE_SLEEP
//#define DEBUG_RISVEGLIO
//#define DEB_SU_TAB

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Mappa HEF PIC16F1708:
//
//Blocco 0: 0x0F80 - 0x0F9F (8064 - 8095) 32 word a 14bit	?  Tabella Bootloader
//Blocco 1: 0x0FA0 - 0x0FBF (8096 - 8127) 32 word a 14bit	?  Tabella DALI
//Blocco 2: 0x0FC0 - 0x0FDF (8128 - 8159) 32 word a 14bit	?  Tabella Logica
//Blocco 3: 0x0FE0 - 0x0FFF (8160 - 8191) 32 word a 14bit	?  Tabella Opticom
//
//Mappa HEF PIC16F1709:
//
//Blocco 0: 0x1F80 - 0x1F9F 32 word a 14bit	?  Tabella Bootloader
//Blocco 1: 0x1FA0 - 0x1FBF 32 word a 14bit	?  Tabella DALI
//Blocco 2: 0x1FC0 - 0x1FDF 32 word a 14bit	?  Tabella Logica
//Blocco 3: 0x1FE0 - 0x1FFF 32 word a 14bit	?  Tabella Opticom
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//#define MICRO_PIC16F1718
#define MICRO_PIC16F1716

#ifdef MICRO_PIC16F1718
    //Indirizzi di memoria delle tabelle
    #define ADDR_START_TABLE_BOOT           0x3F80
    //#define XXXXXXXXX                     0x3FA0
    #define ADDR_START_TABLE_LOGICA         0x3FC0
    #define ADDR_START_TABLE_LOGICA2        0x3FE0
    #define ADDR_START_TABLE_LOGICA_ROM     0x3FF4
#else
    //Indirizzi di memoria delle tabelle
    #define ADDR_START_TABLE_BOOT           0x1F80
    //#define XXXXXXXXX                     0x1FA0
    #define ADDR_START_TABLE_LOGICA         0x1FC0
    #define ADDR_START_TABLE_LOGICA2        0x1FE0
    #define ADDR_START_TABLE_LOGICA_ROM     0x1FF4
#endif


//Posizione tabelle in HE Flash
#define TAB_BOOT                0
//#define XXXXXXXXX             1
#define TAB_LOGICA              2
#define TAB_LOGICA_2            3

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  TIPI DI LAMPADE
//
//  1) 233029001 - COMPLETA/F65 AT 3.2V LiFe: 
//              a) 180 LM; 0.5Ah
//              b) 450 LM; 1.5Ah
//              c) 700 LM; 2 x 1.5Ah
//
//  2) 233029101 - F65 CT 4.8V LTO:
//              a) 300 LM; 1.2Ah
//              b) 500 LM; 1.2Ah
//              c) 800 LM; 2 x 1.2Ah
//              d) 1000 LM; 2 x 1.2Ah
//
//  3) 232029xxx - COMPLETA CT 4.8V LTO:
//              a) 150 LM; 0.5Ah
//              b) 250 LM; 0.5Ah
//              c) 450 LM; 2 x 0.5Ah
//              d) 700 LM; 3 x 0.5Ah
//
//  5) 233029201 - ULTIMATE F65 AT 9.6V LiFe:
//              a) 1500 LM; 1.5Ah
//              b) 2200 LM; 2 x 1.5Ah
//
//  4) 233029301 - ULTIMATE F65 CT 9.6V LTO:
//              a) 1200 LM; 1.5Ah
//              b) 2200 LM; 2 x 1.5Ah
//
//  4b) 233029401 - DEEP ULTIMATE F65 LG OPTI 9.6V LiFe:
//              a) 8W 300LM; 1.5Ah
//              b) 11W 500LM; 1.5Ah
//              c) 24W 800LM; 1.5Ah
//
//  6) 233029xxx - INFINITA CT 9.6V LTO:
//              a) 1700 LM; 2 x 1.5Ah
//
//  7) 233029xxx - T5 GL AT 9.6V LiFe:
//              a) 1250 LM; 1.5Ah
//              b) 2200 LM; 2 x 1.5Ah
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define DEEP_ULTIMATE_F65_LG_OPTI_9_6V_LIFE   //233029201 BIN0393

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#define SW_BUILD_BYTE_LOW       0x11    // Build 17
#define SW_BUILD_BYTE_HI        0x00


#ifdef DEEP_ULTIMATE_F65_LG_OPTI_9_6V_LIFE
//233029401 BIN0393
    #warning DEEP_ULTIMATE_F65_LG_OPTI_9_6V_LIFE

    #define TIPO_LAMP                   0b01000111  //SA, 1000 lumen
    #define TIPO_LAMP_DEFAULT           0b01000000  //SA, 150 LUMEN

    #define MASK_PARAMETRI              0b11000111  //

//versione AT
    #define DUTY_800_1H                350 //*
    #define DUTY_800_1H5               350
    #define DUTY_800_2H                300
    #define DUTY_800_3H                300 //*
    #define DUTY_800_8H                300

    #define DUTY_500_1H                350 //*
    #define DUTY_500_1H5               350
    #define DUTY_500_2H                300
    #define DUTY_500_3H                300 //*
    #define DUTY_500_8H                300

    #define DUTY_300_1H                300 //*
    #define DUTY_300_1H5               300
    #define DUTY_300_2H                300
    #define DUTY_300_3H                300 //*
    #define DUTY_300_8H                300

    #define DUTY_SOFT_START             200

// SHUNT = 0.73
    #define I_SP_800_SE_1H             220
    #define I_SP_800_SE_1H5            147
    #define I_SP_800_SE_2H             110
    #define I_SP_800_SE_3H             73
    #define I_SP_800_SE_8H             27

    #define I_SP_500_SE_1H             137
    #define I_SP_500_SE_1H5            92
    #define I_SP_500_SE_2H             69
    #define I_SP_500_SE_3H             46
    #define I_SP_500_SE_8H             17

    #define I_SP_300_SE_1H             82
    #define I_SP_300_SE_1H5            55
    #define I_SP_300_SE_2H             41
    #define I_SP_300_SE_3H             27
    #define I_SP_300_SE_8H             10

//    #define DUTY_ACK                    DUTY_SOFT_START

    #define ID_SW                       788
    #define RELEASE                     0
    #define TIPO_LG                     0xBB // Lampada NO UVOXY

    #define TIPO_TUBO                   0x05	// Modulo Led
    #define TIPO_BATTERIA               0x18	// LTO 9.6V 1.2Ah

    #define TIPO_LG_SE                  0xC8 // Lampada CBL SE
    #define TIPO_LG_SA                  0xC7 // Lampada CBL SA

    // IDLOC
    #define ID_SW_LOC_0                 7
    #define ID_SW_LOC_1                 8
    #define ID_SW_LOC_2                 2
    #define ID_SW_LOC_3                 RELEASE

#endif


//*********************
// Dichiarazione tipi
//*********************

//typedef signed char int8;
//typedef signed int int16;
//typedef __int24 int24;
//typedef signed long int32;
//typedef unsigned char uchar;
//typedef unsigned char uint8_t;
//typedef unsigned int uint16_t;
//typedef __uint24 uint24_t;
//typedef unsigned long uint32;


// serve per le routines I2C
typedef unsigned char bool;

typedef enum {
    FALSE, TRUE
} bool_t; // FALSE=0, TRUE=1

typedef enum {
    OUTPUT, INPUT
} T_InOut; // OUTPUT=0, INPUT=1

typedef enum {
    OFF, ON
} T_Switch; // OFF=0, ON=1

typedef enum {
    NO, YES
} T_YN; // NO=0, YES=1

typedef enum {
    MANUALE, AUTOMATICO
} T_Modo; // MANUALE=0, AUTOMATICO=1

/************************************/
/************************************/
//  TIMER 1 SECONDO
/************************************/
/************************************/
#ifdef TEMPO_TEST_DEMO

#ifdef SOLO_AUTONOMIA_DEMO
    #define T_1H_AUT_H          0x00
    #define T_1H_AUT_M          0x0E
    #define T_1H_AUT_L          0x10

    #define T_1H5_AUT_H         0x00
    #define T_1H5_AUT_M         0x15
    #define T_1H5_AUT_L         0x18

    #define T_2H_AUT_H          0x00
    #define T_2H_AUT_M          0x1C
    #define T_2H_AUT_L          0x20

    #define T_3H_AUT_H          0x00
    #define T_3H_AUT_M          0x2A
    #define T_3H_AUT_L          0x30

    #define T_8H_AUT_H          0x00
    #define T_8H_AUT_M          0x70
    #define T_8H_AUT_L          0x80

    #define T_FUNZ_H            0x00
    #define T_FUNZ_M            0x00
    #define T_FUNZ_L            0x1E

#else
    //30 sec
    #define T_1H_AUT_H          0x00
    #define T_1H_AUT_M          0x00
    #define T_1H_AUT_L          0x1E
    //30 sec
    #define T_1H5_AUT_H         0x00
    #define T_1H5_AUT_M         0x00
    #define T_1H5_AUT_L         0x1E
    //30 sec
    #define T_2H_AUT_H          0x00
    #define T_2H_AUT_M          0x00
    #define T_2H_AUT_L          0x1E
    //30 sec
    #define T_3H_AUT_H          0x00
    #define T_3H_AUT_M          0x00
    #define T_3H_AUT_L          0x1E
    //30 sec
    #define T_8H_AUT_H          0x00
    #define T_8H_AUT_M          0x00
    #define T_8H_AUT_L          0x1E
    //10sec
    #define T_FUNZ_H            0x00
    #define T_FUNZ_M            0x00
    #define T_FUNZ_L            0x0A

#endif

#else
    #define T_1H_AUT_H          0x00
    #define T_1H_AUT_M          0x0E
    #define T_1H_AUT_L          0x10

    #define T_1H5_AUT_H         0x00
    #define T_1H5_AUT_M         0x15
    #define T_1H5_AUT_L         0x18

    #define T_2H_AUT_H          0x00
    #define T_2H_AUT_M          0x1C
    #define T_2H_AUT_L          0x20

    #define T_3H_AUT_H          0x00
    #define T_3H_AUT_M          0x2A
    #define T_3H_AUT_L          0x30

    #define T_8H_AUT_H          0x00
    #define T_8H_AUT_M          0x70
    #define T_8H_AUT_L          0x80

    #define T_FUNZ_H            0x00
    #define T_FUNZ_M            0x00
    #define T_FUNZ_L            0x1E

#endif

#define CCPTMRS_init    0b00000000 //TMR2 assegnato a tutti i CCPx e PWMx

//*****************************************************************************
// COSTANTI
//*****************************************************************************
#define byte unsigned char
#define word unsigned int

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80

#define NON_VALIDO	0
#define VALIDO		1

#define LOW         0
#define HIGH		1

#define OUTPUT		0
#define INPUT		1

//*****************************************************************************
//COSTANTI UTILIZZATE IN DIVERSI AUTOMI
//*****************************************************************************
#define FVR_BASE        0b11001010  //FVR ON; COMP FVR = 2.048V; ADC FVR = 2.048V
#define FVR_ILED        0b11001001  //FVR ON; COMP FVR = 2.048V; ADC FVR = 1.024V

//per funzione CancellaErrori()
#define SOLO_ERRORI_TEST     1
#define TUTTI_GLI_ERRORI     0  

//id associati agli AUTOMI per l'accesso in mutua eclusione alle risorse condivise
#define ID_DRV_LG           0
#define ID_DRV_CBL          1

//*****************************************************************************
// DEFINIZIONI DI TIPI DI DATO
//*****************************************************************************

typedef union {
    uint8_t cReg;

    struct {
        uint8_t Low : 4;
        uint8_t High : 4;
    } cNibble;

    struct {
        uint8_t b0 : 1;
        uint8_t b1 : 1;
        uint8_t b2 : 1;
        uint8_t b3 : 1;
        uint8_t b4 : 1;
        uint8_t b5 : 1;
        uint8_t b6 : 1;
        uint8_t b7 : 1;
    } cBit;
} tUnion8bit;

typedef union {
    unsigned iReg;

    struct {
        char Low;
        char High;
    } iByte;
} tUnion16bit;

//--------------------------------------------
//  iInteger
//--------------------------------------------

typedef union {
    int iInt;
    char cInt[2];
} iInteger;

//--------------------------------------------
//  iLong
//--------------------------------------------

typedef union {
    long lLng;
    char cLng[4];
} lLong;

//--------------------------------------------
//  tFlagUtili
//--------------------------------------------

typedef union {
    uint32_t iReg;

    struct {
        uint8_t Potenza1H: 1; //1
        uint8_t Potenza1H5: 1; //2
        uint8_t Potenza2H: 1; //3
        uint8_t Potenza3H: 1; //4
        uint8_t Potenza8H: 1; //5
        uint8_t TestFunz: 1; //6
        uint8_t TestAuto: 1; //7
        uint8_t TestInibito: 1; //8 segnala che Opticom ha richiesto l'inibizione dei test
        uint8_t SensingLuceTest: 1; //9 1-test non eseguiti se non c'� luce ambiente
        uint8_t CampioneOpti: 1; //10
        uint8_t DisparinPari: 1; //11 0-pari; 1-dispari
        uint8_t TestRimandato: 1; //12
        uint8_t DisabilitaLedSegnalazione: 1; //13
        uint8_t Spegnimento: 1; //14 1-spegnimento
        uint8_t OpticomDisabilitato: 1; //15 1-disabilita per sempre l'Opticom
        uint8_t SetDurataTest: 1; //16 1-test di durata pari ai setting; 0-test d'autonomia sempre di 1h
        uint8_t RicaricaIncompleta: 1; //17 1-test autonomia iniziato con batteria non completamente carica
        uint8_t ErroreConfigurazione: 1; //18
        uint8_t FunzioneSA: 1; //19 1=SA
        uint8_t FunzioneSE: 1; //20
        uint8_t FunzionePS: 1; //21
        uint8_t RestModePrenotato: 1; //22
        uint8_t PonticelloPresente: 1; //23 1->ponticello presente->1H   0->assente->3H
        uint8_t LampeggiaLed: 1; //24 1->lampeggio dei led di potenza
        uint8_t Lumen800: 1; //25
        uint8_t Lumen500: 1; //26
        uint8_t Lumen300: 1; //27
        uint8_t FineDutySan: 1; //28 1->finito il duty temporale di un profilo
        uint8_t flagFinePeriodoProfilo: 1; //29 1->finto periodo di un profilo (serve anche per forzare una sincronizzazione del profilo stesso)
        uint8_t FunzioneTR: 1; //30
    } iBit;
} tFlagFissiLampada;

//
//DEFAULT: Funz_SA, Potenza1H, 
//
//                            33322222222221111111111000000000
//                            21098765432109876543210987654321
#define INIT_FLAG_FISSI     0b00000000000000000000000000000001
#define RESET_AUTONOMIE     0b11111111111111111111111111100000
//--------------------------------------------
//  tByteErrori
//--------------------------------------------

typedef union {
    uint8_t cReg;

    struct {
        uint8_t flagEFunz : 1; //0 - errore batteria in funzionale
        uint8_t flagEAuto : 1; //1 - errore autonomia
        uint8_t flagELed : 1; //2 - errore di strip, staccata o rotta
        uint8_t flagELedSA : 1; //3 - errore led quando � in accensione SA
        uint8_t flagRicaricaIncompleta : 1; //4 - test auto fallito con batteria non completamente carica
        uint8_t flagEtuboUV : 1; //5 - errore tubo UV
    } cBit;
} tByteErrori;

#define SOLO_ERR_UV         0b00010000
#define CANCELLA_ERR_TEST   0b11100000
//--------------------------------------------
//  tTimer
//--------------------------------------------

typedef union {
    uint24_t slTimer; //HHMMLL

    struct {
        uint8_t Low; //----LL
        uint8_t Medium; //--MM--
        uint8_t High; //HH----
    } cByte;
} tTimer;

//--------------------------------------------
//  tFiltrato
//--------------------------------------------

typedef union {
    uint16_t iReg;

    struct {
        uint16_t flagBatteriaMinimaBassa : 1; //( 0)
        uint16_t flagLedCortoFilt : 1; //( 1)
        uint16_t flagRetePresenteFilt : 1; //( 2)
        uint16_t flagLedStaccatiFilt : 1; //( 3) 1-strip led staccata
        uint16_t flagReteSAPresente : 1; //( 4)
        uint16_t flagBatteriaStaccata : 1; //( 5)
        uint16_t flagBatteriaInCorto : 1; //( 6))
        uint16_t flag1Hn3H : 1; //( 7) 1-1H  0-3H
        uint16_t flagLogicaBusPresente : 1; //( 8)
        uint16_t flagRM : 1; //( 9) 0-> RestMode Off, spegne; 1-> RestMode On, accende
        uint16_t flagBatteriaUNOPresente : 1; //(10)
        uint16_t flagBatteriaDUEPresente : 1; //(11)
    } iBit;
} tFiltrato;

//                             1111110000000000
//                             5432109876543210
#define RESET_SENZA_JUMPER   0b0000000010000000
#define INIT_INGRESSI        0b0000000010010000


//--------------------------------------------
//  filtro
//--------------------------------------------

typedef struct {
    uint8_t cContaCampioni;
    uint8_t cValoreFilt;
} filtro;

//--------------------------------------------
//  misura
//--------------------------------------------

typedef struct {
    uint16_t iSomma;
    uint16_t iMedia;
} misura;

//--------------------------------------------
//  misuralong
//--------------------------------------------

typedef struct {
    uint32_t iSomma;
    uint16_t iMedia;
} misuralong;


//--------------------------------------------
#endif
