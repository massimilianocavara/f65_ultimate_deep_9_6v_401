// AUTOMA_Visualizzatore.c

#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_Visualizzatore.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_Lampada.h"

//#ifdef DEBUG_LG
//    #include "AUTOMA_DriverCablecom.h"
//    extern uint8_t cStatoDriverCBL;
//    extern tFlagDriverCabl uFlagDriverCbl;
//#endif
        

uint8_t cStatoVisualizzatore;
uint8_t cCounterStep; //realizza 1 singolo lampeggio
uint8_t cCounterCicli; //realizza da 1 a 3 lampeggi
uint8_t cDutyArancio;
uint8_t cPeriodArancio;

extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFlagFissiLampada uFlagFissiLampada;

extern tFiltrato uIngressiFilt;
extern tByteErrori uByteErrori;

extern uint8_t DebugReg;

//******************************************************************************
// void fInitAutomaVisualizzatore(void)
//
// funzione: inizializza l'AUTOMA_Visualizzatore
//******************************************************************************
void fInitAutomaVisualizzatore(void) {
    cCounterStep = 1;
    cCounterCicli = 1;
    cStatoVisualizzatore = ST_LED_VERDE;
}

//******************************************************************************
// void fSpegniLed(void)
//
// funzione: spegne i led di segnalazione
//******************************************************************************
void fSpegniLed(void) {
    LATB &= 0b11001111; //azzero LED_R e LED_V
    TRISB &= 0b11001111; //impongo LED_R e LED_V in uscita
}

//******************************************************************************
// void fAccendiLedVerde(void)
//
// funzione: accende il led verde
//******************************************************************************
void fAccendiLedVerde(void) {
    LATB &= 0b11101111;
    LATB |= 0b00100000;
    TRISB &= 0b11001111;
}

//******************************************************************************
// void fAccendiLedRosso(void)
//
// funzione: accende il led rosso
//******************************************************************************
void fAccendiLedRosso(void) {
    LATB &= 0b11011111;
    LATB |= 0b00010000;
    TRISB &= 0b11001111;
}

//******************************************************************************
// void fAccendiArancio(void)
//
// funzione: emulo un half-bridge con un registro
//******************************************************************************
void fAccendiArancio(void) {
    if (0 != cDutyArancio) {
        fAccendiLedVerde();
    } else {
        if (0 != cPeriodArancio) {
            fAccendiLedRosso();
        } else {
            cPeriodArancio = PERIOD_ARANCIO;
            cDutyArancio = DUTY_ARANCIO_V;
            fAccendiLedVerde();
        }
    }
}

//******************************************************************************
// bool_t fDecodificaVerde(void)
//
//******************************************************************************
bool_t fDecodificaVerde(void) {
    if (uFlagInputVisualizzatore.iBit.flagVisVerde1blink) {
        cCounterCicli = 1;
        return TRUE;
    } else {
        if (uFlagInputVisualizzatore.iBit.flagVisVerde2blink) {
            cCounterCicli = 2;
            return TRUE;
        } else {
            if (uFlagInputVisualizzatore.iBit.flagVisVerde3blink) {
                cCounterCicli = 3;
                return TRUE;
            } else {
                cCounterCicli = 3;
                return FALSE;
            }
        }
    }

}

//******************************************************************************
// bool_t fDecodificaRosso(void)
//
//******************************************************************************
bool_t fDecodificaRosso(void) {
    if (uFlagInputVisualizzatore.iBit.flagVisRosso1blink) {
        cCounterCicli = 1;
        return TRUE;
    } else {
        if (uFlagInputVisualizzatore.iBit.flagVisRosso2blink) {
            cCounterCicli = 2;
            return TRUE;
        } else {
            if ((uFlagInputVisualizzatore.iBit.flagVisRosso3blink) || (uFlagInputVisualizzatore.iBit.flagVisRossoBlinkContinuo)) {
                cCounterCicli = 3;
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
}

//******************************************************************************
// bool_t fDecodificaArancio(void)
//
//******************************************************************************
bool_t fDecodificaArancio(void) {
    if (uFlagInputVisualizzatore.iBit.flagVisArancio1blink) {
        cCounterCicli = 1;
        return TRUE;
    } else {
        if (uFlagInputVisualizzatore.iBit.flagVisArancio2blink) {
            cCounterCicli = 2;
            return TRUE;
        } else {
            if ((uFlagInputVisualizzatore.iBit.flagVisArancio3blink) || (uFlagInputVisualizzatore.iBit.flagVisArancioBlinkContinuo) || (uFlagInputVisualizzatore.iBit.flagVisArancioFisso)) {
//            if ((uFlagInputVisualizzatore.iBit.flagVisArancio3blink) || (uFlagInputVisualizzatore.iBit.flagVisArancioBlinkContinuo)) {
                cCounterCicli = 3;
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
}

//******************************************************************************
// void AUTOMA_Visualizzatore(void)
//
// funzione: AUTOMA che implementa il driver dei led di segnalazione;
//				 Secondo il modello LOGICA, ci possono essere segnalazioni verdi, rosse ed arancio
//				 in sequenza
//******************************************************************************

void AUTOMA_Visualizzatore(void) {

    if (uFlagTimer.iBit.flag1msec) {
        if (cPeriodArancio > 0) {
            cPeriodArancio--;
        }
        if (cDutyArancio > 0) {
            cDutyArancio--;
        }
    }    

    if ((uFlagInputVisualizzatore.iBit.flagVisLedSpenti) || (uFlagFissiLampada.iBit.DisabilitaLedSegnalazione)) {
        fSpegniLed();
    } else {
        if (uFlagInputVisualizzatore.iBit.flagVisRossoFisso) {
            fAccendiLedRosso();
        } else {
            if (uFlagInputVisualizzatore.iBit.flagVisArancioSempre) {
                fAccendiArancio();
            } else {
                //sincro con i 250msec
                if (uFlagTimer.iBit.flag250msec) {
                    //DECODIFICA dello stato:
                    if (--cCounterStep == 0) {
                        cCounterStep = 3;
                        if (--cCounterCicli == 0) {
                            cCounterCicli = 3; //anticipo
                            //Si ricomincia ed occorre selezionare il nuovo STATO
                            switch (cStatoVisualizzatore) {
                                //************************************************************************
                                case ST_LED_VERDE:
                                {
                                    cStatoVisualizzatore = ST_LED_ROSSO; //next
                                    if (!fDecodificaRosso()) {
                                        cStatoVisualizzatore = ST_LED_ARANCIO;
                                        if (!fDecodificaArancio()) {
                                            cStatoVisualizzatore = ST_LED_VERDE;
                                            fDecodificaVerde();
                                        }
                                    }
                                    break;
                                }

                                //************************************************************************
                                case ST_LED_ROSSO:
                                {
                                    if (uFlagInputVisualizzatore.iBit.flagVisRossoBlinkContinuo) {
                                        cCounterCicli = 3;
                                    } else {
                                        cStatoVisualizzatore = ST_LED_ARANCIO; //next
                                        if (!fDecodificaArancio()) {
                                            cStatoVisualizzatore = ST_LED_VERDE;
                                            fDecodificaVerde();
                                        }
                                    }
                                    break;
                                }

                                //************************************************************************
                                // cmq stato terminale , occorre tornare in ST_LED_VERDE
                                //************************************************************************
                                case ST_LED_ARANCIO:
                                {
                                    if (uFlagInputVisualizzatore.iBit.flagVisArancioBlinkContinuo) {
                                        cCounterCicli = 3;
                                    } else {
                                        cStatoVisualizzatore = ST_LED_VERDE;
                                        fDecodificaVerde();
                                    }
                                    break;
                                }

                                //************************************************************************
                                default:
                                {
                                    cStatoVisualizzatore = ST_LED_VERDE;
                                    fDecodificaVerde();
                                    break;
                                }

                            }//switch
                        }//if
                    }//if
                }//sincro 250msec

                switch (cStatoVisualizzatore) {
                    //****************
                    case ST_LED_VERDE:
                    {
                        if ((2 == cCounterStep) || (uFlagInputVisualizzatore.iBit.flagVisVerdeFisso)) {
                            fAccendiLedVerde();
                        } else {
                            fSpegniLed();
                        }
                        break;
                    }

                    //****************
                    case ST_LED_ROSSO:
                    {
                        if (cCounterStep == 2) {
                            fAccendiLedRosso();
                        } else {
                            fSpegniLed();
                        }
                        break;
                    }

                    //****************
                    case ST_LED_ARANCIO:
                    {
//                            if (cCounterStep == 2) {
                        if ((cCounterStep == 2) || (uFlagInputVisualizzatore.iBit.flagVisArancioFisso)) {
                            fAccendiArancio();
                        } else {
                            fSpegniLed();
                        }
                        break;
                    }

                    //****************
                    default:
                    {
                        fAccendiLedRosso();
                    }
                }//switch
            }
        }//else
    }//else
}

