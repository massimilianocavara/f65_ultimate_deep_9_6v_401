// AUTOMA_Charger.h

#ifndef	__CHARGER_H
#define	__CHARGER_H

#define	ST_CHARGER_FINECARICA               0	//CHARGER_FINECARICA
#define	ST_CHARGER_RICARICA                 1	//CHARGER_RICARICA
#define	ST_CHARGER_SCARICA                  3	//CHARGER_SCARICA
#define	ST_CHARGER_VERIFICA_BATT            4	//VERIFICA BATTERIA STACCATA
#define ST_CHARGER_DISABLE                  5
#define ST_CHARGER_FINCAR_VERIF_BATT        6
#define ST_CHARGER_WAIT_TEST                7
#define ST_CHARGER_WAIT_RETE                8
#define ST_CHARGER_SCARICA_BLOCCATA         9
#define ST_CHARGER_SLEEP                    10
#define ST_CHARGER_TEST                     11
#define ST_CHARGER_BATTERIA_STACCATA        12
#define ST_CHARGER_MANTENIMENTO             13

//Partitore teorico = 0.1152
//Vref teorica = 2.048V
//
////////con taratura, andiamo a riferirci ad un nuovo Partitore (98% dell'originale) e Vref (+4% dell'originale)
////////
////////Partitore valori tarati = 0.1129
////////Vref valori tarati = 2.13V
//
// 7.0V -> ((7*0.1152)/2.048)*1024 -> 7 * 57.6
#define TENS_BATTERIA_STACCATA          403       
// 7.5V
#define TENS_BATTERIA_RICOLLEGATA       432
// 12V
#define TENS_FINE_CARICA                691
// 10.2V
#define TENS_MANTENIMENTO_LOW           587
//// 8.8V
//#define	SOGLIA_MINIMA                   507
//// 9V
//#define SOGLIA_MINIMA_ALTA              518
// 7.8V
#define	SOGLIA_MINIMA                   450
// 8V
#define SOGLIA_MINIMA_ALTA              460
// 9.6V 
#define TENSIONE_NOMINALE               553
// 4V
#define SOGLIA_BATTERIA_CORTO           230


// 250msec iTimerChargerLong
#define UN_ORA                      14400   //14400*250msec=3600sec
#define DUE_SECONDI                 10      //10x250msec=2.5sec
#define TRE_SECONDI                 12      //12x250msec=3sec
#define TIME_PRIMA_VER_BATT         20      //20x250msec=5sec

#define TIME_PROT                   8       //2sec
// 10msec cTimerChargerShort
#define UN_SEC_SH                   100     //1sec
#define DUE_SEC_SH                  200     //2sec

#ifdef TEMPO_TEST_DEMO
    //#define RICARICA_12H        300 //5 min
    #define RICARICA_12H        10 //10 sec
#else
    #ifdef TEMPO_RIC_DEMO
        #define RICARICA_12H        60 // 1 min
    #else
        #define RICARICA_12H            43200   //12h
    #endif
#endif

//--------------------------------------------
//  tFlagCharger
//--------------------------------------------
typedef union {
    uint8_t cReg;

    struct {
        uint8_t flagRicaricaInCorso : 1; //0
        uint8_t flagFineCarica : 1; //1
        uint8_t flagBatteriaStaccata : 1; //2
    } cBit;
} tFlagCharger;

//                                        76543210
#define	FLAG_FINECARICA                 0b00000011
#define	FLAG_SCARICA                    0b00000000
#define	FLAG_RICARICA                   0b00000001
#define	FLAG_STACCATA                   0b00000001
#define	FLAG_CHARGER_INIT               0b00000001
#define FLAG_CHARGER_OFF                0b00000000





#endif
