//********************************************************************************
//									
// Filename:            MAIN.C
// Project:         	F65_DEEP_ULTIMATE_401_01
// Date:                24/11/2021
// Micro:               16F1718
//                      Vdd=3.3V
// Company:             Beghelli Spa
//
//--------------------------------------------------------------------------------
//
// *******PERIFERICHE*******
//
//  Oscillatore interno: 
//          32MHz -> TMR0
//  Quarzo esterno: 
//          32768Hz -> TMR1
//  Opticom: 	
//          COMP2 con ingressi Vopti(RA0) e Reference(DAC1) per scarica condensatore Opti
//          DAC1 come Reference
//          FVR per DAC1 pari a 2.048V
//          TMR6 per conteggio tempo carica condensatore Opti
//	Sincronizzazione rete: 
//          IOC su RB2 (V_RETE_sync)
//	Accenditore SE: 
//          RC5 (PWM_LED) con CCP1+TMR2
//          Freq = 40KHz
//	Sensore Rete: 
//          analogico su RB3
//	Accensione SA: 
//          RB1 (SA_ON)
//	Cablecom:	
//          RX -> RC3 con IOC
//          TX -> RA7
//          TMR4
//  Logica (seriale):
//          RXD -> RC7
//          TXD -> RC6
//	Analogici: 
//          per ogni ingresso, una misura ogni 5msec circa
//          buffer da 8 elementi -> 40msec per stabilizzare un valore
//          Vref = 1.024V / 2.048V / Vdd
//          RA1-AN1  -> LAMP_ON
//          RA2-AN2  -> I_LED
//          RA3-AN3  -> Vbatt
//          RA5-AN4  -> sel_UV
//          RB3-AN9  -> Vrete
//          RB4-AN11 -> Jumper 1H/n3H
//          RC2-AN14 -> VledSA
//
//  Autonomie:
//          70% della capacit� della batteria
// **********************************************************************************************************************
//
// Partiamo dal 233029201
// 1) Ripristino completamente la LOGICA
// 2) Aggiunte le luminosit� 800LM, 500LM e 300LM
// 3) Aggiunto il Bootloader
//
//
//
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
// >>>>build_19
// 1) Corretta la gestione del caricabatterie: finecarica a 12V con caricatore spento fino a 10.2V e da qui riprendo a caricare
// 2) Cambio la dicitura FINECARICA con MANTENIMENTO.
// 3) BUG: mancava la richiesta di scrittura in Eeprom dopo la taratura, ora realizata introducendo il registro 'RegAggiornaTaratura' con il valore 
//  RICHIESTA_TARATURA = 0x95
// 4) Definita meglio la richiesta di aggiornare l'Eeprom con l'introduzione della funzione 'void fRichiediAggEE(uint8_t mod)' che prevede:
//      0 no parametro, no taratura
//      1 s� parametro, no taratura
//      2 no parametro, s� taratura
// 5) Mancava l'aggiornamento di config da TabellaLogica in AUTOMA_ProtocolloLogica...
// 6) BUG Nel conteggio della durata dell'emergenza per decidere se aprire la finestra Opticom o meno: infatti, non consideravo l'overflow
// 7) Tensione di Minima Alta e Minima portate a 8V e 7.8V
// 8) Allungo i tempi della protezione STRIP STACCATA (in SE e TEST) da 100msec ad 1sec
// 9) Mancava il pilotaggio alto di TX della seriale in Sleep per consentire la riaccensione per RestMode
// 
// >>>>build_18
// 1) Sistemata la gestione di batteria staccata: si genera l'errore solo se mancano entrambe
// 2) Modifica nel DriverOpticom: i cambiamenti lenti del DAC vengono fatti teoricamente anche ogni 20msec, tempo di campione,
//  ma la media impiega circa 1 secondo per andare a regime, quindi devo vincolarmi ad almeno 50 campionamenti per capire ("cCampioniCorrezioneLenta")
//  l'effetto della variazione
// 3) Aggiunto il flag "uFlagEeprom.cBit.flagAggiornaConfig" che consente di aggiornare la configurazione ogni volta (circa 4h:30) che si ripristina TabellaLogica da Eeprom
//
// >>>>build_17
// 1) Separo la versione 233029201 AT dalla 233029301 CT per semplificare la codifica e tenendo conto del fatto che la prima monta il 16F1716,
//  mentre la seconda monta 16F1718
// 2) Non ci sono personalizzazioni di luminosit�, ma solo il doppio flusso nel caso sia presente la seconda batteria
// 3) La protezione di STRIP LED STACCATA ha mostrato una pericolosa debolezza: infatti, non avendo messo una limitazione al PWM, che poteva
//  arrivare al 100%, si generavano situazioni dinamiche con forti assorbimenti (quasi corto di batteria) in quanto il duty PWM_LED
//  tende ad essere aumentato dall'AUTOMA_AccenditoreSE perch� non si arriva mai al setpoint, mentre il CLC lo rende burst, per� ci�
//  non � sufficiente a limitare la corrente che fluisce dalla batteria e dall'induttore, che diventa potenzialmente distruttiva.
//  Metto un limite all'80% per il duty di PWM_LED e al contempo occorre aumentare il contributo su Iled da parte di R708 e R714
// 4) Rallento il filtraggio di LedStaccatiSA, che passa da 100msec ad 1sec
// 5) DEBOLEZZA AUTOMA_Eeprom: le criticit� con la EEPROM (perdita di valori) � legata certamente ad una tensione di accensione non adeguata per scrittura in EEPROM,
//  ma anche ad una DEBOLEZZA dell'AUTOMA che subito al risveglio da Sleep va proprio a scriverla. 
//  Inoltre prevedevo una scrittura da RAM a EEPROM ogni 24h che per� non ha alcun senso, perch� c'� una lettura da EEPROM a RAM ogni 4h, quindi non serve e spreca una scrittura.
//  Per irrobustire ulteriormente, elimino i flag ad 1 bit ed introduco: 
//  un registro "RegAggiornaEeprom" che deve essere posto a RICHIESTA_AGGIORNAMENTO (0xAA) per avviare l'aggiornamento della EEPROM
//  un registro "RegAggiornaParametri" che deve essete posto a RICHIESTA_PARAMETRI (0x55) per consentire l'aggiornamento del registro di configurazione
// 6) Introduco alcune sicurezze per quel che riguarda la PARAMETRIZZAZIONE: 
//  a) scrivo in eeprom il valore di parametrizzazione solo se c'� stato un esplicito comando di scrittura -> tramite "RegAggiornaParametri";
//  b) come parametro di DEFAULT metto lampada SA -> TIPO_LAMP_DEFAULT
//  c) metto una mascheratura dei soli valori accettabili e controllo che non sia zero (non possibile) -> MASK_PARAMETRI
//
// >>>>build_16
// 1) La gestione del risveglio per WDT con attesa di reset crea dei problemi perch� capita molto spesso, quindi evitiamo di farla
//
// >>>>build_15
// 1) Scambio di pin: ACC_SE -> RA5, nBatt1 -> RB6
// 2) Corretta la gestione della valore tarato di 10.2V nell'AUTOMA_Charger: si trova in TabellaLogica2 e viene utilizzato in AUTOMA_AccenditoreSE
// 3) Aggiornato il driver CBL con tutte le migliorie mutuate da UV_Tech47:
//      a) Corretto errore in fDisabilitaInt(), dove usavo IOCENbits.IOCEN3 invece di IOCCNbits.IOCCN3
//      b) Altro bug nell'AUTOMA_DriverCablecom: la soluzione del bug precedente ha mostrato un altro errore presente,
//          cio� la non inizializzazione di uFlagDriverCbl, che lasciava, nei casi di configurazione, settato il bit 'uFlagDriverCbl.iBit.CheckContesa'
//      c) Aggiunta anche l'inizializzazione di "uFlagLogica" in AUTOMA_ProtocolloLogica
//      d) Ulteriore bug, c'era una corsa tra il driver nella routine di interrupt e l'AUTOMA_DriverCablecom. In pratica ST_CBL_IDLE andava a chiamare
//          fAbilitaIntInDiscesa() amche quando era gi� cominciata la ricezione andando aleatoriamente a far fallire la ricezione del fronte dello StartBit
// 4) Corretta la gestione di ACC_SE per la risposta Opticom: deve essere gestito direttamente dall'AUTOMA_DriverOptiCom
// 5) Introdotta la protezione di led in corto anche in AUTOMA_DriverOptiCom per gestire il caso di risposta ACK e NACK
//
//
// >>>>build_13_1
// 1) Ripristino la polarit� originale del COMP1 causa di problemi non ben chiari.
// 2) Uso la variabile "cStripLedCorto" per gestire il glitch e la mascheratura presente nella commutazione SA-SE (se SA presente):
// se al termine della mascheratura, il valore � ad 1 vuol dire che non c'� stato un glitch ma un vero corto. Il glitch � causato dal
// condensatore C102, dopo il ponte, che si scarica sullo shunt di Iled con una durata inferiore ai 5msec. 
// Mettiamo una mascheratura di 5msec con interrupt negativo (Iled alta porta CMP1 in uscita bassa...) che, se scatta, setta "cStripLedCorto" e 
// pone l'interrupt sul fronte positivo, cos�, se si tratta di un glitch, la corrente torner� sotto al DAC2, facendo scattare nuovamente l'int di CMP1, 
// che resetta "cStripLedCorto" e rimette l'int sul fronte negativo. Se invece scade il timer di mascheratura si conclude che sia presente un corto...
//
// >>>>BUILD_13
// 1) Inverto la polarit� del COMPARATORE_1 in modo che dia uscita positica se Iled risulta maggiore di 300mV e bassa viceversa
//
// >>>>build_11/12
// 1) Corretto il canale analogico di VledSA (12 e non 14 come c'era prima...)
// 2) Modifiche all'algoritmo di gestione del corto di strip led
//
// >>>>build_10
// 1) Modifichiamo la taratura: calcoliamo solo il valore di fine carica a 10.2V senza calcolare alcun parametro correttivo: pongo il valore
//  di rifermineto in TabellaLogica2()
// 2) Aggiunta la protezione STRIP LED STACCATA in SA: prima si usava lo ZCD, ora, invece, si fa una misura analogica e si confronta con soglia ad 1V
//
// >>>>build_09
// 1) Nel passare dai 32MHz agli 8MHz, prima di andare in Sleep, mi fermavo aspettando che il PLL si spegnesse, ma ci� blocca
//  il sw e fa intervenire il WDT e non funziona lo Sleep
// 2) Corretto errore nella contesa: abilitato l'interrupt in salita mentre deve essere abilitato in discesa
// 3) Altro errore nella TX CBL: non verificavo la contesa durante l'attesa della trasmissione
// 4) Abilitato il PLL in CONFIG
//
// >>>>build_08
// 1) Eliminiamo il ZCD perch� presenta sempre in uscita una tensione pari a 0.7V per come � fatto (controllo di corrente piuttosto che di tensione).
//  Entriamo direttamente nel CLC con il pin RB0 e ne sfruttiamo il valore digitale
// 2) Corretto l'interrupt di COMP1: era attivo su fronte di salita mentre lo deve essere su fronte di discesa
// 3) Deadlock durante la protezione di corto: ho messo la routine di spegnimento del PWM dentro all'interrupt di COMP1, senza tenere conto del fatto
//  che sarebbe potuta intervenire prima della fine della routine di accensione, in particolare spegnendo il TMR2 prima del primo overflow, causando
//  il blocco del micro nell'attesa di questo overflow. Risolvo disabilitando gli interrupt nella procedura di accensione del PWM
//
// >>>>build_07
// 1) Aggiorno il pinout alla versione C
//
// >>>>build_06
// 1) La protezione del corto viene ora fatta in questo modo: il pin RB6 diventa ACC_SE e pilota il MOS Q701 sotto i led, quello che ora
//  era attivato da uno zener in modo indiretto, e Iled diventa il segnale in ingresso al Comp1 e poi al CLC. 
//  Se Iled supera i 200mV, scatta il Comp1, il cui vettore di interrupt spegne il PWM_LED ed apre il mos Q701.
//  L'ingresso di Comp1 in CLC resta necessario perch� la risposta al corto deve essere il pi� celere possibile, lo stop
//  del CLC garantisce tempi inferiori alla routine di interrupt.
// 2) Il pin RA1 diventa ora Iled ed � contemporaneamente ingresso analogico e ingresso di Comp1 per la protezione
//
// >>>>build_05
// 1) Cambio la protezione sulla strip led introducendo lo ZCD: Vled_SA diventa l'ingresso dello ZCD, Zero Cross Detector, che contiene
//  un comparatore con una reference fissa a 0.75V, e la cui uscita va in ingresso all'LCL di protezione. Se Vled_SA supera 0.75V allora
//  viene bloccato il PWM di pilotaggio dei led. 
//  Il corto viene gestito da Vdrain in ingresso al Comp1 che riceve DAC2 come reference a 200mV circa: se viene superata questa soglia,
//  l'uscita del comparatore che � in ingresso al CLC, blocca il PWM ai led.
// 2) Durante il corto, non si deve lasciare crescere il duty altrimenti nel caso di ripartenza si avranno correnti molto alte: tale intervento
//  viene evidenziato dal bit di interrupt settato
//
// >>>>>build_04
// 1) Passo al compilatore v1.30 che ottimizza essendo PRO
//
// >>>>build_03
// 1) Occorre una TARATURA della Vref interna, che tenga anche conto del partitore di Batteria, il cui valore � quello che deve
//  essere il pi� preciso possibile: si tratta di mettere una tensione di batteria nota e precisa, per esempio 10.2V, in ingresso
//  alla scheda, poi, imponendo un pin ad 1, avviare una misura della Vbat con Vref=FVR[2.048]; quindi si confronta il risultato con
//  il valore previsto nel caso peggiore, Vref=FVR[2.048] + 4%, e si ricava il coefficiente correttivo da applicare a tutte le misure
//  annalogiche per riportarle al valore di riferimento
// 2) BATTERIA STACCATA: per evitare il palleggiamento di spegnimento ed accensione del caricabatteria che si ripercuote sull'eventuale
//  accensione SA, devo verificare la riaccensione dopo un certo tempo, in modo tale che se la batteria non fosse presente, troverei
//  una tensione molto bassa, per esempio ben al di sotto della Minima che, nel caso della 9.6V, � 7.5V.
//
// >>>>02
// 1) Introdotta la label TIPO_UVOXY per attivare o meno i comandi legati al Sanificatore UVOXY
// 2) Controllo del Mantenimento di batteria con doppia soglia per tenere conto del PCM
// 3) Errore di FVR che d� il riferimento all'ADC pari all'1% ma non accettabile per il controllo della batteria
//
// >>>>01
// 1) Partiamo da UV_Tech41
//
//
// **********************************************************************************************************************

#include <htc.h>
#include "global.h"
#include "Main.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_Charger.h"
#include "AUTOMA_Lampada.h"
#include "Timer.h"
#include "AUTOMA_DriverLogica.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "Bootloader.h"
#include "HEFlash.h"
#include "mcc_generated_files/mcc.h"
#include "MODULO_Norma.h"
#include "mcc_generated_files/pin_manager.h"

#pragma config IDLOC0 = ID_SW_LOC_0
#pragma config IDLOC1 = ID_SW_LOC_1
#pragma config IDLOC2 = ID_SW_LOC_2
#pragma config IDLOC3 = ID_SW_LOC_3

uint8_t cInitTimer;

extern tFlagFissiLampada uFlagFissiLampada;
extern tFlagCharger uFlagCharger;
extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFiltrato uIngressiFilt;
extern tStato uStato;
extern tByteErrori uByteErrori;
extern uint8_t TabellaLogica[32];
extern const uint8_t TabellaLogicaDEFAULT[32];
extern uint8_t cStatoLampada;
extern uint8_t cStatoLampadaSA;
extern tFlagTimer uFlagTimer;
extern filtro f1hn3h;
extern uint8_t TabellaBootloader[SIZE_BUFFER_FLASH];
extern uint8_t cTimerSerialeOn;

#ifdef DEBUG_LED_SIGNAL
    extern tByteDebug uDebug;
#endif

extern void AUTOMA_DriverOptiComRx(void);
extern void AUTOMA_ProtOptiComLite(void);
extern void AUTOMA_AccenditoreSE(void);
extern void AUTOMA_AccenditoreSA(void);
extern void AUTOMA_Visualizzatore(void);
extern void AUTOMA_RestMode(void);
extern void AUTOMA_Lampada(void);
extern void AUTOMA_Charger(void);
extern void AUTOMA_Filtri(void);
extern void Timer(void);
extern void AUTOMA_Ingressi(void);
extern void AUTOMA_Eeprom(void);
extern void AUTOMA_DriverCablecom(void);

extern void fInitEeprom(void);
extern void fInitAutomaVisualizzatore(void);
extern void fInitAutomaDriverOptiComRx(void);
extern void fInitAutomaProtOptiComLite(void);
extern void fInitInterrupt(void);
extern void fInitPorte(void);
extern void fInitRegistri(void);
extern void fInitAutomaFiltri(void);
extern void fInitTimer(void);
extern void fInitAutomaLampada(void);
extern void fInitAutomaAccenditoreSE(void);
extern void fInitAutomaAccenditoreSA(void);
extern void fInitAutomaCharger(void);
extern void fInitFiltIngressi(void);
extern void fInitTimerTest(void);
extern void fAccendiLedVerde(void);
extern void fInitAutomaDriverLogica(void);
extern void fInitFiltri(filtro * FiltroDaInit, uint8_t valore);

extern void MODULO_Norma(void);
//extern void fInitAutomaDriverCablecom(void);

extern void fInitAutomaTaratura(void);
extern void AUTOMA_Taratura(void);

extern void fAggiornaConfigDaTabellaLogica(void);

void CheckJumpToBoot(void);

//****************************************************************
// void fInitDaReset(void)
//
// usata solo al reset
//****************************************************************
void fInitDaSleep(void) {
 
    SYSTEM_Initialize(); //inizializza l'hardware
    
    fInitEeprom(); //inizializza TabellaLogica[]
    fAggiornaConfigDaTabellaLogica();
    
    fInitInterrupt();
    fInitAutomaFiltri();
    fInitTimer();
    fInitAutomaAccenditoreSE();
    fInitAutomaAccenditoreSA();
    fInitFiltIngressi();
    fInitAutomaVisualizzatore();    
    fInitAutomaNorma();
    fInitAutomaLampada();
    
    fInitAutomaDriverOptiComRx();
    fInitAutomaProtOptiComLite();
    
    fInitAutomaProtocolloLG();
//    fInitAutomaDriverCablecom();
//    fInitAutomaDriverLogica();
}

//****************************************************************
// void fInitCompleto(void)
//
// inizializzazioni complete
//****************************************************************
void fInitCompleto(void) {
    fInitDaSleep();
    fInitAutomaDriverLogica(); //per evitare di riattivare i 15 minuti di seriale...
    fInitAutomaCharger();
    
    fInitTimerTest();
    fInitAutomaTaratura();
    
    uIngressiFilt.iReg = 0; //resettiamo anche i jumper
    fInitFiltri(&f1hn3h, 1); //default: 1h
    cInitTimer = 8; //2 secondi    
}

//****************************************************************
// void main(void)
//****************************************************************
void main(void) {
    
    CheckJumpToBoot();
    
    fInitCompleto();
   
#ifdef DEB_RESTMODE
    cTimerSerialeOn = 1; // minuti
#else
    cTimerSerialeOn = 15;
#endif
    
    /* sequenza AUTOMI */
    while (1) {

//DEB_SIGNAL
        
        if (uFlagTimer.iBit.flag250msec) {
            if (cInitTimer > 0) {
                cInitTimer--;
            }
        }
        
        if (ST_SLEEP != cStatoLampada) {
            OSCCON = INIT_OSC_32MHZ;
#ifndef DIS_PIN_STRIP_PROG            
            nSleep_SetHigh();
            nSleep_SetDigitalOutput();            
#endif            
        }
        
        asm("clrwdt");
                        
        OPTION_REG = INIT_OPTION;
        
        Timer();
        AUTOMA_Taratura();        
        AUTOMA_Filtri();
        AUTOMA_Ingressi();
        AUTOMA_AccenditoreSE();
        AUTOMA_AccenditoreSA();        
        AUTOMA_Visualizzatore();
        AUTOMA_Lampada();
        MODULO_Norma();
        AUTOMA_Charger();
        //Opticom
        AUTOMA_DriverOptiComRx();        
        AUTOMA_ProtOptiComLite();
        
        AUTOMA_DriverLogica();
        AUTOMA_ProtocolloLogica();              
        AUTOMA_Eeprom();

    }
}

void CheckJumpToBoot(void)
{
    HEFLASH_readBlock((void*)TabellaBootloader, TAB_BOOT, sizeof(TabellaBootloader));
    // E' il primo avvio del prodotto oppure � stato abilitato l'upgrade del firmware ?
    if ((FIRST_START_MICRO == TabellaBootloader[IBUFF_FW_REGISTER]) || (START_UPG_FW == TabellaBootloader[IBUFF_FW_REGISTER]))
    {    
#ifdef MICRO_PIC16F1716

        // S� -> faccio un salto all'inizio del bootloader (GOTO 0x1B00)
        #asm
            MOVLP 0x1B  // (opcode 319B)
            GOTO 0x300  // (opcode 2B00)
        #endasm

#elif defined(MICRO_PIC16F1718)
        // S� -> faccio un salto all'inizio del bootloader (GOTO 0x3B00)
        #asm
            MOVLP 0x3B  // (opcode 31BB)
            GOTO 0x300  // (opcode 2B00)
        #endasm

#endif
    }
    else if (FIRST_START_AFTER_BOOT == TabellaBootloader[IBUFF_FW_REGISTER])
    {
        //--------------------------------------------------------------
        // Upgrade tabella bootloader
        //--------------------------------------------------------------
        HEFLASH_readBlock((void*)TabellaBootloader, TAB_BOOT, sizeof(TabellaBootloader));       
        TabellaBootloader[IBUFF_FW_REGISTER] = FW_NORMAL_MODE;
        HEFLASH_writeBlock(TAB_BOOT, (void*)TabellaBootloader, sizeof(TabellaBootloader));
        //--------------------------------------------------------------
        
        //--------------------------------------------------------------
        // Upgrade tabella Logica
        //--------------------------------------------------------------
        HEFLASH_readBlock((void*)TabellaLogica, TAB_LOGICA, sizeof(TabellaLogica));
 
        TabellaLogica[IND_TIPO_APPARECCHIO_LG] = TabellaLogicaDEFAULT[IND_TIPO_APPARECCHIO_LG];
        TabellaLogica[IND_IDSW_VER_LOW] = TabellaLogicaDEFAULT[IND_IDSW_VER_LOW];
        TabellaLogica[IND_IDSW_REL_IDSW_VER_HI] = TabellaLogicaDEFAULT[IND_IDSW_REL_IDSW_VER_HI];
        TabellaLogica[IND_BUILD_LOW] = TabellaLogicaDEFAULT[IND_BUILD_LOW];
        TabellaLogica[IND_BUILD_HIGH] = TabellaLogicaDEFAULT[IND_BUILD_HIGH];

        HEFLASH_writeBlock(TAB_LOGICA, (void*)TabellaLogica, sizeof(TabellaLogica));
    }
}


