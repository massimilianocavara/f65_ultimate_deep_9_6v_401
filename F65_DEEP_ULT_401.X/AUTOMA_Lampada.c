// AUTOMA_Lampada.c

#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_DriverOptiCom.h"
#include "AUTOMA_AccenditoreSE.h"
#include "AUTOMA_Visualizzatore.h"
#include "AUTOMA_Charger.h"
#include "AUTOMA_Ingressi.h"
#include "MODULO_Norma.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "AUTOMA_AccenditoreSA.h"
#include "AUTOMA_DriverLogica.h"
#include "mcc_generated_files/pin_manager.h"
#include "AUTOMA_Taratura.h"

tFlagLampada uFlagLampada;
tByteErrori uByteErrori;
uint8_t cStatoLampada;
uint16_t iTimeoutAutomaLampada;
tFlagFissiLampada uFlagFissiLampada; //flag che hanno durata permanente, devono essere settati o resettati direttamente
tFlagInputVisualizzatore uFlagInputVisualizzatore;
uint8_t cTimerEmergenza;
uint8_t cTimerArancione;
uint8_t cFaseLampeggio;
uint8_t cTimerRestMode;

//variabili AT e LOGICA:
extern tTimer sTimerCadenzaFunzionale; //corrispondente al LOGICA TimerFunzionale
extern tTimer sTimerCadenzaAutonomia; //corrispondente al LOGICA TimerAutonomia
extern tTimer sTimerDurataTest;
extern tTimer sTimerDelayTest;

//variabili OPTICOM:
extern union tFlagLogica uFlagLogica;
extern uint8_t cBufferInterfacciaLogica;
extern union tFlagDriverLogica uFlagDriverLogica;

#ifdef DEBUG_LED_SIGNAL
    tByteDebug uDebug;
#endif

extern uint16_t iSetPoint;
extern uint16_t iDutyCompletoSE;
extern tFlagOptiCom uFlagDriverOptiCom;
extern uint16_t iTimerAccensione;
extern misura mIled;
extern misura mVled;
extern misura mVbatt;
extern filtro fLedStaccati;
extern filtro fBatteriaInCorto;
extern tFiltrato uIngressiFilt;
extern tFlagAccenditoreSE uFlagAccenditoreSE;
extern union tFlagAccenditoreSA uFlagAccenditoreSA;
extern tFlagTimer uFlagTimer;
extern tFlagCharger uFlagCharger;
extern uint8_t cTimer10msec;
extern uint8_t cTimer250msec;
extern bit bTime20msec;

extern uint8_t cStatoNorma;
//OPTICOM
extern FlagProtOptiCom_t uFlagProtOptiCom;
extern uint16_t iTimerConfigAttiva;

//LOGICA
extern uint8_t TabellaLogica[32];

//NORMA
extern union tFlagNormaFissi uFlagNormaFissi;
extern union tFlagNorma uFlagNorma;

//per SLEEP
//extern uint8_t cStatoRicevitoreCBL;
extern uint8_t cStatoCharger;
extern uint8_t cStatoSA;
extern uint8_t cStatoDriverLogica;

extern tFlagTaratura uFlagTaratura;

extern void fInitRestMode(void);
extern void fInitRegistri(void);
extern void fInitPorte(void);
extern void fInitAutomaVisualizzatore(void);
extern void fInitFiltri(filtro * FiltroDaInit, uint8_t valore);
extern void fInitAutomaCharger(void);
extern void fAggiornaDuty(void);
extern unsigned int Campiona(unsigned char cChannel);
extern void fSpegniPwmSE(void);
extern void fSpegniSA(void);
extern void fInitAutomaProtOptiComLite(void);
extern void fInitAutomaDriverOptiComRx(void);
extern void fInitDaSleep(void);
extern void fAggiornaTimer(void);
extern void fSpegniSlowFastCharge(void);
extern void fAccendiLedRosso(void);
extern void fCancellaErrori(uint8_t mod);
extern unsigned char fDecrementa(tTimer * timer);

extern void fAccendiSA(void);
extern void fImponiPotenzaInfinity(void);
extern void fImponiPotenza(void);

//*******************************************************
// void fImponiDurataTestAuto(void)
// 
// scopo: impone la durata dei test
//
//*******************************************************
void fImponiDurataTestAuto(void) {

    if (!uFlagFissiLampada.iBit.SetDurataTest) {
        // 0 -> sempre 1h
        sTimerDurataTest.cByte.High = T_1H_AUT_H;
        sTimerDurataTest.cByte.Medium = T_1H_AUT_M;
        sTimerDurataTest.cByte.Low = T_1H_AUT_L;                         
    } else {
        if (uFlagFissiLampada.iBit.Potenza8H) {
            sTimerDurataTest.cByte.High = T_8H_AUT_H;
            sTimerDurataTest.cByte.Medium = T_8H_AUT_M;
            sTimerDurataTest.cByte.Low = T_8H_AUT_L;       
        } else {
            if (uFlagFissiLampada.iBit.Potenza3H) {
                sTimerDurataTest.cByte.High = T_3H_AUT_H;
                sTimerDurataTest.cByte.Medium = T_3H_AUT_M;
                sTimerDurataTest.cByte.Low = T_3H_AUT_L;  
            } else {
                if (uFlagFissiLampada.iBit.Potenza2H) {
                    sTimerDurataTest.cByte.High = T_2H_AUT_H;
                    sTimerDurataTest.cByte.Medium = T_2H_AUT_M;
                    sTimerDurataTest.cByte.Low = T_2H_AUT_L;  
                } else {
                    if (uFlagFissiLampada.iBit.Potenza1H5) {
                        sTimerDurataTest.cByte.High = T_1H5_AUT_H;
                        sTimerDurataTest.cByte.Medium = T_1H5_AUT_M;
                        sTimerDurataTest.cByte.Low = T_1H5_AUT_L;  
                    } else {//altrimenti default ad 1h
                        sTimerDurataTest.cByte.High = T_1H_AUT_H;
                        sTimerDurataTest.cByte.Medium = T_1H_AUT_M;
                        sTimerDurataTest.cByte.Low = T_1H_AUT_L;  
                    }
                }
            }
        }             
    }
}

//*******************************************************
// void fInitAutomaLampada(void)
// 
// scopo: inizializza l'automa Lampada
//
//*******************************************************
void fInitAutomaLampada(void) {
    cStatoLampada = ST_LAMP_INIT;
    uFlagLampada.iReg = 0;
    iTimeoutAutomaLampada = TIME_DUE_SECONDI;
}

//*******************************************************
// void fTransToEmergenza(void)
//
// scopo: per il passaggio da Normale ad Emergenza
//
//*******************************************************
void fTransToEmergenza(void) {
    uIngressiFilt.iBit.flagBatteriaInCorto = 0;
    uFlagCharger.cBit.flagBatteriaStaccata = 0;
    //resettiamo questa imposizione remota
    uFlagFissiLampada.iBit.ErroreConfigurazione = 0;
    
    iTimeoutAutomaLampada = TIME_UN_SECONDO; //mascheratura
    cTimerEmergenza = 0;
    cStatoLampada = ST_EMERGENZA_SE;
}


//*******************************************************
// void fTransToSaPsOff(void)
//
//*******************************************************
void fTransToSaPsOff(void) {
    if (uFlagFissiLampada.iBit.FunzioneSE) {
        cStatoLampada = ST_SA_OFF_REMOTO;
    } else {
        cStatoLampada = ST_SA_ON;
    }
}

//*******************************************************
// void fCheckAperturaOpti(void)
//
//
//*******************************************************
void fCheckAperturaOpti(void) {
    if (cTimerEmergenza < TIME_TRE_SECONDI) {
        iTimerConfigAttiva = DUE_ORE; //OpticomLite attivo completamente per 2h
#ifdef NO_OPTICOM_COMPLETO
        //nessun lampeggio
        cTimerArancione = 0;
#else
        cTimerArancione = 60; // 1 minuto di led arancione
#endif
    }
}
    
//*******************************************************
// void fExitFromTest(void)
//
// scopo: decide dove andare a seconda della presenza o meno della rete
//
//*******************************************************
void fExitFromTest(void) {
    uFlagLampada.iBit.flagTestInCorso = 0; //per forzare l'Accenditore a ricaricare la potenza in Emergenza
    uFlagFissiLampada.iBit.RicaricaIncompleta = 0; //resetto per sicurezza
    if (!uIngressiFilt.iBit.flagRetePresenteFilt) {
        if (0 != (TabellaLogica[IND_ACTION_REGISTER] & 0b00000100)) {
            cStatoLampada = ST_EMERGENZA_DISABILITATA;
        } else {
            fImponiPotenza();
        cStatoLampada = ST_EMERGENZA_SE;
        }
    } else {
            fTransToSaPsOff();
    }
}

//*******************************************************
// void fTransToSleep(void)
// 
//*******************************************************
void fTransToSleep(void) {
    cStatoLampada = ST_SLEEP;
}

//*******************************************************
// void fTransToPreSleep(void)
// 
//*******************************************************
void fTransToPreSleep(void) {
#ifdef NO_CBL
    cStatoLampada = ST_SLEEP; //la PRATICA non � CABLECOM quindi va subito in SLEEP
#else
    cStatoLampada = ST_PRE_SLEEP;
    iTimeoutAutomaLampada = TIME_PRE_SLEEP; //15 minuti
#endif
}

//*******************************************************
// void fImposizioneFlagVisualizzatore(void)
// 
// descrizione: set dei flag corretti di Visualizzatore
//
//*******************************************************
void fImposizioneFlagVisualizzatore(void) {

    if (uFlagFissiLampada.iBit.FunzioneTR) {
        uFlagInputVisualizzatore.iBit.flagVisVerdeFisso = 1;
        if (uFlagCharger.cBit.flagBatteriaStaccata) {
            uFlagInputVisualizzatore.iBit.flagVisRossoFisso = 1;
        } 
    } else {
        if (uFlagFissiLampada.iBit.ErroreConfigurazione) {
            uFlagInputVisualizzatore.iBit.flagVisArancioSempre = 1;
        } else {
            if ((uFlagLampada.iBit.flagTestAutoInCorso) || (uFlagNorma.cBit.TestAutonomiaInCorso)) {
                uFlagInputVisualizzatore.iBit.flagVisVerdeFisso = 1;
            } else {
                if ((uFlagLampada.iBit.flagTestFunzInCorso) || (uFlagNorma.cBit.TestFunzionaleInCorso)) {
                    uFlagInputVisualizzatore.iBit.flagVisVerde3blink = 1;
                } else {
                    if ((uFlagLampada.iBit.flagSEAccesa) || (uFlagLampada.iBit.flagEmergenzaDisabilitata)) {
                        //Emergenza
                        uFlagInputVisualizzatore.iBit.flagVisLedSpenti = 1;
                    } else {
                        //VERDE
                        if (uFlagCharger.cBit.flagRicaricaInCorso) {
                            uFlagInputVisualizzatore.iBit.flagVisVerde3blink = 1;
                        } else {
                            if (uFlagCharger.cBit.flagFineCarica) {
                                uFlagInputVisualizzatore.iBit.flagVisVerdeFisso = 1;
                            }
                        }
                        //ROSSO
                        //2b-errori:
                        // 1)batteria staccata
                        // 2)errore autonomia
                        // 3)errore led OR errore led runtime
                        //if (uFlagCharger.cBit.flagBatteriaStaccata) {
                        if (uFlagCharger.cBit.flagBatteriaStaccata) {
                            uFlagInputVisualizzatore.iBit.flagVisRosso3blink = 1; //ALTA priorit�
                        } else {
                            if ((uByteErrori.cBit.flagEAuto) || (uByteErrori.cBit.flagEFunz)) {
                                uFlagInputVisualizzatore.iBit.flagVisRosso1blink = 1;
                            } else {
                                if ((uByteErrori.cBit.flagELed) || (uByteErrori.cBit.flagELedSA)) {
                                    uFlagInputVisualizzatore.iBit.flagVisRosso2blink = 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
//*******************************************************
// void fResetErroriRuntime(void)
// 
// descrizione: side-effect sugli errori dei led
//
//*******************************************************
void fResetErroriRuntime(void) {
    uByteErrori.cBit.flagELedSA = 0;
//    uIngressiFilt.iBit.flagLedCortoFilt = 0;
    uIngressiFilt.iBit.flagLedStaccatiFilt = 0;
    uFlagAccenditoreSE.cBit.flagLedInCorto = 0;
    uFlagAccenditoreSE.cBit.flagLedAperti = 0;
}

//*******************************************************
// bool fControlloTestManualiOpticom(void)
//
// descrizione: verifica dei flag di stato
//
//*******************************************************
bool fControlloTestManualiOpticom(void) {
    bool bRes = TRUE;

    if (!uFlagNorma.cBit.TestInCorso) {
        //vado avanti solo se non c'� nulla di automatico in corso mentre non controllo eventuali test manuali perch� non invocherei questa routine se fossero in corso...        
        if (uFlagProtOptiCom.iBit.TestAutonomiaDaOpti) {
            //messa in esecuzione di un TEST AUTONOMIA
            fInitAutomaVisualizzatore();
//            cTimerGeneric = TIME_1_MINUTO;
            fCancellaErrori(SOLO_ERRORI_TEST);
            if (!uFlagCharger.cBit.flagFineCarica) {
                uFlagFissiLampada.iBit.RicaricaIncompleta = 1;
            }
            fImponiDurataTestAuto();
            
            uFlagNormaFissi.cBit.FineTest = 0; //deve essere resettato
            uFlagLampada.iBit.flagSEAccesa = 0; //devo spegnere e riaccendere in TEST
            uFlagLampada.iBit.flagTestInCorso = 1; //anticipo per AUTOMA_AccenditoreSE
            uFlagLampada.iBit.flagTestAutoInCorso = 1;
            cStatoLampada = ST_TEST_AUTO;
        } else {
            //Richiesta TEST FUNZIONALE?
            if (uFlagProtOptiCom.iBit.TestFunzionaleDaOpti) {
                //messa in esecuzione di un TEST FUNZIONALE
                //reset errori:
                uByteErrori.cBit.flagELed = 0; //che poi verr� traferito ad ErrorRegister
                uByteErrori.cBit.flagEFunz = 0;
                uIngressiFilt.iBit.flagBatteriaInCorto = 0;
                uFlagCharger.cBit.flagBatteriaStaccata = 0;

                fInitFiltri(&fLedStaccati, 0); //side-effect: per evitare una corsa sui led staccati
                sTimerDurataTest.cByte.High = T_FUNZ_H;
                sTimerDurataTest.cByte.Medium = T_FUNZ_M;
                sTimerDurataTest.cByte.Low = T_FUNZ_L;
                uFlagNormaFissi.cBit.FineTest = 0;
                uFlagLampada.iBit.flagSEAccesa = 0; //devo spegnere e riaccendere in TEST
                uFlagLampada.iBit.flagTestInCorso = 1; //anticipo per AUTOMA_AccenditoreSE
                uFlagLampada.iBit.flagTestFunzInCorso = 1;
                cStatoLampada = ST_TEST_FUNZ;
            } else {
                bRes = FALSE;
            }
        }
    } else {
        //return FALSE;
        bRes = FALSE;
    }
    return bRes;
}

//*******************************************************
// bool fCheckTestManualiLgCabl(void)
//
// descrizione: verifica dei flag di stato
// ritorno: TRUE se c'� un cambio di stato, FALSE altrimenti
//
//  riscritta applicando il principio di un unico ingresso ed
//  un'unica uscita
//*******************************************************
bool fCheckTestManualiLgCabl(void) {
    bool bRes = TRUE;

    if (!uFlagNorma.cBit.TestInCorso) {
        //vado avanti solo se non c'� nulla di automatico in corso mentre non controllo eventuali test manuali perch� non invocherei quesa routine se fossero in corso...
        if (uFlagLogica.iBit.TestAutonomiaDaLogica) {
            //messa in esecuzione di un TEST AUTONOMIA
            fInitAutomaVisualizzatore();
//            cTimerGeneric = TIME_1_MINUTO;
            fCancellaErrori(SOLO_ERRORI_TEST);
            if (!uFlagCharger.cBit.flagFineCarica) {
                uFlagFissiLampada.iBit.RicaricaIncompleta = 1;
            }
            
            if (0xFE != cBufferInterfacciaLogica) {
                //da valore arrivato con cmd Logica
                sTimerDurataTest.cByte.High = 0;
                sTimerDurataTest.cByte.Medium = cBufferInterfacciaLogica;
                sTimerDurataTest.cByte.Low = 0;
            } else {
                fImponiDurataTestAuto();
            }
            uFlagNormaFissi.cBit.FineTest = 0; //deve essere resettato
            uFlagLampada.iBit.flagSEAccesa = 0; //devo spegnere e riaccendere in TEST
            cStatoLampada = ST_TEST_AUTO;
            //return TRUE;
        } else {
            //Richiesta TEST FUNZIONALE?
            if (uFlagLogica.iBit.TestFunzionaleDaLogica) {
                //messa in esecuzione di un TEST FUNZIONALE
                //reset errori:
                uByteErrori.cBit.flagELed = 0; //che poi verr� traferito ad ErrorRegister
                uByteErrori.cBit.flagEFunz = 0;
                uIngressiFilt.iBit.flagBatteriaInCorto = 0;
                uFlagCharger.cBit.flagBatteriaStaccata = 0;

                fInitFiltri(&fLedStaccati, 0); //side-effect: per evitare una corsa sui led staccati
                sTimerDurataTest.cByte.High = T_FUNZ_H;
                sTimerDurataTest.cByte.Medium = T_FUNZ_M;
                sTimerDurataTest.cByte.Low = T_FUNZ_L;
                uFlagNormaFissi.cBit.FineTest = 0;
                uFlagLampada.iBit.flagSEAccesa = 0; //devo spegnere e riaccendere in TEST
                cStatoLampada = ST_TEST_FUNZ;
                //return TRUE;
            } else {
                if (uFlagLogica.iBit.AccIncondizionataDaLogica) {
                    cStatoLampada = ST_ACCENSIONE_INCONDIZIONATA;                    
                } else {
                    bRes = FALSE;
                }
            }
        }
    }
    return bRes;
}

//*******************************************************
// void fControlliInEmergenza(void)
//
//*******************************************************
void fControlliInEmergenza(void) {
    
    if ((uFlagTimer.iBit.flag250msec) && (cTimerEmergenza < 255)) {
        cTimerEmergenza++;
    }
    if (uIngressiFilt.iBit.flagRetePresenteFilt) {
        if (uFlagFissiLampada.iBit.RestModePrenotato) {
            uFlagFissiLampada.iBit.RestModePrenotato = 0; //resetto
            iTimeoutAutomaLampada = TIME_DIECI_MINUTI;
            cStatoLampada = ST_REST_MODE_ATTIVO;
        } else {
            fCheckAperturaOpti();
                fTransToSaPsOff();
        }
    } else {
        if ((uFlagProtOptiCom.iBit.RestModedaOpti) || (uFlagLogica.iBit.SleepDaLogica) || (uFlagDriverLogica.cBit.RestModeOFFDaPenna)) {
            uFlagLampada.iBit.flagSEAccesa = 0;
            fTransToSleep();
        } else {
            if (uFlagLogica.iBit.SleepDaCablecom) {
                uFlagFissiLampada.iBit.RestModePrenotato = 1;
            } else {
                if (!fCheckTestManualiLgCabl()) {
                    if (!fControlloTestManualiOpticom()) {
//                        if ((uFlagLogica.iBit.DisabilitaEmergenzaDaLogica) || (uCmdCablecom.iBit.DisabilitaEmergenzaDaCabl)) {
                        if (uFlagLogica.iBit.DisabilitaEmergenza) {
                            uFlagLampada.iBit.flagSEAccesa = 0;
                            cStatoLampada = ST_EMERGENZA_DISABILITATA;                        
                        } else {
                            if (uFlagFissiLampada.iBit.LampeggiaLed) {
                                cFaseLampeggio = 0;
                                cStatoLampada = ST_LAMPEGGIO;
                            }
                            if (0 == iTimeoutAutomaLampada) {
                                //andiamo in sleep solo per batteria in minima
                                if (uIngressiFilt.iBit.flagBatteriaMinimaBassa) {
                                    //se s�,
                                    uFlagLampada.iBit.flagSEAccesa = 0;
                                    fTransToPreSleep();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
//  AUTOMA_Lampada
//------------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
void AUTOMA_Lampada(void) {
    
    uFlagInputVisualizzatore.iReg = 0; //viene resettato qui e non in AUTOMA_Visualizzatore perch� � qui che si decide il tipo di lampeggio
    
    uFlagLampada.iReg = 0;

    if (uFlagTimer.iBit.flag20msec) {
        if (0 != cTimerRestMode) {
            cTimerRestMode--;
        }
    }
    
    if (uFlagTimer.iBit.flag250msec) {
        if (0 != iTimeoutAutomaLampada) {
            iTimeoutAutomaLampada--;
        }
    }
    
    if (uFlagTimer.iBit.flag1sec) {
//        if (0 != cTimerGeneric) {
//            cTimerGeneric--;
//        }
        if (0 != cTimerArancione) {
            cTimerArancione--;            
        }
    }

    if (0 != cTimerArancione) {
        uFlagInputVisualizzatore.iBit.flagVisArancioBlinkContinuo = 1;
    }
    
    switch (cStatoLampada) {
      
        //************************************************************************
        // Stato di attesa di 30 minuti dopo aver trovato un errore led
        //************************************************************************
        case ST_ERRORE_LED_SA:
        {
            uByteErrori.cBit.flagELedSA = 1;
            uFlagLampada.iReg = FLAG_NORMALE_SA_SPENTA;

            fImposizioneFlagVisualizzatore();

            if (!fCheckTestManualiLgCabl()) {
                if (!fControlloTestManualiOpticom()) {
                    //verifica della presenza rete senza aspettare il timer di mascheratura
                    if (!uIngressiFilt.iBit.flagRetePresenteFilt) {
                        //se s�,
                        fTransToEmergenza();
                    } else {
                        if (!uIngressiFilt.iBit.flagReteSAPresente) {
                            cStatoLampada = ST_SA_OFF_RETE_SA;
                        }
                    }
                }
            }
            if (0 == iTimeoutAutomaLampada) {
                //riproviamo da
                fResetErroriRuntime();
                cStatoLampada = ST_SA_OFF_RETE_SA;
            }
            break;
        }
        
        //************************************************************************
        // Stato di spegnimento SA per RETE_SA
        //
        //
        //************************************************************************
        case ST_SA_OFF_RETE_SA:
        {
            uFlagLampada.iReg = FLAG_SA_OFF_RETE_SA;
            uByteErrori.cBit.flagELedSA = 0;
            
            fImposizioneFlagVisualizzatore();
            
            if (!uIngressiFilt.iBit.flagRetePresenteFilt) {
                //il controllo pi� prioritario � quello della presenza rete
                fTransToEmergenza();
            } else {
                if (!fCheckTestManualiLgCabl()) {
                    if (!fControlloTestManualiOpticom()) {
                        //se il controllo di eventuali test da esito negativo, controlliamo l'SA
                        if (uIngressiFilt.iBit.flagReteSAPresente) {
                            cStatoLampada = ST_SA_ON;
                        } else {
                            if (uFlagFissiLampada.iBit.FunzioneSE) {
                                cStatoLampada = ST_SA_OFF_REMOTO;
                            }
                        }
                    }
                }
            }
            break;
        }

        //************************************************************************
        // Stato di spegnimento SA da OPTICOM o LOGICA
        //
        // questo stato va preservato nel caso di spegnimento della lampada,
        // quindi in quel frangente va effettuato un qualche side-effect...
        //************************************************************************
        case ST_SA_OFF_REMOTO:
        {
            uFlagLampada.iReg = FLAG_SA_OFF_REMOTO;
            uByteErrori.cBit.flagELedSA = 0;

            fImposizioneFlagVisualizzatore();
     
            if (!uIngressiFilt.iBit.flagRetePresenteFilt) {//se s�,
                fTransToEmergenza();
            } else {
                if (!fCheckTestManualiLgCabl()) {
                    if (!fControlloTestManualiOpticom()) {
                        if (!uFlagFissiLampada.iBit.ErroreConfigurazione) {
                            //se non c'� errore di configurazione, posso tornare in SA
                            fTransToSaPsOff();
                        }
                    }
                }
            }
            break;
        } 

        //************************************************************************
        // Stato di SA acceso
        //
        //
        //************************************************************************
        case ST_SA_ON:
        {
            uFlagLampada.iReg = FLAG_SA_ON;
            uByteErrori.cBit.flagELedSA = 0;

            fImposizioneFlagVisualizzatore();

            if (uFlagFissiLampada.iBit.ErroreConfigurazione) {
                cStatoLampada = ST_SA_OFF_REMOTO;
            } else {
                if (!uIngressiFilt.iBit.flagRetePresenteFilt) {//se s�,
                    fTransToEmergenza();
                } else {
                    if (!fCheckTestManualiLgCabl()) {
                        if (!fControlloTestManualiOpticom()) {
                            if (!uIngressiFilt.iBit.flagReteSAPresente) {
                                //se manca la RETE_SA vado in ST_SA_OFF_RETE_SA...
                                cStatoLampada = ST_SA_OFF_RETE_SA;
                            } else {
                                if (uFlagFissiLampada.iBit.FunzioneSE) {
                                    cStatoLampada = ST_SA_OFF_REMOTO;
                                } else {
                                    if (uFlagAccenditoreSA.cBit.StripLedAperta) {
                                        uByteErrori.cBit.flagELedSA = 1;
                                        uFlagLampada.iBit.flagSAAccesa = 0; //per evitare una riaccensione in AUTOMA_AccenditoreSA
                                        iTimeoutAutomaLampada = TIME_TRENTA_MINUTI;
                                        cStatoLampada = ST_ERRORE_LED_SA;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            break;
        } 

        //*******************************************************************************
        // Stato di Emergenza
        //*******************************************************************************
        case ST_EMERGENZA_SE:
        {
            uFlagLampada.iReg = FLAG_EMERGENZA;            
            fImposizioneFlagVisualizzatore();            
            fControlliInEmergenza();
            break;
        } 

        //************************************************************************
        // Stato di attesa dello Sleep a tempo
        //
        //************************************************************************
        case ST_PRE_SLEEP:
        {            
#ifdef DEB_NO_PRE_SLEEP
            fTransToSleep();
#else
            uFlagLampada.iReg = FLAG_PRE_SLEEP;
            uFlagInputVisualizzatore.iBit.flagVisLedSpenti = 1;
            if (0 == iTimeoutAutomaLampada) {
                fTransToSleep();
            } else {
                if (uIngressiFilt.iBit.flagRetePresenteFilt) {
                    fTransToSaPsOff();
                }
            }
#endif
            break;
        }
                
        //************************************************************************
        // Stato che si pu� anche togliere, ma pu� essere utile per individuare 
        // una lampada
        //
        //************************************************************************
        case ST_EMERGENZA_DISABILITATA:
        {
            uFlagLampada.iReg = FLAG_EMERGENZA_DISABILITATA;
            fImposizioneFlagVisualizzatore();
            
            if (uFlagLogica.iBit.AbilitaEmergenza) {
                fTransToEmergenza();
            } else {
                fControlliInEmergenza();
            }
            break;
        }

        //*******************************************************************************
        // Stato di Accensione Incondizionata determinata da un comando Logica
        // deve funzionare come un test a durata infinita e non deve dare errori di batteria
        // ma al pi� di led
        //*******************************************************************************
        case ST_ACCENSIONE_INCONDIZIONATA:
        {
            uFlagLampada.iReg = FLAG_ACCENSIONE_INCONDIZIONATA;

            if ((uFlagProtOptiCom.iBit.StopTest) || (uFlagLogica.iBit.StopTestDaLogica) || (uIngressiFilt.iBit.flagBatteriaMinimaBassa)) {
                //arriviamo da LED ACCESO ed andiamo in SA accesa, che poi, nel caso, si spegne
                fExitFromTest();
            } else {
                if ((uFlagAccenditoreSE.cBit.flagLedInCorto) || (uFlagAccenditoreSE.cBit.flagLedAperti)) {//se s�,
                    uByteErrori.cBit.flagELed = 1;
                    fExitFromTest();
                }
            }
            break;
        }


        //************************************************************************
        // Stato di basso assorbimento
        //
        //************************************************************************
        case ST_SLEEP:
        {
            uFlagLampada.iReg = FLAG_SLEEP;
            uFlagInputVisualizzatore.iBit.flagVisLedSpenti = 1; //per evitare l'accensione del led di segnalazione
                        
            fResetErroriRuntime();
            fSpegniPwmSE();
            //spengo le periferiche legate all'OPTICOM:
            DAC1CON0bits.DAC1EN = 0; //spengo DAC1
            CM2CON0bits.C2ON = 0; //spengo COMP2
            DAC2CON0bits.DAC2EN = 0; // spengo DAC2
            CM1CON0bits.C1ON = 0; //spengo COMP1
            PIE2bits.C2IE = 0; //disabilito interrupt COMP2
            CM1CON0bits.C1ON = 0; //spengo COMP1
            PIE2bits.C1IE = 0; //disabilito interrupt COMP1
            PIE2bits.TMR6IE = 0; //disabilito interrupt TMR6
            T6CONbits.TMR6ON = 0; //spengo TMR6
            CLC1CONbits.LC1EN = 0; //disabilito il CLC1
            UART_Disable();

            //spengo le periferiche Logica
            T4CONbits.TMR4ON = 0; //spento TMR4
            
            WPUA = 0;
            WPUB = 0; //disabilito i pull-up            
            WPUC = 0;
            WPUE = 0;
            
            //interrupt on change tutti disabilitati
            IOCAP = 0; //resetto tutti i flag
            IOCAN = 0;
            IOCBP = 0;
            IOCBN = 0;
            IOCCP = 0; //tutti disabilitati
            IOCCN = 0; //tutti disabilitati
            
            ANSELA = 0; //tutti gli I/O sono digitali
            ANSELB = 0;
            ANSELC = 0;
            ADCON0 = 0; //spengo l'ADC
            FVRCON = 0; //spengo l'FVR
            //porte
            LATA = LATA_SLEEP;
            TRISA = TRISA_SLEEP;
            LATB = LATB_SLEEP;
            TRISB = TRISB_SLEEP;
            LATC = LATC_SLEEP;
            TRISC = TRISC_SLEEP;

//            //ridondanti, gi� previsto nelle label sopra
//            V_RETE_sync_SetDigitalInput();
//            V_RETE_ANALOG_SetDigitalInput();

            //stati automi particolari
            cStatoCharger = ST_CHARGER_SLEEP;
            cStatoSA = ST_ACC_SA_SLEEP;
            
//            //ridondanti
//            V_OPTI_SetHigh();
//            V_OPTI_SetDigitalOutput();
//            
//            nSleep_SetLow();
//            nSleep_SetDigitalOutput();
            
            T1CON = 0b10001101; //abilito l'oscillatore secondario; prescaler 1:1
            PIE1bits.TMR1IE = 1;
//            WDTCON = 0b00011001; //4sec di timeout            
            //preparo lo Sleep
            INTCON = 0b01001000; //GIE = 0; risveglio per IOC
            IOCAF = 0;
            IOCBF = 0;
            IOCCF = 0;
            IOCBPbits.IOCBP2 = 1; //abilito RB2 per il risveglio da RIENTRO RETE (V_RETE_sync)
            IOCCPbits.IOCCP7 = 1; //abilito RC7 per il risveglio da RestMode
            
            TMR0 = 0;
            
            OSCCON = INIT_OSC_8MHZ; //oscillatore interno a 8MHz
//            while(OSCSTATbits.PLLR); //finch� non si spegne il PLL, resto qui

#ifdef NO_SLEEP
            uint8_t Trovato;
            Trovato = FALSE;
            while (!Trovato) {
                if (1 == INTCONbits.IOCIF) {
                    INTCONbits.IOCIF = 0;
                    if (1 == IOCAFbits.IOCAF0) {//Rientro Rete
                        IOCAPbits.IOCAP0 = 0; //disabilito RC0 per il risveglio da RIENTRO RETE (attivo alto)
                        IOCAFbits.IOCAF0 = 0;
                        fInitDaSleep();
                        iTimeoutAutomaLampada = TIME_DUE_SECONDI;
                        cStatoLampada = ST_WAIT_RETE;
                        Trovato = TRUE;
                    }
                } else {
                    if (PIR1bits.TMR1IF) {
                        TMR1 = 32768;
                        PIR1bits.TMR1IF = 0;
                        fAggiornaTimer(); //aggiornamento dei timer!
                    } 
                }
            }
#else
            asm("clrwdt");
            asm("sleep");
                    
            if (1 == INTCONbits.IOCIF) {                                
                
                fAccendiSA();
                INTCONbits.IOCIE = 0; //disabilito l'interrupt IOC
                INTCONbits.IOCIF = 0;
                if (1 == IOCBFbits.IOCBF2) {//Rientro Rete (Vrete_sync)               
                    OSCCON = INIT_OSC_32MHZ; //oscillatore interno a 32MHz (con PLL)
                    while(!OSCSTATbits.PLLR); //attendo finch� non viene acceso effettivamente il PLL
                    
                    IOCBPbits.IOCBP2 = 0; //disabilito RB2 per il risveglio da RIENTRO RETE (attivo alto)
                    IOCBFbits.IOCBF2 = 0;
                    //disabilito anche l'interrupt del RestMode perch� potenzialmente pericoloso dato che non � gestito nelle Routine di Interrupt
                    IOCCPbits.IOCCP7 = 0;
                    IOCCFbits.IOCCF7 = 0;

                    fInitDaSleep();
                    
                    iTimeoutAutomaLampada = TIME_DUE_SECONDI;                  
                    cStatoLampada = ST_WAIT_RETE;
                    uFlagLampada.iBit.flagSAAccesa = 1;
                } else {
                    if (1 == IOCCFbits.IOCCF7) { //restmode on
                    
                        OSCCON = INIT_OSC_32MHZ; //oscillatore interno a 32MHz
                        while(!OSCSTATbits.PLLR); //attendo finch� non viene acceso effettivamente il PLL

                        IOCCPbits.IOCCP7 = 0;
                        IOCCFbits.IOCCF7 = 0;
                        fInitDaSleep();
                        RX_LG_SetDigitalInput();
                        RX_LG_SetPullup();

                        TX_LG_SetHigh();
                        TX_LG_SetDigitalOutput();
                        uIngressiFilt.iBit.flagRM = 1; //per evitare il ritorno immediato in rest mode
                        cStatoDriverLogica = ST_DVRLG_REST_MODE_IDLE;
                        cTimerRestMode = 50; //1 sec
                        cStatoLampada = ST_WAIT_REST_MODE;
                    }
                }
            } else {
                if (PIR1bits.TMR1IF) {
                    
#ifdef DEBUG_RISVEGLIO
                    nBatt2_SetHigh();
                    nBatt2_SetDigitalOutput();
#endif                    
                                        
                    OSCCON = INIT_OSC_8MHZ; //oscillatore interno a 8MHz
                    TMR1 = 32768;
                    PIR1bits.TMR1IF = 0;
                    uFlagLampada.iBit.flagAggiornaTimer = 1; //aggiornamento dei timer!
                } 
//                else {
//                    if (STATUSbits.nTO) {
//                        //si � svegliato da sleep per WDT, allora qualcosa � andato storto, meglio resettare
//                        while(1); //muore qui
//                    }
//                }
            }
#endif           
            break;
        } 

        //************************************************************************
        //
        //************************************************************************
        case ST_LAMP_INIT:
        {
            uFlagLampada.iReg = FLAG_WAIT_RETE;
            if (uFlagTaratura.cBit.TaraturaFinita) {
                iTimeoutAutomaLampada = TIME_DUE_SECONDI;
                cStatoLampada = ST_WAIT_RETE;
            } else {
                if (uFlagTaratura.cBit.LedRosso) {
                    uFlagInputVisualizzatore.iBit.flagVisRossoFisso = 1;
                } else {
                    if (uFlagTaratura.cBit.LedVerdeFisso) {
                        uFlagInputVisualizzatore.iBit.flagVisVerdeFisso = 1;
                    } else {
                        if (uFlagTaratura.cBit.SegnalazioneOff) {
                            uFlagInputVisualizzatore.iBit.flagVisLedSpenti = 1;
                        } else {
                            if (uFlagTaratura.cBit.LedArancio) {
                                uFlagInputVisualizzatore.iBit.flagVisArancioSempre = 1;
                            }
                        }
                    }
                }
            }           
            break;
        }
 
        //************************************************************************
        //
        //************************************************************************
        case ST_WAIT_REST_MODE:
        {
            if (!uIngressiFilt.iBit.flagRM) {
                fTransToSleep();
            } else {
                if (0 == cTimerRestMode) {
                    fTransToEmergenza();
                }
            }
            break;
        }
        
        //************************************************************************
        //Stato di attesa della rete
        //
        // ripartiamo sempre con SA ON per evitare che, nel caso manchi la batteria e si
        // debba accendere l'SA, si verifichi una corsa circuitale con l'ingresso in uno stato
        // di reset perenne
        //************************************************************************
        case ST_WAIT_RETE:
        {
            uFlagLampada.iReg = FLAG_WAIT_RETE;
            uFlagInputVisualizzatore.iBit.flagVisRossoFisso = 1;

#ifdef PARTENZA_IN_EMERGENZA           
            fInitAutomaProtOptiComLite();
            fInitAutomaDriverOptiComRx();
            fResetErroriRuntime();
            //cStatoLampada = ST_SA_ON; //ripartiamo sempre con SA accesa
            cStatoLampada = ST_SA_OFF_RETE_SA;
#else                    

                if (uIngressiFilt.iBit.flagRetePresenteFilt) {
                    fInitAutomaProtOptiComLite(); //attiva anche le due ore di finestra
                    fInitAutomaDriverOptiComRx();
                    fResetErroriRuntime();
                    cStatoLampada = ST_SA_ON; //ripartiamo sempre con SA accesa
                } else {
                    if (0 == iTimeoutAutomaLampada) {
                        cStatoLampada = ST_PRE_SLEEP;
                        iTimeoutAutomaLampada = TIME_PRE_SLEEP; //15 minuti
                    }
            }
#endif
            break;
        } 

        //*******************************************************************************
        // Test funzionale
        //*******************************************************************************
        //*******************************************************************************
        case ST_TEST_FUNZ:
        {
            uFlagLampada.iReg = FLAG_TEST_FUNZ;
            fImposizioneFlagVisualizzatore();
            
#ifdef DEBUG_RITARATURA_OPTI
            if (uFlagDriverOptiCom.cBit.flagFaseStabilizzazione) {
                uFlagInputVisualizzatore.iReg = 0;
                uFlagInputVisualizzatore.iBit.flagVisRossoFisso = 1;
            }
#endif
            
            if ((uFlagProtOptiCom.iBit.StopTest) || (uFlagLogica.iBit.StopTestDaLogica)) {
                fExitFromTest(); //@
            } else {
                //verifica Timeout
                if (uFlagNormaFissi.cBit.FineTest) {
                    uFlagNormaFissi.cBit.FineTest = 0;
                    //timeout e rete presente: esco dal test e torno in NORMALE o EMERGENZA
                    fExitFromTest(); //@
                } else {
                    //controllo di eventuali errori e poi uscire sempre in test
                    //ERRORE BATTERIA � quello prioritario
                    if (uIngressiFilt.iBit.flagBatteriaMinimaBassa)//batt in minima ?
                    { //se s�,
                        if (!uFlagFissiLampada.iBit.FunzioneTR) {
                            //segno errore solo se non � TR
                            uByteErrori.cBit.flagEFunz = 1;
                        }
                        fExitFromTest(); //@
                    } else {
                        if ((uFlagAccenditoreSE.cBit.flagLedInCorto) || (uFlagAccenditoreSE.cBit.flagLedAperti)) {
                            if (!uFlagFissiLampada.iBit.FunzioneTR) {
                                //segno errore solo se non � TR
                                uByteErrori.cBit.flagELed = 1;
                            }
                            uFlagLampada.iBit.flagSEAccesa = 0; //per evitare una riaccensione in AUTOMA_AccenditoreSE
                            fExitFromTest(); //@
                        }
                    }
                }
            }
            break;
        } 

        //*******************************************************************************
        // Test autonomia
        //*******************************************************************************
        case ST_TEST_AUTO:
        {
            uFlagLampada.iReg = FLAG_TEST_AUTO;
            fImposizioneFlagVisualizzatore();

#ifdef DEBUG_RITARATURA_OPTI
            if (uFlagDriverOptiCom.cBit.flagFaseStabilizzazione) {
                uFlagInputVisualizzatore.iReg = 0;
                uFlagInputVisualizzatore.iBit.flagVisRossoFisso = 1;
            }
#endif

            //gestione di TempoResiduoLastTest per LOGICA:
            TabellaLogica[IND_TEMPO_RESIDUO_LAST_TEST] = sTimerDurataTest.cByte.Medium; //ad ogni giro salvo quanto resta del test

            if ((uFlagProtOptiCom.iBit.StopTest) || (uFlagLogica.iBit.StopTestDaLogica)) {
                fExitFromTest();
            } else {
                //verifica Timeout
                if (uFlagNormaFissi.cBit.FineTest) {
                    uFlagNormaFissi.cBit.FineTest = 0;
                    //timeout e rete presente: esco dal test e torno in NORMALE o EMERGENZA
                    uFlagFissiLampada.iBit.TestFunz = 0; //reset eventuale test in attesa perch� non necessario
                    fExitFromTest();
                } else {
                    //controllo di eventuali errori e poi uscire sempre in test
                    //ERRORE BATTERIA � quello prioritario
                    if (uIngressiFilt.iBit.flagBatteriaMinimaBassa)//batt in minima ?
                    { //se s�,
                        uByteErrori.cBit.flagEAuto = 1; //segnalo errore di batt
                        if (uFlagFissiLampada.iBit.RicaricaIncompleta) {
                            uByteErrori.cBit.flagRicaricaIncompleta = 1;
                        }                        
                        fExitFromTest();
                    } else {
                        if ((uFlagAccenditoreSE.cBit.flagLedInCorto) || (uFlagAccenditoreSE.cBit.flagLedAperti)) {
                            uByteErrori.cBit.flagELed = 1;
                            uFlagLampada.iBit.flagSEAccesa = 0; //per evitare una riaccensione in AUTOMA_AccenditoreSE
                            fExitFromTest();
                        }
                    }
                }
            }
            break;
        } 

        //************************************************************************
        default:
        {
            cStatoLampada = ST_SA_OFF_RETE_SA;
            break;
        } 
    }//switch
}
