// AUTOMA_Charger.c
//
//
#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "AUTOMA_Charger.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "MODULO_Norma.h"
#include "AUTOMA_Filtri.h"
#include "mcc_generated_files/adc.h"
#include "mcc_generated_files/pin_manager.h"
#include "AUTOMA_ProtocolloLogica.h"

tFlagCharger uFlagCharger;
uint8_t cStatoCharger;
uint8_t cTimerChargerProt;
uint24_t lTimerRicarica;
uint16_t iTensioneRiprendi;

extern tFlagTimer uFlagTimer;
extern tFiltrato uIngressiFilt;
extern tFlagLampada uFlagLampada;
extern misura mVbatt;
extern FlagProtOptiCom_t uFlagProtOptiCom;
extern tFlagFissiLampada uFlagFissiLampada; //flag che hanno durata permanente, devono essere settati o resettati direttamente
extern union tFlagNorma uFlagNorma;
extern tByteErrori uByteErrori;
extern uint8_t TabellaLogica2[32];

//***********************************************
// void fInitAutomaCharger(void)
//
//***********************************************
void fInitAutomaCharger(void) {
    cStatoCharger = ST_CHARGER_WAIT_RETE;
}

//***********************************************
// void fTransToRicarica(void)
//
//***********************************************
void fTransToRicarica(void) {
    cStatoCharger = ST_CHARGER_RICARICA;
    uFlagCharger.cReg = FLAG_CHARGER_INIT;
    lTimerRicarica = RICARICA_12H;
}

//***********************************************
//***********************************************
//***********************************************
void AUTOMA_Charger(void) {
    uFlagCharger.cReg = 0;

    iTensioneRiprendi = (TabellaLogica2[IND_MANTENIMENTO_H] << 8) + TabellaLogica2[IND_MANTENIMENTO_L];
    
    if (uFlagTimer.iBit.flag250msec) {
        if (0 != cTimerChargerProt) {
            cTimerChargerProt--;
        }
    }

    if (uFlagTimer.iBit.flag1sec) {
        if (0 != lTimerRicarica) {
            lTimerRicarica--;
        }
    }

    switch (cStatoCharger) {
        
        //*********************************************************************
        case ST_CHARGER_SLEEP:
        {
            nEN_CHG_SetLow();
            nEN_CHG_SetDigitalOutput();
            if (uIngressiFilt.iBit.flagRetePresenteFilt) {
                fTransToRicarica();
            }
            break;
        }
        
        //*********************************************************************
        case ST_CHARGER_WAIT_RETE:
        {
            if (uIngressiFilt.iBit.flagRetePresenteFilt) {
                fTransToRicarica();
            }
            break;
        }
        
        //*********************************************************************
        case ST_CHARGER_RICARICA:
        {
            uFlagCharger.cBit.flagRicaricaInCorso = 1;
            
            nEN_CHG_SetLow();
            nEN_CHG_SetDigitalOutput();
            
            if (uFlagNorma.cBit.TestInCorso) {
                cStatoCharger = ST_CHARGER_TEST;
            } else {
                if (!uFlagLampada.iBit.flagChargeRun) {
                    cStatoCharger = ST_CHARGER_SCARICA;
                } else {
                    if ((mVbatt.iMedia > TENS_FINE_CARICA) || (0 == lTimerRicarica)) {
                        cStatoCharger = ST_CHARGER_FINECARICA;
                    } else {
                        if (uIngressiFilt.iBit.flagBatteriaStaccata) {
                            cStatoCharger = ST_CHARGER_BATTERIA_STACCATA;
                        }
                    }
                }
            }
            break;
        }
        
        //*********************************************************************
        case ST_CHARGER_TEST:
        {
            //spengo il caricabatteria
            nEN_CHG_SetHigh();
            nEN_CHG_SetDigitalOutput();
            if (!uFlagNorma.cBit.TestInCorso) {
                lTimerRicarica = RICARICA_12H; //reinizializzo le 12h di ricarica
                cStatoCharger = ST_CHARGER_RICARICA;                
            }
            break;
        }
        
        //*********************************************************************
        case ST_CHARGER_SCARICA:
        {  
            //non spengo il caricabatteria
            nEN_CHG_SetLow();
            nEN_CHG_SetDigitalOutput();
            if (uFlagLampada.iBit.flagChargeRun) {
                lTimerRicarica = RICARICA_12H; //reinizializzo le 12h di ricarica
                cStatoCharger = ST_CHARGER_RICARICA;
            }
            break;
        }
        
        //*********************************************************************
        // Serve per il sistema Cablecom, che comunica solo in mancanza di rete,
        // per cui se non manteniamo l'informazione di batteria a fine carica
        // non daremmo mai queta indicazione
        //*********************************************************************
        case ST_CHARGER_SCARICA_BLOCCATA:
        {
            uFlagCharger.cBit.flagFineCarica = 1; //manteniamo questa informazione
            //abilito caricabatteria
            nEN_CHG_SetLow();
            nEN_CHG_SetDigitalOutput();
            
            if ((uFlagLampada.iBit.flagChargeRun) && (!uFlagNorma.cBit.TestInCorso)) {
                lTimerRicarica = RICARICA_12H; //reinizializzo le 12h di ricarica
                cStatoCharger = ST_CHARGER_RICARICA;
            }
            break;
        }
                
        //*********************************************************************
        case ST_CHARGER_FINECARICA:
        {
            uFlagCharger.cBit.flagFineCarica = 1;
            //disabilito caricabatteria
            nEN_CHG_SetHigh();
            nEN_CHG_SetDigitalOutput();
            //se Caricatore Off, passa a Scarica
            if ((!uFlagLampada.iBit.flagChargeRun) || (uFlagNorma.cBit.TestInCorso)) {
                cStatoCharger = ST_CHARGER_SCARICA_BLOCCATA;
            } else {
                if (uIngressiFilt.iBit.flagBatteriaStaccata) {
                    cStatoCharger = ST_CHARGER_BATTERIA_STACCATA;
                } else {
                    if (mVbatt.iMedia < iTensioneRiprendi) {
                        cStatoCharger = ST_CHARGER_MANTENIMENTO;
                    }
                }
            }            

            break;
        }
        
        //*********************************************************************
        case ST_CHARGER_MANTENIMENTO:
        {
            uFlagCharger.cBit.flagFineCarica = 1;
            //abilito il caricabatteria
            nEN_CHG_SetLow();
            nEN_CHG_SetDigitalOutput();
            
            if ((!uFlagLampada.iBit.flagChargeRun) || (uFlagNorma.cBit.TestInCorso)) {
                cStatoCharger = ST_CHARGER_SCARICA_BLOCCATA;
            } else {
                if (uIngressiFilt.iBit.flagBatteriaStaccata) {
                    cStatoCharger = ST_CHARGER_BATTERIA_STACCATA;
                } else {
                    if (mVbatt.iMedia > TENS_FINE_CARICA) {
                        cStatoCharger = ST_CHARGER_FINECARICA;
                    }
                }
            }
            break;
        }
        //*********************************************************************
        case ST_CHARGER_BATTERIA_STACCATA:
        {
            uFlagCharger.cBit.flagBatteriaStaccata = 1;
            //abilito cmq il caricabatteria
            nEN_CHG_SetLow();
            nEN_CHG_SetDigitalOutput();

            //come esco????????
            //1) la batteria viene ricollegata
            //2) viene tolta la rete e si resetta tutto di conseguenza
//            if (mVbatt.iMedia > TENS_BATTERIA_RICOLLEGATA) {
            
            if (!uIngressiFilt.iBit.flagBatteriaStaccata) {
                lTimerRicarica = RICARICA_12H; //reinizializzo le 12h di ricarica
                cStatoCharger = ST_CHARGER_RICARICA;
            }
            break;
        }
        
        //*********************************************************************
        default:
        {
            cStatoCharger = ST_CHARGER_RICARICA;
            break;
        }
        
        //*********************************************************************
    }//switch
}


