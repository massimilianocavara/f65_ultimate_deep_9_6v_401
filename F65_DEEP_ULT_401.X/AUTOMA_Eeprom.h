/* 
 * File:   AUTOMA_Eeprom.h
 * Author: cavaram
 *
 * Created on 2 agosto 2016, 9.34
 */

#ifndef AUTOMA_EEPROM_H
#define	AUTOMA_EEPROM_H

#define ST_EE_IDLE              0
#define ST_EE_AGGIORNA          1
#define ST_EE_AGGIORNA_RAM      2

#ifdef DEB_EEPROM
    #define TEMPO_AGG_RAM           2
#else
    #define TEMPO_AGG_RAM           255    //circa 4h e mezzo
#endif
//#define TEMPO_AGG_EEPROM        1440   //24h
//#define TEMPO_AGGIORNAMENTO     30   //30sec

uint8_t RegAggiornaEeprom;
uint8_t RegAggiornaParametri;
uint8_t RegAggiornaTaratura;

#define RICHIESTA_AGGIORNAMENTO     0xAA
#define RICHIESTA_PARAMETRI         0x55
#define RICHIESTA_TARATURA          0x95


//--------------------------------------------
//  tFlagEeprom
//--------------------------------------------
typedef union {
    uint8_t cReg;

    struct {
        uint8_t flagAggiornaConfig : 1; //0

    } cBit;
} tFlagEeprom;

//--------------------------------------------
//prototipi funzioni
//--------------------------------------------
void fRichiediAggEE(uint8_t mod);
//valori per mod
#define MOD_DEFAULT         0
#define MOD_PARAMETRO       1
#define MOD_TARATURA        2

#endif	/* AUTOMA_EEPROM_H */

