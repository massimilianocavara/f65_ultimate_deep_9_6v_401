// GestionePorte.c

#include <htc.h>
#include "global.h"
#include "AUTOMA_Ingressi.h"
#include "Hardware.h"
#include "stdbool.h"


/************************/
void fInitRegistri(void) {
    //inizializzazione della Fixed Voltage Reference
    FVRCON = FVR_BASE; //FVR ON; COMP FVR = 2.048V; ADC FVR = 2.048V
  
    OSCCON = INIT_OSC_32MHZ;

    OPTION_REG = INIT_OPTION; //TMR0 con prescaler
    INTCON = 0b11100000; //abilitato solo quello del TMR0
    PIE1bits.TMR1IE = 1; //abilito anche TMR1
    
    T1CON = 0b10001101; //abilito l'oscillatore secondario; prescaler 1:1
    WDTCON = 0b00011001; //circa 4sec di TIMEOUT
    CCPTMRS = CCPTMRS_init; //TMR2 associato ad ogni PWM
}

/************************/
void fInitPorte(void) {
    PIN_MANAGER_Initialize();
}

