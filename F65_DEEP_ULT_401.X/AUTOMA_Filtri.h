#ifndef _FILTRI_H
#define _FILTRI_H


//*****************************************************************************
// COSTANTI
//*****************************************************************************
#define	ST_V_RETE		0
#define	ST_V_BATT       1
#define ST_I_LED        2
#define ST_V_LED_SA     3
#define ST_sel_UV       4
#define ST_JUMPER       5
#define ST_LAMPON       6


//#define V_RETE_ANALOG           9   //AN9
//#define	V_BATT_ANALOG           3   //AN3
//#define	I_LED_ANALOG			2   //AN2
//#define JUMPER_ANALOG           11  //AN11
//#define V_LED_SA_ANALOG         14  //AN14

#define	DIV2ALLAX		3   //2^3=8


#define INIT_FILTRO_SA  4096

#endif