/* 
 * File:   AUTOMA_Taratura.h
 * Author: cavaram
 *
 * Created on 16 maggio 2018, 14.13
 */

#include <xc.h>

#ifndef AUTOMA_TARATURA_H
#define	AUTOMA_TARATURA_H

#define ST_INIT_TARATURA        0
#define ST_PRE_TARATURA         1
#define ST_TARATURA             2
#define ST_TARATURA_OK          3
#define ST_NO_TARATURA_OUT      4
#define ST_TARATURA_TERMINE     5
#define ST_TARATURA_IDLE        6
         
//label tempi
//10msec
#define TEMPO_UNSEC_E_MEZZO     150
#define TEMPO_250MSEC           25
#define TEMPO_DUESEC            200
#define TEMPO_UNSEC             100
//250msec
#define DURATA_TARATURA         12 //3sec


#define TRIS_TARATURA           TRISAbits.TRISA2
#define PORT_TARATURA           PORTAbits.RA2
#define SET_TAR_DIGITAL_MODE    ANSELAbits.ANSA2 = 0

#define VBATT_10_2V_MINIMA              554 //si ottiene con Partitore = 0.1129V e Vref = 2.13V

typedef union {
    uint8_t cReg;

    struct {
        uint8_t TaraturaFinita : 1; //0
        uint8_t SegnalazioneOff : 1; //1
        uint8_t LedRosso : 1; //2
        uint8_t LedVerdeFisso : 1; //3
        uint8_t AggiornaTabEeprom : 1; //4
        uint8_t LedArancio : 1; //5
    } cBit;
} tFlagTaratura;




#endif	/* AUTOMA_TARATURA_H */

