/* 
 * File:   rs232.h
 * Author: cavaram
 *
 * Created on 1 agosto 2016, 12.29
 */

#ifndef RS232_H
#define	RS232_H

//======================
// Definizione costanti
//======================

#define ST_DRVLG_IDLE                   0
#define ST_DRVLG_VERIFICA               1
#define ST_DRVLG_RX                     2
#define ST_DRVLG_TX                     3
#define ST_DRVLG_DISABLE                4
//#define ST_DRVLG_SLEEP                  5
#define ST_DRVLG_ATTESA_BUFFER          6
#define ST_DRVLG_WAIT_PARSE             7
 
#define ST_DVRLG_REST_MODE_IDLE         8
#define ST_DVRLG_REST_MODE_ATTIVO       9



#define SIZE_BUFFER_RS232               73

#ifdef DEB_DIP
    #define PTIM_RESET_RS232            5000    //5sec
#else
    #define PTIM_RESET_RS232            30000   // 30000 * 1 msec = 30 sec
#endif

#define PTIM_TIMEOUT_RX_RS232           3   	// 3 * 1 msec = 3 msec
#define PTIM_END_TX_RS232               2       // 2 * 1 msec = 2 msec
//#define PTIM_FINE_RX_FRAME_RS232      5       // 5 * 1 msec = 5 msec
#define TIMEOUT_PARSE                   10      // 10msec

#define PTIM_DELAY_RESET_DISP           200     // 200 * 1 msec = 200 msec
#define PTIM_ATTESA_BUFFER              100     // 100msec



#define TIME_WRITE_500MSEC              500 //x 1msec
#define TIME_WRITE_200MSEC              200 //x 1msec
#define TIME_WRITE_250MSEC              250 //x 1msec

//#define TIME_VERIFICA                   20  //x 1msec
//#define NUMERO_MINIMO                   10
#define TIME_VERIFICA                   50  //x 1msec
#define NUMERO_MINIMO                   30


//====================
// Dichiarazione tipi
//====================

////union con flag dell'automa
//union tFlagSeriale {
//    uint8_t cReg;
//    
//    struct {
//        uint8_t InVerifica : 1;
//        uint8_t InRx : 1;
//        uint8_t InTx : 1;
//        uint8_t JumperA : 1;
//        uint8_t JumperB : 1;
//    } cBit;
//};

union tRS232 {
    uint8_t cReg;

    struct {
        uint8_t RxInCorso : 1; // 0
        uint8_t TxAnswer : 1; // 1
        uint8_t EnableEndTx : 1; // 2
        uint8_t TimeoutRx : 1; // 3
        uint8_t CmdErrato : 1; // 4 -> Serve sono con i comandi di tipo esteso
        uint8_t NewFrameArrived : 1; // 5
        uint8_t AbilitaResetDisp : 1; // 6
        uint8_t RispondiACK : 1; // 7 -> SPECIALE, usato nelle lampade OPTICOM con modulino
    } cBit;
};

union tFlagDriverLogica {
    uint8_t cReg;

    struct {
        uint8_t ParseNewFrame : 1; //0 -> 
        uint8_t RxAddrBroadcastOrGruppo : 1; //1 -> Frame ricevuto con indirizzamento di tipo broadcast o di gruppo
        uint8_t RipristinaStatoJumpers : 1; //2 -> Nelle vecchie Logica ripristinava la configurazione in base ai jumpers, se i jumpers non sono previsti, viene ripristinato il default
        uint8_t RestModeOFFDaPenna : 1; //3 -> 
//        uint8_t RestModeONDaPenna : 1; //4
    } cBit;
};

union tCmdRS232 {
    uint8_t cReg;

    struct {
        //uint8 StartProcedureUpgFlash:1; // b0
        uint8_t NuovoPacchettoUpgFlash : 1; // b1
        uint8_t TotalChkSumUpgFlash : 1; // b2
        //uint8_t FineUpgFw:1; // b3
        //uint8_t RichiestaSwapFw:1; // b4
        //uint8_t WriteSerialID:1; // b5
        //uint8_t WriteDateProduction:1; // b6
        //uint8_t AbilitaResetDisp : 1; // b7
    } cBit;
};

union tFrameRS232 {
    uint8_t cReg;

    struct {
        uint8_t Length : 7; // b0..b6
        uint8_t DirCommunication : 1; // b7
    } cByte;
};

//--------------------------------------------
//--------------------------------------------
//------------------------------------------------------------------------------------
// Definizioni MACRO per la comunicazione seriale RS232
//------------------------------------------------------------------------------------
#define NotBufferRS232Empty()               (cTestaBufferRS232 != cCodaBufferRS232)
#define PutChInBufferRS232(x)               (cBufferRS232[cTestaBufferRS232++] = x)
#define GetChFromBufferRS232()              (cBufferRS232[cCodaBufferRS232++])
#define ResetBufferRS232()                  {cTestaBufferRS232 = 0; cCodaBufferRS232 = 0;}
//------------------------------------------------------------------------------------

//====================
// Prototipi funzioni
//====================
void fInitAutomaDriverLogica(void);
void AUTOMA_DriverLogica(void);
void EUSART_Transmit_ISR(void);
void EUSART_Receive_ISR(void);
void UART_Disable(void);

#endif	/* RS232_H */

