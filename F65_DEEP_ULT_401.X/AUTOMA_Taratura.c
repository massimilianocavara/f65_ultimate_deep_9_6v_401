//AUTOMA_Taratura

#include "global.h"
#include "Timer.h"
#include "AUTOMA_Taratura.h"
#include "AUTOMA_ProtocolloLogica.h"
#include "AUTOMA_Eeprom.h"


uint8_t cStatoTaratura;
tFlagTaratura uFlagTaratura;
uint8_t cTimerDelay;
uint8_t cTimerTaratura;
uint8_t cCoefficienteTaratura;


//extern tTimer uTimer;
extern tFlagTimer uFlagTimer;
extern misura mVbattRaw;
extern uint8_t TabellaLogica2[32];
extern misura mVbatt;

extern void fInitTimer(void);

//********************************************************
void fInitAutomaTaratura(void) {
    uFlagTaratura.cReg = 0;
    cStatoTaratura = ST_INIT_TARATURA;
    cTimerDelay = TEMPO_UNSEC_E_MEZZO; //ritardo iniziale
}

//********************************************************
void fTransToNoTaratura(void) {
    uFlagTaratura.cBit.SegnalazioneOff = 1;
    cTimerDelay = TEMPO_250MSEC;
    cStatoTaratura = ST_NO_TARATURA_OUT;
}

//********************************************************
void AUTOMA_Taratura(void) {
    uFlagTaratura.cReg = 0; //reset dei flag
    
    if (uFlagTimer.iBit.flag10msec) {
        if (0 != cTimerDelay) {
            cTimerDelay--;
        }
    }
    if (uFlagTimer.iBit.flag250msec) {
        if (0 != cTimerTaratura) {
            cTimerTaratura--;
        }
    }

    switch (cStatoTaratura) {
        //-------------------------------------------------------------------
        // Stato iniziale che verifica il pin di richiesta della taratura
        //-------------------------------------------------------------------
        case ST_INIT_TARATURA:
        {
            uFlagTaratura.cBit.SegnalazioneOff = 1;
            SET_TAR_DIGITAL_MODE;
            TRIS_TARATURA = 1;
            if (0 == cTimerDelay) {
                if (1 == PORT_TARATURA) {
                    cTimerDelay = TEMPO_UNSEC; //aspettiamo 1sec
                    cStatoTaratura = ST_PRE_TARATURA;
                } else {
                    fTransToNoTaratura();
                }
            }
            break;
        }
        //-------------------------------------------------------------------
        // Stato di attesa per verificare il pin di richiesta della taratura
        //-------------------------------------------------------------------
        case ST_PRE_TARATURA:
        {
            SET_TAR_DIGITAL_MODE;
            TRIS_TARATURA = 1;
            uFlagTaratura.cBit.SegnalazioneOff = 1;
            if (1 == PORT_TARATURA) {
                if (0 == cTimerDelay) {
                    uFlagTaratura.cBit.LedArancio = 1;
                    fInitTimer(); //azzero perch� voglio iniziare i campionamenti a partire da adesso
                    cTimerTaratura = DURATA_TARATURA;
                    cStatoTaratura = ST_TARATURA;
                }
            } else {
                fTransToNoTaratura();
           }
            break;
        }

        //-------------------------------------------------------------------
        // Stato di taratura: 250 campionamenti di Vbat e poi calcolo del
        // coefficiente di taratura facendo il rapporto con il valore aspettato
        //-------------------------------------------------------------------
        case ST_TARATURA: //taratura
        {
            SET_TAR_DIGITAL_MODE;
            TRIS_TARATURA = 1;
            if (0 == PORT_TARATURA) { //la taratura termina senza modificare il coefficiente di default
                fTransToNoTaratura();
            } else {
                uFlagTaratura.cBit.LedArancio = 1;
                if (0 == cTimerTaratura) {
                    TabellaLogica2[IND_MANTENIMENTO_H] = mVbatt.iMedia >> 8;
                    TabellaLogica2[IND_MANTENIMENTO_L] = mVbatt.iMedia & 0xFF;
//                    RegAggiornaEeprom = RICHIESTA_AGGIORNAMENTO;
//                    RegAggiornaParametri = 0x00;
//                    RegAggiornaTaratura = RICHIESTA_TARATURA;
                    fRichiediAggEE(MOD_TARATURA);
                    cTimerDelay = TEMPO_DUESEC;
                    cStatoTaratura = ST_TARATURA_OK;
                }
            }
            break;
        }

        //-------------------------------------------------------------------
        // Stato di segnalazione di taratura fatta
        //-------------------------------------------------------------------
        case ST_TARATURA_OK:
        {
            uFlagTaratura.cBit.LedVerdeFisso = 1;
            if (0 == cTimerDelay) {
                cTimerDelay = TEMPO_UNSEC;
                cStatoTaratura = ST_TARATURA_TERMINE;
            }
            break;
        }

        //-------------------------------------------------------------------
        // Stato di uscita iniziale senza taratura
        //-------------------------------------------------------------------
        case ST_NO_TARATURA_OUT:
        {
            uFlagTaratura.cBit.LedRosso = 1;
            if (0 == cTimerDelay) {
                cStatoTaratura = ST_TARATURA_IDLE;               
            }
            break;
        }
        
        //-------------------------------------------------------------------
        // Stato di uscita finale con led arancio
        //-------------------------------------------------------------------
        case ST_TARATURA_TERMINE:
        {
            uFlagTaratura.cBit.SegnalazioneOff = 1;
            if (0 == cTimerDelay) {
                cStatoTaratura = ST_TARATURA_IDLE;
            }
            break;
        }
        //-------------------------------------------------------------------
        // Stato di uscita finale della taratura
        //-------------------------------------------------------------------        
        case ST_TARATURA_IDLE:
        {
            uFlagTaratura.cBit.TaraturaFinita = 1;
            break;
        }
        //-------------------------------------------------------------------        
        default:
        {
            cStatoTaratura = ST_TARATURA_IDLE;
        }
    }
}
        
