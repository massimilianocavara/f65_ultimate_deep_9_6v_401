//**********************************************************************
//  MODULO_Norma.c
//
//  Descrizione:
//  contiene il codice che ottempera alla normativa EN62034
//**********************************************************************

#include <htc.h>
#include "global.h"
#include "Timer.h"
#include "MODULO_Norma.h"
#include "AUTOMA_ProtOptiComLite.h"
#include "AUTOMA_AccenditoreSE.h"
#include "AUTOMA_Charger.h"
#include "AUTOMA_Lampada.h"
#include "AUTOMA_ProtocolloLogica.h"


//VARIABILI legate ai test
tTimer sTimerCadenzaFunzionale; //timer che conta la cadenza dei test funzionali
tTimer sTimerCadenzaAutonomia; //timer che conta la cedenza dei test di autonomia

tTimer sTimerDurataTest; //timer che conta la durata del test
tTimer sTimerDelayTest; //timer che conta un eventuale delay
tTimer sTimerRipetizioneTest; //nel caso manchi la rete durante un test di autonomia, ripetiamo il test dopo 7 giorni

tTimer sTimerRipristino;

tByteErrori uByteErrori; //byte che contiene gli errori rilevati nei test

uint8_t cStatoNorma;
uint8_t cTimerGenericNorma;
union tFlagNorma uFlagNorma; //flag settati
union tFlagNormaFissi uFlagNormaFissi;
uint24_t lTimeoutNorma;

extern tFlagFissiLampada uFlagFissiLampada;
//extern uint8_t TabellaOpti[32];
extern uint8_t TabellaLogica[32];
extern tFlagTimer uFlagTimer;
extern tFiltrato uIngressiFilt;
//extern uint16_t iDurataUltimoTest;
extern tFlagAccenditoreSE uFlagAccenditoreSE;
extern union tCmdLogica uCmdLogica;
extern union tErrorRegister uErrorRegister;
//extern filtro fLedCorto;
extern filtro fLedStaccati;
extern filtro fBatteriaInCorto;
extern tFlagCharger uFlagCharger;
extern tFlagLampada uFlagLampada;
extern union tStatusByte uStatusByte;

extern void fInitAutomaVisualizzatore(void);
extern void fInitFiltri(filtro * FiltroDaInit, uint8_t valore);
extern void fImponiDurataTestAuto(void);

//FUNZIONI

//**********************************************************************
//	void fRicaricaTimerRipristino(void)
//
//**********************************************************************
void fRicaricaTimerRipristino(void) {
    sTimerRipristino.cByte.High = TM_RIPRISTINO_H;
    sTimerRipristino.cByte.Medium = TM_RIPRISTINO_M;
    sTimerRipristino.cByte.Low = TM_RIPRISTINO_L;
}

//**********************************************************************
//	void fGuardiaSuTimer(uint24_t maxvalue)
//
//  Descrizione:
//  controlla che il timer non contenga un valore superiore al max previsto
//**********************************************************************
void fGuardiaSuTimer(tTimer * timer, uint24_t maxvalue) {
    if (timer->slTimer > maxvalue) {
        timer->slTimer = maxvalue;
    }
}

//**********************************************************************
//	void fInitAutomaNorma(void)
//
//  Descrizione:
//  inizializza le variabili dell'AUTOMA
//**********************************************************************
void fInitAutomaNorma(void) {
    sTimerRipristino.slTimer = 0;
    uFlagNormaFissi.cReg = 0;
    cStatoNorma = ST_NORMA_IDLE;
}

//**********************************************************************
//	unsigned char fDecrementa(tTimer * timer)
//
//  Descrizione:
//  se ritorna 1->timer a ZERO
//  se ritorna 0->decremento effettuato
//**********************************************************************
unsigned char fDecrementa(tTimer * timer) {
    if ((!timer->cByte.High) && (!timer->cByte.Medium) && (!timer->cByte.Low)) {
        return 1;
    } else {
        if (0 == timer->cByte.Low) {
            if (0 != timer->cByte.Medium) {
                timer->cByte.Medium--;
            } else {
                timer->cByte.High--;
                timer->cByte.Medium--;
            }
        }
        timer->cByte.Low--;
        if ((0 == timer->cByte.Low) && (0 == timer->cByte.Medium) && (0 == timer->cByte.High)) {
            return 1;
        } else {
            return 0;
        }
    }
}

//**********************************************************************
//	unsigned char fZerotimer(tTimer * timer)
//
//  Descrizione:
//  se ritorna 1->timer a ZERO
//**********************************************************************
uint8_t fZerotimer(tTimer * timer) {
    if ((!timer->cByte.High) && (!timer->cByte.Medium) && (!timer->cByte.Low)) {
        return 1;
    } else {
        return 0;
    }
}

//**********************************************************************
//	void fAggiornaTimerTest(void)
//
//  Descrizione:
//  aggiorna le variabili che fungono da timer interni
//**********************************************************************
void fAggiornaTimerTest(void) {
    
    // GESTIONE DURATA_TEST
    // attenzione che deve essere azzerato se scade il timer di un test
    if (fDecrementa(&sTimerDurataTest)) {
        uFlagNormaFissi.cBit.FineTest = 1;
    }

    //SE la lampada non � di tipo TR && il timer ripristino � a zero OPPURE non � stata programmata, ALLORA entro nel decremento dei timer
    if ((!uFlagFissiLampada.iBit.FunzioneTR) && ((fDecrementa(&sTimerRipristino)) ||  (1 == uStatusByte.cBit.NonProgrammata))) {
        
        // GESTIONE RIPETIZIONE TEST
        fGuardiaSuTimer(&sTimerRipetizioneTest, TIME_MAX_DELAYTEST); //(***)

        if (fDecrementa(&sTimerRipetizioneTest)) {
            uFlagNormaFissi.cBit.RipetiTest = 1;
        }

        //GESTIONE TIMER_DELAY    
        fGuardiaSuTimer(&sTimerDelayTest, TIME_MAX_DELAYTEST); //(***)

        if (fDecrementa(&sTimerDelayTest)) {
            uFlagNormaFissi.cBit.DelayTest = 0;
        }
        
        //GESTIONE TIMER_FUNZIONALE
        fGuardiaSuTimer(&sTimerCadenzaFunzionale, TIME_MAX_CADENZA_FUNZ); //(***)

        if (fDecrementa(&sTimerCadenzaFunzionale)) {
            uFlagNormaFissi.cBit.EseguiTestFunz = 1;
            //ricarica la cadenza:
            sTimerCadenzaFunzionale.cByte.High = CADENZA_FUNZ_H;
            sTimerCadenzaFunzionale.cByte.Medium = CADENZA_FUNZ_M;
            sTimerCadenzaFunzionale.cByte.Low = CADENZA_FUNZ_L;
        }

        //GESTIONE TIMER_AUTONOMIA
        if (fDecrementa(&sTimerCadenzaAutonomia)) {
            uFlagNormaFissi.cBit.EseguiTestAuto = 1; //impongo il test di autonomia
            //ricarica la cadenza:
            sTimerCadenzaAutonomia.cByte.High = CADENZA_AUTO_H;
            sTimerCadenzaAutonomia.cByte.Medium = CADENZA_AUTO_M;
            sTimerCadenzaAutonomia.cByte.Low = CADENZA_AUTO_L;
        }
    }

}

//**********************************************************************
//  void fInitTimerTest(void)
//
//  Descrizione:
//  ricarica i timer che cadenzano i test
//**********************************************************************
void fInitTimerTest(void) {
    sTimerCadenzaFunzionale.cByte.High = CADENZA_FUNZ_H;
    sTimerCadenzaFunzionale.cByte.Medium = CADENZA_FUNZ_M;
    sTimerCadenzaFunzionale.cByte.Low = CADENZA_FUNZ_L;
    sTimerCadenzaAutonomia.cByte.High = CADENZA_AUTO_H;
    sTimerCadenzaAutonomia.cByte.Medium = CADENZA_AUTO_M;
    sTimerCadenzaAutonomia.cByte.Low = CADENZA_AUTO_L;
}

//**********************************************************************
//  void fCancellaErrori(uint8_t mod)
//
//  Descrizione:
//  cancella gli errori presenti
//  se mod == 1, cancella solo gli errori dei test
//  se mod == 0, cancellazione completa
//**********************************************************************
void fCancellaErrori(uint8_t mod) {
    if (1 == mod) {
        uByteErrori.cReg &= CANCELLA_ERR_TEST; //preserva l'errore UV
    } else {
        uByteErrori.cReg = 0; //Azzeramento di tutti gli errori
    }
    uIngressiFilt.iBit.flagBatteriaInCorto = 0;
    uFlagCharger.cBit.flagBatteriaStaccata = 0;
//    uIngressiFilt.iBit.flagLedCortoFilt = 0;
    uIngressiFilt.iBit.flagLedStaccatiFilt = 0;
    //side-effect
//    fInitFiltri(&fLedCorto, 0); // no corto
    fInitFiltri(&fLedStaccati, 0); // no led staccati
    fInitFiltri(&fBatteriaInCorto, 0); //no corto

    uFlagAccenditoreSE.cBit.flagLedInCorto = 0;
    uFlagAccenditoreSE.cBit.flagLedAperti = 0;
    uFlagFissiLampada.iBit.RicaricaIncompleta = 0;
}

//**********************************************************************
//  void fTransToTestAuto(void)
//
//  Descrizione: 
//  gestisce il passaggio allo stato di Test di Autonomia
//**********************************************************************
void fTransToTestAuto(void) {
    //messa in esecuzione di un TEST AUTONOMIA
    fInitAutomaVisualizzatore();

    fCancellaErrori(SOLO_ERRORI_TEST);

    //per mantere il sincronismo della cadenza del test, facciamo partire anche il TIMER di una eventuale ripetizione del test
    sTimerRipetizioneTest.cByte.High = DELAY_TEST_H;
    sTimerRipetizioneTest.cByte.Medium = DELAY_TEST_M;
    sTimerRipetizioneTest.cByte.Low = DELAY_TEST_L;
    uFlagNormaFissi.cBit.RipetiTest = 0;

    fImponiDurataTestAuto(); //carica il timer della durata
    
    uFlagNormaFissi.cBit.FineTest = 0;
    uFlagNorma.cBit.TestInCorso = 1; //anticipo per AUTOMA_AccenditoreSE
    uFlagNorma.cBit.TestAutonomiaInCorso = 1;
    cStatoNorma = ST_NORMA_TEST_AUTO;
}

//**********************************************************************
//  void fTransToTestFunz(void)
//
//  Descrizione: 
//  gestisce il passaggio allo stato di Test Funzionale
//**********************************************************************
void fTransToTestFunz(void) {
    //messa in esecuzione di un TEST FUNZIONALE
    //Reset errori:
    uByteErrori.cBit.flagELed = 0; //che poi verr� traferito ad ErrorRegister
    uByteErrori.cBit.flagEFunz = 0;
    uIngressiFilt.iBit.flagBatteriaInCorto = 0;
    uFlagCharger.cBit.flagBatteriaStaccata = 0;

    fInitFiltri(&fLedStaccati, 0); //side-effect: per evitare una corsa sui led staccati
    sTimerDurataTest.cByte.High = T_FUNZ_H;
    sTimerDurataTest.cByte.Medium = T_FUNZ_M;
    sTimerDurataTest.cByte.Low = T_FUNZ_L;

    sTimerRipetizioneTest.slTimer = 0; //*ripetizione appena termina la ricarica
    
    uFlagNormaFissi.cBit.FineTest = 0;
    uFlagNorma.cBit.TestInCorso = 1; //anticipo per AUTOMA_AccenditoreSE
    uFlagNorma.cBit.TestFunzionaleInCorso = 1;
    //lTimeoutNorma = TIME_MASCH;
    cStatoNorma = ST_NORMA_TEST_FUNZ;
}

//**********************************************************************
//  void fTransToTestDelayed(void)
//
//  Descrizione:
//  gestisce il passaggio allo stato di Test di Autonomia ritardato
//**********************************************************************
void fTransToTestDelayed(void) {
    sTimerDelayTest.cByte.High = DELAY_TEST_H;
    sTimerDelayTest.cByte.Medium = DELAY_TEST_M;
    sTimerDelayTest.cByte.Low = DELAY_TEST_L;
    uFlagNormaFissi.cBit.DelayTest = 1;
    cStatoNorma = ST_NORMA_TEST_DELAYED;
}

//**********************************************************************
// void fTransToWaitFineCarica(void)
//
//  Descrizione:
//  gestisce il passaggio allo stato di attesa della fine della carica 
//  della batteria
//**********************************************************************
void fTransToWaitFineCarica(void) {
    lTimeoutNorma = TIME_24H;
    cStatoNorma = ST_NORMA_ATTESA_CARICA;
}

//**********************************************************************
//**********************************************************************
//
//  void MODULO_Norma(void)
//
//  Descrizione:
//  implemanta l'Automa a Stati Finiti che gestisce i test previsti
//  dalla Norma IEC62034 
//
//**********************************************************************
//**********************************************************************
void MODULO_Norma(void) {

    uFlagNorma.cReg = 0;

    if ((uFlagTimer.iBit.flag1sec) || (uFlagLampada.iBit.flagAggiornaTimer)) {
        fAggiornaTimerTest();
    }
    
    if (uFlagTimer.iBit.flag1sec) {
        //GESTIONE Timer Generico
        if (0 != cTimerGenericNorma) {
            cTimerGenericNorma--;
        }
        //GESTIONE TIMER AUTOMA
        if (0 != lTimeoutNorma) {
            lTimeoutNorma--;
        }
    }


    switch (cStatoNorma) {
        //*******************************************************************************
        // Stato Idle
        //*******************************************************************************
        case ST_NORMA_IDLE:
        {
            if (uFlagNormaFissi.cBit.EseguiTestAuto) {
                 uFlagNormaFissi.cBit.EseguiTestAuto = 0;
                 //if ((!uFlagCharger.cBit.flagFineCarica) && (!uFlagFissiLampada.iBit.DisparinPari)) {
                 if ((!uFlagCharger.cBit.flagFineCarica) || (!uFlagFissiLampada.iBit.DisparinPari)) {
                     //se batteria non carica OPPURE pari -> ritardo di 7gg
                     //PS la batteria non carica potrebbe esserlo perch� � in corso un test manuale
                     //oppure un'emergenza, quindi non mi pongo il problema
                     fTransToTestDelayed();
                 } else {
                     fTransToTestAuto();
                 }
            } else {
                if (uFlagNormaFissi.cBit.EseguiTestFunz) {
                    uFlagNormaFissi.cBit.EseguiTestFunz = 0;
                    fTransToTestFunz();
                }
            }
            break;
        }

        //*******************************************************************************
        // Stato Test Funzionale
        //*******************************************************************************
        case ST_NORMA_TEST_FUNZ:
        {
//            uFlagNorma.cBit.TestInCorso = 1;
            uFlagNorma.cReg = FLAG_NORMA_TEST_FUNZ;
            if (!uIngressiFilt.iBit.flagRetePresenteFilt) {//manca la rete  -> annullo test e vado in Ripetizione Test
                cStatoNorma = ST_NORMA_ATTESA_RIPETIZIONE_FUNZ;
            } else {
                //verifica Timeout
                if (uFlagNormaFissi.cBit.FineTest) {
                    uFlagNormaFissi.cBit.FineTest = 0;
                    uFlagNorma.cBit.TestInCorso = 0;
                    //timeout e rete presente: esco dal test e torno in IDLE
                    cStatoNorma = ST_NORMA_IDLE;
                } else {
                    if (uIngressiFilt.iBit.flagBatteriaMinimaBassa)//batt in minima ?
                    { //se s�,
                        uByteErrori.cBit.flagEFunz = 1;
                        uFlagNorma.cBit.TestInCorso = 0;
                        cStatoNorma = ST_NORMA_IDLE;
                    } else {
                        if ((uFlagAccenditoreSE.cBit.flagLedInCorto) || (uFlagAccenditoreSE.cBit.flagLedAperti)) {
                            uByteErrori.cBit.flagELed = 1;
                            uFlagNorma.cBit.TestInCorso = 0;
                            cStatoNorma = ST_NORMA_IDLE;                            
                        }
                    }
                }
            }
            break;
        }

        //*******************************************************************************
        // Stato Test Autonomia
        //*******************************************************************************
        case ST_NORMA_TEST_AUTO:
        {
            uFlagNorma.cReg = FLAG_NORMA_TEST_AUTO;
            
            //gestione di TempoResiduoLastTest per LOGICA:
            TabellaLogica[IND_TEMPO_RESIDUO_LAST_TEST] = sTimerDurataTest.cByte.Medium; //ad ogni giro salvo quanto resta del test

            if (!uIngressiFilt.iBit.flagRetePresenteFilt) {//manca la rete -> annullo test e vado in Ripetizione Test
                //il timer di ripetizione � gi� attivo
                cStatoNorma = ST_NORMA_ATTESA_RIPETIZIONE_AUTO;
            } else {
                //verifica Timeout
                if (uFlagNormaFissi.cBit.FineTest) {
                    uFlagNormaFissi.cBit.FineTest = 0;
                    //timeout e rete presente: esco dal test e torno in IDLE
                    uFlagNorma.cBit.TestInCorso = 0;
                    uFlagNormaFissi.cBit.EseguiTestFunz = 0; //cancello un eventuale test funzionale in coda perch� inutile 
                    cStatoNorma = ST_NORMA_IDLE;
                } else {
                    if (uIngressiFilt.iBit.flagBatteriaMinimaBassa)//batt in minima ?
                    { //se s�,
                        uByteErrori.cBit.flagEAuto = 1; //segnalo errore di batt
                        uFlagNorma.cBit.TestInCorso = 0;
                        cStatoNorma = ST_NORMA_IDLE;
                    } else {
                        if ((uFlagAccenditoreSE.cBit.flagLedInCorto) || (uFlagAccenditoreSE.cBit.flagLedAperti)) {
                            uByteErrori.cBit.flagELed = 1;
                            uFlagNorma.cBit.TestInCorso = 0;
                            cStatoNorma = ST_NORMA_IDLE;                            
                        }
                    }
                }
            }
            break;
        }

        //*******************************************************************************
        // Stato Test Ritardato
        //*******************************************************************************
        case ST_NORMA_TEST_DELAYED:
        {
            uFlagNorma.cBit.DelayInCorso = 1;
            if (!uFlagNormaFissi.cBit.DelayTest) {
                //� tempo di ripetere il test
                if (uFlagCharger.cBit.flagFineCarica) {
                    fTransToTestAuto();
                } else {
                    //batteria non ancora carica, aspetto che sia carica o al limite 24h
                    fTransToWaitFineCarica();
                }
            }
            break;
        }

        //*******************************************************************************
        // Stato Attesa Ripetizione Test Autonomia
        //
        // non viene considerato un test ritardato perch� � un altro test
        //*******************************************************************************
        case ST_NORMA_ATTESA_RIPETIZIONE_AUTO:
        {           
            uFlagNorma.cBit.WaitRipetizioneTestAuto = 1;
            if (uFlagNormaFissi.cBit.RipetiTest) {
                //� tempo di ripetere il test
                if (uFlagCharger.cBit.flagFineCarica) {
                    fTransToTestAuto();
                } else {
                    //batteria non ancora carica, aspetto che sia carica o al limite 24h
                    fTransToWaitFineCarica();
                }
            }
            break;
        }

        //*******************************************************************************
        // Stato Attesa Ripetizione Test Funzionale
        //
        // viene ripetuto appena la batteria giunge a finecarica,
        // non viene considerato un test ritardato perch� � un altro test
        //*******************************************************************************
        case ST_NORMA_ATTESA_RIPETIZIONE_FUNZ:
        {
            uFlagNorma.cBit.WaitRipetizioneTestFunz = 1;
            if (uFlagCharger.cBit.flagFineCarica) {
                fTransToTestFunz();
            }
            break;
        }

        //*******************************************************************************
        // Stato Attesa Ricarica
        //
        // in questo stato si aspetta che la batteria sia giunta a finecarica oppure che 
        // sia scaduto il timer di 24h;
        // potrebbe essere considerato un test ritardato, ma per semplicit� non lo considero tale
        //*******************************************************************************
        case ST_NORMA_ATTESA_CARICA:
        {
            uFlagNorma.cBit.DelayInCorso = 1;
            uFlagNorma.cBit.WaitFinecarica = 1;
            if ((uFlagCharger.cBit.flagFineCarica) || (0 == lTimeoutNorma)) {
                fTransToTestAuto();
            }
            break;
        }               
    }
}

