/* 
 * File:   AUTOMA_Logica.h
 * Author: cavaram
 *
 * Created on 28 giugno 2016, 10.31
 */

#include "global.h"

#ifndef AUTOMA_LOGICA_H
#define	AUTOMA_LOGICA_H

//-------------------------------------------------------
// Decommentare la tipologia di compilazione da attivare
//-------------------------------------------------------

#define ST_LOGICA_IDLE              0
#define ST_LOGICA_FRAME_STD         1
#define ST_LOGICA_FRAME_EXT         2


#define TM_AGGIORNAMENTO            255

#define SIZE_BUFFER_LOGICA_IN       71
#define SIZE_BUFFER_LOGICA_OUT      13
//***********************************************************************
//***********************************************************************
// TABELLA LOGICA
//***********************************************************************
//***********************************************************************

//------------------------------------------------------------------------------
//Indici TabellaLogica[32]
//------------------------------------------------------------------------------
#define IND_SERIAL_L                    0   //Logica ROM
#define IND_SERIAL_H                    1   //Logica ROM
#define IND_WEEK                        2   //Logica ROM
#define IND_YEAR                        3   //Logica ROM
#define	IND_TIPO_APPARECCHIO_LG         4   //Logica ROM
#define	IND_IDSW_VER_LOW                5   //Logica ROM
#define	IND_IDSW_REL_IDSW_VER_HI        6   //Logica ROM
#define IND_TIPO_BATTERIA               7   //solo Logica ROM
#define IND_PARAMETRI_REG               7   //solo AT
#define IND_TIPO_TUBO                   8   //solo Logica ROM
#define	IND_BUILD_LOW                   9
#define	IND_BUILD_HIGH                  10
#define IND_GRUPPI_7_0                  11
#define IND_GRUPPI_15_8                 12
#define IND_SCENE_1_0                   13
#define IND_SCENE_3_2                   14
#define IND_SCENE_5_4                   15
#define IND_SCENE_7_6                   16
#define IND_SCENE_9_8                   17
#define IND_SCENE_11_10                 18
#define IND_SCENE_13_12                 19
#define IND_SCENE_15_14                 20
#define IND_UVOXY_CONFIG_REG            21
#define IND_ACTION_REGISTER             22
#define IND_ACTION_REGISTER_2           23
#define IND_SA_MODE_REGISTER            24
#define IND_TEMPO_RESIDUO_LAST_TEST     25
#define IND_CODICE_PRODOTTO_FINITO_L    26   //Cablecom
#define IND_CODICE_PRODOTTO_FINITO_H    27   //Cablecom
#define IND_GIORNO_PROD                 28   //Cablecom
#define IND_MESE_PROD                   29   //Cablecom
#define IND_ANNO_PROD                   30   //Cablecom
#define IND_ID_PLANT                    31   //Cablecom
//------------------------------------------------------------------------------
//Valori Default TabellaLogica[32]
//------------------------------------------------------------------------------
#define ID_SERIAL_L                     0x21 //ID SERIALE
#define ID_SERIAL_H                     0x43 //
#define WEEK_PRODUCTION                 0x33
#define YEAR_PRODUCTION                 0x44
#define	TIPO_APPARECCHIO_LG             TIPO_LG // NNORMA
#define	IDSW_VER_LOW                    (ID_SW) % 256
#define	IDSW_REL_IDSW_VER_HI            (ID_SW / 256) + (RELEASE * 16)
#define PARAMETRI_REG                   TIPO_LAMP
#define	BUILD_LOW                       SW_BUILD_BYTE_LOW
#define	BUILD_HIGH                      SW_BUILD_BYTE_HI
#define GRUPPI_7_0                      0x00
#define GRUPPI_15_8                     0x00
#define SCENE_1_0                       0xFF
#define SCENE_3_2                       0xFF
#define SCENE_5_4                       0xFF
#define SCENE_7_6                       0xFF
#define SCENE_9_8                       0xFF
#define SCENE_11_10                     0xFF
#define SCENE_13_12                     0xFF
#define SCENE_15_14                     0xFF
#define UVOXY_CONFIG_REG                0b00011111 //controllo da jumper; profilo 24H; ventola al 100%; ON
#define ACTION_REGISTER                 0b10110000 //non programmata; autonomia in base ai jumper; P/D in base a ID
#define ACTION_REGISTER_2               0b00000010 //ric rapid OFF; 1H; Autonomia sempre 1h; Opticom abilitato
#define SA_MODE_REGISTER                0b10100000 //SA da Centraldimming
#define TEMPO_RESIDUO_LAST_TEST         0x00
#define CODICE_PRODOTTO_FINITO_LOW      0x00 //scritto in finelinea
#define CODICE_PRODOTTO_FINITO_HI       0x00

//------------------------------------------------------------------------------
//Indici TabellaLogica2[32]
//------------------------------------------------------------------------------
#define IND_VITA_TUBO_H                 0
#define IND_VITA_TUBO_M                 1
#define IND_VITA_TUBO_L                 2
#define IND_MANTENIMENTO_L              3
#define IND_MANTENIMENTO_H              4

//------------------------------------------------------------------------------
//Valori Default TabellaLogica2[32]
//------------------------------------------------------------------------------
#define INIT_VITA_TUBO_H                0x00
#define INIT_VITA_TUBO_M                0x00
#define INIT_VITA_TUBO_L                0x00

//10.2V con partitore = 0.1152 e Vref = 2.048V -> 587
#define V_MANTENIMENTO_H              0x02
#define V_MANTENIMENTO_L              0x4B  

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Indici TabellaLogicaROM
//------------------------------------------------------------------------------
#define IND_TIPO_APP_ROM                0
#define IND_IDSW_VER_LOW_ROM            1
#define IND_IDSW_REL_IDSW_VER_HI_ROM    2
#define IND_SERIAL_L_ROM                5
#define IND_SERIAL_H_ROM                6
#define IND_TIPO_BATTERIA_ROM           7
#define IND_TIPO_TUBO_ROM               8
#define IND_WEEK_ROM                    10
#define IND_YEAR_ROM                    11

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//Posizione logica della Tabella LOGICA del sistema
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#define POS_ACTION_REG                  0
#define POS_ERROR_REG                   1
#define POS_TMR_CAD_FUNZ_H              2
#define POS_TMR_CAD_FUNZ_M              3
#define POS_TMR_CAD_FUNZ_L              4
#define POS_TMR_CAD_AUTO_H              5
#define POS_TMR_CAD_AUTO_M              6
#define POS_TMR_CAD_AUTO_L              7
#define POS_GR_7_0                      8
#define POS_GR_15_8                     9
#define POS_TEMPO_RESIDUO_TEST          10
//...
#define POS_SA_MODE_REG                 16
#define POS_SA_STATUS_REG               17
#define POS_ACTION_REG_2                18
//...
#define POS_SCENA_1_0                   21
#define POS_SCENA_3_2                   22
#define POS_SCENA_5_4                   23
#define POS_SCENA_7_6                   24
#define POS_SCENA_9_8                   25
#define POS_SCENA_11_10                 26
#define POS_SCENA_13_12                 27
#define POS_SCENA_15_14                 28
//...

//debug
#define POS_DEB_HI                     30
#define POS_DEB_LO                     31
//debug

#define POS_MANTENIMENTO_L          49
#define POS_MANTENIMENTO_H          50

#ifdef DEBUG_TAR
    #define POS_VBATT_L                     46
    #define POS_VBATT_H                     47
#endif

#define POS_PARAMETRI_REG               48
//#define POS_TMR_VITA_H                  49
//#define POS_TMR_VITA_M                  50
//#define POS_TMR_VITA_L                  51
#define POS_BUILD_L                     52
#define POS_BUILD_H                     53
#define POS_UVOXY_CONFIG_REG            54
#define POS_GIORNO                      55
#define POS_MESE                        56
#define POS_ANNO                        57
#define POS_ID_PLANT                    58
#define POS_CODICE_PRODOTTO_FINITO_L    59
#define POS_CODICE_PRODOTTO_FINITO_H    60
#define POS_ID_LAMP_L                   61
#define POS_ID_LAMP_H                   62
//------------------------------------------------------------------------------


// Parametri possibili del comando COMACK
#define ACK                             0x01
#define NACK                            0x03

// indirizzi BufferLogica, frame standard
// in RX
#define INDEXRX_ID_HI           0
#define INDEXRX_ID_LOW          1
#define INDEXRX_CMD             2
#define INDEXRX_PAR             3
// in TX
#define INDEXTX_ID_LOW          0
#define INDEXTX_ID_HI           1
#define INDEXTX_DATI            2
// ulteriori indirizzi frame esteso
#define INDEXRX_BYTE_CONTROLLO  3
#define INDEXRX_CMD_ESTESO      4

//-------------------------------------------------
// Comandi frame Logica standard
//-------------------------------------------------
#define COMPOL                          0b00000000      // Cmd polling
#define COMIT                           0b00000001      // Cmd accensione/inizio test
#define COMFT                           0b00000010      // Cmd spegnimento/fine test
#define COMRST                          0b00111111      // Cmd reset
#define COMRDE                          0b01000000      // Cmd lettura memoria
#define COMENE                          0b10000000      // Cmd abilitazione scrittura in memoria
#define COMWRE                          0b11000000      // Cmd scrittura in memoria
#define COMLI                           0b00000011      // Cmd impostazione luce / scena
#define COMEXT                          0b00001000      // Cmd frame esteso (0x08)
#define COMACK                          0b00001001      // Cmd ack o nack che il modulino invia lampada per comunicare se � riuscito o meno a completare la risposta verso la centrale (0x09)
#define COMPOL2                         0b00001010      // Cmd polling2 (0x0A)
#define COMACT                          0b00000100      // Cmd attuativo
#define COMPOL3                         0b00001011      // Cmd polling3
//-------------------------------------------------

//-------------------------------------------------
// Comandi frame Logica esteso
//-------------------------------------------------
// Opcodes per la lampada
#define OPCOD_UPG_FLASH_START           0x01
#define OPCOD_UPG_FLASH_WRITE           0x02
#define OPCOD_UPG_FLASH_VER_CHKSUM      0x03
#define OPCOD_UPG_FLASH_END             0x04            // Cmd riconosciuto, ma non utilizzato nel software lampada (serve nel bootloader)
#define OPCOD_RESET_SWAP_FW             0x05            // Cmd riconosciuto, ma non utilizzato nel software lampada (serve nel caso di doppio firmware + bootloader)
#define OPCOD_ANYONE_IN_BOOT            0x06            // Cmd non gestito nel software lampadam ma solo nel bootloader (� un comando speciale broadcast con risposta)
#define OPCOD_GET_SW_INFO               0x0A
#define OPCOD_GET_ALL_LAMPADA_INFO      0x0B            // Opcode utilizzato con il modulino esterno di tipo DALI
#define OPCOD_WRITE_SERIAL_ID           0x10            // Scrittura nuovo Serial ID (2 byte)
#define OPCOD_WRITE_DATE_PRODUCTION     0x11            // Scrittua settimana ed anno di produzione (2 byte)
#define OPCOD_WRITE_DEVICE_TYPE         0x12            // Scrittura tipo dispositivo Opticom (2 byte con 11 bit di dato utile)

// Opcodes per il modulino Logica esterno
#define OPCOD_UPG_FLASH_START_MOD       0x51
#define OPCOD_UPG_FLASH_WRITE_MOD       0x52
#define OPCOD_UPG_FLASH_VER_CHKSUM_MOD  0x53
#define OPCOD_UPG_FLASH_END_MOD         0x54            // Non utilizzato ai fini pratici (serve nel bootloader)
#define OPCOD_RESET_SWAP_FW_MOD         0x55            // Non utilizzato ai fini pratici (serve nel caso di doppio firmware + bootloader)
#define OPCOD_ANYONE_IN_BOOT_MOD        0x56            // Serve solo nel bootloader (� un comando che viene usato broadcast con risposta)
#define OPCOD_GET_SW_INFO_MOD           0x5A
//-------------------------------------------------

//-----------------------------------------------------------------------------------
// Tipologie di scritture Logica che possono essere effettuate con il comando COMENE
//-----------------------------------------------------------------------------------
#define WR_SET                      0
#define WR_OR                       1
#define WR_AND                      2
#define WR_XOR                      3
//-----------------------------------------------------------------------------------

#define SEL_BIT_COMRDE              0b11000000
#define SEL_BIT_TIPO_TABELLA		0b00111111
#define TABELLA_PROTETTA            0b00111111
#define SEL_BIT_COMENE              0b11000000
#define SEL_BIT_COMWRE              0b11000000
#define SEL_BIT_ADDRESS             0b00111111
#define SEL_BIT_TIPO_WRITE          0b00000011
#define SEL_BIT_LAMP_NOT_PROG       0b10000000
#define SEL_BIT_COMACT              0b11111100
#define SEL_SUBCMD_COMACT           0b00000011

#define	SEL_BIT_CMD_GRUPPO          0b11110000
#define	CMD_GRUPPO                  0b11100000
#define	SEL_BIT_GRUPPO              0b00001111

#define SUBCMD_CONFIG_CBL           0
#define SUBCMD_SINCROTEST           1
#define SUBCMD_SPEGNISAN            2
#define SUBCMD_ACCENDISAN           3


//------------------------------------------------------------------------------
// Per le chiamate di funzioni condivise tra LOGICA e CABLECOM
//------------------------------------------------------------------------------
#define PROT_LOGICA         0
#define PROT_CBL            1

//------------------------------------------------------------------------------
// Possibili configurazioni di luminosit� (registro PARAMETRI REG))
//------------------------------------------------------------------------------
#define LUMEN300           0
#define LUMEN500           1
#define LUMEN800           2



#define PTIM_DELAY_RESET_DISP           200     // 200 * 1 msec = 200 msec

//***********************************************************************
//***********************************************************************
//***********************************************************************
//***********************************************************************

union tLampadaAddressBit {
    char cReg;

    struct {
        char NotUsed0 : 1; // b0
        char NotUsed1 : 1; // b1
        char Pari : 1; // b2
        char Dispari : 1; // b3
        char Autonomia1h : 1; // b4
        char Autonomia3h : 1; // b5
        char InErrore : 1; // b6
        char NonProgrammata : 1; // b7
    } cBit;
};

union tStatusByte {
    char cReg;

    struct {
        char ErroriPresenti : 1; // b0
        char TestPendente : 1; // b1
        char TestInCorso : 1; // b2
        char EmergenzaDisabilitata : 1; // b3
        char AssenzaRete : 1; // b4
        char nPariDispari : 1; // b5
        char Autonomia3h : 1; // b6
        char NonProgrammata : 1; // b7
    } cBit;
};

union tStatusByte2 {
    char cReg;

    struct {
        char SetDurataTest : 1; // b0 -> 0 = test di autonomia sempre di 1 ora, 1 = test di autonomia di durata pari al setting di Autonomia
        char DurataAutonomia : 3; // b1..b3 -> 001 = 1h, 010 = 1.5h, 011 = 2h, 100 = 3h, 101 = not used, 110 = 8h
        char ReteSA : 1; // b4 -> 0 = Rete SA assente, 1 = Rete SA presente,
        char EmergenzaLocale : 1; // b5 -> 1 = abilitata, 0 = disabilitata
        char StatoCaricaBatt : 1; // b6 -> 0 = in ricarica, 1 = mantenimento
        char StatoOpticom : 1; // b7 -> 0 = Opticom attivo, 1 = Opticom disabilitato
    } cBit;
};

//union tStatusByte3 {
//    char cReg;
//
//    struct {
//        char StatoUvoxy : 1; // b0
//        char FaseUvoxy : 1; // b1
//        char ModFunz : 2; // b2b3
//        char VelVent : 2; // b4b5
//        char TipoUvoxy : 2; //b6b7 deve essere sempre '10'
//    } cBit;
//};

union tActionRegister {
    char cReg;

    struct {
        char TestPendente : 1; // b0
        char Sleep : 1; // b1
        char DisabilitaEmergenza : 1; // b2
        char PariDispariUserDefined : 1; // b3
        char PariDispariID : 1; // b4
        char AutonomiaUserDefined : 1; // b5
        char nAutonomiaInBaseAiJumper : 1; // b6
        char NonProgrammata : 1; // b7
    } cBit;
};

union tActionRegister2 {
    char cReg;

    struct {
        char Set_Durata_Test : 1; // b0 -> 0 = test di autonomia sempre di 1 ora, 1 = test di autonomia di durata pari al setting di Autonomia
        char DurataAutonomia : 3; // b1..b3 -> 001 = 1h, 010 = 1.5h, 011 = 2h, 100 = 3h, 101 = not used, 110 = 8h
        char NotUsed2 : 1; // b4
        char EmergenzaLocale : 1; // b5 -> 0 = disabilitata, 1 = abilitata
        char NotUsed6 : 1; // b6
        char DisabilitaOpticom : 1; // b7 -> 1 = disabilita
    } cBit;
};

union tErrorRegister {
    char cReg;

    struct {
        char BatteriaRicarica : 1; // b0
        char BatteriaFunzionale : 1; // b1
        char BatteriaAutonomia : 1; // b2
        char RicaricaIncompleta : 1; // b3
        char Led : 1; // b4
        char Hardware : 1; // b5
        char TuboUV : 1; // b6
    } cBit;
};

union tSaModeRegister {
    char cReg;

    struct {
        char NotUsed0 : 1; // b0
        char NotUsed1 : 1; // b1
        char NotUsed2 : 1; // b2
        char NotUsed3 : 1; // b3
        char FunzioneSE : 1; // b4
        char FunzioneSAnPS : 1; // b5
        char AutoCentralDimming : 1; // b6 -> 1 = Autodimming in base ai jumpers, 0 = Centraldimming
        char GestSAPSDaBus : 1; // b7
    } cBit;
};

union tSaStatusRegister {
    char cReg;

    struct {
        char DimmerSA : 4; // b0..b3
        char Free4 : 1; // b4
        char FunzioneSAnPS : 1; // b5
        char AutoCentralDimming : 1; // b6
        char TipoSAnSE : 1; // b7 1=SA 0=SE
    } cBit;
};

//union tParametriFunz {
//    char cReg;
//    
//    struct {
//        char Brightness : 4; //0..3
//        char free4 : 1; //4
//        char free5 : 1; //5
//        char SAnPS : 1; //6
//        char SE : 1; //7
//    } cBit;
//};
union tParametriFunz {
    char cReg;
    
    struct {
        char Brightness : 4; //0..3
        char free4 : 1; //4
        char free5 : 1; //5
        char SAnPS : 1; //6
        char SE : 1; //7
    } cBit;
};

//union tDaliEvents {
//    char cReg;
//
//    struct {
//        char DimmerDaBus : 1; // b0
//    } cBit;
//};

union tScena {
    char cReg;

    struct {
        char ScenaLow : 4; // b0..b3
        char ScenaHi : 4; // b4..b7
    } cBit;
};

union tGruppi {
    unsigned iReg;

    struct {
        char cGr_7_0; // b0..b7
        char cGr_15_8; // b8..b15
    } iByte;
};

union tFlagLogica {
    uint24_t iReg;

    struct {
        uint16_t AbilitaScrittura : 1; //0
        uint16_t ComenePendente : 1; //1
        uint16_t AbilitaResetDisp : 1; //2
        uint16_t TestFunzionaleDaLogica : 1; //3
        uint16_t TestAutonomiaDaLogica : 1; //4
        uint16_t StopTestDaLogica : 1; //5
        uint16_t AccIncondizionataDaLogica : 1; //6 1-eseguo il test di autonomia come da configurazione lampada
        uint16_t DisabilitaEmergenza : 1; //7
        uint16_t AggiornaTabInEeprom : 1; //8
        uint16_t ModConfigDaLogica : 1; //9 1=� stato scritto il registro ActionRegister, ActionRegister2 o SaModeRegister
        uint16_t RispondiACK : 1; //10
        uint16_t AccendiSanificaLg : 1; //11
        uint16_t SpegniSanificaLg : 1; //12
        uint16_t AbilitaEmergenza : 1; //13
        uint16_t SleepDaLogica : 1; //14
        uint16_t TxRisposta : 1; //15
        uint16_t RxAddrBroadcastOrGruppo : 1; //16
        uint16_t SleepDaCablecom : 1; //17
    } iBit;
};

#define MASK_RESET_FLAG_LOGICA  0b0000000000000111  //AbilitaScrittura, ComenePendente e AbilitaResetDisp non vanno resettati perch� servono nei comandi multipli
#define FLAG_LOGICA_INIT        0b0000000000000000

//******************************************************************************
// prototipi funzioni
//******************************************************************************
void fInitAutomaProtocolloLG(void);
void AUTOMA_ProtocolloLogica(void);
uint8_t CmdBroadcastOrGruppoOrId(uint8_t *buffer);
void EseguiScrittura(uint8_t *cRegister, uint8_t value);
uint8_t CmdPerMe(uint8_t selection);
uint8_t LeggiTabellaProtetta(uint8_t index);
uint8_t LeggiTabella(uint8_t indice);
uint8_t ScriviTabella(uint8_t index, uint8_t value, uint8_t chiamante);
void fAggiornaVariabiliLogica(void);

#endif	/* AUTOMA_LOGICA_H */

