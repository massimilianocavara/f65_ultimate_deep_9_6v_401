//********* AUTOMA_Lampada *******

#ifndef	__LAMPADA_H
#define	__LAMPADA_H


// REGISTRO DI STATO

#define	ST_TEST_FUNZ                    1
#define	ST_TEST_AUTO                    2
#define	ST_EMERGENZA_SE                 3
#define	ST_SLEEP                        4
#define	ST_WAIT_RETE                    5
#define	ST_WAIT_REST_MODE               6
#define ST_SPEGNIMENTO                  7
#define ST_EMERGENZA_FORZATA            8
#define ST_ACCENSIONE_INCONDIZIONATA    9
#define	ST_LAMPEGGIO                    10
#define ST_SA_OFF_RETE_SA               11
#define ST_SA_OFF_REMOTO                12
#define ST_SA_ON                        13
#define ST_PS_ON                        14
#define ST_EMERGENZA_DISABILITATA       15
#define ST_REST_MODE_ATTIVO             16
#define ST_PRE_SLEEP                    17
#define ST_ERRORE_LED_SA                18
#define ST_EM_LOC_SA_OFF                19
#define ST_EM_LOC_SA_ON                 20
#define ST_EMERGENZA_SA                 21
#define ST_INFINITY                     22
#define ST_INFINITY_SE                  23
#define ST_LAMP_INIT                    24

// 250msec
//#define	TIME_MASCH                      4		//x250msec = 1sec
#define	TIME_ATTESA_WAIT_RETE           40		//x250msec = 10secondi
#define	TIME_ATTESA_MISURE              12		//x250msec = 3secondi
#define	TIME_UN_SECONDO                 4		//x250msec = 1secondo
#define	TIME_DUE_SECONDI                8		//x250msec = 2secondi
#define TIME_TRE_SECONDI                12      //x250msec = 3secondi
#define TIME_QUATTRO_SECONDI            16      //x250msec = 4secondi
#define TIME_CINQUE_SECONDI             20      //x250msec = 5secondi
#define TIME_DIECI_SECONDI              40      //x250msec = 10secondi
#define TIME_VENTI_SECONDI              80
#define TIME_UN_MINUTO                  240     //x250msec = 1minuto
#define	TIME_DIECI_MINUTI               2400	//x250msec = 10minuti
#define TIME_QUINDICI_MINUTI            3600    //X250msec = 15minuti



#ifdef TEMPO_TEST_DEMO
    #define TIME_TRENTA_MINUTI              40      //10 secondi
#else
    #define	TIME_TRENTA_MINUTI              7200	//x250msec = 30minuti
#endif

// 1sec
#define TIME_4MINUTI_16SEC              255
#define TIME_1_MINUTO                   60

#ifdef TEMPO_SLEEP_DEMO
    #define TIME_PRE_SLEEP              8 //2sec
#else
    //250msec
    #define TIME_PRE_SLEEP                  3600
#endif

typedef union {
    uint16_t iReg;

    struct {
        uint16_t flagChargeRun : 1; //0
        uint16_t free_01 : 1; //1
        uint16_t flagSEAccesa : 1; //2
        uint16_t flagTestFunzInCorso : 1; //3
        uint16_t flagSAAccesa : 1; //4
        uint16_t flagDisableOptiCom : 1; //5
        uint16_t flagTestAutoInCorso : 1; //6
        uint16_t flagTestInCorso : 1; //7
        uint16_t flagPSAccesa : 1; //8
        uint16_t flagSAPSspenta : 1; //9
        uint16_t flagAccensioneIncondizionata : 1; //10
        uint16_t flagEmergenzaDisabilitata : 1; //11
        uint16_t flagAggiornaTimer : 1; //12
        uint16_t flagDriverSAAcceso : 1; //13
//        uint16_t flagInfinityAttivo : 1; //14
    } iBit;
} tFlagLampada;

//init di tFlagLampada dei diversi Stati:
//                                        1111110000000000
//                                        5432109876543210
#define	FLAG_NORMALE_SA_SPENTA          0b0000001000000001
#define	FLAG_NORMALE_SA_ACCESA          0b0010000000010001
#define	FLAG_TEST_FUNZ                  0b0000001010001000
#define FLAG_TEST_AUTO                  0b0000001011000000
#define	FLAG_EMERGENZA                  0b0000001000000100
#define	FLAG_SLEEP                      0b0000001000000000
#define FLAG_INFINITY                   0b0100001000000100
#define FLAG_PRE_SLEEP                  0b0000001000000000
#define FLAG_REST_MODE_ON               0b0000001000000100
#define	FLAG_WAIT_RETE                  0b0000001000100000

//                                        1111110000000000
//                                        5432109876543210
#define FLAG_ACCENSIONE_INCONDIZIONATA  0b0000010000000101
#define FLAG_SA_OFF_RETE_SA             0b0000001000000001
#define FLAG_SA_OFF_REMOTO              0b0000001000000001
#define FLAG_SA_ON                      0b0010000000010001
#define FLAG_PS_ON                      0b0000000100000001
#define FLAG_EMERGENZA_DISABILITATA     0b0000100000000000


#endif
