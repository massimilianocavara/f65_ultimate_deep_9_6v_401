#ifndef _TIMER_H
#define _TIMER_H


#define	ST_TMR1_MASTER		0
#define	ST_TMR0_MASTER		1


//COSTANTI DI TEMPO:
#define	TIME_10MSEC         10		//10*1msec
#define	TIME_250MSEC		25		//25*10msec
#define	TIME_1SEC           4		//4*250msec
#define TIME_1MIN           60      //60*1sec
#define TIME_1H             60      //60*1min


//--------------------------------------------
//  tFlagTimer
//--------------------------------------------

typedef union {
    uint16_t iReg;

    struct {
        uint16_t flag1msec_interrupt : 1; //0
        uint16_t flag1sec_interrupt : 1; //1
        uint16_t flag1msec : 1; //2
        uint16_t flag10msec : 1; //3
        uint16_t flag20msec : 1; //4
        uint16_t flag250msec : 1; //5
        uint16_t flag1sec : 1; //6
        uint16_t flag50msec : 1; //7
//        uint16_t flagFinePeriodo : 1; //8
        uint16_t flag1minuto : 1; //9
//        uint16_t flagFineDuty : 1; //9
//        uint16_t flag1ora : 1; //11
    } iBit;
} tFlagTimer;

#define RESET_FLAG          0x0003	//non resetto gli interrupt che sono settati in modo asincrono e resettati da Timer dopo averli letti



#endif
